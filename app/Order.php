<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletes;
class Order extends Model{
    use SoftDeletes, CascadeSoftDeletes;
    protected $fillable = [
        'customer_id',
        'branchoffice_id',
        'quote_id',
        'service',
        'type',
        'dealer_id',
        'zone_start',
        'zone_arrival',
        'customer_name',
        'customer_phone',
        'customer_email',
        'date',
        'round',
        'quantity',
        'price',
        'total',
        'payment_method',
        'status',
        'rated',
        'rated_service',
        'rated_time',
        'rated_amability',
        'rated_health',
        'rated_comment'
    ];

    protected $cascadeDeletes = [
        'packages'
    ];
    protected $dates = [
        'deleted_at'
    ];

    public function packages () {
        return $this->hasMany('App\OrderPackage','order_id','id');
    }

    public function customer() {
        return $this->belongsTo('App\Customer','customer_id','id');
    }

    public function branchoffice() {
        return $this->belongsTo('App\Branchoffice','branchoffice_id','id');
    }

    public function dealer() {
        return $this->belongsTo('App\Dealer','dealer_id','id');
    }
}
