<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $table = "quotes";
    protected $fillable = [
        'order_id',
        'customer_id',
        'firebase_document_uid',
        'details',
        'note',
        'status'
    ];
    public function customer_data() {
        return $this->belongsTo('App\Customer','customer_id','id');
    }
}
