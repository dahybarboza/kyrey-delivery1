<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;
class OrderPackageCollect extends Model
{
    use SoftDeletes, CascadeSoftDeletes;
    protected $table = 'orders_packages_collects';
    protected $dates = [
        'deleted_at'
    ];
    protected $cascadeDeletes = [
        'deliveries'
    ];
    public function deliveries() {
        return $this->hasMany(OrderPackageDelivery::class);
    }
    protected $fillable = [
        'firebase_uid',
        'order_package_id',
        'name',
        'phone',
        'note',
        'quantity',
        'type',
        'address',
        'neighborhood',
        'lat',
        'lng',
        'status'
    ];

}
