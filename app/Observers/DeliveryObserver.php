<?php

namespace App\Observers;

use App\Delivery;
use App\Dealer;
use App\Branchoffice;
use Illuminate\Http\Request;

class DeliveryObserver
{
    var $firestore;
    var $request;
    var $firebasePath = "orders";
    public function __construct(Request $request) {
        $this->request = $request;
        $this->firestore     = app('firebase.firestore');
        $this->firestore     = $this->firestore->database();
    }

    public function created(Delivery $item)
    {
        $dealer = Dealer::find($item->dealer_id);
        $branchoffice = Branchoffice::find($item->branchoffice_id);
        $data = [
            'id'                    => intval($item->id),
            'uid'                   => $item->firebase_uid,
            'branchoffice_id'       => intval($item->branchoffice_id),
            'dealer_id'             => intval($item->dealer_id),
            'customer_id'           => intval($item->customer_id),
            'dealer'                => $dealer->name,
            'dealer_cedula'         => $dealer->cedula,
            'dealer_phone'          => $dealer->phone,
            'dealer_uid'            => $dealer->firebase_uid,
            'branchoffice_lat'      => $branchoffice->lat,
            'branchoffice_lng'      => $branchoffice->lng,
            'branchoffice_phone'    => $branchoffice->phone,
            'name'                  => $item->name,
            'customer_phone'        => $item->phone,
            'note'                  => $item->note,
            'address'               => $item->address,
            'lat'                   => $item->lat,
            'lng'                   => $item->lng,
            'quantity'              => $item->quantity,
            'price'                 => $item->price,
            'type'                  => 'L5',
            'status'                => intval($item->status)
        ];
        $doc = $this->firestore->collection($this->firebasePath)->newDocument();
        $doc->set($data);
        $item->firebase_uid = $doc->id();
        $item->save();
    }

    function updated(Delivery $item) {
        $dealer = Dealer::find($item->dealer_id);
        $branchoffice = Branchoffice::find($item->branchoffice_id);
        $data = [
            'id'                    => intval($item->id),
            'uid'          => $item->firebase_uid,
            'branchoffice_id'       => intval($item->branchoffice_id),
            'dealer_id'             => intval($item->dealer_id),
            'customer_id'           => intval($item->customer_id),
            'dealer'                => $dealer->name,
            'dealer_cedula'         => $dealer->cedula,
            'dealer_phone'          => $dealer->phone,
            'dealer_uid'            => $dealer->firebase_uid,
            'branchoffice_lat'      => $branchoffice->lat,
            'branchoffice_lng'      => $branchoffice->lng,
            'branchoffice_phone'    => $branchoffice->phone,
            'name'                  => $item->name,
            'customer_phone'        => $item->phone,
            'note'                  => $item->note,
            'address'               => $item->address,
            'lat'                   => $item->lat,
            'lng'                   => $item->lng,
            'quantity'              => $item->quantity,
            'price'                 => $item->price,
            'type'                  => 'L5',
            'status'                => intval($item->status)
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }

    function deleting(Delivery $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }

    function deleted(Delivery $item) {
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }
}
