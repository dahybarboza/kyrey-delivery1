<?php

namespace App\Observers;

use App\OrderPackageCollect;
use Illuminate\Http\Request;

class OrderPackageCollectObserver
{
    var $firestore;
    var $firebaseAuth;
    var $request;
    var $firebasePath = "packages_collects";
    public function __construct(Request $request) {
        $this->request = $request;
        $this->firebaseAuth  = app('firebase.auth');
        $this->firestore     = app('firebase.firestore');
        $this->firestore     = $this->firestore->database();
    }
    public function created(OrderPackageCollect $item)
    {
            $data           =  [
                'id'            => intval($item->id),
                'package_id'    => intval($item->order_package_id),
                'name'          => $item->name,
                'phone'         => $item->phone,
                'note'          => $item->note,
                'quantity'      => intval($item->quantity),
                'type'          => intval($item->type),
                'address'       => $item->address,
                'neighborhood'  => $item->neighborhood,
                'lat'           => $item->lat*1,
                'lng'           => $item->lng*1,
                'status'        => intval($item->status)
            ];
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
    }
    function updated(OrderPackageCollect $item) {
            $data           =  [
                'id'            => intval($item->id),
                'uid'           => $item->firebase_uid,
                'package_id'    => intval($item->order_package_id),
                'name'          => $item->name,
                'phone'         => $item->phone,
                'note'          => $item->note,
                'quantity'      => intval($item->quantity),
                'type'          => intval($item->type),
                'address'       => $item->address,
                'neighborhood'  => $item->neighborhood,
                'lat'           => $item->lat*1,
                'lng'           => $item->lng*1,
                'status'        => intval($item->status)
            ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                        ]);
                }
            $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }
    function deleting(OrderPackageCollect $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                        ]);
                }
            $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }
    function deleted(OrderPackageCollect $item) {
        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }
}
