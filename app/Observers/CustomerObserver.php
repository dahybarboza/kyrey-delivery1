<?php

namespace App\Observers;
use App\Customer;
use Illuminate\Http\Request;
use App\Services\FireStorageService;
class CustomerObserver
{
    var $firestore;
    var $firebaseAuth;
    var $request;
    var $firebasePath = "customers";
    public function __construct(Request $request) {
        $this->request = $request;
        $this->firebaseAuth = app('firebase.auth');
        $this->firestore     = app('firebase.firestore');
        $this->firestore    = $this->firestore->database();
    }
    public function created(Customer $item)
    {
        try {
            //Create User Firebase
            $data = [
                'email' => $item->email,
                'emailVerified' => true,
                'password'  => $this->request->input('password'),
                'disabled'  => $item->status==1
            ];
            $dbDealer = $this->firebaseAuth->createUser($data);
            //Disable Firebase Account if create default width status disabled
            if($item->status == 0){
                $this->firebaseAuth->disableUser($dbDealer->uid);
            }
            $item->firebase_uid = $dbDealer->uid;
            $item->save();
            $data = [
                'id'               => $item->id,
                'photo'            => env('APP_URL').'/dashboard/assets/img/default_user.png',
                'name'             => $item->name,
                'ruc'              => $item->ruc,
                'email'            => $item->email,
                'phone'            => $item->phone,
                'status'           => intval($item->status)
            ];
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc->set($data);
        }catch(\Exception $e) {
            if(isset($auth)) {
                $auth->deleteUser($dbDealer->uid);
            }
        }
    }
    function updated(Customer $item) {
        $data = [
            'id'               => $item->id,
            'uid'              => $item->firebase_uid,
            'photo'            => $item->photo,
            'name'             => $item->name,
            'ruc'              => $item->ruc,
            'email'            => $item->email,
            'phone'            => $item->phone,
            'status'           => intval($item->status)
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                    array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                    ]);
                }
                $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
        //  Update Email
        if($item->email != $this->firebaseAuth->getUser($item->firebase_uid)->email){
            $this->firebaseAuth->changeUserEmail($item->firebase_uid, $item->email);
        }
        //  Status
        if($item->status == 0){
            $this->firebaseAuth->disableUser($item->firebase_uid);
        }else{
            $this->firebaseAuth->enableUser($item->firebase_uid);
        }
        //  Update Password
        if($this->request->input('password')!="") {
            $this->firebaseAuth->changeUserPassword($item->firebase_uid, $this->request->input('password'));
        }
    }
    function deleting(Customer $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                    array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                    ]);
                }
                $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }
    function deleted(Customer $item) {
        FireStorageService::delete($item->photo_firebase_uid);
        $this->firebaseAuth->deleteUser($item->firebase_uid);
    }
}
