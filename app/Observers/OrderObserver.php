<?php
namespace App\Observers;

use App\Branchoffice;
use Illuminate\Http\Request;
use App\Dealer;
use App\Order;

class OrderObserver{
    var $firestore;
    var $request;
    var $firebasePath = "orders";

    public function __construct(Request $request) {
        $this->request          = $request;
        $this->firestore        = app('firebase.firestore');
        $this->firestore        = $this->firestore->database();
    }

    public function created(Order $item){
        $dealer = Dealer::find($item->dealer_id);
        $data = [
                    'id'                    => intval($item->id),
                    'branchoffice_id'       => intval($item->branchoffice_id),
                    'branchoffice_name'     => Branchoffice::find($item->branchoffice_id)!=null?Branchoffice::find($item->branchoffice_id)->name:'',
                    'branchoffice_phone'    => Branchoffice::find($item->branchoffice_id)!=null?Branchoffice::find($item->branchoffice_id)->phone:'',
                    'customer_id'           => intval($item->customer_id),
                    'dealer_id'             => intval($item->dealer_id),
                    'service'               => intval($item->service),
                    'type'                  => intval($item->type),
                    'quote_id'              => intval($item->quote_id),
                    'dealer_uid'            => $dealer!=null?$dealer->firebase_uid:'',
                    'dealer_photo'           => $dealer!=null?$dealer->photo:env('APP_URL').'/dashboard/assets/img/default_user.png',
                    'dealer_name'           => $dealer!=null?$dealer->name:'',
                    'dealer_cedula'         => $dealer!=null?$dealer->cedula:'',
                    'dealer_phone'          => $dealer!=null?$dealer->phone:'',
                    'customer_name'         => $item->customer_name,
                    'customer_phone'        => $item->customer_phone,
                    'customer_email'        => $item->customer_email,
                    'date'                  => $item->date,
                    'round'                 => intval($item->round),
                    'quantity'              => intval($item->quantity),
                    'price'                 => intval($item->price),
                    'total'                 => intval($item->total),
                    'payment_method'        => intval($item->payment_method),
                    'status'                => intval($item->status),
                    'rated'                 => false,
                    'rated_stars'           => 0,
                    'rated_comment'         => null
                ];
        $doc = $this->firestore->collection($this->firebasePath)->newDocument();
        $doc->set($data);
        $item->firebase_uid = $doc->id();
        $item->save();
    }

    function updated(Order $item) {
            $dealer = Dealer::find($item->dealer_id);
            $data = [
                    'id'                    => intval($item->id),
                    'uid'                   => $item->firebase_uid,
                    'branchoffice_id'       => intval($item->branchoffice_id),
                    'branchoffice_name'     => Branchoffice::find($item->branchoffice_id)!=null?Branchoffice::find($item->branchoffice_id)->name:'',
                    'branchoffice_phone'    => Branchoffice::find($item->branchoffice_id)!=null?Branchoffice::find($item->branchoffice_id)->phone:'',
                    'customer_id'           => intval($item->customer_id),
                    'dealer_id'             => intval($item->dealer_id),
                    'service'               => intval($item->service),
                    'type'                  => intval($item->type),
                    'quote_id'              => intval($item->quote_id),
                    'dealer_uid'            => $dealer!=null?$dealer->firebase_uid:'',
                    'dealer_photo'           => $dealer!=null?$dealer->photo:env('APP_URL').'/dashboard/assets/img/default_user.png',
                    'dealer_name'           => $dealer!=null?$dealer->name:'',
                    'dealer_cedula'         => $dealer!=null?$dealer->cedula:'',
                    'dealer_phone'          => $dealer!=null?$dealer->phone:'',
                    'customer_name'         => $item->customer_name,
                    'customer_phone'        => $item->customer_phone,
                    'customer_email'        => $item->customer_email,
                    'date'                  => $item->date,
                    'round'                 => intval($item->round),
                    'quantity'              => intval($item->quantity),
                    'price'                 => intval($item->price),
                    'total'                 => intval($item->total),
                    'payment_method'        => intval($item->payment_method),
                    'status'                => intval($item->status),
                    'rated'                 => $item->rated==true,
                    'rated_stars'           => intval($item->rated_stars),
                    'rated_comment'         => $item->rated_comment
                ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }

    function deleting(Order $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }

    function deleted(Order $item) {
        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }

}
