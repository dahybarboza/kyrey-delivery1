<?php

namespace App\Observers;
use App\OrderPackageDelivery;
use Illuminate\Http\Request;
class OrderPackageDeliveryObserver
{
    var $firestore;
    var $request;
    var $firebasePath = "packages_deliveries";
    public function __construct(Request $request) {
        $this->request = $request;
        $this->firestore     = app('firebase.firestore');
        $this->firestore     = $this->firestore->database();
    }
    public function created(OrderPackageDelivery $item)
    {
                $data                =  [
                'id'                 => intval($item->id),
                'package_id'         => intval($item->order_package_id),
                'package_collect_id' => intval($item->order_package_collect_id),
                'name'               => $item->name,
                'phone'              => $item->phone,
                'note'               => $item->note,
                'quantity'           => intval($item->quantity),
                'type'               => intval($item->type),
                'address'            => $item->address,
                'neighborhood'       => $item->neighborhood,
                'lat'                => $item->lat*1,
                'lng'                => $item->lng*1,
                'status'             => intval($item->status)
                ];
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
    }
    function updated(OrderPackageDelivery $item) {
            $data           =  [
                'id'                 => intval($item->id),
                'package_id'         => intval($item->order_package_id),
                'package_collect_id' => intval($item->order_package_collect_id),
                'uid'                => $item->firebase_uid,
                'name'               => $item->name,
                'phone'              => $item->phone,
                'note'               => $item->note,
                'quantity'           => intval($item->quantity),
                'type'               => intval($item->type),
                'address'            => $item->address,
                'neighborhood'       => $item->neighborhood,
                'lat'                => $item->lat*1,
                'lng'                => $item->lng*1,
                'status'             => intval($item->status)
            ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                        ]);
                }
            $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }
    function deleting(OrderPackageDelivery $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                        ]);
                }
            $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }
    function deleted(OrderPackageDelivery $item) {
        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }
}
