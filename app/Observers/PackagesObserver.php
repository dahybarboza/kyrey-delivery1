<?php

namespace App\Observers;
use App\OrderPackage;
use Illuminate\Http\Request;
class PackagesObserver
{
    var $firestore;
    var $request;
    var $firebasePath = "packages";
    public function __construct(Request $request) {
        $this->request = $request;
        $this->firestore     = app('firebase.firestore');
        $this->firestore     = $this->firestore->database();
    }
    public function created(OrderPackage $item)
    {
        $data = [
                    'id'                        => intval($item->id),
                    'order_id'                  => intval($item->order_id),
                    'quantity'                  => intval($item->quantity),
                    'distance'                  => $item->distance *1,
                    'type'                      => $item->type,
                    'total'                     => doubleval($item->total),
                    'status'                    => intval($item->status)

                ];
        $doc = $this->firestore->collection($this->firebasePath)->newDocument();
        $doc->set($data);
        $item->firebase_uid = $doc->id();
        $item->save();
    }
    function updated(OrderPackage $item) {
            $data = [
                'id'                        => intval($item->id),
                'uid'                       => $item->firebase_uid,
                'order_id'                  => intval($item->order_id),
                'quantity'                  => intval($item->quantity),
                'distance'                  => $item->distance *1,
                'type'                      => $item->type,
                'total'                     => doubleval($item->total),
                'status'                    => intval($item->status)
                ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }
    function deleting(OrderPackage $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }
    function deleted(OrderPackage $item) {
        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }
}
