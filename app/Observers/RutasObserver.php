<?php

namespace App\Observers;

use App\Branchoffice;
use App\Customer;
use App\Dealer;
use App\Rutas;
use Illuminate\Http\Request;

class RutasObserver{
    var $firestore;
    var $request;
    var $firebasePath = "rutas";

    public function __construct(Request $request){
        $this->request   = $request;
        $this->firestore = app('firebase.firestore');
        $this->firestore = $this->firestore->database();
    }

    public function created(Rutas $item){
        $dealer = Dealer::find($item->dealer_id);
        $branchoffices = Branchoffice::find($item->branchoffices_id);
        $customers = Customer::find($item->customers_id);

        $data = [
            'id'                        => intval($item->id),
            'uid'                       => $item->firebase_uid,
            'branchoffice_id'           => intval($item->branchoffice_id),
            'customer_id'               => intval($item->customer_id),
            'dealer_id'                 => intval($item->dealer_id),
            'dia'                       => $item->dia,
            'fecha'                     => $item->fecha,
            'hora_inicio_programado'    => $item->hora_inicio_programado,
            'hora_fin_programado'       => $item->hora_fin_programado,
            'hora_inicio_real'          => $item->hora_inicio_real,
            'hora_fin_real'             => $item->hora_fin_real,
            'lat'                       => $item->lat,
            'lng'                       => $item->lng,
            'address'                   => $item->address,
            'neighborhood'              => $item->neighborhood,
            'status'                    => intval($item->status),
            'en_rango'                  => intval($item->en_rango),
            'observaciones'             => $item->observaciones,
            'deleted' => false
        ];

        $doc = $this->firestore->collection($this->firebasePath)->newDocument();
        $doc->set($data);
        $item->firebase_uid = $doc->id();
        $item->save();

    }

    public function updated(Rutas $item){
        $dealer = Dealer::find($item->dealer_id);
        $branchoffices = Branchoffice::find($item->branchoffices_id);
        $customers = Customer::find($item->customers_id);
        $data = [
            'id'                        => intval($item->id),
            'uid'                        => $item->firebase_uid,
            'branchoffice_id'           => intval($item->branchoffice_id),
            'customer_id'               => intval($item->customer_id),
            'dealer_id'                 => intval($item->dealer_id),
            'dia'                       => $item->dia,
            'fecha'                     => $item->fecha,
            'hora_inicio_programado'    => $item->hora_inicio_programado,
            'hora_fin_programado'       => $item->hora_fin_programado,
            'hora_inicio_real'          => $item->hora_inicio_real,
            'hora_fin_real'             => $item->hora_fin_real,
            'lat'                       => $item->lat,
            'lng'                       => $item->lng,
            'address'                   => $item->address,
            'neighborhood'              => $item->neighborhood,
            'status'                    => intval($item->status),
            'en_rango'                  => intval($item->en_rango),
            'observaciones'             => $item->observaciones,
            'deleted' => false
        ];

        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                    array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                    ]);
                }
                $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }

    public function deleting(Rutas $item){
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                    array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                    ]);
                }
                $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }

    }

    function deleted(Rutas $item) {
        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }


}
