<?php

namespace App\Observers;
use App\Dealer;
use Illuminate\Http\Request;
use App\Services\FireStorageService;

class DealerObserver{

    var $firestore;
    var $firebaseAuth;
    var $request;
    var $firebasePath = "dealers";

    public function __construct(Request $request) {
        $this->request      = $request;
        $this->firebaseAuth = app('firebase.auth');
        $this->firestore    = app('firebase.firestore');
        $this->firestore    = $this->firestore->database();
    }

    # INSERT
    public function created(Dealer $item){
        //var_dump('ACA en created'); die;
        try {
            //Create User Firebase
            $data = [
                'email'         => $item->email,
                'emailVerified' => true,
                'password'      => $this->request->input('password'),
                'disabled'      => $item->status==1
            ];
            $dbDealer = $this->firebaseAuth->createUser($data);
            //Disable Firebase Account if create default width status disabled
            if($item->status == 0){
                $this->firebaseAuth->disableUser($dbDealer->uid);
            }
            $item->firebase_uid = $dbDealer->uid;
            $item->save();
            $data = [
                'id'               => $item->id,
                'uid'              => $item->firebase_uid,
                'photo'            => env('APP_URL').'/dashboard/assets/img/default_user.png',
                'name'             => $item->name,
                'cedula'           => $item->cedula,
                'email'            => $item->email,
                'phone'            => $item->phone,
                'password'         => $item->password,
                //'branchoffice_id'  => intval($item->branchoffice_id),
                'lat'              => $item->lat,
                'lng'              => $item->lng,
                'available'        => intval($item->available),
                'status'           => intval($item->status)
            ];
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc->set($data);
        }catch(\Exception $e) {
            if(isset($auth)) {
                $auth->deleteUser($dbDealer->uid);
            }
        }
    }

    # UPDATE
    function updated(Dealer $item) {
        $data = [
            'id'               => $item->id,
            'uid'              => $item->firebase_uid,
           // 'photo'            => $item->photo,
            'name'             => $item->name,
            'cedula'           => $item->cedula,
            'email'            => $item->email,
            'phone'            => $item->phone,
            'password'         => $item->password,
            //'branchoffice_id'  => intval($item->branchoffice_id),
            'lat'              => $item->lat,
            'lng'              => $item->lng,
            'available'        => intval($item->available),
            'status'           => intval($item->status)
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                    array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                    ]);
                }
                $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
        //  Update Email
        if($item->email != $this->firebaseAuth->getUser($item->firebase_uid)->email){
            $this->firebaseAuth->changeUserEmail($item->firebase_uid, $item->email);
        }
        //  Status
        if($item->status == 0){
            $this->firebaseAuth->disableUser($item->firebase_uid);
        }else{
            $this->firebaseAuth->enableUser($item->firebase_uid);
        }
        //  Update Password
        if($this->request->input('password')!="") {
            $this->firebaseAuth->changeUserPassword($item->firebase_uid, $this->request->input('password'));
        }
    }

    # DELETING
    function deleting(Dealer $item) {
        //var_dump('$item->firebase_uid: '.$item->firebase_uid); die;
        $data = ['deleted' => true];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                    array_push($data_list,[
                        'path'=>$key,
                        'value'=>$value
                    ]);
                }
                $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }

    # DELETED
    function deleted(Dealer $item) {


        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                # Creo que borra imagen estas dos líneas
                FireStorageService::delete($item->photo_firebase_uid);
                $this->firebaseAuth->deleteUser($item->firebase_uid);
                # Acá borra del Cloud Firestore
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }

}
