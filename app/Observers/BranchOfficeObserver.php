<?php

namespace App\Observers;

use App\Branchoffice;
use Illuminate\Http\Request;
use App\Services\FireStorageService;

class BranchOfficeObserver
{

    var $firestore;
    var $request;
    var $firebasePath = "branchoffices";

    # INSERT
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->firestore = app('firebase.firestore');
        $this->firestore = $this->firestore->database();
    }

    # CREATE
    public function created(Branchoffice $item)
    {
        $data = [
            'id' => intval($item->id),
            'customer_id'  => intval($item->customer_id),
            'name' => $item->name,
            'phone' => $item->phone,
            'address' => $item->address,
            'lat' => $item->lat,
            'lng' => $item->lng,
            'neighborhood' => $item->neighborhood,
            'status' => intval($item->status),
            'deleted' => false
        ];
        $doc = $this->firestore->collection($this->firebasePath)->newDocument();
        $doc->set($data);
        $item->firebase_uid = $doc->id();
        $item->save();
    }

    # UPDATE
    function updated(Branchoffice $item)
    {
        $data = [
            'id' => intval($item->id),
            'uid' => $item->firebase_uid,
            'name' => $item->name,
            'phone' => $item->phone,
            'address' => $item->address,
            'lat' => $item->lat,
            'lng' => $item->lng,
            'neighborhood' => $item->neighborhood,
            'status' => intval($item->status),
            'deleted' => false
        ];
        if ($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if ($doc->exists()) {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach ($data as $key => $value) {
                    array_push($data_list, [
                        'path' => $key,
                        'value' => $value
                    ]);
                }
                $doc->update($data_list);
            } else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        } else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }

    # DELETING
    function deleting(Branchoffice $item)
    {
        $data = [
            'deleted' => true
        ];
        if ($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if ($doc->exists()) {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach ($data as $key => $value) {
                    array_push($data_list, [
                        'path' => $key,
                        'value' => $value
                    ]);
                }
                $doc->update($data_list);
            } else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }

    # DELETED
    function deleted(Branchoffice $item)
    {
        if ($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if ($doc->exists()) {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }
}
