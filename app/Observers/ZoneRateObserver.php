<?php

namespace App\Observers;
use App\ZoneRate;
use Illuminate\Http\Request;
class ZoneRateObserver
{
    var $firestore;
    var $request;
    var $firebasePath = "zones_rates";
    public function __construct(Request $request) {
        $this->request = $request;
        $this->firestore     = app('firebase.firestore');
        $this->firestore     = $this->firestore->database();
    }
    public function created(ZoneRate $item)
    {
        $data = [
            'id'                => intval($item->id),
            'zone_id'           => intval($item->zone_id),
            'zone_delivery'     => intval($item->zone_delivery),
            'zone_collect_name' => $item->zone->name,
            'zone_delivery_name'=> $item->delivery->name,
            'price'             => intval($item->price),
            'status'            => intval($item->status),
        ];
        $doc = $this->firestore->collection($this->firebasePath)->newDocument();
        $doc->set($data);
        $item->firebase_uid = $doc->id();
        $item->save();
    }
    function updated(ZoneRate $item) {
        $data = [
                    'id'            => intval($item->id),
                    'uid'           => $item->firebase_uid,
                    'zone_id'           => intval($item->zone_id),
                    'zone_delivery'     => intval($item->zone_delivery),
                    'zone_collect_name' => $item->zone->name,
                    'zone_delivery_name'=> $item->delivery->name,
                    'price'             => intval($item->price),
                    'status'            => intval($item->status),
                ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }
    function deleting(ZoneRate $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }
    function deleted(ZoneRate $item) {
        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }
}
