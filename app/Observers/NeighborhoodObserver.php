<?php

namespace App\Observers;
use App\Neighborhood;
use Illuminate\Http\Request;
class NeighborhoodObserver
{
    var $firestore;
    var $request;
    var $firebasePath = "neighborhood";
    public function __construct(Request $request) {
        $this->request = $request;
        $this->firestore     = app('firebase.firestore');
        $this->firestore     = $this->firestore->database();
    }
    function updated(Neighborhood $item) {
        $data = [
                    'id'                    => intval($item->id),
                    'uid'                   => $item->firebase_uid,
                    'zone_id'               => intval($item->zone_id),
                    'name'                  => $item->name,
                    'status'                => intval($item->status)
                ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }else {
            $doc = $this->firestore->collection($this->firebasePath)->newDocument();
            $doc->set($data);
            $item->firebase_uid = $doc->id();
            $item->save();
        }
    }
    function deleting(Neighborhood $item) {
        $data = [
            'deleted'          => true
        ];
        if($item->firebase_uid) {
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                //Prepare data for update
                $data_list = [];
                foreach($data as $key=>$value) {
                        array_push($data_list,[
                           'path'=>$key,
                           'value'=>$value
                        ]);
                }
               $doc->update($data_list);
            }else {
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->set($data);
                $item->firebase_uid = $doc->id();
                $item->save();
            }
        }
    }
    function deleted(Neighborhood $item) {
        if($item->firebase_uid){
            $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
            $doc = $doc->snapshot();
            if($doc->exists()){
                $doc = $this->firestore->collection($this->firebasePath)->document($item->firebase_uid);
                $doc->delete();
            }
        }
    }
}
