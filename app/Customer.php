<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Customer extends Model implements JWTSubject , AuthenticatableContract
{
    use Authenticatable;
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    protected $dates = [
        'deleted_at'
    ];
    protected $table="customers";
    protected $fillable = [
        'name',
        'ruc',
        'email',
        'photo',
        'phone',
        'status'
    ];

//    public function branchoffice(){
//        return $this->belongsTo('App\Branchoffice','branchoffice_id');
//    }

    public function branchoffice() {
            return $this->hasMany('App\Branchoffice');
    }

    protected $hidden = [
        'password'
    ];
}
