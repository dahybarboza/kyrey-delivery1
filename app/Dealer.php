<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class Dealer extends Model implements JWTSubject , AuthenticatableContract{
    use Authenticatable;

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }

    protected $dates = [
        'deleted_at'
    ];

    protected $table = "dealers";

    protected $fillable = [
        //'branchoffice_id',
        'photo',
        'name',
        'cedula',
        'phone',
        'password',
        'email',
        'lat',
        'lng',
        'status'
    ];

//    public function branchoffice(){
//        return $this->belongsTo('App\Branchoffice','branchoffice_id');
//    }

    protected $hidden = [
        'password'
    ];
}
