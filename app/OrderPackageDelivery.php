<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;
class OrderPackageDelivery extends Model
{
    use SoftDeletes, CascadeSoftDeletes;
    protected $table = 'orders_packages_deliveries';
    protected $dates = [
        'deleted_at'
    ];
    protected $fillable = [
        'firebase_uid',
        'order_package_id',
        'order_package_collect_id',
        'name',
        'phone',
        'note',
        'quantity',
        'type',
        'address',
        'neighborhood',
        'lat',
        'lng',
        'status'
    ];
}
