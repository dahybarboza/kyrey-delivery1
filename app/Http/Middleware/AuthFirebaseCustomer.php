<?php
namespace App\Http\Middleware;
use Firebase\Auth\Token\Exception\InvalidToken;
use App\Customer;
use Closure;
class AuthFirebaseCustomer {
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = app('firebase.auth');
        $token = $request->bearerToken();
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }
        try {
            $verifiedIdToken = $auth->verifyIdToken($token);
        } catch (\InvalidArgumentException $e) {
            return response()->json('The token could not be parsed: '.$e->getMessage(),500);
        } catch (InvalidToken $e) {
            return response()->json('The token is invalid: '.$e->getMessage(),401);
        }
        $uid = $verifiedIdToken->getClaim('sub');
        $request->auth = Customer::where('firebase_uid',$uid)->first();
        return $next($request);
    }
}
