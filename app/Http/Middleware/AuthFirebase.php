<?php
namespace App\Http\Middleware;
use Firebase\Auth\Token\Exception\InvalidToken;
use Closure;
class AuthFirebase {
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = app('firebase.auth');
        $token = $request->bearerToken();
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }
        try {
            $verifiedIdToken = $auth->verifyIdToken($token);
        } catch (\InvalidArgumentException $e) {
            return response()->json([
                'error' => 'The token could not be parsed, '.$e->getMessage()
            ], 401);
        } catch (InvalidToken $e) {
            return response()->json([
                'error' => 'The token is invalid, '.$e->getMessage()
            ], 401);
        }
        $uid = $verifiedIdToken->getClaim('sub');
        $user = $auth->getUser($uid);
        if(!$user->emailVerified) {
            return response()->json([
                'err'=>true,
                'message' => 'Su cuenta no ha sido confrmada, por favor ingrese a su correo y siga las instrucciones.'
            ]);
        }
        return $next($request);
    }
}
