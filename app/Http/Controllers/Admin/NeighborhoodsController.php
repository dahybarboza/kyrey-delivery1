<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Neighborhood;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class NeighborhoodsController extends Controller
{
    var $request;
    var $folder = 'admin.neighborhoods';
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Neighborhood();
    }

    public function index() {
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0=>'neighborhoods/HomeController.js'],
            'zone' => $this->request->get('zone')
            ]);
    }

    public function all($zone) {
        $neighborhoods = Neighborhood::where('zone_id',$zone)->get();
        if($neighborhoods) {
            return $this->successResponse([
                'err'  => false,
                'data' => $neighborhoods
            ]);
        } else {
            return $this->errorResponse([
                'err' => true,
                'message' => 'La zona no existe, por favor intente nuevamente'
            ]);
        }
    }

    public function update($id) {
        try {
            DB::beginTransaction();
            $neighborhood = $this->model->find($id);
            if($neighborhood) {
                $neighborhood->status = $this->request->status;
                $neighborhood->save();
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Registro actualizado correctamente.'
                ]);
            } else{
                return $this->errorResponse([
                    'err' =>true,
                    'message' => 'El registro que intenet actualizar no existe.'
                ]);
            }
        } catch(\Exception $e) {
                echo $e->getMessage();
                return $this->errorResponse([
                    'err' => true,
                    'message' => 'No ha sido posible actulizar el registro, por favor intente nuevamente.'
                ]);
            }
    }
}
