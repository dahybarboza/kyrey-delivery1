<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    var $folder = "admin.dealers";
    var $request;
    var $model;
    var $filePath = 'assets/data/dealers/';
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Invoice();
    }

    public function index(){
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'dealers/HomeController.js',
                1 => 'dealers/GoogleController.js'
            ],
            'cssStyles' => [
                0 => 'dealers/style.css'
            ]
        ]);
    }
}
