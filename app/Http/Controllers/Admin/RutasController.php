<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Rutas;
use App\Branchoffice;
use App\Dealer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;




class RutasController extends Controller{
    var $request;
    var $folder = 'admin.rutas';
    var $model;
    public function __construct(Request $request){
        $this->request = $request;
        $this->model = new Rutas();
    }

    public function index() {
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0=>'rutas/HomeController.js'
            ],
        ]);
    }

    public function all() {
        return $this->successResponse([
            'err'=>false,
            'data'=> $this->model->get()->map(function($rutas){
                $rutas->dealer;
                $rutas->branchoffice;
                $rutas->customer;
                $rutas['fecha'] = $rutas->created_at->format('d-m-y');
                return $rutas;
            })
        ]);
    }

    public function create() {
        return view($this->folder.'.create',[
            'jsControllers'=>[
                0=>'rutas/CreateController.js',
                1=>'rutas/L1Controller.js',
                //2=>'rutas/L2AddDestinoController.js',
            ],
            'cssStyles' => [
                0 => 'rutas/style.css'
            ],
            'branchoffices' => Branchoffice::where('status', 1)->get(),
            'customers' => Customer::where('status', 1)->get(),
            'dealers' => Dealer::where('status', 1)->get(),
        ]);
    }

//    function dealers($branchoffice) {
//        return $this->successResponse([
//            'err'   =>false,
//            'data'  => Dealer::where('branchoffice_id',$branchoffice)->get()
//        ]);
//    }

    function branchoffices($customer_id) {
        return $this->successResponse([
            'err'   =>false,
            'data'  => Branchoffice::where('customer_id',$customer_id)->get()
        ]);
    }

    function store() {
        //$data = $this->request->all();
        //var_dump($data);
        try {
            DB::beginTransaction();
            $data = $this->request->all();
            $rutas = new Rutas();

            $rutas->fill($data);
            if($rutas->save()) {
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Ruta registrada correctamente.'
                ]);
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                'err' =>true,
                'message' => 'No ha sido posible registrar orden, por favor verifique su información e intente nuevamente.'
            ]);
        }
    }

    function edit($id){
        # Se consulta el ID del cliente para luego llevar a la vista solamente las sucursales del cliente
        $rutas = Rutas::where('id', $id)->pluck('customer_id')->toArray();
        $customer_id = $rutas[0];
        return view($this->folder.'.edit',[
            'jsControllers'=>[
                0=>'rutas/EditController.js',
                1=>'rutas/L1EditController.js',
            ],
            'id' => $id,
            'cssStyles' => [
                0 => 'orders/style.css'
            ],
            //'zones' => Zone::where('status',1)->get(),
            //'dealers' => Dealer::all(),
            'branchoffices' => Branchoffice::where('status',1)->where('customer_id', $customer_id)->get(),
            'customers' => Customer::where('status', 1)->get(),
            'dealers' => Dealer::where('status', 1)->get(),
            //'quotes' => Quote::where('status','>=',0)->where('status','<=',2)->get()
        ]);
    }

    function find($id) {
        return $this->successResponse([
            'err'=>false,
            'data'=> $this->model->where('id', $id)->get()->map(function($rutas){
                $rutas->dealer;
                $rutas->branchoffice;
                $rutas->customer;
                return $rutas;
            })
        ]);

    }

    public function update($id) {
        try {
            DB::beginTransaction();
            $data = $this->request->all();
            $ruta = $this->model->find($id);
            if($ruta) {
                $ruta->fill($data);
                if($ruta->save()) {
                    $ruta = $this->model->find($ruta->id);
                    $ruta->save();
                    DB::commit();
                    return $this->successResponse([
                        'err' => false,
                        'message' => 'Ruta actualizada correctamente.'
                    ]);
                } else {
                    return $this->errorResponse([
                        'err' => true,
                        'message' => 'No ha sido posible editar orden, por favor verifique su información e intente nuevamente.'
                    ]);
                }
            } else {
                return $this->errorResponse([
                    'err' => true,
                    'message' => 'No ha sido posible localizar la ruta, verifique si ha sido eliminada anteriormente.'
                ]);
            }
        } catch(\Exception $e){
            echo $e->getMessage();
            $this->errorResponse([
                'err' =>true,
                'message' => 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente.'
            ]);
        }
    }

    public function destroy($id) {
        //var_dump('ACA'); return false;
        try {
            DB::beginTransaction();
            $itemData = $this->model->find($id);
            if($itemData) {
                if($itemData->delete()) {
                    DB::commit();
                    return $this->successResponse([
                        'err' => false,
                        'message' => 'Registro eliminado correctamente.'
                    ]);
                } else {
                    return $this->errorResponse([
                        'err' => true,
                        'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
                    ]);
                }
            } else {
                DB::rollback();
                return $this->errorResponse([
                    'err' =>true,
                    'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
                ]);
            }
        }
        catch(\Exception $e){
            echo $e->getMessage();
            DB::rollback();
            return $this->errorResponse([
                'err' =>true,
                'message' => 'No ha sido posible eliminar registro.'
            ]);
        }
    }


}
