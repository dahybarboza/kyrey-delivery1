<?php

namespace App\Http\Controllers\Admin;

use App\Branchoffice;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BranchofficesController extends Controller{

    use ApiResponser;
    var $folder = "admin.branchoffices";
    var $request;
    var $model;

    public function __construct(Request $request){
        $this->request = $request;
        $this->model = new Branchoffice();
    }

    public function index(){
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'branchoffices/HomeController.js',
                1 => 'branchoffices/GoogleController.js'
            ],
            'cssStyles' => [
                0 => 'branchoffices/style.css'
            ],
            'customers'=> Customer::all()
        ]);
    }

    public function all() {
        return $this->successResponse([
            'err'=>false,
            'data'=> $this->model->get()->map(function($customers){
                $customers->customer;
                return $customers;
            })
        ]);
    }

    public function store(){
        try{
            DB::beginTransaction();
            $rules = [
            'name'=>'required|max:255',
            'phone'=>'required|max:15',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ];
        $this->validate($this->request, $rules);
        $branchoffice = new Branchoffice();
        $data = $this->request->all();
        $branchoffice->fill($data);
        if($branchoffice->save()) {
            DB::commit();
            return $this->successResponse([
                'err'=>false,
                'message'=> 'Registro exitoso'
            ]);
        }else {
            return $this->successResponse([
                'err'=>false,
                'message'=> 'No ha sido posible crear el registro.'
            ]);
        }
        }catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                'err'=>false,
                'message'=> 'No ha sido posible crear el registro.'
            ]);
        }
    }

    public function update($id){
        $branchoffice  = $this->model->find($id);
        if($branchoffice) {
                    $rules = [
                        'name'      => 'required:max:255',
                        'phone'     => 'required'
                    ];
                    $this->validate($this->request,$rules);
                    $data = $this->request->all();
                    $branchoffice->fill($data);
                    if($branchoffice->save()){
                        DB::commit();
                        return $this->successResponse([
                            'err'=>false,
                            'message'=> 'Actualización exitosa.'
                        ]);
                    }else{
                        return $this->errorResponse([
                            'err'=>false,
                            'message'=> 'No ha sido posible actualizar registro.'
                        ]);
                    }
        }else{
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'No ha sido posible actualizar registro.'
            ]);
        }
    }

    public function destroy($item){
        try {
            DB::beginTransaction();
            $branchoffice = $this->model->find($item);
            if($branchoffice) {
                if($branchoffice->delete()){
                    DB::commit();
                    return $this->successResponse([
                        'err'=>false,
                        'message'=> 'Eliminación correcta!'
                    ]);
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'No ha sido posible eliminar, por favor intente nuevamente.'
                    ]);
                }
            }else{
                return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'El elemento que intenta eliminar ya no existe.'
                ],404);
            }
        } catch(\Exception $e) {
            DB::rollBack();
            return $this->errorResponse([
                            'err'=>true,
                            'message'=> 'No ha sido posible eliminar, por favor intente nuevamente.'
                        ]);
        }
    }
}