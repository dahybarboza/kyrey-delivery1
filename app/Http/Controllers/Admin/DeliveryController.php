<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Delivery;
use App\Branchoffice;
use App\Customer;
use App\Dealer;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Services\UtilsService;

class DeliveryController extends Controller
{
    var $folder = "admin.delivery";
    var $request;
    var $model;
    var $filePath = 'assets/data/delivery/';

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Delivery();
    }

    public function index(){
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'delivery/HomeController.js',
                1 => 'delivery/GoogleController.js'
            ],
            'cssStyles' => [
                0 => 'delivery/style.css'
            ],

            'branchoffices' =>Branchoffice::where('status',1)->get(),
            'dealers' => Dealer::where('status', '=', 1)->get(),

        ]);
    }

    function all() {
        return Datatables::of(
            Delivery::orderBy('deliveries.status','asc')
                ->join('branchoffices','branchoffices.id','=','deliveries.branchoffice_id')
                ->join('dealers','dealers.id','=','deliveries.dealer_id')
                ->select([
                    'deliveries.id as id',
                    'deliveries.branchoffice_id',
                    'branchoffices.name as branchoffice',

                    'deliveries.dealer_id as dealer_id',
                    'dealers.name as dealer',

                    'deliveries.name as name',
                    'deliveries.phone as phone',
                    'deliveries.note as note',
                    'deliveries.address as address',
                    'deliveries.lat as lat',
                    'deliveries.lng as lng',
                    'deliveries.price as price',
                    'deliveries.quantity as quantity',
                    'deliveries.status as status',
                ])
        )->make(true);
    }

    public function store() {
      //  $data = $this->request->all();
        try {
            DB::beginTransaction();
           // $data = $this->request->all();

            $delivery= new Delivery();
            $data = $this->request->all();
            $delivery->fill($data);
            if ($delivery->save()){
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Datos registrados correctamente.'
                ]);

            }
            else {
                return $this->successResponse([
                    'err'=>false,
                    'message'=> 'No ha sido posible crear el registro.'
                ]);
            }

         //   $this->model->fill($data)->save();

        }
        catch(\Exception $e){
            echo $e->getMessage();
            DB::rollback();
            return $this->errorResponse([
                'err' =>true,
                'message' => 'No ha sido posible crear registro, por favor verifique su información e intente nuevamente.'
            ]);
        }
    }

    public function update($id) {
        $delivery=$this->model->find($id);
        if($delivery){

            $data = $this->request->all();
            $delivery->fill($data);
            if ($delivery->save()){
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Datos registrados correctamente.'
                ]);

            }}
             else {
                return $this->successResponse([
                    'err'=>false,
                    'message'=> 'No ha sido posible crear el registro.'
                ]);
            }

            //   $this->model->fill($data)->save();


    }



    public function destroy($item)
    {
        $dealer = $this->model->find($item);
        if($dealer) {
            if($dealer->delete()) {
                return $this->successResponse([
                    'err'=>false,
                    'message'=> 'Registro eliminado correctamente'
                ]);
            }else {
                return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'El registro no existe.'
                ]);
            }
        }else {
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'El registro no existe.'
            ],404);
        }
    }
    function dealers($branchoffice) {
        return $this->successResponse([
            'err'   =>false,
            'data'  => Dealer::where('branchoffice_id',$branchoffice)->get()
        ]);
    }

}
