<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Order;
class QualityController extends Controller
{
    var $request;
    var $folder = 'admin.quality';
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index() {
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0=>'quality/HomeController.js']
            ]);
    }
    public function all() {
        return Datatables::of(Order::where('status',6)->orderBy('status','asc')->get([
            'id',
            'rated_service',
            'rated_time',
            'rated_health',
            'rated_comment'
        ]))->make(true);
    }
}
