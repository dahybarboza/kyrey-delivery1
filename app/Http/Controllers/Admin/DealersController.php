<?php

namespace App\Http\Controllers\Admin;

use App\Branchoffice;
use App\Dealer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\UtilsService;
use App\Services\FireStorageService;

class DealersController extends Controller{

    var $folder = "admin.dealers";
    var $request;
    var $model;
    var $filePath = 'assets/data/dealers/';

    public function __construct(Request $request){
        $this->request = $request;
        $this->model = new Dealer();
    }

    public function index(){
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'dealers/HomeController.js',
                1 => 'dealers/GoogleController.js'
            ],
            'cssStyles' => [
                0 => 'dealers/style.css'
            ],
            //'branchoffices'=> Branchoffice::all()
        ]);
    }

    public function all() {
        return $this->successResponse([
            'err'=>false,
            'data'=> $this->model->get()->map(function($dealer){
                $dealer->branchoffice;
                return $dealer;
            })
        ]);
    }
    
    public function store(){
            try {
                DB::beginTransaction();
                $rules = [
                    'name'              =>  'required|max:255',
                    'phone'             =>  'required|max:15',
                    'cedula'            =>  'required',
                    'email'             =>  'email',
                    'password'          =>  'required|min:8',
                ];
                $data = $this->request->all();
                if(UtilsService::getUserByEmailLocalDB($data['email']) || UtilsService::getUserByPhoneLocalDB($data['phone'])) {
                    return $this->errorResponse([
                        'err' => true,
                        'message' => 'El correo o teléfono ya se encuentran en uso.'
                    ]);
                }
                if($dealer = $this->model->where('cedula', $this->request->input('cedula'))->first()) {
                    return $this->errorResponse([
                        'err' => true,
                        'message' => 'Ya existe un repartidor con la misma cedula.'
                    ]);
                }
                if($this->validate($this->request, $rules)) {
                    //unset($data['password']);
                    if($dealer = Dealer::create($data)) {
                        $picture = $this->request->file('picture');
                        if($picture) {
                            $filename   = time().rand(111,699).'.' .$picture->getClientOriginalExtension();
                            $fileFullPath = $this->filePath.$filename;
                            $this->request->picture->storeAs($this->filePath,$filename,env('APP_DEFAULT_STORAGE'));
                            $fileUploaded = FireStorageService::uploadWithDeleteLocalFile('dealers',$filename,$fileFullPath);
                            if($fileUploaded) {
                                $dealer->photo = $fileUploaded->src;
                                $dealer->photo_firebase_uid = $fileUploaded->id;
                                $dealer->save();
                            }
                            else {
                                return $this->errorResponse([
                                    'err'=>true,
                                    'message'=> 'No ha sido posible cargar imagen.'
                                ]);
                            }
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'=>false,
                            'message'=> 'Registro exitoso'
                        ]);
                    }else {
                        return $this->errorResponse([
                            'err'=>false,
                            'message'=> 'No ha sido posible guardar registro.'
                        ]);
                    }
                }else{
                    return $this->errorResponse([
                        'err'=>false,
                        'message'=> 'No ha sido posible guardar registro.'
                    ]);
                }
            }catch(\Exception $e) {
                echo $e->getMessage();
                DB::rollBack();
                return $this->errorResponse([
                        'err'=>false,
                        'message'=> 'No ha sido posible guardar registro.'
                    ]);
            }
    }

    public function update($id){
        try{
            DB::beginTransaction();
            $data = $this->request->all();

            //Check Email data
            if($dealerEmail = UtilsService::getUserByEmailLocalDB($data['email'])) {
                if($dealerEmail->id=!$id){
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'El correo ya se encuentran en uso.'
                    ]);
                }
            }

            //Check Phone data
            if($dealerEmail = UtilsService::getUserByPhoneLocalDB($data['email'])) {
                if($dealerEmail->id=!$id){
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'El teléfono ya se encuentran en uso.'
                    ]);
                }
            }

            $dealer  = $this->model->find($id);

            if($dealer) {
                    $rules = [
                        'name'              =>  'required|max:255',
                        'phone'             =>  'required|max:15',
                        'cedula'            =>  'required',
                        'email'             =>  'email'
                    ];

                    if($this->validate($this->request, $rules)) {
                        $data = $this->request->all();

                        //Check password id empty
                        if($data['password']!="") {
                            UtilsService::updateUserPasswordFirebase($data['email'], $data['password']);
                        }
                        //unset($data['password']);
                        //var_dump($dealer); die;
                        if($dealer->fill($data)->save()) {
                            $picture = $this->request->file('picture');
                            if($picture){
                                $fileDlete = $dealer->photo_firebase_uid;
                                $filename   = time().rand(111,699).'.' .$picture->getClientOriginalExtension();
                                $fileFullPath = $this->filePath.$filename;
                                $this->request->picture->storeAs($this->filePath,$filename,env('APP_DEFAULT_STORAGE'));
                                $fileUploaded = FireStorageService::uploadWithReplaceAndDeleteLocalFile('dealers',$filename,$fileFullPath,$fileDlete);
                                if($fileUploaded) {
                                    $dealer->photo = $fileUploaded->src;
                                    $dealer->photo_firebase_uid = $fileUploaded->id;
                                    $dealer->save();
                                }else {
                                    return $this->errorResponse([
                                        'err'=>true,
                                        'message'=> 'No ha sido posible cargar imagen.'
                                    ]);
                                }
                            }
                            DB::commit();
                            return $this->successResponse([
                                'err'=>false,
                                'message'=> 'Registro actualizado correctamente.'
                            ]);
                        }else{
                            //var_dump('No entró'); die;
                            return $this->errorResponse([
                                'err'=>false,
                                'message'=> 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente. Cod: 001'
                            ]);
                        }
                    }else {
                            return $this->errorResponse([
                                'err'=>true,
                                'message'=> 'Faltan datos obligatorios'
                            ],401);
                        }
                }else {
                    //var_dump('No entró'); die;
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente. Cod: 002'
                    ],404);
                } 
        }catch(\Exception $e) {
            //echo $e->getTraceAsString();
            echo $e->getMessage();
            die;
            DB::rollBack();            
            return $this->errorResponse([
                        'err'=>false,
                        'message'=> 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente. Cod: 003'
                    ]);
        }
    }

    public function destroy($item){
        $dealer = Dealer::find($item);
        //var_dump($item); die;
        if($dealer) {
            if($dealer->delete()) {
                return $this->successResponse([
                    'err'=>false,
                    'message'=> 'Registro eliminado correctamente'
                ]);
            }else {
                return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'El registro no existe.'
                ]);
            }
        }else {
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'El registro no existe.'
            ],404);
        }
    }
}
