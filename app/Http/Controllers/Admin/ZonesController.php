<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Zone;
use Illuminate\Support\Facades\DB;
class ZonesController extends Controller
{
    var $folder = "admin.zones";
    var $request;
    var $model;
    var $filePath = 'assets/data/zones/';
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Zone();
    }
    public function index(){
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'zones/HomeController.js'
            ],
            'cssStyles' => [
                0 => 'zones/style.css'
            ]
        ]);
    }

    public function all() {
        return $this->successResponse([
            'err'=>false,
            'data'=> $this->model->get()
        ]);
    }

    public function store() {
      try {
         DB::beginTransaction();
         $data = $this->request->all();
         //echo response()->json($data);
         $this->model->fill($data)->save();
         DB::commit();
         return $this->successResponse([
              'err' => false,
              'message' => 'Datos registrados correctamente.'
         ]);
      }
       catch(\Exception $e){
          echo $e->getMessage();
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible crear registro, por favor verifique su información e intente nuevamente.'
          ]);
       }
    }

    public function update($zone) {
        try {
            DB::beginTransaction();
            $zone = $this->model->find($zone);
            if($zone) {
                $zone->status = $this->request->status;
                $zone->update();
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Registro actualizado correctamente.'
                ]);
            } else{
                return $this->errorResponse([
                    'err' =>true,
                    'message' => 'El registro que intenet actualizar no existe.'
                ]);
            }
        } catch(\Exception $e) {
                echo $e->getMessage();
                return $this->errorResponse([
                    'err' => true,
                    'message' => 'No ha sido posible actulizar el registro, por favor intente nuevamente.'
                ]);
            }
    }

    public function destroy($id) {
      try {
         DB::beginTransaction();
         $itemData = $this->model->find($id);
         if($itemData) {
           if($itemData->delete()) {
              DB::commit();
              return $this->successResponse([
                     'err' => false,
                     'message' => 'Registro eliminado correctamente.'
              ]);
           } else {
              return $this->errorResponse([
                     'err' => true,
                     'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
              ]);
           }
         } else {
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
          ]);
         }
      }
       catch(\Exception $e){
          echo $e->getMessage();
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible eliminar registro.'
          ]);
       }
    }
}
