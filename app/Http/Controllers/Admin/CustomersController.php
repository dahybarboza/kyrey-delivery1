<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\UtilsService;
use App\Services\FireStorageService;
use Yajra\DataTables\Facades\DataTables;

class CustomersController extends Controller
{
    var $folder = "admin.customers";
    var $request;
    var $model;
    var $filePath = 'assets/data/customers/';
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Customer();
    }

    public function index() {
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'customers/HomeController.js',
                1 => 'customers/GoogleController.js'
            ],
            'cssStyles' => [
                0 => 'customers/style.css'
            ]
        ]);
    }

    public function all() {
        $model =  Customer::query();
        return DataTables::eloquent($model)->make(true);
    }

    public function store(){
        try{
            DB::beginTransaction();
            $rules = [
                'name'              =>  'required|max:255',
                'phone'             =>  'required|max:15',
                'email'             =>  'email',
                'password'          =>  'required|min:8',
            ];
            $data = $this->request->all();
            if(UtilsService::getUserByEmailLocalDB($data['email'])){
                return $this->errorResponse([
                    'err' => true,
                    'message' => 'El correo ya se encuentran en uso.'
                ]);
            }
            if(UtilsService::getUserByPhoneLocalDB($data['phone'])) {
                return $this->errorResponse([
                    'err' => true,
                    'message' => 'El teléfono ya se encuentran en uso.'
                ]);
            }
            if($this->validate($this->request, $rules)) {
                unset($data['password']);
                if($dealer = $this->model->create($data)) {
                    $picture = $this->request->file('picture');
                    if($picture) {
                        $filename   = time().rand(111,699).'.' .$picture->getClientOriginalExtension();
                        $fileFullPath = $this->filePath.$filename;
                        $this->request->picture->storeAs($this->filePath,$filename,env('APP_DEFAULT_STORAGE'));
                        $fileUploaded = FireStorageService::uploadWithDeleteLocalFile('dealers',$filename,$fileFullPath);
                        if($fileUploaded) {
                            $dealer->photo = $fileUploaded->src;
                            $dealer->photo_firebase_uid = $fileUploaded->id;
                            $dealer->save();
                        }
                        else {
                            return $this->errorResponse([
                                'err'=>true,
                                'message'=> 'No ha sido posible cargar imagen.'
                            ]);
                        }
                    }
                    DB::commit();
                    return $this->successResponse([
                        'err'=>false,
                        'message'=> 'Registro exitoso'
                    ]);
                }else {
                    return $this->errorResponse([
                        'err'=>false,
                        'message'=> 'No ha sido posible guardar registro.'
                    ]);
                }
            }else{
                return $this->errorResponse([
                    'err'=>false,
                    'message'=> 'No ha sido posible guardar registro.'
                ]);
            }
        }catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                'err'=>false,
                'message'=> 'No ha sido posible guardar registro.'
            ]);
        }
    }

    public function update($id)
    {
        try{
            DB::beginTransaction();
            $data = $this->request->all();
            if($customerEmail = UtilsService::getUserByEmailLocalDB($data['email'])) {
                if($customerEmail->id!=$id) {
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'El correo ya se encuentran en uso.'
                    ]);
                }
            }
            if($customerPhone = UtilsService::getUserByPhoneLocalDB($data['phone'])) {
                if($customerPhone->id!=$id) {
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'El teléfono ya se encuentran en uso.'
                    ]);
                }
            }
            $customer  = $this->model->find($id);
            if($customer) {
                $rules = [
                    'name'              =>  'required|max:255',
                    'phone'             =>  'required|max:15',
                    'email'             =>  'email'
                ];
                if($this->validate($this->request, $rules)) {
                    $data = $this->request->all();
                    //Check password id empty
                    if($data['password']!="") {
                        UtilsService::updateUserPasswordFirebase($data['email'], $data['password']);
                    }
                    unset($data['password']);
                    if($customer->fill($data)->save()) {
                        $picture = $this->request->file('picture');
                        if($picture){
                            $fileDlete = $customer->photo_firebase_uid;
                            $filename   = time().rand(111,699).'.' .$picture->getClientOriginalExtension();
                            $fileFullPath = $this->filePath.$filename;
                            $this->request->picture->storeAs($this->filePath,$filename,env('APP_DEFAULT_STORAGE'));
                            $fileUploaded = FireStorageService::uploadWithReplaceAndDeleteLocalFile('dealers',$filename,$fileFullPath,$fileDlete);
                            if($fileUploaded) {
                                $customer->photo = $fileUploaded->src;
                                $customer->photo_firebase_uid = $fileUploaded->id;
                                $customer->save();
                            }else {
                                return $this->errorResponse([
                                    'err'=>true,
                                    'message'=> 'No ha sido posible cargar imagen.'
                                ]);
                            }
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'=>false,
                            'message'=> 'Registro actualizado correctamente.'
                        ]);
                    }else{
                        return $this->errorResponse([
                            'err'=>false,
                            'message'=> 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente.'
                        ]);
                    }
                }else {
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'Faltan datos obligatorios'
                    ],401);
                }
            }else {
                return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente.'
                ],404);
            }
        }catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                'err'=>false,
                'message'=> 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente.'
            ]);
        }
    }

    public function destroy($item)
    {
        $dealer = $this->model->find($item);
        if($dealer) {
            if($dealer->delete()) {
                return $this->successResponse([
                    'err'=>false,
                    'message'=> 'Registro eliminado correctamente'
                ]);
            }else {
                return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'El registro no existe.'
                ]);
            }
        }else {
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'El registro no existe.'
            ],404);
        }
    }
}
