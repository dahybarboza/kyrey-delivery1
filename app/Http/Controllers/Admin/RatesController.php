<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Zone;
use App\ZoneRate;
use Illuminate\Support\Facades\DB;
class RatesController extends Controller
{
    var $request;
    var $folder = 'admin.zones';
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new ZoneRate();
    }

    public function index() {
        return view($this->folder.'.rates',[
            'jsControllers'=>[
                0=>'zones/RatesController.js'],
            'zone' => Zone::find($this->request->get('zone'))
            ]);
    }

    public function store() {
      try {
         DB::beginTransaction();
         $data = $this->request->all();
         //echo response()->json($data);
         $this->model->fill($data)->save();
         DB::commit();
         return $this->successResponse([
              'err' => false,
              'message' => 'Datos registrados correctamente.'
         ]);
      }
       catch(\Exception $e){
          echo $e->getMessage();
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible crear registro, por favor verifique su información e intente nuevamente.'
          ]);
       }
    }

    public function all($zone) {
        $neighborhoods = ZoneRate::where('zone_id',$zone)->get()->map(function($rate){
            $rate->zone = Zone::find($rate->zone_delivery);
            return $rate;
        });
        if($neighborhoods) {
            return $this->successResponse([
                'err'  => false,
                'data' => $neighborhoods
            ]);
        } else {
            return $this->errorResponse([
                'err' => true,
                'message' => 'La zona no existe, por favor intente nuevamente'
            ]);
        }
    }

    public function update($id) {
        try {
            DB::beginTransaction();
            $prices = $this->model->find($id);
            if($prices) {
                $prices->fill($this->request->all());
                $prices->save();
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Registro actualizado correctamente.'
                ]);
            } else{
                return $this->errorResponse([
                    'err' =>true,
                    'message' => 'El registro que intenet actualizar no existe.'
                ]);
            }
        } catch(\Exception $e) {
                echo $e->getMessage();
                return $this->errorResponse([
                    'err' => true,
                    'message' => 'No ha sido posible actulizar el registro, por favor intente nuevamente.'
                ]);
            }
    }

    public function destroy($id) {
      try {
         DB::beginTransaction();
         $itemData = $this->model->find($id);
         if($itemData) {
           if($itemData->delete()) {
              DB::commit();
              return $this->successResponse([
                     'err' => false,
                     'message' => 'Registro eliminado correctamente.'
              ]);
           } else {
              return $this->errorResponse([
                     'err' => true,
                     'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
              ]);
           }
         } else {
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
          ]);
         }
      }
       catch(\Exception $e){
          echo $e->getMessage();
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible eliminar registro.'
          ]);
       }
    }

    public function zones($zone) {
        $zones = Zone::whereNotExists(function($query) use($zone){
            $query->select(DB::raw(1))
                          ->from('zones_rates')
                          ->whereRaw("zone_delivery = zones.id and zone_id = ".$zone);
        })->get();
        return $this->successResponse([
            'err' => false,
            'data' => $zones
        ]);
    }

    function rate() {
        $collect = $this->request->get('collect');
        if($collect=='Lúke') {
            $collect='Luque';
        }
        $delivery = $this->request->get('delivery');
        if($delivery=='Lúke') {
            $delivery='Luque';
        }
        $rate = Zone::join('zones_rates','zones_rates.zone_id','=','zones.id')
                    ->where('zones.name',$collect)->get()->filter(function($rate) use($delivery){
                        return Zone::find($rate->zone_delivery)->name == $delivery;
                    })->toArray();
        return $this->successResponse([
            'err' => 'false',
            'date' => reset($rate)
        ]);
    }
}
