<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use App\Invoice;
class InvoicesController extends Controller
{
    var $folder = "admin.invoices";
    var $request;
    var $model;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Invoice();
    }

    public function index(){
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'invoices/HomeController.js'
            ]
        ]);
    }

    public function all() {
        return Datatables::of(Invoice::orderBy('status','asc')->get())->make(true);
    }

    public function update($invoice) {
        try {
            $invoice = $this->model->find($invoice);
            if($invoice) {
                $invoice->fill($this->request->all())->save();
                return $this->successResponse([
                    'err' => false,
                    'message' => "Datos actualizados correctamente."
                ]);
            } else {
                return $this->errorResponse([
                    'err' => false,
                    'message' => "No existe el registro o ha sido eliminado anteriormente."
                ]);
            }
        } catch(\Exception $e) {
            return $this->errorResponse([
                'err' => false,
                'message' => "No ha sido posible procesar su solicitud."
            ]);
        }
    }

    public function destroy($id) {
        try {
           DB::beginTransaction();
           $itemData = $this->model->find($id);
           if($itemData) {
             if($itemData->delete()) {
                DB::commit();
                return $this->successResponse([
                       'err' => false,
                       'message' => 'Registro eliminado correctamente.'
                ]);
             } else {
                return $this->errorResponse([
                       'err' => true,
                       'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
                ]);
             }
           } else {
            DB::rollback();
            return $this->errorResponse([
              'err' =>true,
              'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
            ]);
           }
        }
         catch(\Exception $e){
            echo $e->getMessage();
            DB::rollback();
            return $this->errorResponse([
              'err' =>true,
              'message' => 'No ha sido posible eliminar registro.'
            ]);
         }
      }
}
