<?php

namespace App\Http\Controllers\Admin;

use App\Branchoffice;
use App\Customer;
use App\Dealer;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use App\User;
use App\Services\UtilsService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    var $request;
    var $folder = 'admin.home';
    var $filePath = 'assets/data/dealers/';
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index() {
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0=>'home/HomeController.js']
            ]);
    }
    public function profile() {
        return view($this->folder.'.profile',[
            'data'=>Auth::user(),
            'jsControllers'=>[
                0=>'profile/HomeController.js']
            ]);
    }

    function resumen() {
        $data['count_branchoffices']    = Branchoffice::all()->count();
        $data['count_dealers']          = Dealer::all()->count();
        $data['count_customers']        = Customer::all()->count();
        $data['count_orders']           = Order::where('status','>=',0)->where('status','<=',5)->get()->count();
        $data['count_orders_success']           = Order::where('status',6)->count();
        return $this->successResponse([
            'err'   =>  false,
            'data'  =>  $data
        ]);
    }

    public function update() {
        try {
            DB::beginTransaction();
            $user = UtilsService::getUserByEmailLocalDB($this->request->email);
            if($user) {
                if($user->id != Auth::user()->id) {
                    return $this->errorResponse([
                        'err' => true,
                        'El correo ya se encuentra en uso.'
                    ]);
                }
            }
            if($user){
                return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'El correo ya se encuentra en uso'
                ]);
            }else{
            $user = User::find(Auth::user()->id);
            $data = $this->request->all();
            $user->fill($data);
            if(!$user->isClean()) {
                if(Hash::check($this->request->input('password'),Auth::user()->password)){
                            if($this->request->input('newpassword')!="") {
                                $user->password = Hash::make($this->request->input('newpassword'));
                            }else{
                                unset($user->password);
                            }
                            $picture = $this->request->file('picture');
                            if($picture) {
                                $deletePicture = $user->photo;
                                $filename   = time().rand(111,699).'.' .$picture->getClientOriginalExtension();
                                $picture->move($this->filePath,$filename);
                                $user->photo = $this->filePath.$filename;
                                if(!$user->save()) {
                                    return $this->errorResponse([
                                        'err'=>true,
                                        'message'=> 'No ha sido posible cargar imagen.'
                                    ]);
                                }
                                if(file_exists($deletePicture)) {
                                    unlink($deletePicture);
                                }
                            }
                            if($user->save()){
                                DB::commit();
                                return $this->successResponse([
                                    'err'=>false,
                                    'message'=> 'Registro actualizado correctamente.'
                                ]);
                            }else{
                                return $this->errorResponse([
                                    'err'=>true,
                                    'message'=> 'No ha sido posible actualizar, por favor intente nuevamente.'
                                ]);
                            }
                        }else{
                            return $this->errorResponse([
                                'err'=>true,
                                'message'=> 'Passsword incorrecto, ningun dato actualizado'
                            ]);
                        }
                    } else
                    {
                        return $this->successResponse([
                            'err'=>false,
                            'message'=> 'Ningun dato ha sido actualizado'
                        ]);
                    }
                }
        } catch(\Exception $e) {
            DB::rollBack();
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'No ha sido posible actualizar, por favor intente nuevamente.'
            ]);
        }
    }

    public function rate_service() {
        $orders = Order::where('status',6)->get();
        return $this->successResponse([
            'orders'  => $orders->count(),
            'service' => [
                0 => collect($orders)->filter(function($order){ return $order->rated_service==1;})->count(),
                1 => collect($orders)->filter(function($order){ return $order->rated_service==2;})->count(),
                2 => collect($orders)->filter(function($order){ return $order->rated_service==3;})->count(),
                3 => collect($orders)->filter(function($order){ return $order->rated_service==4;})->count(),
                4 => collect($orders)->filter(function($order){ return $order->rated_service==5;})->count()
            ],
            'time' => [
                0 => collect($orders)->filter(function($order){ return $order->rated_time==1;})->count(),
                1 => collect($orders)->filter(function($order){ return $order->rated_time==2;})->count(),
                2 => collect($orders)->filter(function($order){ return $order->rated_time==3;})->count(),
                3 => collect($orders)->filter(function($order){ return $order->rated_time==4;})->count(),
                4 => collect($orders)->filter(function($order){ return $order->rated_time==5;})->count()
            ],
            'health' => [
                0 => collect($orders)->filter(function($order){ return $order->rated_health==1;})->count(),
                1 => collect($orders)->filter(function($order){ return $order->rated_health==2;})->count(),
                2 => collect($orders)->filter(function($order){ return $order->rated_health==3;})->count(),
                3 => collect($orders)->filter(function($order){ return $order->rated_health==4;})->count(),
                4 => collect($orders)->filter(function($order){ return $order->rated_health==5;})->count()
            ],
            'friendly' => [
                0 => collect($orders)->filter(function($order){ return $order->rated_friendly==1;})->count(),
                1 => collect($orders)->filter(function($order){ return $order->rated_friendly==2;})->count(),
                2 => collect($orders)->filter(function($order){ return $order->rated_friendly==3;})->count(),
                3 => collect($orders)->filter(function($order){ return $order->rated_friendly==4;})->count(),
                4 => collect($orders)->filter(function($order){ return $order->rated_friendly==5;})->count()
            ]

        ]);
    }
}
