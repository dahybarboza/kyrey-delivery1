<?php

namespace App\Http\Controllers\Admin;

use App\Branchoffice;
use App\Dealer;
use App\ExpressFees;
use App\Http\Controllers\Controller;
use App\Neighborhood;
use App\Order;
use App\OrderPackage;
use App\OrderPackageCollect;
use App\OrderPackageDelivery;
use App\Quote;
use App\Services\UtilsService;
use App\Zone;
use App\ZoneRate;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Maatwebsite\Excel\Excel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;



class OrdersController extends Controller{
    var $request;
    var $folder = 'admin.orders';
    var $model;
    public function __construct(Request $request){
        $this->request = $request;
        $this->model = new Order();
    }

    public function index() {
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0=>'orders/HomeController.js'
            ],
        ]);
    }

    public function create() {
        return view($this->folder.'.create',[
            'jsControllers'=>[
                0=>'orders/CreateController.js',
                1=>'orders/L1Controller.js',
                2=>'orders/L2Controller.js',
                3=>'orders/L3Controller.js',
                4=>'orders/L4Controller.js',
                5=>'orders/L2AddDestinoController.js',
                6=>'orders/L3AddOrigenesController.js',
                7=>'orders/L3AddDestinoController.js',
                8=>'orders/L4AddOrigenesController.js',
                9=>'orders/L4AddDestinosController.js',

            ],
            'cssStyles' => [
                0 => 'orders/style.css'
            ],
            'branchoffices' => Branchoffice::where('status',1)->get(),
            'zones' => Zone::where('status',1)->get(),
            'quotes' => Quote::where('status','>=',0)->where('status','<=',2)
                ->whereNotExists(function($query) {
                    $query->select(DB::raw(1))
                        ->from('orders')
                        ->whereRaw('quotes.id = orders.quote_id');
                })
                ->get(),
            'dealers' => Dealer::where('status', '=', 1)->get()
        ]);
    }

    function store() {

        try {
            DB::beginTransaction();
            $data = $this->request->all();
            $order= new Order();
            if($data['date']=="") {
                $data['date'] = Carbon::now();
            } else {
                if($data['service']==1 && $data['type']==2) {
                    $data['date'] = Carbon::createFromFormat('d/m/Y',$data['date'],config('app.timezone'));
                } else if($data['service']==2){
                    $data['date'] = Carbon::createFromFormat('d/m/Y',$data['date'],config('app.timezone'));
                }
            }
            $order->fill($data);
            if($order->save()) {
                $order = $this->model->find($order->id);
                foreach($data['packages'] as $packageData) {
                    $order->quantity = $order->quantity + $packageData['quantity'];
                    $order->total = $order->total + $packageData['total'];
                    $package = new OrderPackage();
                    $package->order_id = $order->id;
                    $package->quantity = $packageData['quantity'];
                    $package->distance = $packageData['distance'];
                    $package->type     = $packageData['type'];
                    $package->total    = $packageData['total'];
                    $package->status   = 0;//Nuevo
                    if($package->save()) {
                        if($package->type!='L4') {
                            //Store Addresses for collect
                            foreach($packageData['collect'] as $collect) {
                                OrderPackageCollect::create([
                                    'order_package_id'    => $package->id,
                                    'name'          => $collect['name'],
                                    'phone'         => $collect['phone'],
                                    'note'          => $collect['note'],
                                    'quantity'      => $collect['quantity'],
                                    'type'          => isset($collect['type'])!=""?$collect['type']:0,
                                    'address'       => $collect['address'],
                                    'neighborhood'  => $collect['neighborhood'],
                                    'lat'           => $collect['lat'],
                                    'lng'           => $collect['lng'],
                                    'status'        => 0,
                                ]);
                            }
                            //Store Address for collect
                            foreach($packageData['delivery'] as $delivery) {
                                OrderPackageDelivery::create([
                                    'order_package_id'    => $package->id,
                                    'name'          => $delivery['name'],
                                    'phone'         => $delivery['phone'],
                                    'note'          => $delivery['note'],
                                    'quantity'      => isset($delivery['quantity'])?$delivery['quantity']:0,
                                    'type'          => isset($delivery['type'])!=""?$delivery['type']:0,
                                    'address'       => $delivery['address'],
                                    'neighborhood'  => $delivery['neighborhood'],
                                    'lat'           => $delivery['lat'],
                                    'lng'           => $delivery['lng'],
                                    'status'        => 0
                                ]);
                            }
                        } else {
                            foreach($packageData['collect'] as $collect) {
                                $collectPackage = new OrderPackageCollect();
                                $collectPackage->fill([
                                    'order_package_id'    => $package->id,
                                    'name'          => $collect['name'],
                                    'phone'         => $collect['phone'],
                                    'note'          => $collect['note'],
                                    'quantity'      => $collect['quantity'],
                                    'type'          => isset($collect['type'])!=""?$collect['type']:0,
                                    'address'       => $collect['address'],
                                    'neighborhood'  => $collect['neighborhood'],
                                    'lat'           => $collect['lat'],
                                    'lng'           => $collect['lng'],
                                    'status'        => 0
                                ]);
                                if($collectPackage->save()) {
                                    if(isset($collect['deliveries'])) {
                                        foreach($collect['deliveries'] as $delivery) {
                                            OrderPackageDelivery::create([
                                                'order_package_id'      => $package->id,
                                                'order_package_collect_id'    => $collectPackage->id,
                                                'name'                  => $delivery['name'],
                                                'phone'                 => $delivery['phone'],
                                                'note'                  => $delivery['note'],
                                                'quantity'              => isset($delivery['quantity'])?$delivery['quantity']:0,
                                                'type'                  => isset($delivery['type'])!=""?$delivery['type']:0,
                                                'address'               => $delivery['address'],
                                                'neighborhood'          => $delivery['neighborhood'],
                                                'lat'                   => $delivery['lat'],
                                                'lng'                   => $delivery['lng'],
                                                'status'                => 0
                                            ]);
                                        }
                                    }
                                }

                            }
                        }
                    } else {
                        DB::rollBack();
                        return $this->errorResponse([
                            'err'     => true,
                            'message' => 'Algunos elementos presentan falta de datos, por favor verifique su información e intente nuevamente.'
                        ]);
                    }
                }
                $order->save();
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Orden de envio registrada correctamente.'
                ]);
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                'err' =>true,
                'message' => 'No ha sido posible registrar orden, por favor verifique su información e intente nuevamente.'
            ]);
        }
    }

    public function all() {
        return Datatables::of(Order::orderBy('status','asc')->get())->make(true);
    }

    public function edit($id) {
        return view($this->folder.'.edit',[
            'jsControllers'=>[
                0=>'orders/EditController.js',
                1=>'orders/L1EditController.js',
                2=>'orders/L2EditController.js',
                3=>'orders/L2EditDestinoController.js',
                4=>'orders/L3EditController.js',
                5=>'orders/L3EditOrigenesController.js',
                6=>'orders/L3EditDestinoController.js',
                7=>'orders/L4EditController.js',
                8=>'orders/L4EditOrigenesController.js',
                9=>'orders/L4EditDestinosController.js'
            ],
            'id' => $id,
            //'dealers' => Dealer::all(),
            'cssStyles' => [
                0 => 'orders/style.css'
            ],
            'zones' => Zone::where('status',1)->get(),
            'branchoffices' => Branchoffice::where('status',1)->get(),
            'quotes' => Quote::where('status','>=',0)->where('status','<=',2)->get(),
            'dealers' => Dealer::where('status', '=', 1)->get()
        ]);
    }

    public function find($id) {
        return $this->successResponse($this->model->where('id',$id)->get()->map(function($order) {
            $order->packages->map(function($package) {
                $package->collect  = OrderPackageCollect::where('order_package_id',$package->id)->get();
                $package->delivery = OrderPackageDelivery::where('order_package_id',$package->id)->get();
                $package->collect->map(function($collect) {
                    $collect->deliveries = OrderPackageDelivery::where('order_package_collect_id',$collect->id)->get();
                    $collect->assigneds = collect($collect->deliveries)->sum('quantity');
                    return $collect;
                });
                return $package;
            });
            return $order;
        }));
    }

    public function update($id) {
        try {
            DB::beginTransaction();
            $data = $this->request->all();
            $order = $this->model->find($id);
            if($order) {
                if($data['service']==1 && $data['type']==2) {
                    $data['date'] = Carbon::createFromFormat('d/m/Y',$data['date'],config('app.timezone'));
                } else if($data['service']==2) {
                    $data['date'] = Carbon::createFromFormat('d/m/Y',$data['date'],config('app.timezone'));
                    $data['type'] = 3;
                } else {
                    $data['date'] = Carbon::now();
                }
                $order->fill($data);
                if($order->save()) {
                    $order = $this->model->find($order->id);
                    if($order->service==1) {
                        $order->total = 0;
                    }
                    $order->quantity = 0;
                    foreach($data['packages'] as $packageData) {
                        $order->quantity = $order->quantity + $packageData['quantity'];
                        if($order->service==1) {
                            $order->total = $order->total + $packageData['total'];
                        }
                        isset($packageData['id'])?$package = OrderPackage::find($packageData['id']):$package = new OrderPackage();
                        if($package) {
                            $continuarPackage = true;
                            if(isset($packageData['action'])) {
                                if($packageData['action'] == 'delete') {
                                    $package->delete();
                                    $continuarPackage = false;
                                }
                            }
                            $package->order_id = $order->id;
                            $package->quantity = $packageData['quantity'];
                            $package->distance = $packageData['distance'];
                            $package->type     = $packageData['type'];
                            $package->total    = $packageData['total'];
                            $package->status   = $packageData['status'];
                            if($continuarPackage) {
                                if($package->save()) {
                                    if($package->type!='L4') {
                                        //Store Addresses for collect
                                        foreach($packageData['collect'] as $collect) {
                                            isset($collect['id'])?$packageCollect = OrderPackageCollect::find($collect['id']):$packageCollect = new OrderPackageCollect();
                                            $dataCollect = [
                                                'order_package_id'    => $package->id,
                                                'name'          => $collect['name'],
                                                'phone'         => $collect['phone'],
                                                'note'          => $collect['note'],
                                                'quantity'      => $collect['quantity'],
                                                'type'          => isset($collect['type'])!=""?$collect['type']:0,
                                                'address'       => $collect['address'],
                                                'neighborhood'  => $collect['neighborhood'],
                                                'lat'           => $collect['lat'],
                                                'lng'           => $collect['lng'],
                                                'status'        => 0
                                            ];
                                            if(isset($collect['action'])) {
                                                switch($collect['action']) {
                                                    case 'delete' : {
                                                        $packageCollect->fill($dataCollect)->delete();
                                                        break;
                                                    }
                                                    default : {
                                                        $packageCollect->fill($dataCollect)->save();
                                                        break;
                                                    }
                                                }
                                            } else{
                                                $packageCollect->fill($dataCollect)->save();
                                            }
                                        }
                                        //Store Address for collect
                                        foreach($packageData['delivery'] as $delivery) {
                                            isset($delivery['id'])?$packageDelivery = OrderPackageDelivery::find($delivery['id']):$packageDelivery = new OrderPackageDelivery();
                                            $dataDelivery = [
                                                'order_package_id'    => $package->id,
                                                'name'          => $delivery['name'],
                                                'phone'         => $delivery['phone'],
                                                'note'          => $delivery['note'],
                                                'quantity'      => isset($delivery['quantity'])?$delivery['quantity']:0,
                                                'type'          => isset($delivery['type'])!=""?$delivery['type']:0,
                                                'address'       => $delivery['address'],
                                                'neighborhood'  => $delivery['neighborhood'],
                                                'lat'           => $delivery['lat'],
                                                'lng'           => $delivery['lng'],
                                                'status'        => 0
                                            ];
                                            if(isset($delivery['action'])) {
                                                switch($delivery['action']) {
                                                    case 'delete' : {
                                                        $packageDelivery->delete();
                                                        break;
                                                    }
                                                    default : {
                                                        $packageDelivery->fill($dataDelivery)->save();
                                                        break;
                                                    }
                                                }
                                            } else{
                                                $packageDelivery->fill($dataDelivery)->save();
                                            }
                                        }
                                    } else {
                                        foreach($packageData['collect'] as $collect) {
                                            isset($collect['id'])?$packageCollect = OrderPackageCollect::find($collect['id']):$packageCollect = new OrderPackageCollect();
                                            $dataCollect          =  [
                                                'order_package_id'    => $package->id,
                                                'name'                => $collect['name'],
                                                'phone'               => $collect['phone'],
                                                'note'                => $collect['note'],
                                                'quantity'            => $collect['quantity'],
                                                'type'                => isset($collect['type'])!=""?$collect['type']:0,
                                                'address'             => $collect['address'],
                                                'neighborhood'        => $collect['neighborhood'],
                                                'lat'                 => $collect['lat'],
                                                'lng'                 => $collect['lng'],
                                                'status'              => 0
                                            ];
                                            //echo json_encode($dataCollect);
                                            if(isset($collect['action'])) {
                                                switch($collect['action']) {
                                                    case 'delete' : {
                                                        $packageCollect->fill($dataCollect)->delete();
                                                        break;
                                                    }
                                                    default : {
                                                        $packageCollect->fill($dataCollect)->save();
                                                        if(isset($collect['deliveries'])) {
                                                            foreach($collect['deliveries'] as $delivery) {
                                                                isset($delivery['id'])?$packageDelivery = OrderPackageDelivery::find($delivery['id']):$packageDelivery = new OrderPackageDelivery();
                                                                $dataDelivery           =  [
                                                                    'order_package_id'      => $package->id,
                                                                    'order_package_collect_id'    => $packageCollect->id,
                                                                    'name'                  => $delivery['name'],
                                                                    'phone'                 => $delivery['phone'],
                                                                    'note'                  => $delivery['note'],
                                                                    'quantity'              => isset($delivery['quantity'])?$delivery['quantity']:0,
                                                                    'type'                  => isset($delivery['type'])!=""?$delivery['type']:0,
                                                                    'address'               => $delivery['address'],
                                                                    'neighborhood'          => $delivery['neighborhood'],
                                                                    'lat'                   => $delivery['lat'],
                                                                    'lng'                   => $delivery['lng'],
                                                                    'status'                => 0
                                                                ];
                                                                switch($delivery['action']) {
                                                                    case 'delete' : {
                                                                        $packageDelivery->delete();
                                                                        break;
                                                                    }
                                                                    default : {
                                                                        $packageDelivery->fill($dataDelivery)->save();
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    DB::rollBack();
                                    return $this->errorResponse([
                                        'err'     => true,
                                        'message' => 'Algunos elementos presentan falta de datos, por favor verifique su información e intente nuevamente.'
                                    ]);
                                }
                            }
                        }
                    }
                    if($order->customer_id!=null) {
                        UtilsService::sendPushOrderCustomer($order);
                    }
                    if($order->dealer_id!=null) {
                        UtilsService::sendPushOrderDealer($order);
                    }
                    $order->save();
                    DB::commit();
                    return $this->successResponse([
                        'err' => false,
                        'message' => 'Orden actualizada correctamente.'
                    ]);
                } else {
                    return $this->errorResponse([
                        'err' => true,
                        'message' => 'No ha sido posible editar orden, por favor verifique su información e intente nuevamente.'
                    ]);
                }
            } else {
                return $this->errorResponse([
                    'err' => true,
                    'message' => 'No ha sido posible localizar orden, verifique si ha sido eliminada anteriormente.'
                ]);
            }
        } catch(\Exception $e){
            echo $e->getMessage();
            $this->errorResponse([
                'err' =>true,
                'message' => 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente.'
            ]);
        }
    }

    public function zone_rates($start,$arrival) {
        return $this->successResponse([
            'err'   => false,
            'data'  => ZoneRate::where([
                'zone_id' => $start,
                'zone_arrival' => $arrival,
                'status'  => 1])->first()
        ]);
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            $itemData = $this->model->find($id);
            if($itemData) {
                if($itemData->delete()) {
                    DB::commit();
                    return $this->successResponse([
                        'err' => false,
                        'message' => 'Registro eliminado correctamente.'
                    ]);
                } else {
                    return $this->errorResponse([
                        'err' => true,
                        'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
                    ]);
                }
            } else {
                DB::rollback();
                return $this->errorResponse([
                    'err' =>true,
                    'message' => 'No ha sido posible eliminar registro, por favor intente dentro de un momento más.'
                ]);
            }
        }
        catch(\Exception $e){
            echo $e->getMessage();
            DB::rollback();
            return $this->errorResponse([
                'err' =>true,
                'message' => 'No ha sido posible eliminar registro.'
            ]);
        }
    }

    function dealers($branchoffice) {
        return $this->successResponse([
            'err'   =>false,
            'data'  => Dealer::where('branchoffice_id',$branchoffice)->get()
        ]);
    }

    public function neighborhoods($zone) {
        return $this->successResponse([
            'err'   => false,
            'data'  => Neighborhood::where('zone_id',$zone)->where('status',1)->get()
        ]);
    }

    public function quote($quote) {
        return $this->successResponse([
            'err' => false,
            'data' => Quote::find($quote)
        ]);
    }

    public function fees() {
        return $this->successResponse([
            'err'  => false,
            'data' => ExpressFees::first()
        ]);
    }

    function rates() {
        $rates = Zone::join('zones_rates','zones_rates.zone_id','=','zones.id')->get()->map(function($rate) {
            return [
                'id',
                'zone_collect'   => $rate->name,
                'zone_delivery'  => Zone::find($rate->zone_delivery)->name,
                'price'          => $rate->price
            ];
        });
        return $this->successResponse([
            'err' => 'false',
            'data' => $rates
        ]);
    }

    public function exportAllOrders(){

        return Excel::download(new OrdersExport, 'orders.xlsx');
    }

}
