<?php

namespace App\Http\Controllers\Admin;

use App\Quote;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\UtilsService;
use App\Services\FireStorageService;
use Yajra\DataTables\Facades\DataTables;
class QuotesController extends Controller
{
    var $folder = "admin.quotes";
    var $request;
    var $model;
    var $filePath = 'assets/data/quotes/';
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Quote();
    }

    public function index(){
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0 => 'quotes/HomeController.js'
            ]
        ]);
    }

    public function all() {
        $model =  Quote::all()->map(function($quote){
            $quote->customer_data;
            return $quote;
        });
        return DataTables::collection($model)->make(true);
    }

    public function find($quote) {
        $quote = $this->model->find($quote);
        $quote->customer_data;
        return $this->successResponse([
            'err' => false,
            'data' => $quote
        ]);
    }

    public function update($id)
    {
        try{
            DB::beginTransaction();
            $data = $this->request->all();
            //Check Email data
             $quote = $this->model->find($id);
             $quote->fill($data);
             if($quote->save()) {
                if($quote->status == 1) {
                    UtilsService::sendPushNotification([
                        'user'      => $quote->customer_id,
                        'type'      => 1, //Customer,
                        'title'     => 'Cotización #'.$quote->id.' ha sido aceptada',
                        'message'   => $quote->note!=''?$quote->note:'Pronto aparecera la orden de corizacion en su listado de ordenes principales.'
                    ]);
                } else if($quote->status ==2) {
                    UtilsService::sendPushNotification([
                        'user'      => $quote->customer_id,
                        'type'      => 1, //Customer,
                        'title'     => 'Cotización #'.$quote->id.' ah sido rechazada',
                        'message'   => $quote->note!=''?$quote->note:'Lo sentimos, su orden no ah sido aprobada, si requiere mayor informacion por favor contacte a soporte.'
                    ]);
                }
                 DB::commit();
                return $this->successResponse([
                    'err'       =>  false,
                    'message'   =>  'Datos actualizados correctamente.'
                ]);
             } else {
                 return $this->errorResponse([
                     'err'      =>  true,
                     'mesage'   =>  'No ha sido posible actualizar el registro.'
                 ]);
              }
        }catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();            
            return $this->errorResponse([
                        'err'=>false,
                        'message'=> 'No ha sido posible actualizar registro, por favor verifique su información e intente nuevamente.'
                    ]);
        }
    }

    public function destroy($item)
    {
        $dealer = $this->model->find($item);
        if($dealer) {
            if($dealer->delete()) {
                return $this->successResponse([
                    'err'=>false,
                    'message'=> 'Registro eliminado correctamente'
                ]);
            }else {
                return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'El registro no existe.'
                ]);
            }
        }else {
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'El registro no existe.'
            ],404);
        }
    }
}
