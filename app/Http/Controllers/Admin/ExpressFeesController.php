<?php

namespace App\Http\Controllers\Admin;

use App\ExpressFees;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExpressFeesController extends Controller
{
    var $request;
    var $folder = 'admin.express-fees';
    var $model;
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new ExpressFees();
    }

    public function index() {
        return view($this->folder.'.index',[
            'jsControllers'=>[
                0=>'express-fees/HomeController.js']
         ]);
    }
    function find($id) {
        return $this->successResponse([
            'err' => false,
            'data'=> ExpressFees::find(1)
        ]);
    }

    function update($id) {
        try {
            DB::beginTransaction();
            $fees = $this->model->find($id);
            if($fees) {
                $fees->fill($this->request->all())->update();
            } else {
            $this->model->fill($this->request->all())->save();  
            }
            DB::commit();
        } catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                'err' => true,
                'message' => 'No ha sido posible actualizar datos.'
            ]);
        }
    }
}