<?php

namespace App\Http\Controllers\Api\Dealer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UtilsService;
class GeneralController extends Controller
{
    var $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    function send_notification() {
        try {
            $data = $this->request->all('users');
            foreach($data['users'] as $user) {
                if(isset($user['user'])){
                    UtilsService::sendPushNotification([
                        'user'    => $user['user'],
                        'title'   => $user['title'],
                        'message' => $user['message'].'',
                        'type'    => $user['type']
                    ]);
                }
            }
            return $this->successResponse([
                'err'       => false,
                'message'   => 'Notificacion enviada correctamente.'
            ]);
        }catch(\Exception $e) {
            echo $e->getMessage();
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'NO ha sido posible enviar notificación, por favor intente nuevamente.'
            ]);
        }
    }
}