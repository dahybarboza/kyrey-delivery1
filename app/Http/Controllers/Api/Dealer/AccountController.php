<?php

namespace App\Http\Controllers\Api\Dealer;
use App\Http\Controllers\Controller;
use App\Dealer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;;
class AccountController extends Controller
{
    var $request;
    var $model;
    var $filePath = "assets/data/dealers/";
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Dealer();
    }

    public function available() {
        try {
            DB::beginTransaction();
            $dealer = $this->model->find($this->request->auth->id);
            if($dealer) {
                $dealer->available = $this->request->input('available');
                if($dealer->save()) {
                    DB::commit();
                    if($this->request->input('available') == 1) {
                        return $this->successResponse([
                            'err' => false,
                            'message'=> 'Usuario conectado'
                        ]);
                    }else {
                        return $this->successResponse([
                            'err' => false,
                            'message'=> 'Usuario desconectado'
                        ]);
                    }
                }else {
                    return $this->errorResponse([
                        'err' => true,
                        'message'=> 'No ha sido posible actualizar el estado de usuario.'
                    ]);
                }
            } else {
                return $this->errorResponse([
                    'err' => true,
                    'message'=> 'El perfil no pertenece al usuario.'
                ]);
            }
        } catch(\Exception $e) {
            DB::rollBack();
            return $this->errorResponse([
                    'err' => true,
                    'message'=> 'No ha sido posible actualizar el estado de usuario.'
                ]);
        }
    }
    
    function updategps() {
        $dealer = $this->model->where(['firebase_uid' => $this->request->input('user_uid')])->first();
        if($dealer) {
            $dealer->lat = $this->request->input('lat');
            $dealer->lng = $this->request->input('lng');
            $dealer->save();
            return $this->successResponse([
                'err'=>false,
                'message'=> 'Ubicacion actualizada correctamente.'
            ]);
        }else{
            return $this->successResponse([]);
        }
    }
}
