<?php

namespace App\Http\Controllers\Api\Dealer;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Delivery;
use App\Services\UtilsService;
class DeliveryController extends Controller
{
    var $request;
    var $model;
    public function __construct(Request $request) {
        $this->request = $request;
        $this->model = new Delivery();
    }

    public function store() {
      try {
         DB::beginTransaction();


     //    $data = $this->request->all();

         $data['dealer_id'] = $this->request->auth->id;
         $data['branchoffice_id'] = $this->request->auth->branchoffice_id;
         $customer = Customer::where('phone',$data['phone'])->first();
         if($customer) {
             $data['customer_id'] = $customer->id;
         }
         $data['status'] = 1;//Entregando automaticamente
         $this->model->fill($data)->save();
         DB::commit();
         return $this->successResponse([
              'err' => false,
              'message' => 'Datos registrados correctamente.'
         ]);
      }
       catch(\Exception $e){
          echo json_encode([
              'message' => $e->getMessage(),
              'File' => $e->getFile(),
              'Line' => $e->getLine()
          ]);
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible crear registro, por favor verifique su información e intente nuevamente.'
          ]);
       }
    }

    function status($id) {
        try {
            DB::beginTransaction();
            if($order = Delivery::find($id)) {
                //Check if the order has acepted and asigned
                if($order->dealer_id == $this->request->auth->id) {
                        $order->status = $this->request->input('status');
                        $order->save();
                        UtilsService::sendPushNotificationOrSMS($order);
                        DB::commit();
                        return $this->successResponse([
                            'err'       => false,
                            'message'   => 'Estado de orden actualizado correctamente'
                        ]);
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no le ha sido asignada, contacte a soporte.'
                    ]);
                }
            }else{
                return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no existe, por favor verifique si ha sido cancelda.'
                    ]);
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'No ha sido posible iniciar entrega, por favor intente nuevamente.'
                ],500);
        }
    }
}
