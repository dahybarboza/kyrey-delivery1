<?php

namespace App\Http\Controllers\Api\Dealer;

use App\Http\Controllers\Controller;
use App\Observers\OrderObserver;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Order;
use App\OrderPackage;
use App\OrderPackageCollect;
use App\OrderPackageDelivery;
use App\Services\UtilsService;
use Illuminate\Support\Facades\DB;
use stdClass;

class OrdersController extends Controller
{
    var $request;
    public function __construct(Request $request) {
        $this->request = $request;
    }

    function detail($order) {
            if($order = Order::find($order)) {
                if($order->dealer_id == $this->request->auth->id) {
                        $order->complements;
                        $order->subtotal = $order->getSubtotal();
                        $order->total = $order->getTotal($order->id);
                        $order->items->map(function($item){
                            $item->item;
                            $item->complements->map(function($complement){
                                $complement->complement;
                                return $complement;
                            });
                            return $item;
                        });
                        return $this->successResponse([
                            'err'       => false,
                            'data'      => [
                                'id'                => $order->id,
                                'client'            => $order->name,
                                'address'           => $order->address,
                                'items'             => $order->items,
                                'discount'          => $order->discount,
                                'subtotal'          => $order->subtotal,
                                'delivery_charge'   => $order->delivery_charge,
                                'total'             => $order->total,
                                'pay_format'        => $order->pay_format,
                                'note'              => $order->note
                            ]
                        ]);
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>__('messages.dealer_order_was_not_assigned')
                    ]);
                }
            }else{
                return $this->errorResponse([
                        'err'=>true,
                        'message'=>__('messages.not_found')
                    ]);
            }
    }

    function status($order) {
        try {
            DB::beginTransaction();
            if($order = Order::find($order)) {
                //Check if the order has acepted and asigned
                if($order->dealer_id == $this->request->auth->id) {
                        $order->status = $this->request->input('status');
                        $order->save();
                        if($order->customer_id) {
                            UtilsService::sendPushOrderCustomer($order);
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'       => false,
                            'message'   => 'Estado de orden actualizado correctamente'
                        ]);
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no le ha sido asignada, contacte a soporte.'
                    ]);
                }
            }else{
                return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no existe, por favor verifique si ha sido cancelda.'
                    ]);
            }
        } catch(\Exception $e){
            DB::rollBack();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'No ha sido posible iniciar entrega, por favor intente nuevamente.'
                ],500);
        }
    }

    function cancel($uid) {
        try{
            DB::beginTransaction();
            if($order = Order::find($this->request->input('order'))) {
                if($order->dealer_id == $this->request->auth->id) {
                        $order->status = 7;
                        $order->canceled_canceller = 1;//Dealer
                        //$order->canceled_type = $this->request->input('type');
                        $order->canceled_date = Carbon::now()->toDateTimeString();
                        $order->save();
                        if($order->customer_id) {
                            UtilsService::sendPushOrderCustomer($order);
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'       => false,
                            'message'   => 'Entrega cancelada correctamente'
                        ]);
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'La orden no le ha sido asignada.'
                    ]);
                }
            }else{
                $order = new Order();
                $order->firebase_uid = $uid;
                $orderObserver = new OrderObserver($this->request);
                $orderObserver->deleted($order);
                return $this->successResponse([
                    'err'       => false,
                    'message'   => 'Entrega cancelada correctamente'
                ]);
            }
        }catch(\Exception $e){
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'No ha sido posible cancelar la orden'
                ],500);
        }
    }

    function packages_status() {
        try {
            DB::beginTransaction();
            if($order = Order::find($this->request->input('order'))) {
                //Check if the order has acepted and asigned
                if($order->dealer_id == $this->request->auth->id) {
                    $package = OrderPackage::find($this->request->input('package'));
                    if($package) {
                        $package->status = $this->request->input('status');
                        $package->save();
                        if($order->customer_id) {
                            switch($package->status) {
                              /*  case 1: {
                                    UtilsService::sendPushNotification([
                                        'user'    => $order->customer_id,
                                        'title'   => 'Recolectando paquete #'.$package->id,
                                        'message' => 'Su paquete se encuentra en turno de recolección, puede dar seguimiento en la lista de paquetes de la orden correspondiente.',
                                        'type'    => 1
                                    ]);break;}*/
                                case 1: {
                                    UtilsService::sendPushNotification([
                                        'user'    => $order->customer_id,
                                        'title'   => 'Recolección finalizada paquete #'.$package->id.".",
                                        'message' => 'La recolección de su paquete ha finalizado, se le notificará cuando el estado sea actualizado',
                                        'type'    => 1
                                    ]);break;}
                            /*    case 2: {
                                    UtilsService::sendPushNotification([
                                        'user'    => $order->customer_id,
                                        'title'   => 'Realizando entrega paquete #'.$package->id.".",
                                        'message' => 'Se ha iniciado la entrega de su paquete, se le notificará cuando el estado sea actualizado',
                                        'type'    => 1
                                    ]);break;}*/
                                case 2: {
                                        UtilsService::sendPushNotification([
                                            'user'    => $order->customer_id,
                                            'title'   => 'Entrega finalizada paquete #'.$package->id." .",
                                            'message' => 'La entrega de su paquete ha finalizado, si tiene algun detalle con su orden por favor contacte a soporte.',
                                            'type'    => 1
                                        ]);break;}
                                case 3: {
                                    UtilsService::sendPushNotification([
                                        'user'    => $order->customer_id,
                                        'title'   => 'El paquete #'.$package->id.' ha sido cancelado.',
                                        'message' => 'Su paquete ha sido cancelado, por favor para mayor detalle contacte al repartidor o  soporte.',
                                        'type'    => 1
                                    ]);break;}
                            }
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'       => false,
                            'message'   => 'Estado de orden actualizado correctamente'
                        ]);
                    } else {
                        return $this->errorResponse([
                            'Lo sentimos, el paquete ya no está disponible.'
                        ]);
                    }
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no le ha sido asignada, contacte a soporte.'
                    ]);
                }
            }else{
                return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no existe, por favor verifique si ha sido cancelda.'
                    ]);
            }
        } catch(\Exception $e){
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'No ha sido posible actualizar el estado del paquete.'
                ],500);
        }
    }

    function packages_collects_status() {
        try {
            DB::beginTransaction();
            $package = OrderPackage::find($this->request->input('package'));
            if($package) {
                $order = Order::where([
                    'id' => $package->order_id,
                    'dealer_id' => $this->request->auth->id])->first();
                if($order) {
                    $collect = OrderPackageCollect::where([
                        'id' => $this->request->input('collect'),
                        'order_package_id' => $package->id
                    ])->first();
                    if($collect) {
                        $collect->status = $this->request->input('status');
                        $collect->save();
                        switch($collect->status) {
                            case 1: {
                                    UtilsService::sendPushNotification([
                                    'user'    => $order->customer_id,
                                    'title'   => 'Retiro de paquete #'.$collect->order_package_id,
                                    'message' => "El paquete indicado, con -$collect->name- esta en proceso de retiro.",
                                    'type'    => 1
                                ]);break;}
                            case 2: {
                                    UtilsService::sendPushNotification([
                                    'user'    => $order->customer_id,
                                    'title'   => 'Retiro de paquete #'.$collect->order_package_id." completado.",
                                    'message' => "El paquete indicado, con -$collect->name- ha sido retirado con exito, se le notificará cuando el estado sea actualizado.",
                                    'type'    => 1
                                ]);break;}
                            case 3: {
                                    $title   = "";
                                    $message = "";
                                    switch($this->request->input('type')) {
                                        case 1:{
                                            $title = "Retiro de orden #$order->id ha sido cancelado";
                                            $message = 'No ha sido posible localizar la dirección de entrega indicada en los datos de retiro.';
                                            break;
                                        }
                                        case 2:{
                                            $title = "Retiro de orden #$order->id ha sido cancelado";
                                            $message = 'No ha sido posible localizar a la persona indicada en los datos de retiro.';
                                            break;
                                        }
                                        case 3:{
                                            $title = "Retiro de orden #$order->id ha sido cancelado";
                                            $message = 'La persona indicada en los datos ha rechazado el retiro.';
                                            break;
                                        }
                                        case 4:{
                                            $title = "Retiro de orden #$order->id ha sido cancelado";
                                            $message = 'El retiro ha sido cancelado por el repartidor, por favor para mayor información contacte a soporte.';
                                            break;
                                        }
                                        default :{
                                            $title = "Retiro de orden #$order->id ha sido cancelado";
                                            $message = 'El retiro de su orden ha sido cancelada, si tiene alguna duda al respecto por favor contacte a soporte.';
                                        }
                                    }
                                    UtilsService::sendPushNotification([
                                        'user'    => $order->customer_id,
                                        'title'   => $title,
                                        'message' => $message,
                                        'type'    => 1
                                    ]);break;}
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'=>false,
                            'message'=>'Estado actualizado correctamente.'
                        ]);
                    } else{
                        return $this->errorResponse([
                            'err'=>true,
                            'message'=>'La orden no existe, por favor verifique si ha sido cancelda.'
                        ]);
                    }
                } else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no existe, por favor verifique si ha sido cancelda.'
                    ]);
                }
            } else{
                return $this->errorResponse([
                    'err'=>true,
                    'message'=>'El paquete no existe, por favor verifique si ha sido canceldo anteriormente o contacte a soporte.'
                ]);
            }
        } catch(\Exception $e){
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'No ha sido posible actualizar el estado del paquete.'
                ],500);
        }
    }

    function packages_deliveries_status() {
        try {
            DB::beginTransaction();
            $package = OrderPackage::find($this->request->input('package'));
            if($package) {
                $order = Order::where([
                    'id' => $package->order_id,
                    'dealer_id' => $this->request->auth->id])->first();
                if($order) {
                    $delivery = OrderPackageDelivery::where([
                        'id' => $this->request->input('delivery'),
                        'order_package_id' => $package->id
                    ])->first();
                    if($delivery) {
                        $delivery->status = $this->request->input('status');
                        $delivery->save();
                        switch($delivery->status) {
                            case 1: {
                                    UtilsService::sendPushNotification([
                                    'user'    => $order->customer_id,
                                    'title'   => 'Entrega de paquete #'.$delivery->order_package_id,
                                    'message' => "El paquete indicado, con -$delivery->name- esta en proceso de retiro.",
                                    'type'    => 1
                                ]);break;}
                            case 2: {
                                    UtilsService::sendPushNotification([
                                    'user'    => $order->customer_id,
                                    'title'   => 'Entrega de paquete #'.$delivery->order_package_id." completado.",
                                    'message' => "El paquete indicado, con -$delivery->name- ha sido entregado con exito.",
                                    'type'    => 1
                                ]);break;}
                            case 3: {
                                    $title   = "";
                                    $message = "";
                                    switch($this->request->input('type')) {
                                        case 1:{
                                            $title = "Entrega de orden #$order->id ha sido cancelada";
                                            $message = 'No ha sido posible localizar la dirección de entrega indicada en los datos de entrega.';
                                            break;
                                        }
                                        case 2:{
                                            $title = "Entrega de orden #$order->id ha sido cancelada";
                                            $message = 'No ha sido posible localizar a la persona indicada en los datos de entrega.';
                                            break;
                                        }
                                       case 3:{
                                            $title = "Entrega de orden #$order->id ha sido cancelada";
                                            $message = 'La persona indicada en los datos ha rechazado el retiro.';
                                            break;
                                        }
                                        case 4:{
                                            $title = "Entrega de orden #$order->id ha sido cancelada";
                                            $message = 'El retiro ha sido cancelado por el repartidor, por favor para mayor información contacte a soporte.';
                                            break;
                                        }
                                        default :{
                                            $title = "Entrega de orden #$order->id ha sido cancelada";
                                            $message = 'El retiro de su orden ha sido cancelada, si tiene alguna duda al respecto por favor contacte a soporte.';
                                        }
                                    }
                                    UtilsService::sendPushNotification([
                                        'user'    => $order->customer_id,
                                        'title'   => $title,
                                        'message' => $message,
                                        'type'    => 1
                                    ]);break;}
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'=>false,
                            'message'=>'Estado actualizado correctamente.'
                        ]);
                    } else{
                        return $this->errorResponse([
                            'err'=>true,
                            'message'=>'La orden no existe, por favor verifique si ha sido cancelda.'
                        ]);
                    }
                } else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no existe, por favor verifique si ha sido cancelda.'
                    ]);
                }
            } else{
                return $this->errorResponse([
                    'err'=>true,
                    'message'=>'El paquete no existe, por favor verifique si ha sido canceldo anteriormente o contacte a soporte.'
                ]);
            }
        } catch(\Exception $e){
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=> 'No ha sido posible actualizar el estado del paquete.'
                ],500);
        }
    }

    function delivered($order) {
        try{
            DB::beginTransaction();
            if($order = Order::find($order)) {
                if($order->dealer_id == $this->request->auth->id) {
                    if($order->status == 3) {
                        $order->status = 4;
                        $order->save();
                        if($order->client_id) {
                            UtilsService::sendPushOrderCustomer($order);
                        }
                        DB::commit();
                        return $this->successResponse([
                            'err'       => false,
                            'message'   => 'Entrega finalizada correctamente.'
                        ]);
                    }else{
                     return $this->errorResponse([
                        'err'=>true,
                        'message'=>'No ha sido posible finalizar entrega.'
                    ]);
                    }
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>'La orden no le ha sido asignada o fue removida'
                    ]);
                }
            }else{
                return $this->errorResponse([
                        'err'=>true,
                        'message'=>'Verifique si aun existe la orden de envio.'
                    ]);
            }
        }catch(\Exception $e){
            DB::rollBack();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=>'No ha sido posible finalizar entrega.'
                ],500);
        }
    }


}
