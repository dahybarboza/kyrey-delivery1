<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Dealer;
use App\Client;
use App\Customer;
use Illuminate\Support\Facades\DB;
use App\Services\UtilsService;
class AuthController extends Controller
{
    var $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    function create_account_client() {
        try {
                DB::beginTransaction();
                /*
                * We dont use Validator function because we need use Transaction for handle this signup
                * with observers create data on firebase and on error automaticaly rollback including on observer
                */
                //**Check if user account no existe whit this email.
                $data = $this->request->all();
                $data['status'] = 1;//Default enable dealer by signin app
                if($userEmail = UtilsService::getUserByEmailLocalDB($data['email'])  || $userPhone = UtilsService::getUserByPhoneLocalDB($data['phone'])) {
                    if($userEmail) {
                        return $this->errorResponse([
                            'err' => true,
                            'message' => 'Ya existe una cuenta registrada con el mismo numero de email'
                        ],401);
                    } else {
                        return $this->errorResponse([
                            'err' => true,
                            'message' => 'Ya existe una cuenta registrada con el mismo numero de telefono'
                        ],401);
                    }

                }
                if(!Customer::create($data)) {
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'No ha sido posible registrar cuenta'
                    ]);
                }
                UtilsService::sendEmailVerificationClientLinkFirebase($data['email']);
                DB::commit();
                return $this->successResponse([
                        'err' => false,
                        'message' => 'Cuenta creada correctamente, por favor ingrese a su correo para confirmar su registro.'
                    ]);
        } catch (\Exception $e) {
            echo $e->getMessage();
            return $this->errorResponse([
                        'err'=>true,
                        'message'=> 'No ha sido posible registrar cuenta'
                    ]);
            DB::rollBack();
        }
    }
}
