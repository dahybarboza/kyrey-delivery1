<?php

namespace App\Http\Controllers\Api\Client;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Services\FireStorageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Services\UtilsService;
class AccountController extends Controller
{
    var $folder = "administrator.clients";
    var $request;
    var $model;
    var $filePath = "assets/data/clients/";
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Customer();
    }

    public function index(){
        return $this->successResponse([
            'err' => false,
            'message' => 'Acceso restringido'
        ]);
    }

    public function update_account()
    {
        try {
            DB::beginTransaction();
            $client  = $this->model->find($this->request->auth->id);
            if($client) {
            $rules = [
                'name'              =>  'required|max:255',
                'email'             =>  'required|max:255',
                'phone'             =>  'required|max:15'
            ];
            $data = $this->request->all();
            if(!$this->model->where('phone',$data['phone'])->where('id','!=',$client->id)->first()) {
                if(!$this->model->where('email',$data['email'])->where('id','!=',$client->id)->first()){
                    if($this->validate($this->request, $rules)) {
                        //Check password id empty
                        unset($data['password']);
                        unset($data['photo']);
                        $client->fill($data);
                        //Check is any are modified
                        if($client->save()) {
                            $picture = $this->request->input('photo');
                            if($picture) {
                                $fileDelete = $client->photo_firebase_uid;
                                $image = str_replace('data:image/jpeg;base64,', '', $this->request->input('photo'));
                                $image = str_replace(' ', '+', $image);
                                $filename = Str::random(16).'.jpg';
                                $fileFullPath =  $this->filePath.$filename;
                                Storage::disk(env('APP_DEFAULT_STORAGE'))->put($fileFullPath, base64_decode($image));
                                $fileUploaded = FireStorageService::uploadWithReplaceAndDeleteLocalFile('clients',$filename,$fileFullPath,$fileDelete);
                                if($fileUploaded) {
                                    $client->photo = $fileUploaded->src;
                                    $client->photo_firebase_uid = $fileUploaded->id;
                                    $client->save();
                                } else {
                                    return $this->errorResponse([
                                        'err'=>false,
                                        'message'=> 'No ha sido posible cargar imagen'
                                    ]);
                                }
                            }
                            if($this->request->input('password')!="") {
                                UtilsService::updateUserPasswordFirebase($this->request->auth->email,$this->request->input('password'));
                            }
                            DB::commit();
                            return $this->successResponse([
                                'err'=>false,
                                'message'=> 'Datos actualizados correctamente.'
                            ]);
                        }else {
                            return $this->errorResponse([
                                'err'=>false,
                                'message'=> 'No ha sido posible actualizar sus datos, por favor intente dentro de un momento más.'
                            ]);
                        }
                    }else{
                        return $this->errorResponse([
                            'err'=>true,
                            'message'=> 'No es posible actualizar, faltan datos obligatorios'
                        ],401);
                    }
                }else {
                    return $this->errorResponse([
                        'err'=>false,
                        'message'=> 'El correo ya se encuentra en uso, por favor utilize otro.'
                    ]);
                }
            }else{
                return $this->errorResponse([
                    'err'=>false,
                    'message'=> 'El teléfono ya se encuentra en uso, por favor utilize otro.'
                ]);
            }
        }else{
            return $this->errorResponse([
                'err'=>true,
                'message'=> 'La cuenta a acualizar no existe.'
            ],404);
        }
        } catch(\Exception $e) {
            echo $e->getMessage();
            DB::rollBack();
            return $this->errorResponse([
                'err'       => true,
                'message'   => 'No ha sido posible actualizar sus datos, por favor intente dentro de un momento más.'
            ]);
        }
    }

    public function destroy($item)
    {
        $client = $this->model->find($item);
        if($client) {
                if($client->delete()) {
                    return $this->successResponse([
                        'err'=>false,
                        'message'=>__('messages.delete_successfull')
                    ]);
                }else {
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>__('messages.delete_fail')
                    ]);
                }
        }else {
            return $this->errorResponse([
                'err'=>true,
                'message'=>__('messages.delete_fail')
            ],404);
        }
    }

    function update_city() {
        try{
            $client = $this->model->find($this->request->auth->id);
            $data = $this->request->all();
            if($client) {
                if($client->update([
                    'state_id'=> $data['state_id'],
                    'city_id' => $data['city_id']
                ])) {
                    return $this->successResponse([
                        'err'=>false,
                        'message'=>__('messages.update_success')
                    ]);
                }else{
                    return $this->errorResponse([
                        'err'=>true,
                        'message'=>__('messages.update_fail')
                    ]);
                }
            }else {
                return $this->errorResponse([
                    'err'=>true,
                    'message'=>__('messages.update_fail')
                ]);
            }
        } catch(\Exception $e){
            echo $e->getMessage();
            return $this->errorResponse([
                    'err'=>true,
                    'message'=>__('messages.update_fail')
                ]);
        }

    }
}
