<?php
namespace App\Http\Controllers\Api\Client;

use App\Order;
use App\Http\Controllers\Controller;
use App\OrderPackage;
use App\OrderPackageCollect;
use App\OrderPackageDelivery;
use App\Quote;
use App\ZoneRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Services\UtilsService;
use Illuminate\Support\Facades\Storage;
use App\User;
use Illuminate\Support\Str;
use App\Services\FireStorageService;
use App\Invoice;
class OrdersController extends Controller {
    var $request;
    var $model;
    var $filePath = "assets/data/quotations/";
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new Order();
    }

    public function index() {
         return $this->errorResponse([
             'message' => 'Acción no permitida'
         ], 401);
     }

    public function create() {
        try {
            DB::beginTransaction();
            $data = $this->request->all();
            $order = new Order();
            //Types: 1 -> Express, 2 -> From Quotation
            $order->type            = $this->request->input('type')!=null?$this->request->input('type'):1;
            $order->customer_id     = $this->request->auth->id;
            $order->customer_name   = $this->request->auth->name;
            $order->customer_phone  = $this->request->auth->phone;
            $order->customer_email  = $this->request->auth->email;
            $order->payment_method  = $this->request->invoice['payment_method'];
            //Default Express Inmediato
            $order->service         = 1;
            $order->date = Carbon::now();
            if($order->type ==1) {
                $hourNow = date('H:i');
                if($hourNow <= strtotime('10:30')){
                    $order->round = 1;
                    // out 11
                } else if($hourNow >= strtotime('10:31') && $hourNow <= strtotime('13:30')) {
                    $order->round = 2;
                    // out 14
                } else if($hourNow >= strtotime('13:31') && $hourNow <= strtotime('15:30')) {
                    // out 16
                    $order->round = 3;
                } else if($hourNow >= strtotime('15:31')) {
                    $order->round   = 1;
                    $order->date    = Carbon::now()->addDay(1)->format('d/m/y');
                }
            } else if($data['type']==2) {
                $order->round = $data['round'];
                $order->date = Carbon::parse($data['date']);
                //echo json_encode($order->date);
            } else {
                $order->service = 2;//Courier
            }
            //Default Express - Inmediato
            if($order->save()) {
                foreach($data['packages'] as $package) {
                    $order->quantity = $order->quantity + $package['quantity'];
                    $order->total = $order->total + $package['total'];
                    if($package['type']=='L4') {
                        $packageDB = new OrderPackage();
                        $packageDB->fill($package);
                        $packageDB->order_id = $order->id;
                        if($packageDB->save()) {
                            foreach($package['collect'] as $collect) {
                                $collectDB = new OrderPackageCollect();
                                $collectDB->fill($collect);
                                $collectDB->order_package_id = $packageDB->id;
                                if($collectDB->save()) {
                                    foreach($collect['delivery'] as $delivery) {
                                            $deliveryDB = new OrderPackageDelivery();
                                            $deliveryDB->fill($delivery);
                                            $deliveryDB->order_package_id = $packageDB->id;
                                            $deliveryDB->order_package_collect_id = $collectDB->id;
                                            $deliveryDB->save();
                                    }
                                }
                            }
                        }
                    } else {
                        $packageDB = new OrderPackage();
                        $packageDB->fill($package);
                        $packageDB->order_id = $order->id;
                        if($packageDB->save()) {
                            //Save all Collects
                            foreach($package['collect'] as $collect) {
                                $collectDB = new OrderPackageCollect();
                                $collectDB->fill($collect);
                                $collectDB->order_package_id = $packageDB->id;
                                $collectDB->save();
                            }
                            //Save all Deliveries
                            foreach($package['delivery'] as $delivery) {
                                $deliveryDB = new OrderPackageDelivery();
                                $deliveryDB->fill($delivery);
                                if($packageDB->type!='L2') {
                                    $deliveryDB->quantity = $packageDB->quantity;
                                }
                                $deliveryDB->order_package_id = $packageDB->id;
                                $deliveryDB->save();
                            }
                        }
                    }
                }

                $order->save();
                UtilsService::sendPushNotification([
                    'user'      => User::first()->id,
                    'type'      => 2, //Enterprise,
                    'title'     => 'Nueva orden',
                    'message'   => 'La orden puede localizarla en el apartado de ordenes con folio #'.$order->id
                ]);
                //Requiere Factura?
                if($this->request->input('invoice')) {
                    $data = $this->request->input('invoice');
                    $data['phone']          = $order->customer->phone;
                    $data['email']          = $order->customer->email;
                    $data['total']          = $order->total;
                    $data['order_id']       = $order->id;
                    $data['payment_method'] = $order->payment_method;
                    Invoice::create($data);
                }
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => "Orden enviada con exito!."
                ]);
            }
        } catch(\Exception $e) {
            echo $e->getLine().$e->getMessage();
            return $this->errorResponse([
                'err' => true,
                'message' => 'No ha sido posible procesar su solicitud, por favor intente nuevamente dentro de un momento más.'
            ]);
        }
    }

    public function destroy($order,$uid) {
        try {
            DB::beginTransaction();
            $order = Order::where([
                'customer_id' => $this->request->auth->id,
                'id' => $order,
                'firebase_uid' => $uid
            ])->first();
                if($order) {
                    if($order->status == 0) {
                        $packages = $order->packages;
                        foreach($packages as $package) {
                            $package = OrderPackage::find($package->id);
                            $package->delete();
                        }
                        $order->delete();
                        DB::commit();
                        return $this->successResponse([
                            'err' => false,
                            'message' => 'Orden eliminada con exito'
                        ]);
                    } else {
                        return $this->errorResponse([
                            'err' => false,
                            'message' => 'La orden ya ha sido aceptada, si desea concelarla por favor contacte con la sucursal asignada.'
                        ]);
                    }
                } else {
                    $firestore     = app('firebase.firestore');
                    $firestore     = $firestore->database();
                    $doc = $firestore->collection('orders')->document($uid);
                    $doc->delete();
                    return $this->successResponse([
                        'err' => false,
                        'message' => 'Orden eliminada con exito'
                    ]);
                    return $this->errorResponse([
                        'err' => false,
                        'message' => 'La orden no existe o ha sido eliminada del sistema.'
                    ]);
                }
        } catch(\Exception $e) {
            return $this->errorResponse([
                'err' => true,
                'message' => 'No ha sido posible eliminar orden, por favor intente nuevamente.'
            ]);
        }
    }

    public function quotation() {
        try {
            DB::beginTransaction();
            $data = $this->request->input('data');
            $quote = new Quote();
            $quote->customer_id = $this->request->auth->id;
            $quote->details = $data['note'];//Note fron customer
            $document = $data['file'];
            $filename = Str::random(16).'.xlsx';
            $fileFullPath =  $this->filePath.$filename;
            Storage::disk(env('APP_DEFAULT_STORAGE'))->put($fileFullPath, base64_decode($document));
            $fileUploaded = FireStorageService::uploadWithDeleteLocalFile('quotations',$filename,$fileFullPath);
            if($fileUploaded) {
                $quote->document = $fileUploaded->src;
                $quote->firebase_document_uid = $fileUploaded->id;
                $quote->save();
                UtilsService::sendPushNotification([
                    'user'      => User::first()->id,
                    'type'      => 2, //Enterprise,
                    'title'     => 'Nuva solicitud de cotización',
                    'message'   => 'La solicitud de cotizacion puede localizarla en el apartado de COTIZACIONES con folio #'.$quote->id
                ]);
                DB::commit();
                return $this->successResponse([
                    'err' => false,
                    'message' => 'Solicitud procesada correctamente, en breve recibirá información sobre su orden cotización.'
                ]);
            } else {
                return $this->errorResponse([
                    'err'=>false,
                    'message'=> 'No ha sido posible cargar imagen'
                ]);
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
            return $this->errorResponse([
                'err'       => true,
                'message'   => 'No ha sido posible procesar su solicitud, por favor intente dentro de un momento más.'
            ]);
        }
    }

    public function rate() {
      try {
         DB::beginTransaction();
         $data = $this->request->all();
         $itemData = $this->model->find($data['order']);
         if($itemData){
           if($itemData->fill([
               'rated' => $data['rated'],
               'rated_service' => $data['service'],
               'rated_time' => $data['time'],
               'rated_health' => $data['health'],
               'rated_friendly' => $data['friendly'],
               'rated_comment' => $data['comment']
           ])->isDirty()) {
              $itemData->save();
              DB::commit();
              return $this->successResponse([
                     'err' => false,
                     'message' => 'Datos actualizados correctamente.'
              ]);
           } else {
              return $this->successResponse([
                     'err' => false,
                     'message' => 'Ningún dato ha cambiado.'
              ]);
           }
         } else {
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible registrar orden, por favor verifique su información e intente nuevamente.'
          ]);
         }
      }
       catch(\Exception $e){
          echo $e->getMessage();
          DB::rollback();
          return $this->errorResponse([
            'err' =>true,
            'message' => 'No ha sido posible registrar orden, por favor verifique su información e intente nuevamente.'
          ]);
       }
    }
}
