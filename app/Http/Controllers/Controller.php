<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Traits\ApiResponser;
use Twilio\Rest\Client;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ApiResponser;
    function sendPush($daTags=[], $title, $description)
    {
            // Select channel for OneSignal
            $app="";
            $key="";
            switch($daTags[1]['value']) {
                case 1: { // Customers
                    $app = env('ONESIGNAL_APP_CUSTOMERS_ID');
                    $key = env('ONESIGNAL_API_CUSTOMERS_KEY');
                break;
                }
                case 2: { // Admin/Enterprise
                    $app = env('ONESIGNAL_APP_ID');
                    $key = env('ONESIGNAL_API_KEY');
                break;
                }
                case 3: { // Dealers
                    $app = env('ONESIGNAL_APP_DEALERS_ID');
                    $key = env('ONESIGNAL_API_DEALERS_KEY');
                break;
                }
            }
            $content = ["en" => $description];
            $head 	 = ["en" => $title];
            $fields = array(
            'app_id' => $app,
            'included_segments' => array('All'),
            'filters' => $daTags,
            'data' => array("foo" => "bar"),
            'contents' => $content,
            'headings' => $head,
            );
            $fields = json_encode($fields);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, env('ONESIGNAL_API'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
            'Authorization: Basic '.$key));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            $response = curl_exec($ch);
            curl_close($ch);

        return $response;
    }

    function sendSMS($phone,$message) {
        $client = new Client(env('TWILIO_KEY'),env('TWILIO_TOKEN'));
        $client->messages->create(
            str_replace(' ','',$phone),//Clear empty spaces
            [
                'from' => env('TWILIO_PHONE'),
                'body' => $message
            ]
        );
    }
}
