<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller{
    var $request;
    public $folder  = "home.";

    public function __construct(Request $request){
        $this->request = $request;
    }

    function index(Request $request) {
        return View($this->folder.'index',[
            'jsControllers'=>[
                0=>'LoginController.js'
            ]
        ]);
    }

    public function login() {
        $data = $this->request->all();
        if($user = User::where('email',$data['email'])->first()) {
            if(Hash::check($data['password'], $user->password))
            {
                auth()->attempt([
                    'email'     =>$data['email'],
                    'password'  =>$data['password']
                    ]);
                return $this->successResponse([
                            'err'       =>  false,
                            'message'   =>  'Acceso correcto, redirigiendo...'
                        ]);
            } else {
            return $this->errorResponse([
                    'err'=>true,
                    'message'=>'Datos de acceso incorrectos'
                ]);
            }
        } else {
            return $this->errorResponse([
                'err'       =>  true,
                'message'   =>  'Datos de acceso incorrectos'
            ]);    
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->to('/');
    }
}
