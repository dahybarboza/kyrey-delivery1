<?php
namespace App\Services;
use App\User;
use App\Manager;
use App\Administrator;
use App\Customer;
use App\Dealer;
use App\Http\Controllers\Controller as Ctrl;
use Illuminate\Support\Facades\Storage;

class UtilsService {
    /**
     * Check if email already exist on firebase service auth
     *
     * @param [string] $email
     * @return void
     */
    static function getUserByEmailFirebase($email) {
        try {
            $authFirebase = app('firebase.auth');
            $user = $authFirebase->getUserByEmail($email);
            return $user!=null;
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            return false;
        }
    }

    /**
     * [getUserStatusVerifiedFirebase] function is used to check id an account was verified
     *
     * @param [string] $email
     * @return void
     */
    static function getUserStatusVerifiedFirebase($email) {
        try {
            $auth = app('firebase.auth');
            $user = $auth->getUserByEmail($email);
            return $user->emailVerified;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * [authUserFirebase] function is used for login on web system by firebase service
     *
     * @param [string] $email
     * @param [string] $password
     * @return void
     */
    static function authUserFirebase($email,$password) {
        try {
            $auth = app('firebase.auth');
            $user = $auth->signInWithEmailAndPassword($email, $password);
            return $user!=null;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * [sendEmailVerificationLinkFirebase] function is used for send email by serive firebase to confirm email registration
     *
     * @param [string] $email
     * @return void
     */
    static function sendEmailVerificationLinkFirebase($email) {
        try {
            $auth = app('firebase.auth');
            $url =  env('APP_URL').env('home').'/auth/action?mode=verifyEmail&status=1';
            $auth->sendEmailVerificationLink($email,['continueUrl' => $url]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * [sendEmailVerificationLinkFirebase] function is used for send email by serive firebase to confirm email registration
     *
     * @param [string] $email
     * @return void
     */
    static function sendEmailVerificationClientLinkFirebase($email) {
        try {
            $auth = app('firebase.auth');
            $user = $auth->sendEmailVerificationLink($email);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * [sendPasswordResetLinkFirebase] send email by  firebase service to reset email
     *
     * @param [string] $email
     * @return void
     */
    static function sendPasswordResetLinkFirebase($email) {
        try {
            $authFirebase = app('firebase.auth');
            $url =  env('APP_URL').env('home');
            $user = $authFirebase->sendPasswordResetLink($email,['continueUrl' => $url]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * [updateUserPasswordFirebase] is used for update password, this function return
     * true on successfull or false when fail
     * @param [string] $email
     * @param [string] $password
     * @return void
     */
    static function updateUserPasswordFirebase($email,$password) {
        try {
            $authFirebase = app('firebase.auth');
            $user = $authFirebase->getUserByEmail($email);
            $authFirebase->changeUserPassword($user->uid,$password);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * [getUserByPhoneLocalDB] try get an objeto from local database by $phone
     * this return objet or false when dont find anything
     * @param [string] $phone
     * @return object,boolean
     */
    static function getUserByEmailLocalDB($email) {
        try {
            $user = User::where('email',$email)->first();
            if($user) {
                return $user;
            }
            $dealer = Dealer::where('email',$email)->first();
            if($dealer) {
                return $dealer;
            }
            $manager = Customer::where('email',$email)->first();
            if($manager) {
                return $manager;
            }
            return false;
        } catch(\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * [getUserByPhoneLocalDB] try get an objeto from local database by $phone
     * this return objet or false when dont find anything
     * @param [string] $phone
     * @return object,boolean
     */
    static function getUserByPhoneLocalDB($phone) {
        try {
            $user = User::where('phone',$phone)->first();
            if($user) {
                return $user;
            }
            $dealer = Dealer::where('phone',$phone)->first();
            if($dealer) {
                return $dealer;
            }
            $manager = Customer::where('phone',$phone)->first();
            if($manager) {
                return $manager;
            }
            return false;
        } catch(\Exception $e) {
            return false;
        }
    }

    /**
     * [getUserStatusLocalDB], recibe a string email an try find on local database, return status
     *
     * @param [string] $email
     * @return boolean
     */
    static function getUserStatusLocalDB($email) {
        try {
            $user = User::where('email',$email)->first();
            if($user) {
                return $user->status;
            }
            $dealer = Dealer::where('email',$email)->first();
            if($dealer) {
                return $dealer->status;
            }
            $manager = Customer::where('email',$email)->first();
            if($manager) {
                return $manager->status;
            }
            return false;
        } catch(\Exception $e) {
            return false;
        }
    }

    /**
     * [sendPushOrderClient] This function receives an objet type Order to send push notification
     * depending on the status of order
     * @param [Order] $order
     * @param [string] $title
     * @param [string] $message
     * @return void
     */
    static function sendPushOrderCustomer($order,$title="",$message="") {
        switch($order->status*1) {
            case 1:{
                if($title==""){$title   = "Orden #$order->id ha sido aceptada";}
                if($message==""){$message = "Su orden ha sido aceptada, en breve recibira notificaciones del estado de su orden.";}
                break;}
            case 2: {
                if($title==""){$title      = "Orden #$order->id ha sido asignada";}
                if($message==""){$message    = 'Su orden ha sido asignada a un repartidor, en breve recibira mas notificaciones del estado de su orden.';}
                break;}
            case 3: {
                if($title==""){$title   = "Realizando retiros de su orden #$order->id";}
                if($message==""){$message = "Su orden se encuentra en turno de retiros, puede dar seguimiento en la lista órdenes.";}
                break;}
            case 4: {
                if($title==""){$title   = 'Retiros finalizados de su orden #'.$order->id.".";}
                if($message==""){$message = 'Los retiros asignados a su orden han sido realizados, se le notificará cuando el estado sea actualizado';}
                    break;}
            case 5: {
                if($title==""){$title   = 'Orden #'.$order->id." en turno de entrega.";}
                if($message==""){$message = 'Su orden se encuentra en turno de entrega, se le notificará cuando el estado sea actualizado';}
                break;}
            case 6: {
                if($title==""){$title   = 'Entrega finalizada de su orden #'.$order->id.".";}
                if($message==""){$message = 'La entrega de su orden ha finalizado, si tiene dudas por favor contacte a soporte.';}
                    break;}
            case 7: {
                if($title==""){$title     = 'Orden #'.$order->id." ha sido cancelada.";}
                if($message==""){$message = 'Su orden ha sido cancelado si tiene dudas al respecto, por favor contacte al repartidor o soporte.';}
                    break;}
        }
        if($order->customer_id) {
            $data = [
                'user'    => $order->customer_id,
                'title'   => $title,
                'message' => $message,
                'type'    => 1
            ];
            self::sendPushNotification($data);
        }
    }

    /**
     * [sendPushOrderDealer] This function receives an objet type Order to send push notification
     * depending on the status of order
     * @param [Order] $order
     * @param [string] $title
     * @param [string] $message
     * @return void
     */
    static function sendPushOrderDealer($order,$title="",$message="") {
        switch($order->status) {
            case -1: {
                if($title==""){$title = 'Orden removida';}
                if($message==""){$message='La orden con folio #'.$order->id.' ha sido removida de su lista de entregas.';}
                break;
            }
            case 2: {
                if($title==""){$title = 'Nueva orden asignada';}
                if($message==""){$message='Se le ha asignado una nueva orden con folio #'.$order->id;}
                break;}
            case 7: {
                if($title==""){$title = 'Orden cancelada';}
                if($message==""){$message='La orden con folio #'.$order->id.' ha sido cancelada';}
                break;
            }
        }
        if($order->dealer_id) {
            $data = [
                'user'    => $order->dealer_id,
                'title'   => $title,
                'message' => $message,
                'type'    => 3
            ];
            self::sendPushNotification($data);
        }
    }

    static function sendPushBranchOffice($order,$title="",$message="") {
        switch($order->status) {
            case -1: {
                if($title==""){$title = __('messages.branchoffice_order_was_canceled',['id'=>$order->id]);}
                if($message){$message = __('messages.branchoffice_order_was_canceled_message');}
                break;
            }
            case 0: {
                if($title==""){$title = __('messages.branchoffice_order_new',['id'=>$order->id]);}
                if($message){$message = __('messages.branchoffice_order_new_message');}
                break;
            }
        }
        if($order->dealer_id) {
            $data = [
                'user'    => $order->branchoffice_id,
                'title'   => $title,
                'message' => $message,
                'type'    => 4
            ];
            self::sendPushNotification($data);
        }
    }

    static function sendPushNotificationOrSMS($order) {
        if($order->customer_id) {
            $data = [];
            switch($order->status) {
                case 1: {
                    $data = [
                        'user'    => $order->customer_id,
                        'title'   => 'Orden en turno de entrega',
                        'message' => "Su orden con folio #$order->id se encuentra en turno de entrega; Puedes descargar el aplicativo en Play Store para hacer el trackeo en tiempo real de tus servicios.",
                        'type'    => 1
                    ];
                    self::sendPushNotification($data);
                    break;
                }
                case 2: {
                    $data = [
                        'user'    => $order->customer_id,
                        'title'   => 'Orden entregada',
                        'message' => "Su orden con folio #$order->id ha sido entregada; Puedes descargar el aplicativo en Play Store para hacer el trackeo en tiempo real de tus servicios.",
                        'type'    => 1
                    ];
                    self::sendPushNotification($data);
                    break;
                }
                case 3: {
                    $data = [
                        'user'    => $order->customer_id,
                        'title'   => 'Orden entregada',
                        'message' => "Su orden con folio #$order->id ha sido cancelada; Puedes descargar el aplicativo en Play Store para hacer el trackeo en tiempo real de tus servicios.",
                        'type'    => 1
                    ];
                    self::sendPushNotification($data);
                    break;
                }
            }

        } else {
            $ctrl = new Ctrl();
            switch($order->status) {
                case 1: {
                    $ctrl->sendSMS($order->phone, "Orden folio #$order->id en turno de entrega; Descarga nuestro aplicativo en Play Store y realiza seguimiento en tiempo real");
                    break;
                }
                case 2: {
                    $ctrl->sendSMS($order->phone, "Orden folio #$order->id ha sido entregada; Descarga nuestro aplicativo en Play Store y realiza seguimiento en tiempo real");
                    break;
                }
                case 3: {
                    $ctrl->sendSMS($order->phone, "Orden folio #$order->id ha sido cancelada; Descarga nuestro aplicativo en Play Store y realiza seguimiento en tiempo real");
                    break;
                }
            }
        }
    }

    static function sendPushNotification($data) {
        $filters = [
            0 => [
                'field'     => 'tag',
                'key'       => 'user_id',
                'relation'  => '=',
                'value'     => $data['user']
            ],
            1 => [
                'field'     => 'tag',
                'key'       => 'type',
                'relation'  => '=',
                'value'     => $data['type']
                ]
        ];
        $ctrl = new Ctrl();
        $ctrl->sendPush($filters, $data['title'],$data['message']);
    }

    static function deleteFile($path) {
        try {
            if(Storage::disk('public')->exists($path)){
                Storage::disk('public')->delete($path);
                return true;
            }
            return true;
         } catch(\Exception $e) {
            return false;
        }
    }

    static function is_base64_encoded($data)
    {
        if (base64_encode(base64_decode($data, true)) === $data && imagecreatefromstring(base64_decode($data))) {
            return true;
            }
        return false;
    }

    static function setLocationLang($lang="en") {
        session(['applocale' => $lang]);
    }
}
