<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table="deliveries";
    protected $fillable = [
        'branchoffice_id',
        'dealer_id',
        'customer_id',
        'firebase_uid',
        'name',
        'phone',
        'note',
        'address',
        'lat',
        'lng',
        'quantity',
        'price',
        'status'
    ];
}
