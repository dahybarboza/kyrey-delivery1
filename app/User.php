<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
class User extends Model implements AuthenticatableContract
{
    use Authenticatable;
    protected $fillable = [
        'lang',
        'type_id',
        'photo',
        'name',
        'enterprise',
        'phone',
        'email',
        'password',
        'status'
    ];
    protected $hidden = [
        'password',
        'token'
    ];
    protected $cascadeDeletes = [
        'branchoffices',
        'dealers',
        'managers',
        'categories',
        'items',
        'complements'
    ];
    protected $dates = [
        'deleted_at'
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
