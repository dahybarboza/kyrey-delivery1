<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneRate extends Model
{
    protected $table = "zones_rates";
    protected $fillable = [
        'zone_id',
        'zone_delivery',
        'price',
        'status'
    ];
    public function zone(){
        return $this->belongsTo('App\Zone','zone_id');
    }
    public function delivery(){
        return $this->belongsTo('App\Zone','zone_delivery');
    }
}
