<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DealerModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Dealer::observe(\App\Observers\DealerObserver::class);
    }
}
