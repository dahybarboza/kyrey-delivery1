<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ZoneModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Zone::observe(\App\Observers\ZoneObserver::class);
    }
}
