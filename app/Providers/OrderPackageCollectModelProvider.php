<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrderPackageCollectModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\OrderPackageCollect::observe(\App\Observers\OrderPackageCollectObserver::class);
    }
}
