<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ExpressFeesModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\ExpressFees::observe(\App\Observers\ExpressFeesObserver::class);
    }
}
