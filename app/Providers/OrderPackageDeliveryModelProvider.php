<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrderPackageDeliveryModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\OrderPackageDelivery::observe(\App\Observers\OrderPackageDeliveryObserver::class);
    }
}
