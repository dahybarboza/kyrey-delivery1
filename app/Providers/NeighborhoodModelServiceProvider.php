<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class NeighborhoodModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Neighborhood::observe(\App\Observers\NeighborhoodObserver::class);
    }
}
