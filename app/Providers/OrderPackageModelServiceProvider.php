<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrderPackageModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\OrderPackage::observe(\App\Observers\PackagesObserver::class);
    }
}