<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DeliveryModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Delivery::observe(\App\Observers\DeliveryObserver::class);
    }
}
