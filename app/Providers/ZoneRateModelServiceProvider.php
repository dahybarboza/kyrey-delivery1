<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ZoneRateModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\ZoneRate::observe(\App\Observers\ZoneRateObserver::class);
    }
}
