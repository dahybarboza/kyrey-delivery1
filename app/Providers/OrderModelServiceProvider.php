<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OrderModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Order::observe(\App\Observers\OrderObserver::class);
    }
}