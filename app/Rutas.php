<?php

namespace App;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Rutas extends Model{
    use SoftDeletes, CascadeSoftDeletes;
    protected $fillable = [
        'firebase_uid',
        'customer_id',
        'dealer_id',
        'branchoffice_id',
        'dia',
        'fecha',
        'hora_inicio_programado',
        'hora_fin_programado',
        'hora_inicio_real',
        'hora_fin_real',
        'lat',
        'lng',
        'address',
        'neighborhood',
        'en_rango',
        'observaciones',
        'status',
    ];

    protected $dates = [
        'deleted_at'
    ];


    public function customer() {
        return $this->belongsTo('App\Customer','customer_id','id');
    }

    public function branchoffice() {
        return $this->belongsTo('App\Branchoffice','branchoffice_id','id');
    }

    public function dealer() {
        return $this->belongsTo('App\Dealer','dealer_id','id');
    }


}
