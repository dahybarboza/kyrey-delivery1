<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branchoffice extends Model{
    protected $dates = [
        'deleted_at'
    ];
    protected $fillable = [
        'firebase_uid',
        'customer_id',
        'name',
        'phone',
        'address',
        'lat',
        'lng',
        'neighbohood',
        'status'
    ];

/*    public function dealers() {
        return $this->hasMany('App\Dealer');
    }*/


    public function customer(){
        return $this->belongsTo('App\Customer','customer_id');
    }
}
