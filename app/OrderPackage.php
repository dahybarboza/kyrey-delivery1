<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletes;
class OrderPackage extends Model
{
    use SoftDeletes, CascadeSoftDeletes;
    protected $table = 'orders_packages';
    protected $fillable = [
        'order_id',
        'firebase_uid',
        'order_id',
        'quantity',
        'distance',
        'type',
        'total',
        'status'
    ];
    protected $cascadeDeletes = [
        'collects',
        'deliveries'
    ];
    protected $dates = [
        'deleted_at'
    ];
    public function collects () {
        return $this->hasMany(OrderPackageCollect::class);
    }
    public function deliveries() {
        return $this->hasMany(OrderPackageDelivery::class);
    }
}
