<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressFees extends Model
{
    protected $table    = "express_fees";
    protected $fillable = [
        'local_price',
        'external_price',
        'external_price_km'
    ];
}
