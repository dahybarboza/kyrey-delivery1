<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix'=>'/', 'middleware'=>'request.https'],function ($router) {

    //Export
//    $router->get('/orders/exportAllOrders','OrdersController@exportAllOrders')->name('orders.export');

   // $router->get('/rates','OrdersController@rates');

    $router->get('/','HomeController@index')->name('home');
    $router->post('/login','HomeController@login');
    $router->get('/logout','HomeController@logout');
    $router->get('/login','HomeController@index')->name('login');
    $router->group(['prefix' => 'admin', 'namespace'=>'Admin', 'middleware' => 'auth'], function() use($router) {
           $router->group(['prefix' => 'home'], function() use($router) {
            $router->get('/','HomeController@index');
            $router->get('/resumen','HomeController@resumen');
            $router->get('/rate_service','HomeController@rate_service');
            $router->get('/profile','HomeController@profile');
            $router->post('/update','HomeController@update');
        });
        $router->group(['prefix' => 'service-quanlity'], function() use($router) {
            $router->get('/','QualityController@index');
            $router->get('/all','QualityController@all');
        });

        //Delivery
        $router->group(['prefix' => 'delivery'], function() use($router) {
            $router->get('/','DeliveryController@index');
            $router->get('/all','DeliveryController@all');
            $router->post('/store','DeliveryController@store');
            $router->post('/update/{delivery}','DeliveryController@update');
            $router->get('/destroy/{delivery}','DeliveryController@destroy');
            $router->get('/dealers/{branchoffice}','DeliveryController@dealers');
        });

        //BranchOffices
        $router->group(['prefix'=>'branchoffices'],function() use($router) {
            $router->get('/','BranchofficesController@index');
            $router->get('/all','BranchofficesController@all');
            $router->post('/store','BranchofficesController@store');
            $router->post('/update/{branchoffice}','BranchofficesController@update');
            $router->get('/destroy/{branchoffice}','BranchofficesController@destroy');
            $router->get('/cities/{state}','BranchofficesController@cities');
        });


        //Customers
        $router->group(['prefix'=>'customers'],function() use($router) {
            $router->get('/','CustomersController@index');
            $router->get('/all','CustomersController@all');
            $router->post('/store','CustomersController@store');
            $router->post('/update/{id}','CustomersController@update');
            $router->get('/destroy/{id}','CustomersController@destroy');
        });
        //Dealers
        $router->group(['prefix'=>'dealers'],function() use($router) {
            $router->get('/','DealersController@index');
            $router->get('/all','DealersController@all');
            $router->post('/store','DealersController@store');
            $router->post('/update/{id}','DealersController@update');
            $router->get('/destroy/{id}','DealersController@destroy');
        });

        //Orders
        $router->group(['prefix'=>'orders'],function() use($router) {
            $router->get('/','OrdersController@index');
            $router->get('/all','OrdersController@all');
            $router->get('/create','OrdersController@create');
            $router->post('/store','OrdersController@store');
            $router->get('/find/{order}','OrdersController@find');
            $router->get('/edit/{order}','OrdersController@edit');
            $router->post('/update/{id}','OrdersController@update');
            $router->get('/destroy/{id}','OrdersController@destroy');
            $router->get('/neighborhoods/{zone}','OrdersController@neighborhoods');
            $router->get('/zone_rates/{start}/{arrival}','OrdersController@zone_rates');
            $router->get('/dealers/{branchoffice}','OrdersController@dealers');
            $router->get('/fees','OrdersController@fees');
            $router->get('/rates','OrdersController@rates');

            $router->get('/exportAllOrders','OrdersController@exportAllOrders')->name('orders.export');
        });

        //Invoices
        $router->group(['prefix' => 'invoices'], function() use($router) {
            $router->get('/', 'InvoicesController@index');
            $router->get('/all','InvoicesController@all');
            $router->post('/update/{id}','InvoicesController@update');
            $router->get('/destroy/{id}','InvoicesController@destroy');
        });

        //Quotes
        $router->group(['prefix'=>'quotes'],function() use($router) {
            $router->get('/','QuotesController@index');
            $router->get('/all','QuotesController@all');
            $router->get('/find/{order}','QuotesController@find');
            $router->post('/store','QuotesController@store');
            $router->post('/update/{id}','QuotesController@update');
            $router->get('/destroy/{id}','QuotesController@destroy');
        });



        //Express-fees
        $router->group(['prefix'=>'express-fees'],function() use($router) {
            $router->get('/','ExpressFeesController@index');
            $router->get('/find/{id}','ExpressFeesController@find');
            $router->post('/update/{id}','ExpressFeesController@update');
        });

        //Zones
        $router->group(['prefix'=>'zones'],function() use($router) {
            $router->get('/','ZonesController@index');
            $router->get('/all','ZonesController@all');
            $router->post('/store','ZonesController@store');
            $router->post('/update/{zone}','ZonesController@update');
            $router->get('/destroy/{zone}','ZonesController@destroy');
            $router->group(['prefix' => 'rates'], function() use($router) {
                $router->get('/','RatesController@index');
                $router->get('/all/{zone}','RatesController@all');
                $router->post('/store','RatesController@store');
                $router->post('update/{rate}','RatesController@update');
                $router->get('/destroy/{rate}','RatesController@destroy');
                $router->get('/zones/{zone}','RatesController@zones');
                $router->get('/rate','RatesController@rate');
            });
        });

        # Gestión de Rutas
        $router->group(['prefix'=>'rutas'],function() use($router) {
            $router->get('/','RutasController@index');
            $router->get('/all','RutasController@all');
            $router->get('/create','RutasController@create');
            $router->get('/dealers/{branchoffice}','RutasController@dealers');
            $router->get('/branchoffices/{customers_id}','RutasController@branchoffices');
            $router->post('/store','RutasController@store');
            $router->get('/edit/{id}','RutasController@edit');
            $router->get('/find/{id}','RutasController@find');
            $router->post('/update/{id}','RutasController@update');
            $router->get('/destroy/{id}','RutasController@destroy');
//            $router->get('/neighborhoods/{zone}','OrdersController@neighborhoods');
//            $router->get('/zone_rates/{start}/{arrival}','OrdersController@zone_rates');

//            $router->get('/fees','OrdersController@fees');
//            $router->get('/rates','OrdersController@rates');
        });



    });
});
