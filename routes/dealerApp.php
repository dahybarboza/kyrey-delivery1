<?php
Route::group(['prefix'=>'dealer'],function() use($router) {
    $router->post('create_account','AuthController@create_account_dealer');
    $router->group(['namespace'=>'Dealer',],function()  use($router) {
        $router->post('updategps','AccountController@updategps');
        $router->group(['middleware'=>['auth.firebase','auth.firebaseDealer']],function() use($router) {
            $router->group(['prefix' => 'account'],function() use($router) {
                $router->post('available','AccountController@available');
            });
            $router->group(['prefix'=>'orders'],function() use($router) {
                    $router->get('detail/{order}','OrdersController@detail');
                    $router->post('status/{order}','OrdersController@status');
                    $router->get('delivered/{order}','OrdersController@delivered');
                    $router->post('cancel/{uid}','OrdersController@cancel');
                    //Packages Collect
                    $router->post('packages/status','OrdersController@packages_status');
                    $router->post('packages/collects/status','OrdersController@packages_collects_status');
                    $router->post('packages/collects/cancel','OrdersController@packages_collects_cancel');
                    $router->post('packages/deliveries/status','OrdersController@packages_deliveries_status');
                    $router->post('packages/deliveries/cancel','OrdersController@packages_deliveries_cancel');
            });
            $router->group(['prefix' => 'deliveries'], function() use ($router) {
                //Delivery
                $router->post('store/','DeliveryController@store');
                $router->post('status/{id}','DeliveryController@status');
            });
            $router->post('send_notification','GeneralController@send_notification');
        });
    });
});
