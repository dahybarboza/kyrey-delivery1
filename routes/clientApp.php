<?php
Route::group(['prefix'=>'client'],function() use($router){
    //for Create account is open
    $router->post('create_account','AuthController@create_account_client');
    //For next request we need Firebase Token valid
    $router->group(['middleware'=>['auth.firebase','auth.firebaseCustomer'],'namespace' => 'Client'],function() use($router) {
        //We check if token is for here
        $router->post('update_account','AccountController@update_account');
        //Orders
        $router->group(['prefix'=>'orders'],function() use($router) {
            $router->get('/','OrdersController@index');
            $router->get('find/{id}','OrdersController@find');
            $router->post('create','OrdersController@create');
            $router->get('destroy/{id}/{uid}','OrdersController@destroy');
            $router->post('quotation','OrdersController@quotation');
            $router->post('rate', 'OrdersController@rate');
        });
        //Payments
        $router->group(['prefix'=>'payments'],function() use($router) {
            $router->get('settings','PaymentsController@settings');
        });
        $router->post('send_notification','GeneralController@send_notification');
    });
});
