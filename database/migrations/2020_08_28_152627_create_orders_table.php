<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase_uid')->nullable();
            $table->unsignedBigInteger('branchoffice_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('dealer_id')->nullable();
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->string('customer_email')->nullable();
            $table->integer('service')->default(1)->comment('1 - Express, 2 - Currier');
            $table->integer('type')->default(1)->comment('1 - Inmediato, 2 - Programado');
            $table->date('date')->useCurrent();
            $table->integer('round')->default(1);
            $table->integer('quantity')->default(0);
            $table->unsignedBigInteger('quote_id')->nullable();
            $table->double('total')->default(0);
            $table->integer('payment_method')->default(0);
            $table->integer('status')->default(0);
            $table->integer('canceled_canceller')->nullable();
            $table->timestamp('canceled_date')->nullable();
            $table->boolean('rated')->default(false);
            $table->integer('rated_service')->default(0);
            $table->integer('rated_time')->default(0);
            $table->integer('rated_friendly')->default(0);
            $table->integer('rated_health')->default(0);
            $table->string('rated_comment')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('orders');
    }
}
