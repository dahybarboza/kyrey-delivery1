<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpressFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('express_fees', function (Blueprint $table) {
            $table->id();
            $table->string('firebase_uid')->nullable();
            $table->float('local_price')->default(0);
            $table->float('external_price')->default(0);
            $table->float('external_price_km')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('express_fees');
    }
}
