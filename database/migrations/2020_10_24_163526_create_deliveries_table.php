<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branchoffice_id')->nullable();
            $table->unsignedBigInteger('dealer_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('firebase_uid')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('note')->nullable();
            $table->string('address');
            $table->decimal('lat',11,8)->nullable()->default(0);
            $table->decimal('lng',11,8)->nullable()->default(0);
            $table->integer('quantity')->nullable();
            $table->integer('price')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
