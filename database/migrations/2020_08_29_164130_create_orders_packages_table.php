<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase_uid')->nullable();
            $table->unsignedBigInteger('order_id');
            $table->integer('quantity');
            $table->float('distance');
            $table->string('type',2);
            $table->float('total');
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('orders_packages', function(Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_packages');
    }
}
