<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('dealers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase_uid')->nullable();
            //$table->unsignedBigInteger('branchoffice_id')->nullable();
            $table->string('photo')->default(env('APP_URL').'/dashboard/assets/img/default_user.png');
            $table->string('photo_firebase_uid')->nullable();
            $table->string('name');
            $table->string('cedula')->unique();
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->string('password')->nullable();
            $table->decimal('lat',10,8)->nullable()->default(0);
            $table->decimal('lng',11,8)->nullable()->default(0);
            $table->integer('status')->default(0);
            $table->integer('available')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

//        Schema::table('dealers', function(Blueprint $table) {
//            $table->foreign('branchoffice_id')->references('id')->on('branchoffices')->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('dealers');
    }
}
