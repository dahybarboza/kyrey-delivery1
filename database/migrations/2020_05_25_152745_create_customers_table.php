<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase_uid')->nullable();
            //$table->unsignedBigInteger('branchoffice_id')->nullable();
            $table->string('photo')->default(env('APP_URL').'/dashboard/assets/img/default_user.png');
            $table->string('photo_firebase_uid')->nullable();
            $table->string('name');
            $table->string('ruc')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });

//        Schema::table('customers', function(Blueprint $table) {
//            $table->foreign('branchoffice_id')->references('id')->on('branchoffices')->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('customers');
    }
}
