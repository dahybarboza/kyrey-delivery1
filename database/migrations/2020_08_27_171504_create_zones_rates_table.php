<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonesRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase_uid')->nullable();
            $table->unsignedBigInteger('zone_id')->nullable();
            $table->unsignedBigInteger('zone_delivery')->nullable();
            //$table->unsignedBigInteger('zone_arrival')->nullable();
            $table->float('price')->nullable();
//            $table->float('price_a')->nullable();
//            $table->float('price_b')->nullable();
//            $table->float('price_c')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
        Schema::table('zones_rates', function(Blueprint $table) {
            $table->foreign('zone_id')->references('id')->on('zones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zones_rates');
    }
}
