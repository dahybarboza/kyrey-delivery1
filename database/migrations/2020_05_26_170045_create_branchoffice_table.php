<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchofficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('branchoffices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase_uid')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->decimal('lat',10,8)->nullable()->default(0);
            $table->decimal('lng',11,8)->nullable()->default(0);
            $table->string('neighborhood')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('branchoffices', function(Blueprint $table) {
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('branchoffices');
    }
}
