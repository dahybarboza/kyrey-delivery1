<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('rutas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase_uid')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('dealer_id')->nullable();
            $table->unsignedBigInteger('branchoffice_id')->nullable();
            $table->string('dia')->nullable();
            $table->date('fecha')->nullable();
            $table->time('hora_inicio_programado')->nullable();
            $table->time('hora_fin_programado')->nullable();
            $table->time('hora_inicio_real')->nullable();
            $table->time('hora_fin_real')->nullable();
            $table->string('observaciones')->nullable();
            $table->decimal('lat',10,8)->nullable()->default(0);
            $table->decimal('lng',11,8)->nullable()->default(0);
            $table->string('address')->nullable();
            $table->string('neighborhood')->nullable();
            $table->integer('en_rango')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('rutas');
    }
}
