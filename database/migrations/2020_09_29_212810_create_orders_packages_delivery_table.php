<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersPackagesDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_packages_deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_package_id');
            $table->unsignedBigInteger('order_package_collect_id')->nullable();
            $table->string('firebase_uid')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->string('note')->nullable();
            $table->integer('quantity');
            $table->integer('type')->nullable();
            $table->string('address');
            $table->string('neighborhood');
            $table->decimal('lat',11,8)->nullable()->default(0);
            $table->decimal('lng',11,8)->nullable()->default(0);
            $table->integer('status')->default(0);
            $table->integer('canceled_canceller')->nullable();
            $table->integer('canceled_type')->nullable();
            $table->timestamp('canceled_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('orders_packages_deliveries', function(Blueprint $table) {
            $table->foreign('order_package_id')->references('id')->on('orders_packages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_packages_collect');
    }
}
