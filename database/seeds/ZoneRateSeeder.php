<?php
use App\ZoneRate;
use Illuminate\Database\Seeder;
class ZoneRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Registros predeterminados para los usuarios
        $faker = Faker\Factory::create();
        //Default  Primer Usuario
        $zones = Array(
            [
                'zone_id'   =>  1,
                    'rates'     =>  Array(
                        [
                            'zone_arrival'  =>  1,
                            'price_a'       =>  20000,
                            'price_b'       =>  18000,
                            'price_c'       =>  17000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  2,
                            'price_a'       =>  25000,
                            'price_b'       =>  22500,
                            'price_c'       =>  21500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  3,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  4,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  5,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  6,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ]
                    )
            ],
            [
                'zone_id'   =>  2,
                    'rates'     =>  Array(
                        [
                            'zone_arrival'  =>  1,
                            'price_a'       =>  25000,
                            'price_b'       =>  22500,
                            'price_c'       =>  21500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  2,
                            'price_a'       =>  20000,
                            'price_b'       =>  18000,
                            'price_c'       =>  17000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  3,
                            'price_a'       =>  25000,
                            'price_b'       =>  22500,
                            'price_c'       =>  21500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  4,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  5,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  6,
                            'price_a'       =>  35000,
                            'price_b'       =>  32000,
                            'price_c'       =>  30000,
                            'status'        =>  0
                        ]
                    )
            ],
            [
                'zone_id'   =>  3,
                    'rates'     =>  Array(
                        [
                            'zone_arrival'  =>  1,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  2,
                            'price_a'       =>  25000,
                            'price_b'       =>  22500,
                            'price_c'       =>  21500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  3,
                            'price_a'       =>  20000,
                            'price_b'       =>  18000,
                            'price_c'       =>  17000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  4,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  5,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  6,
                            'price_a'       =>  35000,
                            'price_b'       =>  32000,
                            'price_c'       =>  30000,
                            'status'        =>  0
                        ]
                    )
            ],
            [
                'zone_id'   =>  4,
                    'rates'     =>  Array(
                        [
                            'zone_arrival'  =>  1,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  2,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  3,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  4,
                            'price_a'       =>  20000,
                            'price_b'       =>  18000,
                            'price_c'       =>  17000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  5,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  6,
                            'price_a'       =>  35000,
                            'price_b'       =>  32000,
                            'price_c'       =>  30000,
                            'status'        =>  0
                        ]
                    )
            ],
            [
                'zone_id'   =>  5,
                    'rates'     =>  Array(
                        [
                            'zone_arrival'  =>  1,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  2,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  3,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  4,
                            'price_a'       =>  300000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  5,
                            'price_a'       =>  20000,
                            'price_b'       =>  18000,
                            'price_c'       =>  17000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  6,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ]
                    )
            ],
            [
                'zone_id'   =>  6,
                    'rates'     =>  Array(
                        [
                            'zone_arrival'  =>  1,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  2,
                            'price_a'       =>  35000,
                            'price_b'       =>  32000,
                            'price_c'       =>  30000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  3,
                            'price_a'       =>  40000,
                            'price_b'       =>  38000,
                            'price_c'       =>  35000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  4,
                            'price_a'       =>  35000,
                            'price_b'       =>  32000,
                            'price_c'       =>  30000,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  5,
                            'price_a'       =>  30000,
                            'price_b'       =>  27000,
                            'price_c'       =>  25500,
                            'status'        =>  0
                        ],
                        [
                            'zone_arrival'  =>  6,
                            'price_a'       =>  20000,
                            'price_b'       =>  18000,
                            'price_c'       =>  17000,
                            'status'        =>  0
                        ]
                    )
            ]
        );
        foreach($zones as $zone) {
            foreach($zone['rates'] as $item) {
                $item['zone_id'] = $zone['zone_id'];
                ZoneRate::create($item);
            }
        }
    }
}
