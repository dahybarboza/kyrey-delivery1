<?php

use Illuminate\Database\Seeder;
use App\ExpressFees;
class ExpressFeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $fees = new ExpressFees();
      $fees->local_price        = 20000;
      $fees->external_price     = 20000;
      $fees->external_price_km  = 2500;
      $fees->save();
    }
}
