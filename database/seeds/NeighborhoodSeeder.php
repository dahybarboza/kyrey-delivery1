<?php
use App\Neighborhood;
use Illuminate\Database\Seeder;

class NeighborhoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zones = Array(
            [
                'zone_id' => 1,
                'neighborhoods' => Array(
                    [
                        'code'      => 1,
                        'name'      => 'Santisima Trinidad',
                        'status'    => 0
                    ],
                    [
                        'code'      => 2,
                        'name'      => 'Tablada Nueva',
                        'status'    => 0
                    ],
                    [
                        'code'      => 3,
                        'name'      => 'Mbocayaty',
                        'status'    => 0
                    ],
                    [
                        'code'      => 4,
                        'name'      => 'Madanme Lynch',
                        'status'    => 0
                    ],
                    [
                        'code'      => 5,
                        'name'      => 'San Jorge ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 6,
                        'name'      => 'Ycua Sati',
                        'status'    => 0
                    ],
                    [
                        'code'      => 7,
                        'name'      => 'Santo Domingo',
                        'status'    => 0
                    ],
                    [
                        'code'      => 8,
                        'name'      => 'Herrera',
                        'status'    => 0
                    ],
                    [
                        'code'      => 9,
                        'name'      => 'San Cristobal',
                        'status'    => 0
                    ],
                    [
                        'code'      => 10,
                        'name'      => 'Mcal Estigarribia',
                        'status'    => 0
                    ],
                    [
                        'code'      => 11,
                        'name'      => 'Villa Aurelia',
                        'status'    => 0
                    ],
                    [
                        'code'      => 12,
                        'name'      => 'Vista Alegre',
                        'status'    => 0
                    ],
                    [
                        'code'      => 13,
                        'name'      => 'Hipodromo ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 14,
                        'name'      => 'San Pablo ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 15,
                        'name'      => 'Terminal ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 16,
                        'name'      => 'Pinoza',
                        'status'    => 0
                    ]
                )
            ],
            [
                'zone_id' => 2,
                'neighborhoods'     => Array(
                    [
                        'code'      => 1,
                        'name'      => 'San Antonio',
                        'status'    => 0
                    ],
                    [
                        'code'      => 2,
                        'name'      => 'San Jorge',
                        'status'    => 0
                    ],
                    [
                        'code'      => 3,
                        'name'      => 'Las Mercedes',
                        'status'    => 0
                    ],
                    [
                        'code'      => 4,
                        'name'      => 'Sajonia',
                        'status'    => 0
                    ],
                    [
                        'code'      => 5,
                        'name'      => 'Tacumbu',
                        'status'    => 0
                    ],
                    [
                        'code'      => 6,
                        'name'      => 'Barrio Obrero',
                        'status'    => 0
                    ],
                    [
                        'code'      => 7,
                        'name'      => 'Roberto L Pettit',
                        'status'    => 0
                    ],
                    [
                        'code'      => 8,
                        'name'      => 'Republicano',
                        'status'    => 0
                    ],
                    [
                        'code'      => 9,
                        'name'      => 'Santa Ana',
                        'status'    => 0
                    ]
                )

            ],
            [
                'zone_id' => 3,
                'neighborhoods'     => Array(
                    [
                        'code'      => 1,
                        'name'      => 'Arecaya',
                        'status'    => 0
                    ],
                    [
                        'code'      => 2,
                        'name'      => 'Central I',
                        'status'    => 0
                    ],
                    [
                        'code'      => 3,
                        'name'      => 'Central II',
                        'status'    => 0
                    ],
                    [
                        'code'      => 4,
                        'name'      => 'Caacupemi',
                        'status'    => 0
                    ],
                    [
                        'code'      => 5,
                        'name'      => 'Zeballos Cue',
                        'status'    => 0
                    ],
                    [
                        'code'      => 6,
                        'name'      => 'San Blas',
                        'status'    => 0
                    ],
                    [
                        'code'      => 7,
                        'name'      => 'Botanico',
                        'status'    => 0
                    ]
                )
            ],
            [
                'zone_id' => 4,
                'neighborhoods'     => Array(
                    [
                        'code'      => 1,
                        'name'      => 'San Rafael ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 2,
                        'name'      => 'Santa Lucia ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 3,
                        'name'      => 'Felicidad',
                        'status'    => 0
                    ],
                    [
                        'code'      => 4,
                        'name'      => 'Ita Enramada',
                        'status'    => 0
                    ],
                    [
                        'code'      => 5,
                        'name'      => '4 Mojones',
                        'status'    => 0
                    ],
                    [
                        'code'      => 6,
                        'name'      => 'Cañada San Miguel ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 7,
                        'name'      => 'San Roque ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 8,
                        'name'      => 'San José',
                        'status'    => 0
                    ],
                    [
                        'code'      => 9,
                        'name'      => 'Santa Rosa 1',
                        'status'    => 0
                    ],
                    [
                        'code'      => 10,
                        'name'      => 'Santa Rosa 2',
                        'status'    => 0
                    ],
                    [
                        'code'      => 11,
                        'name'      => 'Puerto Pabla',
                        'status'    => 0
                    ],
                    [
                        'code'      => 12,
                        'name'      => 'San Isidro ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 13,
                        'name'      => 'Unión Progreso',
                        'status'    => 0
                    ],
                    [
                        'code'      => 14,
                        'name'      => 'Rosedal ',
                        'status'    => 0
                    ],
                    [
                        'code'      => 15,
                        'name'      => 'Arroyo Seco',
                        'status'    => 0
                    ]
                )
            ],
            [
                'zone_id' => 5,
                'neighborhoods'     => Array(
                    [
                        'code'      =>  1,
                        'name'      =>  'Ycua Caranda´y',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  2,
                        'name'      =>  'Mora Cue',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  3,
                        'name'      =>  'Loma Merlo',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  4,
                        'name'      =>  'Cañada Garay',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  5,
                        'name'      =>  'Yukyry',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  6,
                        'name'      =>  'Tercer Barrio ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  7,
                        'name'      =>  '1° Barrio',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  8,
                        'name'      =>  '3° Barrio',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  9,
                        'name'      =>  '4° Barrio',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  10,
                        'name'      =>  'Costa Sosa',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  11,
                        'name'      =>  'Maka´i ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  12,
                        'name'      =>  'Marambure',
                        'status'    =>  0
                    ],

                )
            ],
            [
                'zone_id' => 6,
                'neighborhoods'     => Array(
                    [
                        'code'      =>  1,
                        'name'      =>  'Laurelty',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  2,
                        'name'      =>  'Cañada San Rafael ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  3,
                        'name'      =>  'San Miguel ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  4,
                        'name'      =>  'Villa Amelia ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  5,
                        'name'      =>  'Tres Bocas ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  6,
                        'name'      =>  'Barcequillo',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  7,
                        'name'      =>  'Florida Lucerito',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  8,
                        'name'      =>  'Los Nogales ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  9,
                        'name'      =>  'Caacup Miraflorez',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  10,
                        'name'      =>  'Capilla del Monte ',
                        'status'    =>  0
                    ],
                    [
                        'code'      =>  11,
                        'name'      =>  'Mita´i',
                        'status'    =>  0
                    ]
                )
            ]
        );
        foreach($zones as $zone) {
            foreach($zone['neighborhoods'] as $item) {
                $item['zone_id'] = $zone['zone_id'];
                Neighborhood::create($item);
            }
        }
    }
}
