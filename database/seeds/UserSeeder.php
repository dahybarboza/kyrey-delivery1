<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        //Registros predeterminados para los usuarios
        $faker = Faker\Factory::create();
        //Default  Primer Usuario
        $usuario = new User();
        $usuario->fill([
            'name'   => 'Administrador',
            'enterprise'  => $faker->company,
            'photo'     => 'dashboard/assets/img/default_user.png',
            'email'    => 'admin@admin.com',
            'phone' => '1122334455',
            'password' => Hash::make('adminadmin')
        ])->save();
    }
}
