<?php
use App\Zone;
use Illuminate\Database\Seeder;
class ZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Registros predeterminados para los usuarios
        $faker = Faker\Factory::create();
        //Default  Primer Usuario
        $zones = Array(
            [
                'name'      =>  'Shopping del Sol',
                'name_long' =>  '',
                'status'    =>  0
            ],
            [
                'name'      =>  'Canal 9',
                'status'    =>  0
            ],
            [
                'name'      =>  'Lambare',
                'status'    =>  0
            ],
            [
                'name'      =>  'San Lorenzo',
                'status'    =>  0
            ],
            [
                'name'      =>  'MRA',
                'status'    =>  0
            ],
            [
                'name'      => 'Luque',
                'status'    =>  0
            ]
        );
        foreach($zones as $zone) {
            $insert = new Zone();
            $insert->fill($zone)->save();
        }
    }
}
