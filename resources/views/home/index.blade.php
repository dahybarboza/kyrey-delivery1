@extends('layout.main')
@section('content')
<body class="c-app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                {!! Form::model(null,['id'=>"frmLogin"]) !!}
                  <h1>Inicia sesión</h1>
                  <p class="text-muted">Ingrese sus datos de acceso</p>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                        </svg>
                      </span>
                    </div>
                    <input class="form-control" id="email" name="email" type="email" placeholder="Correo electrónico" required>
                  </div>
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <svg class="c-icon">
                          <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-lock-locked"></use>
                        </svg>
                      </span>
                    </div>
                    <input class="form-control" id="password" name="password" type="password" placeholder="Contraseña" required>
                  </div>
                  <div class="row">
                    <div class="col-6">
                      <button class="btn btn-primary px-4" type="submit">Ingresar</button>
                    </div>
                  </div>
                {!! Form::close()!!}
              </div>
            </div>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>{{env('APP_NAME')}}</h2>
                  <p>La actitud perfecta para tus entregas</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
  @endsection