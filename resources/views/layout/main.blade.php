<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Kyrey Delivery">
    <meta name="author" content="PalaSoft">
    <meta name="keyword" content="Kyrey Delivery, Delivery Paraguay">
    <title>{{env('APP_TITLE')}}</title>
{{--    <link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-icon-57x57.png">--}}
{{--    <link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-icon-60x60.png">--}}
{{--    <link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-icon-72x72.png">--}}
{{--    <link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-icon-76x76.png">--}}
{{--    <link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-icon-114x114.png">--}}
{{--    <link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-icon-120x120.png">--}}
{{--    <link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-icon-144x144.png">--}}
{{--    <link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-icon-152x152.png">--}}
{{--    <link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-icon-180x180.png">--}}
{{--    <link rel="icon" type="image/png" sizes="192x192" href="assets/favicon/android-icon-192x192.png">--}}
{{--    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">--}}
{{--    <link rel="icon" type="image/png" sizes="96x96" href="assets/favicon/favicon-96x96.png">--}}
{{--    <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">--}}
{{--    <link rel="manifest" href="assets/favicon/manifest.json">--}}
    <meta name="msapplication-TileColor" content="#ffffff">
{{--    <meta name="msapplication-TileImage" content="assets/favicon/ms-icon-144x144.png">--}}
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->
    <link href="{{ Asset('dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ Asset('dashboard/vendors/@coreui/chartjs/css/coreui-chartjs.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Asset('plugins/datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Asset('plugins/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Asset('plugins/datatable/datatables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css" rel="stylesheet"  type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.themes.min.css" rel="stylesheet"  type="text/css">
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" rel="stylesheet">
    <link href="https://cdn.rawgit.com/rikmms/progress-bar-4-axios/0a3acf92/dist/nprogress.css" rel="stylesheet" type="text/css"/>
    <link href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.bootstrap-v4.min.css"  rel="stylesheet" rel="stylesheet" >
    <link href="{{ Asset('dashboard/css/custome.css').'?v='.mt_rand(100000, 999999) }}" rel="stylesheet">
    <link rel="icon" type="image/png" href=" {{ asset('dashboard/assets/img/favicon.png') }}">
    @if(isset($cssStyles))
        @foreach( $cssStyles as $css_style)
            <link href="{{ Asset('app/home/'.$css_style).'?v='.mt_rand(100000, 999999) }}" rel="stylesheet">
        @endforeach
    @endif
  </head>
  <body class="c-app">
    <input type="hidden" id="domainHost" value="{{ env('APP_URL') }}">
    <input type="hidden" name="routeAdmin" id="routeAdmin" value="{!! env('admin') !!}">
    <div class="c-wrapper c-fixed-components">
      <div class="c-body">
        <main class="c-main">
          <div class="container-fluid">
            <div class="fade-in">
              <div class="row">
                <div class="col-sm-12">
                    @yield('content')
                </div>
                <!-- /.col-->
              </div>
              <!-- /.row-->
            </div>
          </div>
        </main>
      </div>
    </div>
    <!-- jQuery -->
    <script src="{{ Asset('plugins/jquery/jquery-3.4.1.min.js') }}"></script>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ Asset('dashboard/vendors/@coreui/coreui/js/coreui.bundle.min.js') }}"></script>
    <!--[if IE]><!-->
    <script src="{{ Asset('dashboard/vendors/@coreui/icons/js/svgxuse.min.js') }}"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <script src="{{ Asset('dashboard/vendors/@coreui/utils/js/coreui-utils.js') }}"></script>
    <!-- Custome Plugins-->
    <script src="{{ Asset('plugins/jquery/jquery-ui.js') }}"></script>
    <script src="{{ Asset('plugins/datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ Asset('plugins/datepicker/bootstrap-datepicker.es.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.rawgit.com/rikmms/progress-bar-4-axios/0a3acf92/dist/index.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script src="{{ Asset('plugins/validate/jquery.validate.min.js') }}"></script>
    <script src="{{ Asset('plugins/validate/messages_es.min.js') }}"></script>
    <script src="{{ Asset('plugins/datatable/datatables.min.js') }}"></script>
    <script src="{{ Asset('plugins/bootbox/bootbox.min.js') }}"></script>
    <!-- Script App -->
    <script src="{{ Asset('app/Config.js').'?v='.mt_rand(100000, 999999) }}"></script>
    <script src="{{ Asset('app/Core.js').'?v='.mt_rand(100000, 999999) }}"></script>
    @if(isset($jsControllers))
        @foreach( $jsControllers as $jsController)
            <script src='{!! Asset("app/".$jsController).'?v='.mt_rand(100000, 999999) !!}'></script>
        @endforeach
    @endif
  </body>
</html>
