<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
          <svg class="c-icon c-icon-lg">
            <use xlink:href="{{  Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-menu') }}"></use>
          </svg>
        </button>
        <ul class="c-header-nav ml-auto mr-4">
          <li class="c-header-nav-item d-md-down-none mx-2">
            <a class="c-header-nav-link" href="#">
                <svg class="c-icon">
                  <use xlink:href="{{  Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-user') }}"></use>
                  </svg>&nbsp;&nbsp;
                {!! Auth::user()->name  !!}
            </a>   
          </li>
          <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <svg class="c-icon">
                    <use xlink:href="{{  Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-lock-locked') }}"></use>
                </svg>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
              <div class="dropdown-header bg-light py-2"><strong>{{__('account')}}</strong></div>
              <a class="dropdown-item" href="{{ Asset(env('admin')).'/home/profile' }}">
                <svg class="c-icon mr-2">
                  <use xlink:href="{{  Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-user') }}"></use>
                </svg> Perfil
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{!! Asset('')."logout" !!}">
                <svg class="c-icon mr-2">
                  <use xlink:href="{{  Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-account-logout') }}"></use>
                </svg> Salir
              </a>
            </div>
          </li>
        </ul>
      </header>
