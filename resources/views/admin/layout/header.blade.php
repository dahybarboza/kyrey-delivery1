<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <ul class="c-sidebar-nav">
        <div class="c-sidebar-brand d-lg-down-none" style="background: #ED502E;">
            <div class="c-sidebar-brand-minimized">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="{{ Asset('').Auth::user()->photo }}" class="logo-profile" style="margin-left: 0px;">
                    </div>
                </div>
            </div>
            <div class="c-sidebar-brand-full">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="{{ Asset('').Auth::user()->photo }}" class="logo-profile">
                        <div class="title-profile" style="word-wrap: break-word;">
                            {{--                            <h4 style="font-size:1.35vw;">{{ substr(Auth::user()->enterprise,0,16).'...' }}</h4>--}}
                            <h4 style="font-size:1.35vw;">{{ substr(Auth::user()->enterprise,0,16) }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/home') }}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-speedometer') }}"></use>
                </svg>
                Dashboard</a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="http://traccar.palasoft.com.py:8082/" target="_blank">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-location-pin') }}"></use>
                </svg>
                Sistema de rastreo
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/orders') }}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-layers') }}"></use>
                </svg>
                Órdenes
            </a>
        </li>

       <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/delivery') }}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-short-text') }}"></use>
              </svg>
               Delivery
           </a>
      </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/quotes') }}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-exposure') }}"></use>
                </svg>
                Cotizaciones
            </a>
        </li>
{{--        <li class="c-sidebar-nav-item">--}}
{{--            <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/invoices') }}">--}}
{{--                <svg class="c-sidebar-nav-icon">--}}
{{--                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-balance-scale') }}"></use>--}}
{{--                </svg>--}}
{{--                Facturaciones--}}
{{--            </a>--}}
{{--        </li>--}}
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/service-quanlity') }}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-badge') }}"></use>
                </svg>
                Calidad del servicio
            </a>
        </li>
{{--    <li class="c-sidebar-nav-item">
       <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/rutas') }}">
           <svg class="c-sidebar-nav-icon">
               <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-map') }}"></use>
           </svg>
           Gestión de Rutas
       </a>
   </li>--}}
   <li class="c-sidebar-nav-item">
       <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/branchoffices') }}">
           <svg class="c-sidebar-nav-icon">
               <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-fork') }}"></use>
           </svg>
           Sucursales
       </a>
   </li>
   <li class="c-sidebar-nav-item">
       <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/dealers') }}">
           <svg class="c-sidebar-nav-icon">
               <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-truck') }}"></use>
           </svg>
           Repartidores
       </a>
   </li>
   <li class="c-sidebar-nav-item">
       <a class="c-sidebar-nav-link" href="{{ Asset(env('admin').'/customers') }}">
           <svg class="c-sidebar-nav-icon">
               <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-people') }}"></use>
           </svg>
           Clientes
       </a>
   </li>
   <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
       <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
           <svg class="c-sidebar-nav-icon">
               <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-dollar') }}"></use>
           </svg>
           Tarifas
       </a>
       <ul class="c-sidebar-nav-dropdown-items">
           <li class="c-sidebar-nav-item">
               <a class="c-sidebar-nav-link" href="{{ Asset(env('admin')).'/express-fees' }}">
                   <span class="c-sidebar-nav-icon"></span> Express
               </a>
           </li>
           <li class="c-sidebar-nav-item">
               <a class="c-sidebar-nav-link" href="{{ Asset(env('admin')).'/zones' }}">
                   <span class="c-sidebar-nav-icon"></span> Express Programado
               </a>
           </li>
       </ul>
   </li>
</ul>
<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div>
