@extends('admin.layout.main')
@section('content')
<input type="hidden" id="zone_delivery" value="{{$zone->id}}">
<div class="card">
    <div class="card-header"><strong>Tarifas | <b>{{$zone->name}}</b></strong>
        <button class="btn btn-dark float-right" data-toggle="modal" data-target="#mdlNew" style="cursor: pointer; margin-right:5px;">
            Nueva
        </button>
    </div>
    <div class="card-body">
        <table class="table table-hover" id="arrData" style="width:100%;">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Zona Destino</th>
                    <th width="100">Costo</th>
                    <th width="1">Estatus</div>
                    <th width="80">Acciones</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
{!! Form::model(null,['id'=>"frmNew"]) !!}
<div class="modal fade" id="mdlNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" name="zone_id" value="{{ $_GET['zone'] }}">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="vat">Zona destino</label>
                        <select class="form-control" name="zone_delivery" id="zone_delivery" required></select>
                        </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="company">Costo</label>
                        <input class="form-control" id="price" name="price" type="number" required>
                    </div>
                    </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="vat">Estatus</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="1">Activa</option>
                            <option value="0">Inactiva</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success">Guardar</button>
        </div>
    </div>
  </div>
</div>
{{ Form::close() }}
{!! Form::model(null,['id'=>"frmEdit"]) !!}
<div class="modal fade" id="mdlEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <input type="hidden" name="id" id="id" required>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="company">Zona destino</label>
                        <input class="form-control" id="name" type="text" required disabled>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="company">Costo</label>
                        <input class="form-control" id="price" name="price" type="number" required>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="vat">Estatus</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="1">Activa</option>
                            <option value="0">Inactiva</option>
                        </select>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger">Editar</button>
      </div>
    </div>
  </div>
</div>
{{ Form::close() }}
@endsection
