@extends('admin.layout.main')
@section('content')
    <div class="card">
        <div class="card-header"><strong>Cotizaciones</strong></div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="1">Folio</th>
                    <th>Cliente</th>
                    <th width="1">Télefono</th>
                    <th width="1">Correo</th>
                    <th width="1">Excel</th>
                    <th width="1">Estatus</th>
                    <th width="80">Acciones</th>
                </tr>
            </thead>
            <tbody></tbody>
            </table>
        </div>
    </div>
{!! Form::model(null,['id'=>"frmEdit"]) !!}
<div class="modal fade" id="mdlEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id" name="id" required>
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                            <div class="form-group">
                            <label for="company">Nombre</label>
                            <input class="form-control" id="name" name="name" type="text" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                            <div class="form-group">
                            <label for="company">Detalles</label>
                            <textarea class="form-control" id="details" name="details" type="text" disabled></textarea>
                        </div>
                    </div>
                </div>
                <h5>Cotización</h5>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="company">Ingresar nota para el cliente</label>
                        <textarea class="form-control" id="note" name="note" type="text"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="vat">Estatus</label>
                            <select class="form-control" id="status" name="status">
                                <option value="0">Nuevo</option>
                                <option value="1">Aprobada</option>
                                <option value="2">Rechazado</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger">Editar</button>
      </div>
    </div>
  </div>
</div>
{{ Form::close() }}
@endsection
