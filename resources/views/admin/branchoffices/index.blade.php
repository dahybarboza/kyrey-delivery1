@extends('admin.layout.main')
@section('content')

{{--    <style type="text/css">--}}
{{--        /* Set the size of the div element that contains the map */--}}
{{--        #map {--}}
{{--            height: 280px;--}}
{{--            /* The height is 400 pixels */--}}
{{--            width: 100%;--}}
{{--            /* The width is the width of the web page */--}}
{{--        }--}}
{{--    </style>--}}

    <div class="card">
        <div class="card-header"><strong>Sucursales</strong>
            <button class="btn btn-dark" style="float:right;" data-toggle="modal" data-target="#mdlNew" id="btnNew">Agregar nueva sucursal</button>
        </div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Cliente</th>
                    <th>Sucursal</th>
                    <th>Teléfono</th>
                    <th width="1">Estado</th>
                    <th width="80">Acciones</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    {!! Form::model(null,['id'=>"frmNew"]) !!}
    <div class="modal fade" id="mdlNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva sucursal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="vat">Cliente</label>
                                <select class="form-control" name="customer_id" id="customer_id" required>
                                    <option value="">Seleccionar</option>
                                    @foreach($customers as $key => $customer)
                                        <option value="{!! $customer->id !!}">{!! $customer->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                      <div class="form-group">
                                <label for="company">Nombre de la sucursal</label>
                                <input class="form-control" id="name" name="name" type="text" required>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="company">Teléfono</label>
                                <input class="form-control" id="phone" name="phone" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="ccmonth">Dirección</label>
                                        <input class="form-control" id="address" name="address" type="text" required>
                                        <input type="hidden" id="lat" name="lat" required>
                                        <input type="hidden" id="lng" name="lng" required>
                                        <input type="hidden" name="neighborhood" id="neighborhood" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="vat">Estado</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="1">Activa</option>
                                    <option value="0">Inactiva</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="">Vista previa de la ubicación</label>
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    {!! Form::model(null,['id'=>"frmEdit"]) !!}
    <div class="modal fade" id="mdlEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar datos de la sucursal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" required>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="customer_id">Cliente</label>
                                <select class="form-control" name="customer_id" id="customer_id" required>
                                    <option value="">Seleccionar</option>
                                    @foreach($customers as $key => $customer)
                                        <option value="{!! $customer->id !!}">{!! $customer->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="name">Nombre de la sucursal</label>
                                <input class="form-control" id="name" name="name" type="text" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="phone">Teléfono</label>
                                <input class="form-control" id="phone" name="phone" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="address">Dirección</label>
                                        <input class="form-control" id="address" name="address" type="text" required>
                                        <input type="hidden" id="lat" name="lat" required>
                                        <input type="hidden" id="lng" name="lng" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="status">Estado</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="1">Activa</option>
                                    <option value="0">Inactiva</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="mapEdit"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    <!-- Autocomplete Address -->
{{--    <div id="map"></div>--}}
    <div id="infowindow-content">
        <span id="place-name" class="title"></span>
        <span id="place-id"></span><br>
        <span id="place-address"></span>
    </div>
@endsection