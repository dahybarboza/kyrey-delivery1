@extends('admin.layout.main')
@section('content')
    <div class="card">
        <div class="card-header"><strong>Solicitudes de facturación</strong></div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="1">Orden</th>
                        <th>Cliente</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th width="1">F. Pago</th>
                        <th width="1">R.U.C.</th>
                        <th width="1">Total</th>
                        <th width="1">Estatus</th>
                        <th width="80">Acciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
{!! Form::model(null,['id'=>"frmEdit"]) !!}
<div class="modal fade" id="mdlEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar estado de solicitud</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <input type="hidden" id="id" name="id" required>
            <div class="row">
                <div class="col-sm-6">
                        <div class="form-group">
                        <label for="company">Nombre</label>
                        <input class="form-control" id="name" name="name" type="text" required>
                    </div>
                </div>
                <div class="col-sm-6">
                        <div class="form-group">
                        <label for="company">R.U.C.</label>
                        <input class="form-control" id="ruc" name="ruc" type="text" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                        <div class="form-group">
                        <label for="company">Teléfono</label>
                        <input class="form-control" id="phone" name="phone" type="text">
                    </div>
                </div>
                <div class="col-sm-6">
                        <div class="form-group">
                        <label for="company">Correo</label>
                        <input class="form-control" id="email" name="email" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                        <div class="form-group">
                        <label for="company">Total</label>
                        <input class="form-control" id="total" name="total" type="text" required>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="vat">Forma de pago</label>
                        <select class="form-control" name="payment_method" id="payment_method" required>
                            <option value="">Seleccionar</option>
                            <option value="1">Efectivo</option>
                            <option value="2">Tarjeta</option>
                            <option value="3">Transferencia</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="vat">Estatus</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="0">Pendiente</option>
                            <option value="1">Procesada</option>
                            <option value="2">Rechazada</option>
                        </select>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger">Editar</button>
      </div>
    </div>
  </div>
</div>
{{ Form::close() }}
@endsection
