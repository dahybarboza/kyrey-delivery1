@extends('admin.layout.main')
@section('content')
    {!! Form::model(null,['id'=>"frmNew"]) !!}
        <input type="hidden" id="idOrder" value="{{$id}}">
        <div class="card">
            <div class="card-header"><strong>Editar órden de envios</strong></div>
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        Datos de cliente
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="vat">Nombre de cliente</label>
                                    <input type="text" class="form-control" name="customer_name" id="customer_name" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="vat">Teléfono de cliente</label>
                                    <input type="text" class="form-control" name="customer_phone" id="customer_phone" placeholder="Ej: +59521643528 ó +595 24 643 528" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="vat">Correo de cliente</label>
                                    <input type="email"  class="form-control" name="customer_email" id="customer_email">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="vat">Forma de pago</label>
                                    <select class="form-control" name="payment_method" id="payment_method" required>
                                        <option value="">Seleccionar</option>
                                        <option value="1">Efectivo</option>
                                        <option value="2">Tarjeta</option>
                                        <option value="3">Transferencia</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        Datos de orden
                    </div>
                    <div class="card-body">
                        <div class="row">
{{--                            <div class="col-sm-2">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="vat">Sucursal</label>--}}
{{--                                    <select type="text" class="form-control" name="branchoffice_id" id="branchoffice_id">--}}
{{--                                        <option value="">Seleccionar</option>--}}
{{--                                        @foreach($branchoffices as $branchoffice)--}}
{{--                                            <option value="{{$branchoffice->id}}">{{$branchoffice->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="vat">Delivery - Repartidor</label>
                                    <select type="text" class="form-control" name="dealer_id" id="dealer_id">
                                        <option value="">Seleccionar</option>
                                        @foreach($dealers as $dealer)
                                            <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

{{--                            <div class="col-sm-3">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="vat">Repartidor</label>--}}
{{--                                    <select type="text" class="form-control" name="dealer_id" id="dealer_id"></select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="vat">Servicio</label>
                                    <select type="text" class="form-control" name="service" id="service" required>
                                        <option value="">Seleccionar</option>
                                        <option value="1">Express</option>
                                {{--       <option value="2">Currier</option>--}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3" id="type_view">
                                <div class="form-group">
                                    <label for="vat">Tipo</label>
                                    <select type="text" class="form-control" name="type" id="type">
                                        <option value="">Seleccionar</option>
                                        <option value="1">Inmediato</option>
                                        <option value="2">Programado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3" id="date_view">
                                <div class="form-group">
                                    <label for="vat">Fecha de salida</label>
                                    <input type="text" class="form-control datepicker" name="date" id="date">
                                </div>
                            </div>
                            <div class="col-sm-3" id="round_view">
                                <div class="form-group">
                                    <label for="vat">Tanda de salida</label>
                                    <select type="text" class="form-control" name="round" id="round">
                                        <option value="1">Seleccionar</option>
                                        <option value="2">1er tanda - 11 Hrs</option>
                                        <option value="3">2da tanda - 14 Hrs</option>
                                        <option value="4">3ra tanda - 16 Hrs</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3" id="quote_view">
                                <div class="form-group">
                                    <label for="vat">Cotización de referencia</label>
                                    <select type="text" class="form-control" name="quote_id" id="quote_id">
                                        <option value="">Seleccionar</option>
                                        @foreach($quotes as $quote)
                                            <option value="{{$quote->id}}">{{$quote->id}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3" id="total_view">
                                <div class="form-group">
                                    <label for="vat">Personalizar precio</label>
                                    <input type="text" class="form-control" name="total" id="total">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="vat">Estado</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <select class="form-control" name="status" id="status" required>
                                            <option value="0">Nuevo</option>
                                            <option value="1">Asignado</option>
                                            <option value="2">En proceso de entrega</option>
                                            <option value="3">Entregado</option>
                                            <option value="4">Cancelado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <!-- Fin Bloque-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header"><strong>Lista de paquetes</strong>
                                <button type="button" href="#" data-toggle="modal" data-target="#mdlTypeLogistic" class="btn btn-info" style="float:right;">
                                    <svg class="c-icon" style="cursor: pointer;">
                                        <use xlink:href="{!! Asset('') !!}dashboard/vendors/@coreui/icons/svg/free.svg#cil-plus"></use>
                                    </svg>
                                </button>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover" id="arrData" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tipo</th>
                                            <th width="1" class="text-center">Paquetes</th>
                                            <th width="1" class="text-center">Origenes</th>
                                            <th width="1" class="text-center">Destinos</th>
                                            <th width="1" class="text-center">Distancia</th>
                                            <th width="1" class="text-center">Total</th>
                                            <th width="80">Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <h5 for="" id="totalPreview">₲0</h5>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="{!! Asset('').env('admin').'/orders' !!}" id="btnCancelOrder" class="btn btn-secondary float-left">Cancelar</a>
                <button type="submit" class="btn btn-danger float-right btn-guardar-orden">Actualizar</button>
            </div>
        </div>
    {{ Form::close() }}
    <div class="modal fade" id="mdlTypeLogistic" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Tipo de logistica</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="1"><b>L1</b> - Un lugar de retiro, un destino de entrega.</button>
{{--             <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="2"><b>L2</b> - Un lugar de retiro, varios destinos de entrega.</button>--}}
{{--             <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="3"><b>L3</b> - varios lugares de retiro, un destino de entrega.</button>--}}
{{--             <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="4"><b>L4</b> - Varios lugares de retiro, varios destinos de entrega.</button>--}}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnLogisticCancel">Cancelar</button>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="map"></div>
        </div>
    </div>
    @include('admin/orders/L1Edit')
    @include('admin/orders/L2Edit')
    @include('admin/orders/L3Edit')
    @include('admin/orders/L4Edit')
@endsection
