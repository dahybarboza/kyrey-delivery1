<form id="frmL3AddOrigenes">
    <div class="modal fade" id="mdlOrigenesL3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L3</b> - Varios lugares de retiro, Un lugar de entrega</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" id="frmAddOrigenL3Form">
                            <div class="card">
                                <div class="card-header">
                                    <label style="margin-top: 8px">
                                        Agregar retiro
                                    </label>
                                    <button type="submit" id="btnAddOrigenL3" class="btn btn-primary float-right" style="margin-left: 5px;">
                                        <svg class="c-icon mr-2" style="cursor: pointer;">
                                        <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-save"></use>
                                        </svg>
                                    </button>
                                    <button type="button" id="btnAddOrigenL3Cancel" class="btn btn-secondary float-right">
                                        <svg class="c-icon mr-2" style="cursor: pointer;">
                                        <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-ban"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="hidden" id="idOrigenL3">
                                            <div class="form-group">
                                                <label for="vat">Nombre</label>
                                                <input type="text" class="form-control" name="name" id="name" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="vat">Teléfono</label>
                                                <input type="number" class="form-control" name="phone" id="phone" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Cantidad de paquetes</label>
                                                <input type="number" class="form-control" name="quantity" id="quantity" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Tipo de paquete</label>
                                                <select name="type" id="type" class="form-control" required>
                                                    <option value="1">A</option>
                                                    <option value="2">B</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="vat">Nota</label>
                                                <input type="text" class="form-control" name="note" id="note">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Dirección de retiro</label>
                                                <input type="text" class="form-control" name="address" id="address" required>
                                                <input type="hidden" class="form-control" name="address_lat" id="address_lat" required>
                                                <input type="hidden" class="form-control" name="address_lng" id="address_lng" required>
                                                <input type="hidden" class="form-control" name="neighborhood" id="neighborhood" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" id="frmAddOrigenL3List">
                            <div class="card">
                                <div class="card-header">
                                    <label style="margin-top: 8px">
                                        Lista de destinos
                                    </label>
                                    <button type="button" id="btnAddOrigenL3Add" class="btn btn-primary float-right" style="margin-left: 5px;">
                                        <svg class="c-icon mr-2" style="cursor: pointer;">
                                            <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-plus"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                           <tr>
                                                <th width="1">#</th>
                                                <th width="1">Paqtes</th>
                                                <th width="1">Km's</th>
                                                <th width="80">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="listOrigenesL3"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="card">
                                <div class="card-header">Vista previa de ruta</div>
                                <div class="card-body">
                                    <div id="mapL3Origen" class="height: 400px; width: 100%;"></div>
                                    <div id="mapL3Origenes" class="height: 400px; width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnAddOrigenesL3Cancel">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="btnAddOrigenesL3Save">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</form>
<form id="frmL3AddDestino">
    <div class="modal fade" id="mdlDestinoL3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L3</b> - Varios lugares de retiro, Un lugar de entrega</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-header">
                                Datos de entrega
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Nombre</label>
                                            <input type="text" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Teléfono</label>
                                            <input type="number" class="form-control" name="phone" id="phone" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Nota</label>
                                            <input type="text" class="form-control" name="note" id="note">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Direccion de entrega</label>
                                            <input type="text" class="form-control" name="address" id="address" required>
                                            <input type="hidden" class="form-control" name="address_lat" id="address_lat" required>
                                            <input type="hidden" class="form-control" name="address_lng" id="address_lng" required>
                                            <input type="hidden" class="form-control" name="neighborhood" id="neighborhood" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-header">Vista previa de ruta</div>
                            <div class="card-body">
                                <div id="mapL3Destino" class="height: 400px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </div>
    </div>
</form>
<!--Draw Route Google Maps -->
<div class="modal fade" id="mdlRouteL3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ruta : <b>L3</b> - Ruta de retiro y entrega</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">Vista previa de ruta</div>
                        <div class="card-body">
                            <div id="mapL3Route" class="height: 400px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
    </div>
    </div>
</div>
