@extends('admin.layout.main')
@section('content')
    <div class="card">
        <div class="card-header"><strong>Ordenes</strong>
            <a href="{{Asset('').env('admin').'/orders/create'}}" class="btn btn-dark" style="float:right;" >Nuevo</a>
            <button class="btn btn-dark float-right" id="btnUpdate" style="cursor: pointer; margin-right:5px;">
                Actualizar
            </button>

            <a href="{{route('orders.export')}}" class="btn btn-dark float-right" id="$btnExportar" style="cursor: pointer; margin-right:5px;">
                Exportar
            </a>



            <div class="col-sm-2" id="type_view" style='float:right'>
                <div class="form-group">
                    <select type="text" class="form-control" id="filtro1" name="filtro" style='float:right'>
                        <option hidden selected>Selecciona una opción</option>
                        <option value="8">Todos</option>
                        <option value="0">Nuevo</option>
                        <option value="1">Asignado</option>
                        <option value="2">En proceso de entrega</option>
                        <option value="3">Entregado</option>
                        <option value="4">Cancelado</option>
                    </select>
                </div>
            </div>
            <label for="vat" style='float:right; padding-top:5px'>Estado:</label>

            <div class="col-sm-2" id="type_view" style='float:right'>
                <div class="form-group">
                    <select type="text" class="form-control" name="filtro" id="filtro2" style='float:right'>
                        <option hidden selected>Selecciona una opción</option>
                        <option value="3">Ambos</option>
                        <option value="1">Inmediato</option>
                        <option value="2">Programado</option>
                    </select>
                </div>
            </div>
            <label for="vat" style='float:right; padding-top:5px'>Tipo:</label>



        </div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
                <thead style="width:100%;">
                <tr style="width:100%";>
                    <th width="auto">#</th>
                    <th width="auto" class="text-center">Folio</th>
                    <th width="auto" class="text-center">Cliente</th>
                    <th width="auto" class="text-center">F. Pago</th>
                    <th width="auto" class="text-center">Servicio</th>
                    <th width="auto" class="text-center">Tipo</th>
                    <th width="auto" class="text-center">Paquetes</th>
                    <th width="auto" class="text-center">Total</th>
                    <th width="auto" class="text-center">Tanda</th>
                    <th width="auto" class="text-center">Fecha Y Hora</th>
                    <th width="auto" class="text-center">Estado</th>
                    <th width="auto" class="text-center">Acciones</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
