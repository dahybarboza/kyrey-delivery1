<form id="frmL1AddOrigen">
    <div class="modal fade" id="mdlOrigenL1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-full" role="document">
        <div class="modal-content modal-content-full" style="">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L1</b> - Un lugar de retiro, un destino de entrega</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-header">
                                Datos de retiro
                            </div>
                            <div class="card-body">
                                <input type="hidden" name="idOrigenL1" id="idOrigenL1">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Nombre</label>
                                            <input type="text" class="form-control" name="l1_name" id="l1_name" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Teléfono</label>
                                            <input type="text" placeholder="Ej: +59521643528 ó +595 24 643 528" class="form-control" name="l1_phone" id="l1_phone" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Nota</label>
                                            <input type="text" class="form-control" name="l1_note" id="l1_note">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Cantidad de paquetes</label>
                                            <input type="number" class="form-control" name="l1_quantity" id="l1_quantity" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Tipo de paquete</label>
                                            <select name="l1_type" id="l1_type" class="form-control" required>
                                                <option value="1">Sobre</option>
                                                <option value="2">Paquete</option>
                                                <option value="3">Alimento</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Direccion de recolección</label>
                                            <input type="text" class="form-control" name="l1_address" id="l1_address" required>
                                            <input type="hidden" class="form-control" name="l1_address_lat" id="l1_address_lat" required>
                                            <input type="hidden" class="form-control" name="l1_address_lng" id="l1_address_lng" required>
                                            <input type="hidden" class="form-control" name="l1_neighborhood" id="l1_neighborhood" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-header">Vista previa de ruta</div>
                            <div class="card-body">
                                <div id="mapL1Origen" class="height: 400px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
{{--                <button type="submit" class="btn btn-primary">Guardar</button>--}}
            </div>
        </div>
        </div>
    </div>
</form>
<form id="frmL1AddDestino">
    <div class="modal fade" id="mdlDestinoL1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-full" role="document">
        <div class="modal-content modal-content-full" style="">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L1</b> - Un lugar de retiro, un destino de entrega</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-header">
                                Datos de entrega
                            </div>
                            <div class="card-body">
                                <input type="hidden" name="idDestinoL1" id="idDestinoL1">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Nombre</label>
                                            <input type="text" class="form-control" name="l1_name" id="l1_name" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Teléfono</label>
                                            <input type="text" placeholder="Ej: +59521643528 ó +595 24 643 528" class="form-control" name="l1_phone" id="l1_phone" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="vat">Nota</label>
                                            <input type="text" class="form-control" name="l1_note" id="l1_note">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Direccion de entrega</label>
                                            <input type="text" class="form-control" name="l1_address" id="l1_address" required>
                                            <input type="hidden" class="form-control" name="l1_address_lat" id="l1_address_lat" required>
                                            <input type="hidden" class="form-control" name="l1_address_lng" id="l1_address_lng" required>
                                            <input type="hidden" class="form-control" name="l1_neighborhood" id="l1_neighborhood" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-header">Vista previa de ruta</div>
                            <div class="card-body">
                                <div id="mapL1Destino" class="height: 400px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
{{--                <button type="submit" class="btn btn-primary">Guardar</button>--}}
            </div>
        </div>
        </div>
    </div>
</form>
<!--Draw Route Google Maps -->
<div class="modal fade" id="mdlRouteL1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-full" role="document">
    <div class="modal-content modal-content-full" style="">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ruta : <b>L1</b> - Un lugar de retiro, un destino de entrega</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">Vista previa de ruta</div>
                        <div class="card-body">
                            <div id="mapL1Route" class="height: 400px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
    </div>
    </div>
</div>
