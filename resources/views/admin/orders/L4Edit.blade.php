<form id="frmL4AddOrigen">
    <div class="modal fade" id="mdlOrigenesL4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L4</b> - Varios lugares de retiro, Varios lugares de entrega</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12" id="frmL4AddOrigenForm">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <label style="margin-top: 8px">
                                                Agregar retiro
                                            </label>
                                            <button type="button" id="btnAddOrigenL4" class="btn btn-success float-right" style="margin-left: 5px;">
                                                <svg class="c-icon mr-2" style="cursor: pointer;">
                                                <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-save"></use>
                                                </svg>
                                            </button>
                                            <button type="button" id="btnAddOrigenL4Cancel" class="btn btn-secondary float-right">
                                                <svg class="c-icon mr-2" style="cursor: pointer;">
                                                <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-ban"></use>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="hidden" id="idOrigenL4">
                                                    <div class="form-group">
                                                        <label for="vat">Nombre</label>
                                                        <input type="text" class="form-control" name="name" id="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="vat">Teléfono</label>
                                                        <input type="number" class="form-control" name="phone" id="phone" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">Cantidad de paquetes</label>
                                                        <input type="number" class="form-control" name="quantity" id="quantity" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">Tipo de paquete</label>
                                                        <select name="type" id="type" class="form-control" required>
                                                            <option value="1">A</option>
                                                            <option value="2">B</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="vat">Nota</label>
                                                        <input type="text" class="form-control" name="note" id="note">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="">Dirección de retiro</label>
                                                        <input type="text" class="form-control" name="address" id="address" required>
                                                        <input type="hidden" class="form-control" name="address_lat" id="address_lat" required>
                                                        <input type="hidden" class="form-control" name="address_lng" id="address_lng" required>
                                                        <input type="hidden" class="form-control" name="neighborhood" id="neighborhood" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="card">
                                        <div class="card-header">Vista previa de ruta</div>
                                        <div class="card-body">
                                            <div id="mapL4Origenes" class="height: 400px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12" id="frmAddOrigenL4List">
                            <div class="card">
                                <div class="card-header">
                                    <label style="margin-top: 8px; font-weight: bolder;">
                                        Lista de Origenes
                                    </label>
                                    <button type="button" id="btnAddOrigenL4Add" class="btn btn-primary float-right" style="margin-left: 5px;">
                                        <svg class="c-icon mr-2" style="cursor: pointer;">
                                            <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-plus"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                           <tr>
                                                <th width="1">#</th>
                                                <th width="1">Paquetes</th>
                                                <th width="1">Disponibles</th>
                                                <th width="1">Asignados</th>
                                                <th>Remitente</th>
                                                <th>Dirección</th>
                                                <th width="80">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="listOrigenesL4"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnAddOrigenesL4Cancel">Cancelar</button>
                    <button type="submit" class="btn btn-primary" id="btnAddOrigenesL4Save">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</form>
<form id="frmL4AddDestinos">
    <div class="modal fade" id="mdlDestinoL4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L4</b> - Varios lugares de retiro, Un lugar de entrega</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12" id="frmL4AddDestinosForm">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-header">
                                        <label style="margin-top: 8px; font-weight: bolder;">
                                            Agregar Destino
                                        </label>
                                        <button type="button" id="btnAddDestinoL4" class="btn btn-success float-right" style="margin-left: 5px;">
                                            <svg class="c-icon mr-2" style="cursor: pointer;">
                                            <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-save"></use>
                                            </svg>
                                        </button>
                                        <button type="button" id="btnAddDestinoL4Cancel" class="btn btn-secondary float-right">
                                            <svg class="c-icon mr-2" style="cursor: pointer;">
                                            <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-ban"></use>
                                            </svg>
                                        </button>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="hidden" id="idDestinoL4">
                                                <div class="form-group">
                                                    <label for="vat">Nombre</label>
                                                    <input type="text" class="form-control" name="destino_name" id="destino_name" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="vat">Teléfono</label>
                                                    <input type="number" class="form-control" name="destino_phone" id="destino_phone" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="">Cantidad de paquetes</label>
                                                    <input type="number" class="form-control" name="destino_quantity" id="destino_quantity" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="vat">Nota</label>
                                                    <input type="text" class="form-control" name="destino_note" id="destino_note">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="">Dirección de retiro</label>
                                                    <input type="text" class="form-control" name="destino_address" id="destino_address" required>
                                                    <input type="hidden" class="form-control" name="destino_address_lat" id="destino_address_lat" required>
                                                    <input type="hidden" class="form-control" name="destino_address_lng" id="destino_address_lng" required>
                                                    <input type="hidden" class="form-control" name="destino_neighborhood" id="destino_neighborhood" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="card">
                                    <div class="card-header">Vista previa de ruta</div>
                                    <div class="card-body">
                                        <div id="mapL4Destinos" class="height: 400px; width: 100%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12" id="frmListDestinosOrigenesL4">
                        <div class="card">
                            <div class="card-header">
                                <label style="margin-top: 8px; font-weight: bolder;">
                                    Lista de Origenes
                                </label>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped">
                                    <thead>
                                       <tr>
                                            <th width="1">#</th>
                                            <th width="1">Paquetes</th>
                                            <th width="1">Disponibles</th>
                                            <th width="1">Asignados</th>
                                            <th width="1">Distancia</th>
                                            <th>Remitente</th>
                                            <th>Dirección</th>
                                            <th width="1">Destinos</th>
                                        </tr>
                                    </thead>
                                    <tbody id="listOrigenesDestinosL4"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12" id="frmAddDestinosL4List">
                        <div class="card">
                            <div class="card-header bg-dark-gray">
                                <input type="hidden" id="idOrigenDestinosL4">
                                <label style="margin-top: 8px; font-weight: bolder;">
                                    Lista de Destinos
                                </label>
                                <button type="button" id="btnAddDestinosSaveL4" class="btn btn-success float-right" style="margin-left: 5px;">
                                    <svg class="c-icon mr-2" style="cursor: pointer;">
                                    <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-save"></use>
                                    </svg>
                                </button>
                                <button type="button" id="btnAddDestinoL4Add" class="btn btn-primary float-right" style="margin-left: 5px;">
                                    <svg class="c-icon mr-2" style="cursor: pointer;">
                                        <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-plus"></use>
                                    </svg>
                                </button>
                                <button type="button" id="btnListAddDestinoL4Cancel" class="btn btn-secondary float-right">
                                    <svg class="c-icon mr-2" style="cursor: pointer;">
                                    <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-ban"></use>
                                    </svg>
                                </button>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped">
                                    <thead>
                                       <tr>
                                            <th width="1">#</th>
                                            <th width="1">Paquetes</th>
                                            <th width="1">Distancia</th>
                                            <th>Destinatario</th>
                                            <th>Direccion</th>
                                            <th width="80">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="listDestinosL4"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
        </div>
    </div>
</form>
