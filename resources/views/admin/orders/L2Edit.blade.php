<form id="frmL2AddOrigen">
    <div class="modal fade" id="mdlOrigenL2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L2</b> - Un lugar de retiro, varios destinos de entrega</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="idOrigenL2" id="idOrigenL2">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-header">
                                    Datos de retiro
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="vat">Nombre</label>
                                                <input type="text" class="form-control" name="name" id="name" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="vat">Teléfono</label>
                                                <input type="text" placeholder="Ej: +59521643528 ó +595 24 643 528" class="form-control" name="phone" id="phone" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="vat">Nota</label>
                                                <input type="text" class="form-control" name="note" id="note">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Cantidad de paquetes</label>
                                                <input type="number" class="form-control" name="quantity" id="quantity" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Tipo de paquete</label>
                                                <select name="type" id="type" class="form-control" required>
                                                    <option value="1">A</option>
                                                    <option value="2">B</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Direccion de recolección</label>
                                                <input type="text" class="form-control" name="address" id="address" required>
                                                <input type="hidden" class="form-control" name="address_lat" id="address_lat">
                                                <input type="hidden" class="form-control" name="address_lng" id="address_lng">
                                                <input type="hidden" class="form-control" name="neighborhood" id="neighborhood">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="card">
                                <div class="card-header">Vista previa de ruta</div>
                                <div class="card-body">
                                    <div id="mapL2Origen" class="height: 400px; width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</form>
<form id="frmL2AddDestinos">
    <div class="modal fade" id="mdlDestinosL2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Logistica : <b>L2</b> - Un lugar de retiro, varios destinos de entrega</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="idDestinoL2" id="idDestinoL2">
                    <div class="row">
                        <div class="col-sm-4" id="frmAddDestinoL2Form">
                            <div class="card">
                                <div class="card-header">
                                    <label style="margin-top: 8px">
                                        Agregar destinatario
                                    </label>
                                    <button type="submit" id="btnAddDestinoL2" class="btn btn-primary float-right" style="margin-left: 5px;">
                                        <svg class="c-icon mr-2" style="cursor: pointer;">
                                        <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-save"></use>
                                        </svg>
                                    </button>
                                    <button type="button" id="btnAddDestinoL2Cancel" class="btn btn-secondary float-right">
                                        <svg class="c-icon mr-2" style="cursor: pointer;">
                                        <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-ban"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="hidden" id="indexDestinoL2">
                                            <div class="form-group">
                                                <label for="vat">Nombre</label>
                                                <input type="text" class="form-control" name="name" id="name" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="vat">Teléfono</label>
                                                <input type="text" placeholder="Ej: +59521643528 ó +595 24 643 528" class="form-control" name="phone" id="phone" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Cantidad de paquetes</label>
                                                <input type="number" class="form-control" name="quantity" id="quantity" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="vat">Nota</label>
                                                <input type="text" class="form-control" name="note" id="note">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Dirección de retiro</label>
                                                <input type="text" class="form-control" name="address" id="address" required>
                                                <input type="hidden" class="form-control" name="address_lat" id="address_lat" required>
                                                <input type="hidden" class="form-control" name="address_lng" id="address_lng" required>
                                                <input type="hidden" class="form-control" name="neighborhood" id="neighborhood" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" id="frmAddDestinoL2List">
                            <div class="card">
                                <div class="card-header">
                                    <label style="margin-top: 8px">
                                        Lista de destinos[Retiro:<b id="idLimitePaquetes"></b>/Entrega:<b id="idPaquetesAsignados">0</b>]
                                    </label>
                                    <button type="button" id="btnAddDestinoL2Add" class="btn btn-primary float-right" style="margin-left: 5px;">
                                        <svg class="c-icon mr-2" style="cursor: pointer;">
                                            <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-plus"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                           <tr>
                                                <th width="1">#</th>
                                                <th width="1">Paqtes</th>
                                                <th width="1">Km's</th>
                                                <th width="80">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="listDestinosL2"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="card">
                                <div class="card-header">Vista previa de ruta</div>
                                <div class="card-body">
                                    <div id="mapL2Destino" class="height: 400px; width: 100%;"></div>
                                    <div id="mapL2Destinos" class="height: 400px; width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnAddDestinosL2Cancel">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="btnAddDestinosL2Save">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</form>
<!--Draw Route Google Maps -->
<div class="modal fade" id="mdlRouteL2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="width: 195%!important;margin-left: -47%;">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ruta : <b>L2</b> - Ruta de retiro y entrega</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">Vista previa de ruta</div>
                        <div class="card-body">
                            <div id="mapL2Route" class="height: 400px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
    </div>
    </div>
</div>
