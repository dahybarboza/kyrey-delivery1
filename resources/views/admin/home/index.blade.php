@extends('admin.layout.main')
@section('content')
<div class="card">
    <div class="card-header"><strong>Dashboard</strong>
      <div class="row">
          <div class="col-sm-12">
            <button class="btn btn-info float-right" id="btnUpdate" style="cursor: pointer;">
                <svg class="c-icon mr-2">
                    <use xlink:href="{!! Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-reload') !!}"></use>
                </svg>
            </button>
          </div>
      </div>
    </div>
    <div class="card-body">

      <div class="card-group mb-4">
        <div class="card">
          <div class="card-body font-weight-bold">
            <div class="text-muted text-right mb-4">
                Sucursales
              <svg class="c-icon c-icon-2xl">
                <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-fork') }}"></use>
              </svg>
            </div>
            <div class="text-value-lg" id="count_branchoffices">0</div>
          </div>
          <div class="card-footer px-3 py-2">
            <a class="btn-block text-muted d-flex justify-content-between align-items-center" style="text-decoration:none;" href="{{Asset('').env('admin').'/branchoffices'}}"><span class="small font-weight-bold">Ver más</span>
              <svg class="c-icon">
                <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-chevron-right') }}"></use>
              </svg>
            </a>
          </div>
        </div>
        <div class="card">
          <div class="card-body font-weight-bold">
            <div class="text-muted text-right mb-4">
                Repartidores
              <svg class="c-icon c-icon-2xl">
                <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-truck') }}"></use>
              </svg>
            </div>
            <div class="text-value-lg" id="count_dealers">0</div>
          </div>
          <div class="card-footer px-3 py-2">
            <a class="btn-block text-muted d-flex justify-content-between align-items-center" style="text-decoration:none;" href="{{Asset('').env('admin').'/dealers'}}"><span class="small font-weight-bold">Ver más</span>
              <svg class="c-icon">
                <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-chevron-right') }}"></use>
              </svg>
            </a>
          </div>
        </div>
        <div class="card">
            <div class="card-body font-weight-bold">
                <div class="text-muted text-right mb-4 ">
                    Clientes
                  <svg class="c-icon c-icon-2xl">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-people') }}"></use>
                  </svg>
                </div>
                <div class="text-value-lg" id="count_customers">0</div>
            </div>
            <div class="card-footer px-3 py-2">
            <a class="btn-block text-muted d-flex justify-content-between align-items-center" style="text-decoration:none;" href="{{Asset('').env('admin').'/customers'}}"><span class="small font-weight-bold">Ver más</span>
              <svg class="c-icon">
                <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-chevron-right') }}"></use>
              </svg>
            </a>
          </div>
        </div>
        <div class="card">
            <div class="card-body bg-warning text-white font-weight-bold">
                <div class="text-muted text-right mb-4">
                    Ordenes en curso
                  <svg class="c-icon c-icon-2xl">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-layers') }}"></use>
                  </svg>
                </div>
                <div class="text-value-lg" id="count_orders">0</div>
            </div>
            <div class="card-footer px-3 py-2">
              <a class="btn-block text-muted d-flex justify-content-between align-items-center" style="text-decoration:none;" href="{{Asset('').env('admin').'/orders'}}"><span class="small font-weight-bold">Ver más</span>
                <svg class="c-icon">
                  <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-chevron-right') }}"></use>
                </svg>
              </a>
            </div>
        </div>
        <div class="card">
            <div class="card-body bg-success text-white font-weight-bold" style="background:#ED502E;">
                <div class="text-muted text-right mb-4">
                    Ordenes entregadas
                  <svg class="c-icon c-icon-2xl">
                    <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-check') }}"></use>
                  </svg>
                </div>
                <div class="text-value-lg" id="count_orders_success">0</div>
            </div>
            <div class="card-footer px-3 py-2">
            <a class="btn-block text-muted d-flex justify-content-between align-items-center" style="text-decoration:none;" href="{{Asset('').env('admin').'/orders'}}"><span class="small font-weight-bold">Ver más</span>
              <svg class="c-icon">
                <use xlink:href="{{ Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-chevron-right') }}"></use>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="card">
    <div class="card-header"><strong>Indicadores de calidad, basados en <label id="countServices">0</label> servicios brindados.</strong>
      <div class="row">
          <div class="col-sm-12">
            <button class="btn btn-info float-right" id="btnUpdateRates" style="cursor: pointer;">
                <svg class="c-icon mr-2">
                    <use xlink:href="{!! Asset('dashboard/vendors/@coreui/icons/svg/free.svg#cil-reload') !!}"></use>
                </svg>
            </button>
          </div>
      </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Forma</div>
                    <div class="card-body">
                        <div class="c-chart-wrapper">
                        <canvas id="canvas-forma"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Tiempo</div>
                    <div class="card-body">
                        <div class="c-chart-wrapper">
                        <canvas id="canvas-tiempo"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Amabilidad</div>
                    <div class="card-body">
                        <div class="c-chart-wrapper">
                        <canvas id="canvas-amabilidad"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Protocolo Sanitario</div>
                    <div class="card-body">
                        <div class="c-chart-wrapper">
                        <canvas id="canvas-salud"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
