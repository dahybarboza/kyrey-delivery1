@extends('admin.layout.main')
@section('content')
<input type="hidden" id="route" value="{{ env('store') }}">
    {!! Form::model(null,['id'=>"frmProfile"]) !!}
    <div class="card">
        <div class="card-header"><strong>Detalles de cuenta</strong></div>
        <div class="card-body">

            <div class="row">
                <div class="col-sm-2 text-center">
                    <img src="{!! Asset('').$data->photo !!}" id="previewDefault" class="profilePreview">
                    <hr>
                    <input type="file" name="picture" id="picture">
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Nombre</label>
                                <input class="form-control" id="name" name="name" type="text" value="{{ $data->name }}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company">Empresa</label>
                                <input class="form-control" id="enterprise" name="enterprise" type="text" value="{{ $data->enterprise }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Email</label>
                                <input class="form-control" type="text" value="{{ $data->email }}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company">Teléfono</label>
                                <input class="form-control" id="phone" name="phone" type="text" minlength="10" maxlength="15" value="{{ $data->phone }}" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="vat"> <b>Contraseña actual</b> (Requerido para actualizar datos)</label>
                        <input class="form-control" id="password" name="password" type="password" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="company"><b>Contraseña nueva</b></label>
                        <input class="form-control" id="newpassword" name="newpassword" type="password" minlength="8">
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-ghost-success active" type="submit" aria-pressed="true">Actualizar</button>
        </div>
    </div>
    {{ Form::close() }}
@endsection
