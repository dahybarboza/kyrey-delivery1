@extends('admin.layout.main')
@section('content')
    {!! Form::model(null,['id'=>"frmNew"]) !!}
    <div class="card">
        <div class="card-header"><strong>Nueva ruta</strong></div>
        <div class="card-body">

            <div class="card">
                <div class="card-header">
                    Detalles
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="customers_id">Cliente</label>
                                <select type="text" class="form-control" name="customers_id" id="customers_id" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    @foreach($customers as $c)
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="branchoffice_id">Sucursal</label>
                                <select type="text" class="form-control" name="branchoffice_id" id="branchoffice_id" required>
{{--                                    <option value="" disabled selected>Seleccionar</option>--}}
{{--                                    @foreach($branchoffices as $branchoffice)--}}
{{--                                        <option value="{{$branchoffice->id}}">{{$branchoffice->name}}</option>--}}
{{--                                    @endforeach--}}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="dealer_id">Repartidor / Delivery</label>
                                <select type="text" class="form-control" name="dealer_id" id="dealer_id" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                        @foreach($dealers as $dealer)
                                            <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="dia_id">Día / Periodicidad</label>
                                <select type="text" class="form-control" name="dia" id="dia" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    <option value="1">Lunes</option>
                                    <option value="2">Martes</option>
                                    <option value="3">Miércoles</option>
                                    <option value="4">Jueves</option>
                                    <option value="5">Viernes</option>
                                    <option value="6">Sábado</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Horario programado, rango y estado
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="hora_entrada_id">Hora de entrada</label>
                                <input type="time" class="form-control" min="07:00" max="23:00" name="hora_entrada" id="hora_entrada" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="hora_salida_id">Hora de salida</label>
                                <input type="time" class="form-control" min="07:00" max="23:00" name="hora_salida" id="hora_salida" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="en_rango">En Rango</label>
                                <select type="text" class="form-control" name="en_rango" id="en_rango" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="status">Estado</label>
                                <select type="text" class="form-control" name="status" id="status" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Ubicación
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="">Dirección</label>
                                <input type="text" class="form-control" name="l1_address" id="l1_address" required>
                                <input type="hidden" class="form-control" name="l1_address_lat" id="l1_address_lat" required>
                                <input type="hidden" class="form-control" name="l1_address_lng" id="l1_address_lng" required>
                                <input type="hidden" class="form-control" name="l1_neighborhood" id="l1_neighborhood" required>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <label for="">Vista previa de la ruta</label>
                            <div id="mapL1Origen" class="height: 400px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Datos adicionales
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="">Observaciones</label>
                            <textarea class="form-control" name="observaciones" id="observaciones" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Fin Bloque-->
            @php
            /*
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header"><strong>Lista de paquetes</strong>
                            <button type="button" href="#" data-toggle="modal" data-target="#mdlTypeLogistic" class="btn btn-info" style="float:right;">
                                <svg class="c-icon" style="cursor: pointer;">
                                    <use xlink:href="{!! Asset('') !!}dashboard/vendors/@coreui/icons/svg/free.svg#cil-plus"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover" id="arrData" style="width:100%;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="1">Tipo</th>
                                    <th width="1" class="text-center">Paquetes</th>
                                    <th width="1" class="text-center">Origenes</th>
                                    <th width="1" class="text-center">Destinos</th>
                                    <th width="1" class="text-center">Distancia</th>
                                    <th width="20" class="text-center">Total</th>
                                    <th width="1" class="text-center">Acciones</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <h5 for="" id="totalPreview">₲0</h5>
                </div>
            </div>
            */
            @endphp

        </div>
        <div class="card-footer">
            <a href="{!! Asset('').env('admin').'/rutas' !!}" id="btnCancelOrder" class="btn btn-secondary float-right">Cancelar</a>
            <button type="submit" class="btn btn-success btn-guardar float-right mr-1">Guardar</button>
        </div>
    </div>
    {{ Form::close() }}


    @php
        /*


    <div class="modal fade" id="mdlTypeLogistic" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tipo de logistica</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="1"><b>L1</b> - Un lugar de retiro, un destino de entrega.</button>
                    {{--             <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="2"><b>L2</b> - Un lugar de retiro, varios destinos de entrega.</button>--}}
                    {{--             <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="3"><b>L3</b> - varios lugares de retiro, un destino de entrega.</button>--}}
                    {{--             <button class="btn btn-light btn-block text-left" id="btnLogistic" data-index="4"><b>L4</b> - varios lugares de retiro, varios destinos de entrega.</button>--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnLogisticCancel">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="map"></div>
        </div>
    </div>

    */

    @endphp

    <div class="row">
        <div class="col-sm-12">
            <div id="map"></div>
        </div>
    </div>

    {{--    @include('admin/orders/L1')--}}
    {{--    @include('admin/orders/L2')--}}
    {{--    @include('admin/orders/L3')--}}
    {{--    @include('admin/orders/L4')--}}
@endsection
