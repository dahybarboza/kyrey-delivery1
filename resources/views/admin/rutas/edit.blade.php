@extends('admin.layout.main')
@section('content')
    {!! Form::model(null,['id'=>"frmEdit"]) !!}
    <input type="hidden" id="idRuta" value="{{$id}}">
    <div class="card">
        <div class="card-header"><strong>Editar ruta</strong></div>
        <div class="card-body">

            <div class="card">
                <div class="card-header">
                    Detalles
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="customers_id">Cliente</label>
                                <select type="text" class="form-control" name="customers_id" id="customers_id" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    @foreach($customers as $c)
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="branchoffice_id">Sucursal</label>
                                <select type="text" class="form-control" name="branchoffice_id" id="branchoffice_id" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    @foreach($branchoffices as $branchoffice)
                                        <option value="{{$branchoffice->id}}">{{$branchoffice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="dealer_id">Repartidor / Delivery</label>
                                <select type="text" class="form-control" name="dealer_id" id="dealer_id" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    @foreach($dealers as $dealer)
                                        <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="dia">Día / Periodicidad</label>
                                <select type="text" class="form-control" name="dia" id="dia" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    <option value="1">Lunes</option>
                                    <option value="2">Martes</option>
                                    <option value="3">Miércoles</option>
                                    <option value="4">Jueves</option>
                                    <option value="5">Viernes</option>
                                    <option value="6">Sábado</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Horario programado, rango y estado
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="hora_entrada">Hora de entrada</label>
                                <input type="time" class="form-control" min="07:00" max="23:00" name="hora_entrada" id="hora_entrada" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="hora_salida">Hora de salida</label>
                                <input type="time" class="form-control" min="07:00" max="23:00" name="hora_salida" id="hora_salida" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="en_rango">En Rango</label>
                                <select type="text" class="form-control" name="en_rango" id="en_rango" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    <option value="1">Si</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="status">Estado</label>
                                <select type="text" class="form-control" name="status" id="status" required>
                                    <option value="" disabled selected>Seleccionar</option>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Ubicación
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="">Dirección</label>
                                <input type="text" class="form-control" name="l1_address" id="l1_address" required >
                                <input type="hidden" class="form-control" name="l1_address_lat" id="l1_address_lat" required>
                                <input type="hidden" class="form-control" name="l1_address_lng" id="l1_address_lng" required>
                                <input type="hidden" class="form-control" name="l1_neighborhood" id="l1_neighborhood" required>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <label for="">Vista previa de la ruta</label>
                            <div id="mapL1Origen" class="height: 400px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Datos adicionales
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="">Observaciones</label>
                            <textarea class="form-control" name="observaciones" id="observaciones" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Fin Bloque-->


        </div>
        <div class="card-footer">
            <a href="{!! Asset('').env('admin').'/rutas' !!}" id="btnCancelOrder" class="btn btn-secondary float-right">Cancelar</a>
            <button type="submit" class="btn btn-danger btn-guardar float-right mr-1">Editar</button>
        </div>
    </div>
    {{ Form::close() }}

    <div class="row">
        <div class="col-sm-12">
            <div id="map"></div>
        </div>
    </div>

@endsection
