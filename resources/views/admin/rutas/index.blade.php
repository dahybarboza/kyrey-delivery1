@extends('admin.layout.main')
@section('content')
    <div class="card">
        <div class="card-header"><strong>Rutas</strong>
{{--            <button class="btn btn-dark" style="float:right;" data-toggle="modal" data-target="#mdlNew" id="btnNew">Agregar nueva ruta</button>--}}
            <a class="btn btn-dark"  href="{{Asset('').env('admin').'/rutas/create'}}" style="float:right;" >Agregar nueva ruta</a>
        </div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Cliente</th>
                    <th>Sucursal</th>
                    <th>Día</th>
                    <th>Delivery</th>
                    <th>Entrada P.</th>
                    <th>Salida P.</th>
                    <th>Entrada Real</th>
                    <th>Salida Real</th>
                    <th>Observaciones</th>
                    <th>En rango</th>
                    <th>Estado</th>
                    <th>Fecha</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <script>
        {{--window.getData = function () {--}}
        {{--    tblData.rows().remove().draw();--}}
        {{--    Core.get(route + "/all").then(function (res) {--}}
        {{--        //console.log(res);--}}
        {{--        //return false;--}}
        {{--        arrData = res.data.data;--}}

        {{--        Core = {--}}
        {{--            crud: {--}}
        {{--                getAll: function() {--}}
        {{--                    return axios.get(urlWeb + "/" + route + '/all');--}}
        {{--                },--}}
        {{--                create: function() {--}}
        {{--                    return axios.post(urlWeb + "/" + route + '/create', itemData);--}}
        {{--                },--}}
        {{--                find: function(id) {--}}
        {{--                    return axios.get(urlWeb + "/" + route + '/find/' + id);--}}
        {{--                },--}}
        {{--                update: function(id) {--}}
        {{--                    return axios.post(urlWeb + "/" + route + '/update/' + id, itemData);--}}
        {{--                },--}}
        {{--                destroy: function(id) {--}}
        {{--                    return axios.get(urlWeb + "/" + route + '/destroy/' + id);--}}
        {{--                }--}}
        {{--            },--}}

        {{--            buttons: function(id, index) {--}}
        {{--                buttons =--}}
        {{--                    `<center>--}}
        {{--                     <a href="${urlBase + route + "/edit/" + id}">--}}
        {{--                        <svg class="c-icon mr-2" id="btnEdit" data-index='${id}' style="cursor: pointer;">--}}
        {{--                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>--}}
        {{--                        </svg>--}}
        {{--                     </a>--}}

        {{--                     <svg class="c-icon mr-2" id="btnDelete" data-index='${index}'  style="cursor: pointer;">--}}
        {{--                        <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>--}}
        {{--                    </svg>--}}


        {{--                </center>`;--}}
        {{--                return buttons;--}}
        {{--            },--}}
        {{--        }--}}

        {{--        res.data.data.forEach(function (item, index) {--}}
        {{--            tblData.row.add([--}}
        {{--                index,--}}
        {{--                item.customer.name,--}}
        {{--                item.branchoffice.name,--}}
        {{--                item.dia,--}}
        {{--                item.dealer.name,--}}
        {{--                item.hora_inicio_programado,--}}
        {{--                item.hora_fin_programado,--}}
        {{--                item.hora_inicio_real,--}}
        {{--                item.hora_fin_real,--}}
        {{--                item.observaciones,--}}
        {{--                item.en_rango == 1 ? 'Si' : 'No',                        --}}
        {{--                {{ (new Carbon\Carbon(item.created_at))->format('d-m-y') }},--}}
        {{--                item.status == 1 ? 'Activo' : 'Inactivo',--}}
        {{--                Core.buttons(item.id, index)--}}
        {{--            ]).draw();--}}
        {{--        });--}}
        {{--    }).catch(function (err) {--}}
        {{--        console.log(err);--}}
        {{--        Core.showToastStr('error', 'No ha sido posible cargar la tabla.');--}}
        {{--    });--}}
        {{--}--}}
    </script>

@endsection
