@extends('admin.layout.main')
@section('content')
    <div class="card">
        <div class="card-header"><strong>Repartidores</strong>
            <button class="btn btn-dark" style="float:right;" data-toggle="modal" data-target="#mdlNew" id="btnNew">Agregar nuevo repartidor</button>
        </div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Foto</th>
                    <th>Nombre</th>
                    <th>Cedula</th>
                    {{--                    <th>Sucursal</th>--}}
                    <th >Télefono</th>
                    <th >Estado</th>
                    <th width="1">Acciones</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    {!! Form::model(null,['id'=>"frmNew"]) !!}
    <div class="modal fade" id="mdlNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg	" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo repartidor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-sm-3 text-center">
                            <img src="{!! Asset('dashboard/assets/img/default_user.png') !!}" id="previewDefaultEdit" class="profilePreview">
                        </div>
                    <!--
                        <div class="col-sm-3 text-center">
                            <img src="{!! Asset('dashboard/assets/img/default_user.png') !!}" id="previewDefault" class="profilePreview">
                            <hr>
                            <input type="file" name="picture" id="picture">
                        </div>
                        -->

                        <div class="col-sm-9">
                            {{--                <div class="row">--}}
                            {{--                    <div class="col-sm-12">--}}
                            {{--                        <div class="form-group">--}}
                            {{--                            <label for="vat">Sucursal</label>--}}
                            {{--                            <select class="form-control" name="branchoffice_id" id="branchoffice_id" required>--}}
                            {{--                                <option value="">Seleccionar</option>--}}
                            {{--                                @foreach($branchoffices as $key => $branchoffice)--}}
                            {{--                                    <option value="{!! $branchoffice->id !!}">{!! $branchoffice->name !!}</option>--}}
                            {{--                                @endforeach--}}
                            {{--                            </select>--}}
                            {{--                        </div>--}}
                            {{--                    </div>--}}
                            {{--                </div>--}}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="company">Nombre y Apellido</label>
                                        <input class="form-control" id="name" name="name" type="text" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="company">Cédula - RUC</label>
                                        <input class="form-control" id="cedula" name="cedula" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company">Teléfono</label>
                                <input class="form-control" id="phone" name="phone" type="text" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Correo electrónico</label>
                                <input class="form-control" id="email" name="email" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="ccmonth">Contraseña (Longitud mínima 8 caracteres)</label>
                                <input class="form-control" id="password" name="password" type="password" placeholder="" required required minlength="8">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Estado</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    {!! Form::model(null,['id'=>"frmEdit"]) !!}
    <div class="modal fade" id="mdlEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar repartidor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="id" required>
                    <div class="row">

                        <div class="col-sm-3 text-center">
                            <img src="{!! Asset('dashboard/assets/img/default_user.png') !!}" id="previewDefaultEdit" class="profilePreview">
                        </div>

                        {{--   <div class="col-sm-3 text-center">
                             <img src="{!! Asset('dashboard/assets/img/default_user.png') !!}" id="previewDefaultEdit" class="profilePreview">
                             <hr>
                             <input type="file" name="picture" id="picture">
                         </div>--}}

                        <div class="col-sm-9">
                            {{--                        <div class="row">--}}
                            {{--                            <div class="col-sm-12">--}}
                            {{--                                <div class="form-group">--}}
                            {{--                                    <label for="vat">Sucursal</label>--}}
                            {{--                                    <select class="form-control" name="branchoffice_id" id="branchoffice_id" required>--}}
                            {{--                                        <option value="">Seleccionar</option>--}}
                            {{--                                        @foreach($branchoffices as $key => $branchoffice)--}}
                            {{--                                            <option value="{!! $branchoffice->id !!}">{!! $branchoffice->name !!}</option>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    </select>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="company">Nombre</label>
                                        <input class="form-control" id="name" name="name" type="text" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="company">Cédula / RUC</label>
                                        <input class="form-control" id="cedula" name="cedula" type="text" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="form-group">
                                <label for="company">Teléfono</label>
                                <input class="form-control" id="phone" name="phone" type="text" required>
                            </div>



                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Correo electrónico</label>
                                <input class="form-control" id="email" name="email" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="ccmonth">Contraseña (Longitud minima 8)</label>
                                <input class="form-control" id="password" name="password" type="password" placeholder="" required minlength="8">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Estado</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection