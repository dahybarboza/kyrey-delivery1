@extends('admin.layout.main')
@section('content')
    <div class="card">
        <div class="card-header"><strong>Ordenes Delivery</strong>
            <button class="btn btn-dark" style="float:right;" data-toggle="modal" data-target="#mdlNew" id="btnNew">Nuevo</button>

            <button class="btn btn-dark float-right" id="btnUpdate" style="cursor: pointer; margin-right:5px;">
                Actualizar
            </button>
        </div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
                <thead>
                <tr>
                    <th>#</th>
                    <th width="1">Folio</th>
                    <th>Sucursal</th>
                    <th>Repatidor</th>
                    <th>Cliente</th>
                    <th width="1">Paquetes</th>
                    <th width="1">Total</th>
                    <th width="1">Estatus</th>
                    <th width="80">Acciones</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    {!! Form::model(null,['id'=>"frmNew"]) !!}
    <div class="modal fade" id="mdlNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva orden Delivery</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Sucursal</label>
                                <select type="text" class="form-control" name="branchoffice_id" id="branchoffice_id">
                                    <option value="">Seleccionar</option>
                                    @foreach($branchoffices as $branchoffice)
                                        <option value="{{$branchoffice->id}}">{{$branchoffice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Repartidor</label>
                                <select type="text" class="form-control" name="dealer_id" id="dealer_id">
                                    <option value="">Seleccionar</option>
                                    @foreach($dealers as $dealer)
                                        <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company">Nombre Cliente </label>
                                <input class="form-control" id="name" name="name" type="text" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{--  <label for="company" title="Debe respetar el formato indicado, de lo contrario la función de notificaciones no estará operativa para la orden actual.">
                                     <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' style="cursor: pointer;">
                                         <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-info"></use>
                                     </svg> Teléfono:</label>
                                 <input class="form-control" id="phone" name="phone" placeholder="Ej: +59521643528 ó +595 24 643 528" type="text" required>
                             --}}
                                <label for="company">Teléfono</label>
                                <input class="form-control" id="phone" name="phone" type="text" placeholder="Ej: +59521643528 ó +595 24 643 528" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label  for="ccmonth">Dirección</label>

                                <input class="form-control" id="address" name="address" type="text" required>
                                <input type="hidden" id="lat" name="lat" required>
                                <input type="hidden" id="lng" name="lng" required>
                                <input type="hidden" name="neighborhood" id="neighborhood" required>
                                {{--
                                <input type="text" class="form-control" id="address" name="address"  placeholder="" >
                                <input type="hidden" class="form-control" name="lat" id="lat" >
                                <input type="hidden" class="form-control" name="lng" id="lng" >
                                 --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="note">Nota de entrega:</label>
                                <textarea class="form-control" name="note" id="note" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Paquetes</label>
                                <input type="number"
                                       class="form-control" name="quantity" id="quantity" aria-describedby="helpId" placeholder="Número de paquetes">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Costo</label>
                                <input type="text"
                                       class="form-control" name="price" id="price" aria-describedby="helpId" placeholder="Costo del servicio">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="vat">Estatus</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="0">Pendiente</option>
                                    <option value="1">Entregando</option>
                                    <option value="2">Entregado</option>
                                    <option value="3">Cancelado</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    {!! Form::model(null,['id'=>"frmEdit"]) !!}
    <div class="modal fade" id="mdlEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar orden Delivery</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id" required>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Sucursal</label>
                                <select type="text" class="form-control" name="branchoffice_id" id="branchoffice_id">
                                    <option value="">Seleccionar</option>
                                    @foreach($branchoffices as $branchoffice)
                                        <option value="{{$branchoffice->id}}">{{$branchoffice->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="vat">Repartidor</label>
                                <select type="text" class="form-control" name="dealer_id" id="dealer_id">
                                    <option value="">Seleccionar</option>
                                    @foreach($dealers as $dealer)
                                        <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company">Nombre</label>
                                <input class="form-control" id="name" name="name" type="text" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company" title="Debe respetar el formato indicado, de lo contrario la función de notificaciones no estará operativa para la orden actual.">
                                    <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' style="cursor: pointer;">
                                        <use xlink:href="{{Asset('')}}dashboard/vendors/@coreui/icons/svg/free.svg#cil-info"></use>
                                    </svg> Teléfono:</label>
                                <input class="form-control" id="phone" name="phone" placeholder="Ej: +59521643528 ó +595 24 643 528" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="ccmonth">Dirección</label>
                                <input class="form-control" id="address" name="address" type="text" placeholder="" required>
                                <input type="hidden" class="form-control" name="lat" id="lat" required>
                                <input type="hidden" class="form-control" name="lng" id="lng" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="note">Nota de entrega:</label>
                                <textarea class="form-control" name="note" id="note" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Paquetes</label>
                                <input type="number"
                                       class="form-control" name="quantity" id="quantity" aria-describedby="helpId" placeholder="Número de paquetes">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Costo</label>
                                <input type="text"
                                       class="form-control" name="price" id="price" aria-describedby="helpId" placeholder="Costo del servicio">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="vat">Estatus</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="0">Pendiente</option>
                                    <option value="1">Entregando</option>
                                    <option value="2">Entregado</option>
                                    <option value="3">Cancelado</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection
