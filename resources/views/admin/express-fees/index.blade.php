@extends('admin.layout.main')
@section('content')
{!! Form::model(null,['id'=>"frmEdit"]) !!}
<div class="card">
    <input type="hidden" id="idFees">
    <div class="card-body">
        <h4 class="card-title">Configuración tarifas de servicio express</h4>
        <div class="row">
          <div class="col-sm-4">
            <div class="card">
              <div class="card-header">
                Tarifa preferencial dentro de Asunción
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="">Precio por orden de envío</label>
                  <input type="text" class="form-control" name="local_price" id="local_price" min="0" required>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="card">
              <div class="card-header">
                Tarifa dinámica
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6" hidden>
                    <div class="form-group">
                      <label for="">Precio base</label>
                      <input type="number" class="form-control" name="external_price" id="external_price" min="0" value="0" required>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="">Precio por Km</label>
                      <input type="number" class="form-control" name="external_price_km" id="external_price_km" min="0" required>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="card-footer text-right">
      <button type="submit" class="btn btn-success">Guardar</button>
    </div>
</div>
{{ Form::close() }}
@endsection
