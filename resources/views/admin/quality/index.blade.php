@extends('admin.layout.main')
@section('content')
    <div class="card">
        <div class="card-header"><strong>Indicadores de calidad de servicio</strong>
            <a href="{{Asset('').env('admin').'/orders/create'}}" class="btn btn-dark" style="float:right;" >Nuevo</a>
            <button class="btn btn-dark float-right" id="btnUpdate" style="cursor: pointer; margin-right:5px;">
                Actualizar
            </button>
        </div>
        <div class="card-body">
            <table class="table table-hover" id="arrData" style="width:100%;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="120">Folio de orden</th>
                        <th width="1">Forma</th>
                        <th width="1">Tiempo</th>
                        <th width="150">Protocolo de Salud</th>
                        <th>Comentario/Sugerencia</th>
                        <th width="80">Acciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
