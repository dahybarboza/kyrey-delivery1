Core = {
    status: function(status) {
        switch (parseInt(status)) {
            case 0:{
                return 'Inactivo';
                break;
            }
            case 1:{
                return 'Activo';
                break;
            }
        }
    },
    post: function(url, data) {
        return axios.post(urlWeb + "/" + url, data);
    },
    get: function(url) {
        return axios.get(urlWeb + "/" + url);
    },
    buttons: function(index) {
        buttons = `<center>
                        <svg class="c-icon mr-2" id="btnEdit" data-index='${index}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                        </svg>
                        <svg class="c-icon mr-2" id="btnDelete" data-index='${index}' style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                        </svg>
                    </center>`;
        return buttons;
    },
    setDataTable: function(name) {
        tblData = $('#' + name).DataTable({
            lengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, 'Todo']
            ],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
                pageLength: 5
            },
            "scrollX": true,
            "autoWidth": true,
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
            }]
        });
    },
    setDataTable2: function(name) {
        tblData2 = $('#' + name).DataTable({
            lengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, 'Todo']
            ],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
                pageLength: 5
            },
            "scrollX": true,
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
            }]
        });
    },
    setDataTableCustome: function(id){
        var tableCUstome = $(`#${id}`).DataTable({
            lengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, 'Todo']
            ],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
                pageLength: 5
            },
            "scrollX": true,

        });
        return tableCUstome;
    },
    clearForm: function(form) {
        $(form)[0].reset();
    },
    config:{
        getUrlWeb:  urlWeb,
        getUrlBase: urlBase
    },
    //CRUD ITEMS
    crud: {
        getAll: function() {
            return axios.get(urlWeb + "/" + route + '/all');
        },
        create: function() {
            return axios.post(urlWeb + "/" + route + '/create', itemData);
        },
        find: function(id) {
            return axios.get(urlWeb + "/" + route + '/find/' + id);
        },
        update: function(id) {
            return axios.post(urlWeb + "/" + route + '/update/' + id, itemData);
        },
        destroy: function(id) {
            return axios.get(urlWeb + "/" + route + '/destroy/' + id);
        },

    },
    showToastStr: function(type, message) {
        switch(type){
            case "error":{
                toastr.error(message);
                break;
            }
            case "success":{
                toastr.success(message);
                break;
            }
            case "info":{
                toastr.info(message);
                break;
            }
            case "warning":{
                toastr.warning(message);
                break;
            }
        }
    },
    showAlert: function(type, data) {
        switch(type){
            case "error":{
                Swal.fire({
                    icon:'error',
                    title:'Algo no ha ido bien...',
                    text:data
                });
                break;
            }
            case "success":{
                Swal.fire({
                    icon:'success',
                    title:'Acción realizada correctamente.',
                    text:data
                });
                break;
            }
            case "info":{
                Swal.fire({
                    icon:'info',
                    title:'Importante',
                    text:data
                });
                break;
            }
            case "warning":{
                Swal.fire({
                    icon:'warning',
                    title:'Advertencia',
                    text:data
                });
                break;
            }
        }
    },
    setMiles: function(x) {
        x = parseFloat(x).toFixed(0);
        x = x+"";
      var parts = x.toString().split(",");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      return parts.join(".");
    },
    showProccessingMessage: function (message){
        $.blockUI({
            message: message,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            baseZ: 4000});
     },
    hideProccessingMessage: function (){
         $.unblockUI();
      },
    getSearchParams: function(k){
        var p={};
        location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
        return k?p[k]:p;
       },
    recargarConCache: function(){
        location.reload()
    },
    recargarSinCache(){
        location.reload(true)
    },
    getUser: {
        Id: function(){
            return sessionStorage.getItem('user');
        },
        Level: function(){
            return sessionStorage.getItem('level');
        }
    },
    orderStatusText(status){
        switch(status){
            case 0:{  status = 'Nueva'; break;}
       //     case 1:{  status = 'Aprovada'; break;}
            case 1:{  status = 'Asignada'; break;}
            case 2:{  status = 'Entregando'; break;}
            case 3:{  status = 'Entregada'; break;}
            case 4:{  status = 'Rechazada'; break;}
        }
        return status;
    },
    getDistancia : function(data) {
            rad = function(x) {return x*Math.PI/180;}
            var R = 6378.137; //Radio de la tierra en km
            var dLat = rad( data.b.lat - data.a.lat );
            var dLong = rad( data.b.lng - data.a.lng );
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(data.a.lat)) * Math.cos(rad(data.b.lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c;
            return d.toFixed(3); //Return with tree decimals
    },
    print: function(item){
        var divToPrint=document.getElementById(item);
        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write(`
            <html>
                <head>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                </head>
                <body onload="window.print()">
                ${divToPrint.innerHTML}
                </body>
            </html>`);
        newWin.document.close();
        setTimeout(function(){newWin.close();},10);
    },
    getGeocoder: function(dataGps) {
        const promise = new Promise((resolve, reject)  => {
            const geocoder = new google.maps.Geocoder();
            geocoder.geocode({ location: dataGps }, (results, status) => {
                if (status === "OK") {
                    if (results[0]) {
                        resolve(results[0]);
                    } else {
                        reject(false);
                    }
                    } else {
                        reject(false)
                    }
            });
        });
        return promise;
    },
    setLocalStorage: function(name,data) {
        let promise = new Promise((resolve,reject) => {
            localStorage.setItem(name,JSON.stringify(data));
            resolve(true);
        });
        return promise;
    },
    getLocalStorage: function(name) {
        let promise = new Promise((resolve,reject) => {
            data = JSON.parse(localStorage.getItem(name));
            if(data){
                resolve(data);
            } else{
                reject(false);
            }
        });
        return promise;
    },
    removeLocalStorage: function(name) {
        let promise = new Promise((resolve,reject) => {
            if(Array.isArray(name)) {
                name.map(function(item){
                    localStorage.removeItem(item);
                });
            } else {
                localStorage.removeItem(name);
            }
            resolve(true);
        });
        return promise;
    },
    clearLocalStorage: function(name) {
        let promise = new Promise((resolve,reject) => {
            localStorage.clear();
            resolve(true);
        });
        return promise;
    },
};

//Eventos Generales

// EDITAR
$(document).on('click', '#btnEdit', function() {
    indexSelected = $(this).data('index');
    itemData = arrData[$(this).data('index')];
    showItem();
});

// BORRAR
$(document).on('click', '#btnDelete', function() {
    item = arrData[$(this).data('index')];
    bootbox.confirm({
        message: 'Por favor confirme la eliminación, esta acción será irreversible <br><center><b>¿Desea continuar?</b></center>',
        buttons: {
            confirm: {
                label: 'Sí',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result==true) {
                Core.crud.destroy(item.id).then(function(res) {
                    Core.showToastStr('success',res.data.message);
                }).catch(function(err) {
                    console.log(err);
                    Core.showToastStr('error', err.response.data.error.message);
                }).finally(function() {
                    getData();
                });
            }
        }
    });
});

// ON LOAD
$( window ).on('load',function() {
    var d = new Date();
    var currDay = d.getDate();
    var currMonth = d.getMonth();
    var currYear = d.getFullYear();
    var startDate = new Date(currYear, currMonth, currDay);
    //Aplicar idioma
    $('.datepicker').datepicker({
        language: 'es',
        format:"dd/mm/yyyy",
        todayHighlight: true,
        minDate: 0
    });
    $('.datepicker').datepicker('setDate',startDate);
    //Collapse and Expand Sidebar default on reload page
    if(localStorage.getItem('toggleSidebar')!=null) {
        if(localStorage.getItem('toggleSidebar')==1) {
            $('#sidebar').addClass('c-sidebar-minimized');
            $('#sidebar>ul.c-sidebar-nav').removeClass('ps');
        }
    }
    $('.c-sidebar-minimizer').click(function(){
        if(localStorage.getItem('toggleSidebar')!=null) {
            if(localStorage.getItem('toggleSidebar')==1) {
                localStorage.setItem('toggleSidebar',0);
            } else {
                localStorage.setItem('toggleSidebar',1);
            }
        }else {
            localStorage.setItem('toggleSidebar',1);
        }
    });
});



loadProgressBar();
