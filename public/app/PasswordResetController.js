route = '';
$("#frmResetpassword").validate({
    submitHandler: function (form) {
        if($("#password").val()==$("#confirmar").val()&&$("#confirmar").val()!="") {
                    var itemData = new FormData(form);
                    Core.post('resetpassword?token='+Core.getSearchParams('token'),itemData)
                      .then(function (response){
                        if(response.data.err==false){
                            Core.showToastStr('success',response.data.message);
                            $('#showFrmSuccess').attr('hidden',false);
                            $('#showFrmForgotAccess').attr('hidden',true);
                        }else{
                          Core.showToastStr('error',response.data.message);
                        }
                      })
                      .catch(function (error) {
                        Core.showToastStr('error',lang.error_internal);
                      });
        }else{
            $("#alertLoadingResetPassword").attr('hidden',true);
          Core.showToastStr('error',lang.password_wrong);
        }
    }
});
function init(){
    $('#frmForgotSuccess').css('display','none');
}
init();
