route = '';
$("#frmSignUp").validate({
    submitHandler: function (form) {
        console.log(form);
        if($('#frmSignUp input[name=password]').val()==$("#frmSignUp input[name=password_confirmation]").val() && $("#frmSignUp input[name=password_confirmation]").val()!="") {
                var formId = document.getElementById("frmSignUp");
                itemData = new FormData(formId);
                Core.post('signup',itemData).then(function (res) {
                    if(res.data.err==false){
                        $('#frmSignUpSuccess').css('display','');
                        $('#frmSignUpData').css('display','none');
                        Core.showToastStr('success',res.data.message);
                    }else{
                        Core.showToastStr('warning',res.data.message);
                    }
                })
                .catch(function (err) {
                    console.log(err);
                        Core.showToastStr('error',err.response.data.error.message);
                });
        }else{
            Core.showToastStr('error',lang.password_not_match);
        }
    }
});

$('#frmSignUp select[name=type]').change(function() {
    if($(this).val() == 1) {
        $('#containerFormEnterprise').css('display','none');
        $('#frmSignUp input[name=enterprise]').removeAttr('required');
    } else{
        $('#containerFormEnterprise').css('display','');
        $('#frmSignUp input[name=enterprise]').attr('required',true);
    }
});

$('#frmSignUp select[name=state_id]').change(function(){
    $('#frmSignUp select[name=city_id]').empty();
    $('#frmSignUp select[name=city_id]').append($('<option></option>').val('').html(lang.select));
    Core.get("api/general/states/"+$(this).val()+"/cities").then(function(res){
        arrCities = res.data;
        arrCities.forEach(function(item,index) {
            $('#frmSignUp select[name=city_id]').append($('<option></option>').val(item.id).html(item.name));
            });
    }).catch(function(err){
        Core.showToastStr('error',lang.cant_load_cities);
    })
});


function init() {
    $('#containerFormEnterprise').css('display','none');
    $('#frmSignUpSuccess').css('display','none');
}

init();
