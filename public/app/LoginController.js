$("#frmLogin").validate({
    submitHandler: function (form) {
        itemData = new FormData(form);
        Core.post(route+'login', itemData)
        .then(function (res) {
            if(res.data.err==false) {
                Core.showToastStr('success',res.data.message);
                setTimeout(function(){
                    window.location = urlWeb +"/"+ $('#routeAdmin').val() +"/home";
                    //console.log('Redireccionar aquí: '+urlWeb+"/"+$('#routeAdmin').val()+"/home");
                    //return false;
                },2000);
            }else{
                Core.showToastStr('error',res.data.message);
            }
        })
        .catch(function (err) {
            Core.showToastStr('error',err.response.data.error.message);
        });
    },
    errorPlacement: function(error,element) {
        return true;
    },
    highlight: function (element) {
        $('.card-body').addClass('was-validated')
        $(element).parent().removeClass('has-success').addClass('is-invalid');
    },
    unhighlight: function (element) {
        $('.card-body').addClass('was-validated')
        $(element).parent().removeClass('has-error').addClass('has-success');
    },
    errorElement: 'span',
    errorClass: 'help-block',
});


