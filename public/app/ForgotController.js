route = '';
    $("#frmForgo").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
            Core.post('forgot',itemData)
            .then(function (response) {
                Core.showToastStr('success',response.data.message);
             $('#frmForgo').css('display','none');
             $('#frmForgotSuccess').css('display','');
            })
            .catch(function (err) {
              Core.showToastStr('error',err.response.data.error.message);
            });
    }});
function init(){
    $('#frmForgotSuccess').css('display','none');
}
init();

