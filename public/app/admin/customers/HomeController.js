route = $('#idRoute').val() + "/customers";
arrStates = [];

//Crate new Dealer
$("#frmNew").validate({
    submitHandler: function (form) {
        $('.btn-success ').text('Guardando ...').prop('disabled', true);
        itemData = new FormData(form);
        Core.post(route + '/store', itemData)
            .then(function (res) {
                $('#mdlNew').modal('hide');
                $('#frmNew').trigger('reset');
                $('#previewDefault').attr('src', urlBase + 'dashboard/assets/img/default_user.png');
                Core.showToastStr("success", res.data.message);
                $('.btn-success').text('Guardar').prop('disabled', false);

            })
            .catch(function (err) {
                Core.showToastStr('error', err.response.data.error.message);
                $('.btn-success ').text('Guardar').prop('disabled', false);
            }).finally(function () {
            $('.btn-success ').text('Guardar').prop('disabled', false);
            tblData.ajax.reload();
        });
    }
});

$('#frmNew input[name=photo]').change(function (e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e);
            $('#previewDefault').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }
});




//Edit Dealer
$("#frmEdit").validate({
    submitHandler: function (form) {
        $('.btn-danger').text('Actualizando ...').prop('disabled', true);
        itemData = new FormData(form);
        Core.post(route + '/update/' + $('#id').val(), itemData)
            .then(function (res) {
                $('#mdlEdit').modal('hide');
                Core.showToastStr("success", res.data.message);
                //      $("#frmEdit").trigger('reset');
                $('.btn-danger').text('Actualizar').prop('disabled', false);

            })
            .catch(function (err) {
                Core.showToastStr('error', err.response.data.error.message);
                $('.btn-danger').text('Actualizar').prop('disabled', false);

            }).finally(function () {
            $('.btn-danger').text('Actualizar ').prop('disabled', false);

            tblData.ajax.reload();

        });
    }
});




window.showItem = function () {
    $('#id').val(itemData.id);
    $('#frmEdit select[name=branchoffice_id]').val(itemData.branchoffice_id);
    $('#frmEdit input[name=name]').val(itemData.name);
    $('#frmEdit input[name=ruc]').val(itemData.ruc);
    $('#frmEdit input[name=email]').val(itemData.email);
    $('#frmEdit input[name=phone]').val(itemData.phone);
    $('#frmEdit input[name=cedula]').val(itemData.cedula);
    $('#frmEdit input[name=password]').val('');
    $('#frmEdit select[name=status]').val(itemData.status);
    $('#previewDefaultEdit').attr('src', itemData.photo);

}




$('#frmEdit input[name=photo]').change(function (e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e);
            $('#previewDefaultEdit').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }
});

window.getData = function () {
    tblData = $('#arrData').DataTable({
        processing: true,
        serverSide: true,
        ajax: "customers/all",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'photo', name: 'photo'},
            {data: 'name', name: 'name'},
            {data: 'ruc', name: 'ruc'},
            {data: 'phone', name: 'phone'},
            {data: 'email', name: 'email'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'}
        ],
        lengthMenu: [
            [10, 50, 100, -1],
            [10, 50, 100, 'Todo']
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            pageLength: 5
        },
        "scrollX": true,
        "autoWidth": true,
        columnDefs: [{
            targets: [0],
            visible: false,
            searchable: false
        },
            {
                targets: [1],
                render: function (data, type, row, meta) {
                    return `<img src="${data}" style="width:35px; height:35px; border-radius:50%;">`
                }
            },
            {
                targets: [2],
                render: function (data, type, row, meta) {
                    return row.name;
                }
            },
            {
                targets: [3],
                render: function (data, type, row, meta) {
                    return row.ruc != null ? row.ruc : '';
                }
            },
            {
                targets: [4],
                render: function (data, type, row, meta) {
                    return row.phone;
                }
            },
            {
                targets: [5],
                render: function (data, type, row, meta) {
                    return row.email;
                }
            },
            {
                targets: [6],
                render: function (data, type, row, meta) {
                    return `${row.status == 1 ? 'Activo' : 'Inactivo'}`;
                }
            },
            {
                targets: [7],
                render: function (data, type, row, meta) {
                    return `<center>
                                <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                                    <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                                </svg>
                                <svg class="c-icon mr-2" id="btnDeleteCustome" data-index='${meta.row}' style="cursor: pointer;">
                                    <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                                </svg>
                            </center>`;
                }
            }]
    });
    tblData.on('draw', function () {
        arrData = tblData.rows().data().toArray();
    });
}

$(document).on('click', '#btnDeleteCustome', function () {
    item = arrData[$(this).data('index')];
    bootbox.confirm({
        message: 'Por favor confirme la eliminación, esta acción será irreversible, <br><center><b>¿Desea continuar?</b></center>',
        buttons: {
            confirm: {
                label: 'Sí',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                Core.crud.destroy(item.id).then(function (res) {
                    Core.showToastStr('success', res.data.message);
                }).catch(function (err) {
                    Core.showToastStr('error', err.response.data.error.message);
                }).finally(function () {
                    tblData.ajax.reload();
                });
            }
        }
    });
});

function init() {
    $(document).ready(function () {
        getData();
    });
}

init();
