route = $('#idRoute').val()+ "/quality";
arrPackages = [];
tblData = $('#arrData').DataTable({
    processing: true,
    serverSide: true,
    ajax: "orders/all",
    columns: [
        { data: 'id',                  name: 'id'},
        { data: 'rated_service',       name: 'rated_service'},
        { data: 'rated_time',          name: 'rated_time'},
        { data: 'rated_health',        name: 'rated_health'},
        { data: 'status',              name: 'status'},
        { data: 'created_at',          name: 'created_at'}

    ],
    lengthMenu: [
        [10, 50, 100, -1],
        [10, 50, 100, 'Todo']
    ],
    language: {
        url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
        pageLength: 5
    },
    "scrollX": true,
    "autoWidth": true,
    columnDefs: [{
        targets: [0],
        visible: false,
        searchable: false
    },
    {
        targets: [1],
        render: function(data, type, row, meta) {
            return row.id;
        }
    },
    {
        targets: [2],
        render: function(data, type, row, meta) {
            return row.rated_service;
        }
    },
    {
        targets: [3],
        render: function(data, type, row, meta) {
            return row.rated_time
        }
    },
    {
        targets: [4],
        render: function(data, type, row, meta) {
            return row.rated_health
        }
    },
    {
        targets: [5],
        render: function(data, type, row, meta) {
            return '<center>'+row.rated_comment!=null?row.rated_comment:''+'</center>';
        }
    },
    {
        targets: [6],
        render: function(data, type, row, meta) {
            return `<center>
                        <a href="${urlBase+"admin/orders/edit/"+row.id}">
                            <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-file"></use>
                            </svg>
                        </a>
                    </center>`
        }
    }]
});
tblData.on( 'draw', function () {
    arrData = tblData.rows().data().toArray();
    console.log(arrData);
});
//Crate new Dealer
$("#frmNew").validate({
    submitHandler: function (form) {
        itemData = new FormData(form);
        Core.post(route+'/store', itemData)
        .then(function (res) {
            $('#mdlNew').modal('hide');
            $('#frmNew').trigger('reset');
            Core.showToastStr("success",res.data.message);
        })
        .catch(function (err) {
            Core.showToastStr('error',err.response.data.error.message);
        }).finally(function(){
            tblData.ajax.reload();
        });
}});
//Edit Dealer
$("#frmEdit").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
            Core.post(route+'/update/'+$('#id').val(), itemData)
            .then(function (res){
                $('#mdlEdit').modal('hide');
                Core.showToastStr("success",res.data.message);
                $("#frmEdit").trigger('reset');
            })
            .catch(function (err) {
                Core.showToastStr('error',err.response.data.error.message);
            }).finally(function(){
                tblData.ajax.reload();
            });
    }});
window.showItem = function(){
    $('#id').val(itemData.id);
    $('#frmEdit select[name=branchoffice_id]').val(itemData.branchoffice_id);
    $('#frmEdit input[name=name]').val(itemData.name);
    $('#frmEdit input[name=email]').val(itemData.email);
    $('#frmEdit input[name=phone]').val(itemData.phone);
    $('#frmEdit input[name=cedula]').val(itemData.cedula);
    $('#frmEdit input[name=password]').val('');
    $('#frmEdit select[name=status]').val(itemData.status);
    $('#previewDefaultEdit').attr('src',itemData.photo);
}
$('#frmNew input[name=picture]').change(function(e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
            reader.onload = function(e) {
                console.log(e);
                $('#previewDefault').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
    }
});
$('#frmEdit input[name=picture]').change(function(e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
            reader.onload = function(e) {
                console.log(e);
                $('#previewDefaultEdit').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
    }
});
window.getData = function() {
    tblData.ajax.reload();
}
$('#btnUpdate').click(function() {
    tblData.ajax.reload();
})

function init() {
}
init();
