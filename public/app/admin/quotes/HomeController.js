route = $('#idRoute').val()+ "/quotes";
arrStates = [];
//Edit Dealer
$("#frmEdit").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
            Core.post(route+'/update/'+$('#id').val(), itemData)
            .then(function (res){
                $('#mdlEdit').modal('hide');
                Core.showToastStr("success",res.data.message);
                $("#frmEdit").trigger('reset');
            })
            .catch(function (err) {
                Core.showToastStr('error',err.response.data.error.message);
            }).finally(function(){
                tblData.ajax.reload();
            });
    }});
window.showItem = function(){
    $('#id').val(itemData.id);
    $('#frmEdit input[name=name]').val(itemData.customer_data.name);
    $('#frmEdit textarea[name=note]').val(itemData.note);
    $('#frmEdit input[name=price]').val(itemData.price);
    $('#frmEdit textarea[name=details]').val(itemData.details);
    $('#frmEdit select[name=status]').val(itemData.status);
}
window.getData = function() {
    tblData = $('#arrData').DataTable({
        processing: true,
        serverSide: true,
        ajax: "quotes/all",
        columns: [
            { data: 'id',                       name:'id'},
            { data: 'customer.name',            name: 'customer.name' },
            { data: 'customer.phone',           name: 'customer.phone'},
            { data: 'document',                 name: 'document'},
            { data: 'status', data: 'status'},
            { data: 'created_at',               name: 'created_at' },
            { data: 'updated_at',               name: 'updated_at' }
        ],
        lengthMenu: [
            [10, 50, 100, -1],
            [10, 50, 100, 'Todo']
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            pageLength: 5
        },
        "scrollX": true,
        "autoWidth": true,
        columnDefs: [{
            targets: [0],
            visible: false,
            searchable: false
        },
        {
            targets: [1],
            render: function(data, type, row, meta) {
                return row.id
            }
        },
        {
            targets: [2],
            render: function(data, type, row, meta) {
                return row.customer_data.name
            }
        },
        {
            targets: [3],
            render: function(data, type, row, meta) {
                return row.customer_data.phone
            }
        },
        {
            targets: [4],
            render: function(data, type, row, meta) {
                return row.customer_data.email
            }
        },
        {
            targets: [5],
            render: function (data, type, row, meta) {
                return `<center>
                            <a href="${row.document}" _target='blank'>
                                <svg class="c-icon mr-2" style="cursor: pointer;">
                                    <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-file"></use>
                                </svg>
                            </a>
                        </center>`;
            }
        },
        {
            targets: [6],
            render: function (data, type, row, meta) {
                switch(row.status) {
                    case 0: { return 'Nueva'}
                    case 1: { return 'Aceptada'}
                    case 2: { return 'Rechazada'}
                }       
            }
        },
        {
            targets: [7],
            render: function(data, type, row, meta){
                console.log(meta);
                return `<center>
                            <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                            </svg>
                            <svg class="c-icon mr-2" id="btnDeleteCustome" data-index='${meta.row}' style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                            </svg>
                        </center>`;
            }
        }]
    });
    tblData.on( 'draw', function () {
        arrData = tblData.rows().data().toArray();
    });
}
$(document).on('click', '#btnDeleteCustome', function() {
    item = arrData[$(this).data('index')];
    bootbox.confirm({
        message: 'Por favor confirme la eliminación, esta acción será irreversible, <br><center><b>¿Desea continuar?</b></center>',
        buttons: {
            confirm: {
                label: 'Sí',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result==true) {
                Core.crud.destroy(item.id).then(function(res) {
                    Core.showToastStr('success',res.data.message);
                }).catch(function(err) {
                    Core.showToastStr('error', err.response.data.error.message);
                }).finally(function() {
                    tblData.ajax.reload();
                });
            }
        }
    });
});
function init() {
    $(document).ready(function() {
        getData();
    });
}
init();