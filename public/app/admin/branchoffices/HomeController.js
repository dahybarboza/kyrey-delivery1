route = $('#idRoute').val() + "/branchoffices";
arrStates = [];
Core.setDataTable('arrData');

// Validar formulario de crear nueva sucursal (Botón guardar)
$("#frmNew").validate({
    submitHandler: function (form) {
        $('.btn-success').text('Guardando ...').prop('disabled', true);
        itemData = new FormData(form);
        Core.post(route + '/store', itemData)
            .then(function (res) {
                $('#mdlNew').modal('hide');
                $('#frmNew').trigger('reset');

                Core.showToastStr("success", res.data.message);
                $('.btn-success').text('Guardar').prop('disabled', false);
            })
            .catch(function (err) {
                Core.showToastStr('error', err.response.data.error.message);
                $('.btn-success').text('Guardar').prop('disabled', false);
            }).finally(function () {
            $('.btn-success').text('Guardar').prop('disabled', false);
            getData();
            setTimeout(function (){
                window.location.href = urlBase + route;
            }, 1000);
        });
    }
});

// Validar formulario de editar sucursal (Botón editar)
$("#frmEdit").validate({
    submitHandler: function (form) {
        $('.btn-danger').text('Actualizando ...').prop('disabled', true);
        itemData = new FormData(form);
        Core.post(route + '/update/' + $('#id').val(), itemData)
            .then(function (res) {
                $('#mdlEdit').modal('hide');
                Core.showToastStr("success", res.data.message);
                $('.btn-danger').text('Actualizar').prop('disabled', false);
            })
            .catch(function (err) {
                Core.showToastStr('error', err.response.data.message);
                $('.btn-danger').text('Actualizar').prop('disabled', false);

            }).finally(function () {
            $('.btn-danger').text('Actualizar').prop('disabled', false);
            getData();
            setTimeout(function (){
                window.location.href = urlBase + route;
            }, 1000);
        });
    }
});

// Deshabilitar enter
$('#frmNew, #frmEdit').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});

// Traer datos
window.getData = function () {
    tblData.rows().remove().draw();
    Core.get(route + "/all").then(function (res) {
        arrData = res.data.data;
        res.data.data.forEach(function (item, index) {
            tblData.row.add([
                index,
                item.customer.name,
                item.name,
                item.phone,
                item.status == 1 ? 'Activa' : 'Inactiva',
                Core.buttons(index)
            ]).draw();
        });

        // Llama a la función para cargar el mapa
        //loadMap();
    }).catch(function (err) {
        Core.showToastStr('error', 'No ha sido posible cargar sucursales');
    });
}

// Mostrar items
window.showItem = function () {
    $('#frmEdit input[name=id]').val(itemData.id);
    $('#frmEdit select[name=customer_id]').val(itemData.customer_id);
    $('#frmEdit input[name=name]').val(itemData.name);
    $('#frmEdit input[name=phone]').val(itemData.phone);
    $('#frmEdit input[name=address]').val(itemData.address);
    $('#frmEdit input[name=lat]').val(itemData.lat);
    $('#frmEdit input[name=lng]').val(itemData.lng);
    $('#frmEdit select[name=status]').val(itemData.status);

    // Se le llama a la función que carga el mapa
    setTimeout(function (){
        loadMapEdit();
        // Esperamos un segundo y luego cargamos el marcador del mapa
        setTimeout(function (){
            var center = new google.maps.LatLng(itemData.lat, itemData.lng);
            map.panTo(center);
            addMarker(center);
        }, 1000);
    }, 200);
}

// Función principal
function init() {
    getData();
}

init();
