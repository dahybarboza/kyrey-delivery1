    function initMap() {
        //
        // return false;
        //
        // const uluru = {lat: -25.2637399, lng: -57.57592599999999};
        // const map = new google.maps.Map(document.getElementById("map"), {
        //     zoom: 18,
        //     center: uluru,
        // });
        // // The marker, positioned at Uluru
        // const marker = new google.maps.Marker({
        //     position: uluru,
        //     map: map,
        // });
        //
        // //For New Branch Office
        // autocomplete = new google.maps.places.Autocomplete(
        //     $('#frmNew input[name=address]')[0], {types: ['geocode']});
        // autocomplete.setFields(['geometry']);
        // autocomplete.addListener('place_changed', function () {
        //     var place = autocomplete.getPlace();
        //     //console.log(place);
        //     $('#frmNew input[name=lat]').val(place.geometry.location.lat());
        //     $('#frmNew input[name=lng]').val(place.geometry.location.lng());
        //
        //     const uluru = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
        //
        //     const map = new google.maps.Map(document.getElementById("map"), {
        //         zoom: 18,
        //         center: uluru,
        //     });
        //     // The marker, positioned at Uluru
        //     const marker = new google.maps.Marker({
        //         position: uluru,
        //         map: map,
        //     });
        //
        // });
        //
        //
        // //For Edit Branch Office
        // autocompleteEdit = new google.maps.places.Autocomplete(
        //     $('#frmEdit input[name=address]')[0], {types: ['geocode']});
        // autocompleteEdit.setFields(['geometry']);
        // autocompleteEdit.addListener('place_changed', function () {
        //     var place = autocompleteEdit.getPlace();
        //     //console.log(place);
        //     $('#frmEdit input[name=lat]').val(place.geometry.location.lat());
        //     $('#frmEdit input[name=lng]').val(place.geometry.location.lng());
        // });
    }

    let map = 'x';
    let markers = [];

    let geo = navigator.geolocation;

    function loadMap() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const center = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    map = new google.maps.Map(document.getElementById('map'), {zoom: 14, center: center});
                    map.setCenter(center);
                    //Click Listener Google Maps
                    map.addListener("click", (event) => {
                        //console.log('Evento click');
                        deleteMarkers();
                        addMarker(event.latLng);
                        $('#frmNew input[name=lat]').val(event.latLng.lat());
                        $('#frmNew input[name=lng]').val(event.latLng.lng());
                        Core.getGeocoder(event.latLng).then(function (res) {
                            neighborhood = "";
                            for (var component of res.address_components) {
                                for (var type of component.types) {
                                    if (component.types[0] == 'locality' && component.types[1] == 'political') {
                                        neighborhood = component.long_name
                                        break;
                                    }
                                }
                                if (neighborhood != "") {
                                    break;
                                }
                            }
                            $('#frmNew input[name=neighborhood]').val(neighborhood);
                            $('#frmNew input[name=address]').val(res.formatted_address);
                        }).catch(function () {
                            Core.showToastStr('error', 'No ha sido posible obtener dirección');
                        });
                    });
                },
                () => {
                    console.log('errorCallback');
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        }
        autocompleteA = new google.maps.places.Autocomplete($('#frmNew input[name=address]')[0], {types: ['geocode']});
        autocompleteA.setFields(['geometry']);
        autocompleteA.addListener('place_changed', function () {
            var place = autocompleteA.getPlace();
            $('#frmNew input[name=lat]').val(place.geometry.location.lat());
            $('#frmNew input[name=lng]').val(place.geometry.location.lng());
            Core.getGeocoder(place.geometry.location).then(function (res) {
                neighborhood = "";
                for (var component of res.address_components) {
                    for (var type of component.types) {
                        if (component.types[0] == 'locality' && component.types[1] == 'political') {
                            neighborhood = component.long_name;
                            //console.log(neighborhood);
                            break;
                        }
                    }
                    if (neighborhood != "") {
                        break;
                    }
                }
                $('#frmNew input[name=neighborhood]').val(neighborhood);
                $('#frmNew input[name=address]').val(res.formatted_address);
            }).catch(function () {
                Core.showToastStr('error', 'No ha sido posible obtener dirección');
            });
            clearMarkers();
            var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            map.panTo(center);
            addMarker(center);
        });
    }

    function loadMapEdit(lat, lng){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const center = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    map = new google.maps.Map(document.getElementById('mapEdit'), {zoom: 14, center: center});
                    map.setCenter(center);
                    //Click Listener Google Maps
                    map.addListener("click", (event) => {
                        console.log('Click Map');
                        deleteMarkers();
                        addMarker(event.latLng);
                        $('#frmEdit input[name=lat]').val(event.latLng.lat());
                        $('#frmEdit input[name=lng]').val(event.latLng.lng());
                        Core.getGeocoder(event.latLng).then(function (res) {
                            neighborhood = "";
                            for (var component of res.address_components) {
                                for (var type of component.types) {
                                    if (component.types[0] == 'locality' && component.types[1] == 'political') {
                                        neighborhood = component.long_name
                                        break;
                                    }
                                }
                                if (neighborhood != "") {
                                    break;
                                }
                            }
                            $('#frmEdit input[name=neighborhood]').val(neighborhood);
                            $('#frmEdit input[name=address]').val(res.formatted_address);
                        }).catch(function () {
                            Core.showToastStr('error', 'No ha sido posible obtener dirección');
                        });
                    });
                },
                () => {
                    console.log('errorCallback');
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        }
        autocompleteA = new google.maps.places.Autocomplete($('#frmEdit input[name=address]')[0], {types: ['geocode']});
        autocompleteA.setFields(['geometry']);
        autocompleteA.addListener('place_changed', function () {
            console.log('place_changed map');
            var place = autocompleteA.getPlace();
            $('#frmEdit input[name=lat]').val(place.geometry.location.lat());
            $('#frmEdit input[name=lng]').val(place.geometry.location.lng());
            Core.getGeocoder(place.geometry.location).then(function (res) {
                neighborhood = "";
                for (var component of res.address_components) {
                    for (var type of component.types) {
                        if (component.types[0] == 'locality' && component.types[1] == 'political') {
                            neighborhood = component.long_name;
                            //console.log(neighborhood);
                            break;
                        }
                    }
                    if (neighborhood != "") {
                        break;
                    }
                }
                $('#frmEdit input[name=neighborhood]').val(neighborhood);
                $('#frmEdit input[name=address]').val(res.formatted_address);
            }).catch(function () {
                Core.showToastStr('error', 'No ha sido posible obtener dirección');
            });
            clearMarkers();
            var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            map.panTo(center);
            addMarker(center);
        });


    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        const marker = new google.maps.Marker({
            position: location,
            map: map,
        });
        markers.push(marker);
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Sets the map on all markersL1 in the array.
    function setMapOnAll(map) {
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    googleKey = $('#googleKey').val();
    document.write(`<script src="https://maps.googleapis.com/maps/api/js?key=${googleKey}&libraries=places&callback=loadMap" async defer></script>'`);
