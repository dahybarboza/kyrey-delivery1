route = $('#idRoute').val()+ "/delivery";
arrPackages = [];
tblData = $('#arrData').DataTable({
    processing: true,
    serverSide: true,
    ajax: "delivery/all",
    columns: [
        { data: 'id',               name: 'deliveries.id'},
        { data: 'branchoffices',    name: 'branchoffices.name'},
        { data: 'dealer',           name: 'dealers.name'},
        { data: 'name',             name: 'deliveries.name'},
        { data: 'quantity',         name: 'deliveries.quantity'},
        { data: 'price',            name: 'deliveries.price'},
        { data: 'status',           name: 'deliveries.status'},
        { data: 'created_at',       name: 'deliveries.created_at'}
    ],
    lengthMenu: [
        [10, 50, 100, -1],
        [10, 50, 100, 'Todo']
    ],
    language: {
        url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
        pageLength: 5
    },
    "scrollX": true,
    "autoWidth": true,
    columnDefs: [{
        targets: [0],
        visible: false,
        searchable: false
    },
        {
            targets: [1],
            render: function(data, type, row, meta) {
                return row.id;
            }
        },
        {
            targets: [2],
            render: function(data, type, row, meta) {
                return row.branchoffice;
            }
        },
        {
            targets: [3],
            render: function(data, type, row, meta) {
                return row.dealer
            }
        },
        {
            targets: [4],
            render: function(data, type, row, meta) {
                return row.name
            }
        },
        {
            targets: [5],
            render: function(data, type, row, meta) {
                return '<center>'+row.quantity+'</center>';
            }
        },
        {
            targets: [6],
            render: function(data, type, row, meta) {
                return '<center>₲'+Core.setMiles( row.price )+'</center>';
            }
        },
        {
            targets: [7],
            render: function (data, type, row, meta) {
                switch(row.status) {
                    case 0: { return 'Pendiente'; break;}
                    case 1: { return 'Entregando'; break;}
                    case 2: { return 'Entregado'; break;}
                    case 3: { return 'Cancelado'; break;}
                }
                return `${row.status==1?'Activo':'Inactivo'}`;
            }
        },
        {
            targets: [8],
            render: function(data, type, row, meta) {
                return `<center>
                        <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                        </svg>
                        <svg class="c-icon mr-2" id="btnDelete" data-index='${meta.row}' style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                        </svg>
                    </center>`;
            }
        }]
});
tblData.on( 'draw', function () {
    arrData = tblData.rows().data().toArray();
    // console.log(arrData);
});
//Crate new Dealer
$("#frmNew").validate({
    submitHandler: function (form) {
        $('.btn-success').text('Guardando ...').prop('disabled', true);
        itemData = new FormData(form);
        Core.post(route+'/store', itemData)
            .then(function (res) {
                $('#mdlNew').modal('hide');
                $('#frmNew').trigger('reset');
                Core.showToastStr("success",res.data.message);
                $('.btn-success').text('Guardar').prop('disabled', false);

            })
            .catch(function (err) {
                Core.showToastStr('error',err.response.data.error.message);
                $('.btn-success').text('Guardar').prop('disabled', false);

            }).finally(function(){
            $('.btn-success').text('Guardar').prop('disabled', false);
            tblData.ajax.reload();
        });
    }});
//Edit Dealer
$("#frmEdit").validate({
    submitHandler: function (form) {
        $('.btn-danger').text('Actualizando ...').prop('disabled', true);
        itemData = new FormData(form);
        Core.post(route+'/update/'+ $('#id').val(), itemData)
            .then(function (res){
                $('#mdlEdit').modal('hide');
                Core.showToastStr("success",res.data.message);
                $('.btn-danger').text('Actualizar').prop('disabled', false);

            })
            .catch(function (err) {
                Core.showToastStr('error',err.response.data.error.message);
                $('.btn-danger').text('Actualizar').prop('disabled', false);

            }).finally(function(){
            $('.btn-danger').text('Actualizar').prop('disabled', false);
            tblData.ajax.reload();
            setTimeout(function (){
                window.location.href = urlBase + route;
            }, 1000);
        });
    }});
/*
$('#frmNew select[name=branchoffice_id]').on('change',(function() {
    $('#frmNew select[name=dealer_id]').empty();
    Core.get(route+'/dealers/'+$(this).val())
        .then(function (res) {
            $.each(res.data.data,function(index,item) {
                $('#frmNew select[name=dealer_id]').append($('<option data-index="'+index+'"></option>').val(item.id).html(item.name));
            });
        }).catch(function (err) {
            console.log(err);
            Core.showToastStr('error',err.response.data.error.message);
        });
}));

$('#frmEdit select[name=branchoffice_id]').on('change',(function() {
    $('#frmEdit select[name=dealer_id]').empty();
    Core.get(route+'/dealers/'+$(this).val())
        .then(function (res) {
            $.each(res.data.data,function(index,item) {
                $('#frmEdit select[name=dealer_id]').append($(`<option data-index="'+index+'" ${item.id == itemData.dealer_id ?'selected':''}></option>`).val(item.id).html(item.name));
            });
        }).catch(function (err) {
            console.log(err);
            Core.showToastStr('error',err.response.data.error.message);
        });
}));*/

window.showItem = function() {
    $('#id').val(itemData.id);
    $('#frmEdit select[name=branchoffice_id]').val(itemData.branchoffice_id).trigger('change');
    $('#frmEdit select[name=dealer_id]').val(itemData.dealer_id).trigger('change');
    $('#frmEdit input[name=name]').val(itemData.name);
    $('#frmEdit input[name=email]').val(itemData.email);
    $('#frmEdit input[name=phone]').val(itemData.phone);
    $('#frmEdit textarea[name=note]').val(itemData.note);
    $('#frmEdit input[name=address]').val(itemData.address);
    $('#frmEdit input[name=lat]').val(itemData.lat);
    $('#frmEdit input[name=lng]').val(itemData.lng);
    $('#frmEdit input[name=quantity]').val(itemData.quantity);
    $('#frmEdit input[name=price]').val(itemData.price);
    $('#frmEdit select[name=status]').val(itemData.status);
}

window.getData = function() {
    tblData.ajax.reload();
}

$('#btnUpdate').click(function() {
    tblData.ajax.reload();
})

function init() {
    getData();
}
init();
