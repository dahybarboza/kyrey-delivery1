    route = $('#idRoute').val()+ "/home";
    arrStates = [];
    var tblData = $('#arrData').DataTable({
            lengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, 'Todos']
            ],
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
                pageLength: 5
            },
            "scrollX": true,
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
            }]
    });
    function getResumen() {
        Core.get(route+"/resumen").then(function(res) {
            tblData.rows().remove().draw();
            resumen = res.data.data;
            $('#count_branchoffices').html(resumen.count_branchoffices);
            $('#count_dealers').html(resumen.count_dealers);
            $('#count_customers').html(resumen.count_customers);
            $('#count_orders').html(resumen.count_orders);
            $('#count_orders_success').html(resumen.count_orders_success);
        }).catch(function(err) {
            console.log(err);
            Core.showToastStr('error',lang.cant_load_sumary_data_sales);
        });
    }
    function graphService() {
        Core.get(route+"/rate_service").then(function(data) {
            $('#countServices').html(data.data.orders);
            window['chartForma'] = new Chart(document.getElementById('canvas-forma'), {
                type: 'pie',
                data: {
                  labels: ['1 Estrella', '2 Estrellas', '3 Estrellas','4 Estrellas','5 Estrellas'],
                  datasets: [{
                    data: data.data.service,
                    backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C'],
                    hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C']
                  }]
                },
                options: {
                  responsive: true
                }
            });
            window['chartTiempo'] = new Chart(document.getElementById('canvas-tiempo'), {
                type: 'pie',
                data: {
                  labels: ['1 Estrella', '2 Estrellas', '3 Estrellas','4 Estrellas','5 Estrellas'],
                  datasets: [{
                    data: data.data.time,
                    backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C'],
                    hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C']
                  }]
                },
                options: {
                  responsive: true
                }
            });
            window['chartAmabilidad'] = new Chart(document.getElementById('canvas-amabilidad'), {
                type: 'pie',
                data: {
                  labels: ['1 Estrella', '2 Estrellas', '3 Estrellas','4 Estrellas','5 Estrellas'],
                  datasets: [{
                    data: data.data.friendly,
                    backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C'],
                    hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C']
                  }]
                },
                options: {
                  responsive: true
                }
            });
            window['chartSalud'] = new Chart(document.getElementById('canvas-salud'), {
                type: 'pie',
                data: {
                  labels: ['1 Estrella', '2 Estrellas', '3 Estrellas','4 Estrellas','5 Estrellas'],
                  datasets: [{
                    data: data.data.health,
                    backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C'],
                    hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56','#ED502E', '#60B95C']
                  }]
                },
                options: {
                  responsive: true
                }
            });
        });
    }
    $('#btnUpdate').click(function(){
        init();
    });
    function init() {
        getResumen();
        graphService();
    }
    //Hanlde Complements
    init();
