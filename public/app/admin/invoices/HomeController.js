route = $('#idRoute').val()+ "/invoices";
arrPackages = [];
tblData = $('#arrData').DataTable({
    processing: true,
    serverSide: true,
    ajax: "invoices/all",
    columns: [
        { data: 'id',               name: 'id'},
        { data: 'order_id',         name: 'order_id'},
        { data: 'name',             name: 'name'},
        { data: 'phone',            name: 'phone'},
        { data: 'email',            name: 'email'},
        { data: 'payment_method',   name: 'payment_method'},
        { data: 'ruc',              name: 'ruc'},
        { data: 'status',           name: 'status'},
        { data: 'created_at',       name: 'created_at'}
    ],
    lengthMenu: [
        [10, 50, 100, -1],
        [10, 50, 100, 'Todo']
    ],
    language: {
        url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
        pageLength: 5
    },
    "scrollX": true,
    "autoWidth": true,
    columnDefs: [{
        targets: [0],
        visible: false,
        searchable: false
    },
    {
        targets: [1],
        render: function(data, type, row, meta) {
            return row.order_id;
        }
    },
    {
        targets: [2],
        render: function(data, type, row, meta) {
            return row.name;
        }
    },
    {
        targets: [3],
        render: function(data, type, row, meta) {
            return row.phone
        }
    },
    {
        targets: [4],
        render: function(data, type, row, meta) {
            return row.email
        }
    },
    {
        targets: [5],
        render: function(data, type, row, meta) {
            var payment = 'Efectivo';
            switch(row.payment_method) {
                case 1: {
                    payment = 'Efectivo';
                    break;
                }
                case 2: {
                    payment = 'Tarjeta';
                    break;
                }
                case 3: {
                    payment = 'Transferencia';
                    break;
                }
            }
            return '<center>'+payment+'</center>';
        }
    },
    {
        targets: [6],
        render: function(data, type, row, meta) {
            return row.ruc;
        }
    },
    {
        targets: [7],
        render: function(data, type, row, meta) {
            return '<center>₲'+Core.setMiles(row.total)+'</center>';
        }
    },
    {
        targets: [8],
        render: function (data, type, row, meta) {
            switch(row.status) {
                case 0: { return 'Pendiente'; break;}
                case 1: { return 'Procesada'; break;}
                case 2: { return 'Rechazada'; break;}
            }
        }
    },
    {
        targets: [9],
        render: function(data, type, row, meta) {
            return `<center>
                        <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                        </svg>
                        <svg class="c-icon mr-2" id="btnDelete" data-index='${meta.row}' style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                        </svg>
                    </center>`;
        }
    }]
});
tblData.on( 'draw', function () {
    arrData = tblData.rows().data().toArray();
    console.log(arrData);
});
//Crate new Dealer
$("#frmNew").validate({
    submitHandler: function (form) {
        itemData = new FormData(form);
        Core.post(route+'/store', itemData)
        .then(function (res) {
            $('#mdlNew').modal('hide');
            $('#frmNew').trigger('reset');
            Core.showToastStr("success",res.data.message);
        })
        .catch(function (err) {
            Core.showToastStr('error',err.response.data.error.message);
        }).finally(function(){
            tblData.ajax.reload();
        });
}});
//Edit Dealer
$("#frmEdit").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
            Core.post(route+'/update/'+$('#id').val(), itemData)
            .then(function (res){
                $('#mdlEdit').modal('hide');
                Core.showToastStr("success",res.data.message);
                $("#frmEdit").trigger('reset');
            })
            .catch(function (err) {
                Core.showToastStr('error',err.response.data.error.message);
            }).finally(function(){
                tblData.ajax.reload();
            });
    }});
window.showItem = function() {
    $('#id').val(itemData.id);
    $('#frmEdit input[name=name]').val(itemData.name);
    $('#frmEdit input[name=email]').val(itemData.email);
    $('#frmEdit input[name=phone]').val(itemData.phone);
    $('#frmEdit input[name=ruc]').val(itemData.ruc);
    $('#frmEdit input[name=total]').val(itemData.total);
    $('#frmEdit select[name=payment_method]').val(itemData.payment_method);
    $('#frmEdit select[name=status]').val(itemData.status);
}

window.getData = function() {
    tblData.ajax.reload();
}

$('#btnUpdate').click(function() {
    tblData.ajax.reload();
})

function init() {
}
init();
