route = $('#idRoute').val()+"/home";
$("#frmProfile").validate({
    submitHandler: function (form) {
        itemData = new FormData(form);
      Core.post(route+'/update', itemData)
      .then(function (res) {
            Core.showToastStr("success",res.data.message);
            setTimeout(function(){
                location.reload();
            },2000);
      })
      .catch(function (err) {
          Core.showToastStr('error',err.response.data.error.message);
      });
}});
$('#frmProfile input[name=picture]').change(function(e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
            reader.onload = function(e) {
                console.log(e);
              $('#previewDefault').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
    }
});
