    route = $('#idRoute').val()+ "/zones";
    arrStates = [];
    Core.setDataTable('arrData');
    //Create Zone
    $("#frmNew").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
          Core.post(route+'/store', itemData)
          .then(function (res){
                $('#mdlNew').modal('hide');
                Core.showToastStr("success",res.data.message);
                $("#frmNew").trigger('reset');
          })
          .catch(function (err) {
              Core.showToastStr('error',err.response.data.error.message);
          }).finally(function(){
            getData();
          });
    }});
    //Edit Zone
    $("#frmEdit").validate({
            submitHandler: function (form) {
                itemData = new FormData(form);
              Core.post(route+'/update/'+$('#id').val(), itemData)
              .then(function (res){
                    $('#mdlEdit').modal('hide');
                    Core.showToastStr("success",res.data.message);
                    $("#frmEdit").trigger('reset');
              })
              .catch(function (err) {
                  Core.showToastStr('error',err.response.data.error.message);
              }).finally(function(){
                getData();
              });
        }});
    window.getData = function() {
        tblData.rows().remove().draw();
        Core.get(route+"/all").then(function(res){
            arrData = res.data.data;
            res.data.data.forEach(function (item, index){
                tblData.row.add([
                    index,
                    item.name,
                    `<center>
                        <a href="${urlWeb+'/'+route+"/rates?zone="+item.id}">
                            <svg class="c-icon mr-2" id="btnEdit" data-index='${index}' style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-list-rich"></use>
                            </svg>
                        </a>
                    </center>`,
                    item.status==1?'Activo':'Inactivo',
                    `<center>
                        <svg class="c-icon mr-2" id="btnEdit" data-index='${index}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                        </svg>
                        <svg class="c-icon mr-2" id="btnDelete" data-index='${index}' style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                        </svg>
                    </center>`
                ]).draw();
            });
        }).catch(function(err){
            console.log(err);
            Core.showToastStr('error','No ha sido posible cargar repartidores.');
        });
    }
    window.showItem = function(){
        $('#id').val(itemData.id);
        $('#frmEdit input[name=name]').val(itemData.name);
        $('#frmEdit select[name=status]').val(itemData.status);
    }
    function init(){
        getData();
    }
    init();
//https://www.youtube.com/watch?v=g9Yld0dsLBQ