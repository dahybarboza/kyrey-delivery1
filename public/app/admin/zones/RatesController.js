    route = $('#idRoute').val()+ "/zones/rates";
    arrStates = [];
    Core.setDataTable('arrData');
    $("#frmNew").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
          Core.post(route+'/store', itemData)
          .then(function (res){
                $('#mdlNew').modal('hide');
                Core.showToastStr("success",res.data.message);
                $("#frmNew").trigger('reset');
                getZones();
          })
          .catch(function (err) {
              Core.showToastStr('error',err.response.data.error.message);
          }).finally(function(){
            getData();
          });
    }});
    //Edit Dealer
    $("#frmEdit").validate({
            submitHandler: function (form) {
              itemData = new FormData(form);
              Core.post(route+'/update/'+$('#id').val(), itemData)
              .then(function (res){
                    $('#mdlEdit').modal('hide');
                    Core.showToastStr("success",res.data.message);
                    $("#frmEdit").trigger('reset');
              })
              .catch(function (err) {
                  Core.showToastStr('error',err.response.data.error.message);
              }).finally(function(){
                getData();
              });
        }});
    window.getData = function() {
        tblData.rows().remove().draw();
        Core.get(route+"/all/"+$('#frmNew input[name=zone_id]').val()).then(function(res){
            arrData = res.data.data;
            res.data.data.forEach(function (item, index){
                tblData.row.add([
                    index,
                    item.zone.name,
                    `₲${Core.setMiles(item.price)}`,
                    item.status==1?'Activo':'Inactivo',
                    `<center>
                        <svg class="c-icon mr-2" id="btnEdit" data-index='${index}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                        </svg>
                        <svg class="c-icon mr-2" id="btnDelete" data-index='${index}' style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                        </svg>
                    </center>`
                ]).draw();
            });
        }).catch(function(err){
            console.log(err);
            Core.showToastStr('error','No ha sido posible cargar repartidores.');
        }).finally(function(){
            getZones();
        });
    }
    window.showItem = function(){
        $('#frmEdit input[id=name]').val(itemData.zone.name);
        $('#frmEdit input[name=id]').val(itemData.id);
        $('#frmEdit input[name=price]').val(itemData.price);
        $('#frmEdit select[name=status]').val(itemData.status);
    }
    window.getZones = function() {
        $('#frmNew select[name=zone_delivery]').empty();
        Core.get(route+'/zones/'+$('#frmNew input[name=zone_id]').val()).then(function(data){
            $.each(data.data.data, function(index,item) {
                $('#frmNew select[name=zone_delivery]').append($('<option data-index="'+index+'"></option>').val(item.id).html(item.name));
            });
        })
    }
    function init(){
        getData();
    }
    init();
