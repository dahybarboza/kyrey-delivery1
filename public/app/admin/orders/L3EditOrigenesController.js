var mapL3AddOrigen;
var markersL3Origen = [];
function loadMapL3OrigenAdd() {
  if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
          (position) => {
                const center = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                if(!mapL3AddOrigen) {
                  mapL3AddOrigen = new google.maps.Map(document.getElementById('mapL3Origen'), {zoom: 14, center: center});
                }
                clearMarkersL3Origen();
                lat = $('#frmL3AddOrigenes input[name=address_lat]').val();
                lng = $('#frmL3AddOrigenes input[name=address_lng]').val();
                if(lat!=""&&lng!="") {
                  const center = {
                      lat: lat*1,
                      lng: lng*1
                  };
                  console.log(center);
                  addMarkerL3AddOrigen(center);
                  mapL3AddOrigen.setCenter(center);
                } else {
                  mapL3AddOrigen.setCenter(center);
                }
                //Click Listener Google Maps
                mapL3AddOrigen.addListener("click", (event) => {
                $('#frmL3AddOrigenes input[name=address_lat]').val(event.latLng.lat());
                $('#frmL3AddOrigenes input[name=address_lng]').val(event.latLng.lng());
                clearMarkersL3Origen();
                addMarkerL3AddOrigen(event.latLng);
                Core.getGeocoder(event.latLng).then(function(res){
                  neighborhood = "";
                  for(var component of res.address_components) {
                    for(var type of component.types) {
                      if(component.types[0] =='locality' && component.types[1]=='political') {
                        neighborhood = component.long_name
                        break;
                      }
                    }
                    if(neighborhood!="") {
                        console.log(neighborhood);
                      break;
                    }
                  }
                  $('#frmL3AddOrigenes input[name=neighborhood]').val(neighborhood);
                  $('#frmL3AddOrigenes input[name=address]').val(res.formatted_address);
                });
              });
          },
          () => {
              handleLocationError(true, infoWindow, map.getCenter());
          }
      );
  }
  autocompleteL3AddDestino = new google.maps.places.Autocomplete($('#frmL3AddOrigenes input[name=address]')[0], {types: ['geocode']});
  autocompleteL3AddDestino.setFields(['geometry']);
  autocompleteL3AddDestino.addListener('place_changed', function() {
      var place = autocompleteL3AddDestino.getPlace();
      $('#frmL3AddOrigenes input[name=address_lat]').val(place.geometry.location.lat());
      $('#frmL3AddOrigenes input[name=address_lng]').val(place.geometry.location.lng());
      Core.getGeocoder(place.geometry.location).then(function(res){
        neighborhood = "";
        for(var component of res.address_components) {
          for(var type of component.types) {
            if(component.types[0] =='locality' && component.types[1]=='political') {
              neighborhood = component.long_name;
              console.log(neighborhood);
              break;
            }
          }
          if(neighborhood!="") {
            break;
          }
        }
        $('#frmL3AddOrigenes input[name=neighborhood]').val(neighborhood);
        $('#frmL3AddOrigenes input[name=address]').val(res.formatted_address);
      })
      clearMarkersL3Origen();
      var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
      mapL3AddOrigen.panTo(center);
      addMarkerL3AddOrigen(center);
  });
}

// Adds a marker to the map and push to the array.
function addMarkerL3AddOrigen(location) {
  const marker = new google.maps.Marker({
    position: location,
    map: mapL3AddOrigen,
  });
  markersL3Origen.push(marker);
}

// Sets the map on all markersL2 in the array.
function setMapOnAllL3Origen(map) {
  for (let i = 0; i < markersL3Origen.length; i++) {
    markersL3Origen[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkersL3Origen() {
  setMapOnAllL3Origen(null);
}

// Shows any markers currently in the array.
function showMarkersL3Origen() {
  setMapOnAllL3Origen(mapL3AddOrigen);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkersL3Origen() {
  clearMarkersL3Origen();
  markersL3Origen = [];
}

$('#frmL3AddOrigenes').validate({
  submitHandler: function (form) {
      itemData = new FormData(form);
      action = 'add';
      idDestino=null;
      if($('#idOrigenL3').val()!="") {
        if(arrOrigenesL3[$('#idOrigenL3').val()].id!=null) {
          idDestino = arrOrigenesL3[$('#idOrigenL3').val()].id;
          action = 'edit'
        }
      }
      entrega = {
        id              : idDestino,
        action          : action,
        name:             $('#frmL3AddOrigenes input[name=name]').val(),
        phone:            $('#frmL3AddOrigenes input[name=phone]').val(),
        note:             $('#frmL3AddOrigenes input[name=note]').val(),
        quantity:         $('#frmL3AddOrigenes input[name=quantity]').val(),
        address:          $('#frmL3AddOrigenes input[name=address]').val(),
        lat:              $('#frmL3AddOrigenes input[name=address_lat]').val(),
        lng:              $('#frmL3AddOrigenes input[name=address_lng]').val(),
        neighborhood:     $('#frmL3AddOrigenes input[name=neighborhood]').val()
      };
      if($('#idOrigenL3').val()!="") {
        //Actualizar destino
          arrOrigenesL3[$('#idOrigenL3').val()]= entrega;
      } else {
        arrOrigenesL3.push(entrega);
      }
      Core.setLocalStorage('arrOrigenesL3',arrOrigenesL3);
      //listarDestinosL3();//Redraw lista
      Core.showToastStr('success','Destino de entrega agregada correctamente, esto será permanente al guardar.');
      $('#frmL3AddOrigenes').trigger('reset').validate().resetForm();
      $('.is-valid').removeClass('is-valid');
      $('.is-invalid').removeClass('is-invalid');
      if($('#idOrigenL3').val()!="") {
        $('#btnAddOrigenL3Cancel').trigger('click');
        $('#idOrigenL3').val('');
      } else {
        bootbox.confirm({
          message: '<center><b>¿Desea agregar otro destino de entrega?</b></center>',
          buttons: {
              confirm: {
                        label: 'Sí',
                        className: 'btn-success'
                  },
              cancel: {
                        label: 'No',
                        className: 'btn-danger'
                  }
              },
            callback: function(result) {
                if (!result) {
                     $('#btnAddOrigenL3Cancel').trigger('click');
                    }
                }
        });
      }
  },
  errorPlacement: function(error, element) {
    $(element).addClass('is-invalid');
  },
  highlight: function(element) {
    $(element).addClass("is-invalid");
    },
  unhighlight: function(element) {
        $(element).removeClass("is-invalid").addClass('is-valid');
  }
});

$(document).on('click','#btnEditOrigenL3', function() {
  data = arrOrigenesL3[$(this).data('index')];
  if(data.id!=null) {
    $('#frmL3AddOrigenes input[name=idOrigenL3]').val(data.id);
  } else {
    $('#frmL3AddOrigenes input[name=idOrigenL3]').val('');
  }
  $('#idOrigenL3').val($(this).data('index'));
  $('#btnAddOrigenL3Add').trigger('click');
  $('#frmAddOrigenL3Form').show();
  $('#mapL3Origenes').hide();
  $('#mapL3Origen').show();
  $('#frmL3AddOrigenes input[name=name]').val(data.name);
  $('#frmL3AddOrigenes input[name=phone]').val(data.phone);
  $('#frmL3AddOrigenes input[name=note]').val(data.note);
  $('#frmL3AddOrigenes input[name=quantity]').val(data.quantity);
  $('#frmL3AddOrigenes input[name=address]').val(data.address);
  $('#frmL3AddOrigenes input[name=address_lat]').val(data.lat);
  $('#frmL3AddOrigenes input[name=address_lng]').val(data.lng);
  $('#frmL3AddOrigenes input[name=neighborhood]').val(data.neighborhood);
  loadMapL3OrigenesAdd();
});
$(document).on('click','#btnDeleteOrigenL3',function() {
index = $(this).data('index');
bootbox.confirm({
   message: 'Está acción será irreversible, <br><center><b>¿Desea continuar?</b></center>',
   buttons: {
       confirm: {
                label: 'Sí',
                className: 'btn-success'
           },
       cancel: {
                label: 'No',
                className: 'btn-danger'
           }
      },
    callback: function(result) {
         if (result==true) {
              arrOrigenesL3.splice(index,1);
              Core.setLocalStorage('arrOrigenesL3',arrOrigenesL3).then(function() {
                listarOrigenesL3();
              });
            }
         }
});
});

$('#btnAddOrigenesL3Save').click(function() {
    if(arrOrigenesL3.length>0) {
      itemSelected.collect = arrOrigenesL3;
      //Quantity Packages
      let quantity = 0;
      $.each(arrOrigenesL3, function(index,item) {
        console.log(index,item);
        quantity =  quantity + parseInt(item.quantity);
      });
      //Distance
      let distance = 0;
      $.each(arrOrigenesL3, function(index,item) {
        console.log(index,item.distance);
        distance =  distance + parseFloat(item.distance);
      });
      itemSelected.quantity = quantity;
      itemSelected.distance = distance;
      console.log(distance);
      localStorage.setItem('arrData',JSON.stringify(arrData));
      drawData();
      $('#mdlOrigenesL3').modal('hide');
    } else {
      Core.showToastStr('error','Debe agregar minimo 1 destino de entrega');
    }
  });
