var destino=null;
let mapL4Destino = 'x';
let markersL4Destino = [];
  function loadMapL4Destinos() {
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const center = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    mapL4Destino = new google.maps.Map(document.getElementById('mapL4Destinos'), {zoom: 14, center: center});
                    lat = $('#frmL4AddDestinosForm input[name=destino_address_lat]').val();
                    lng = $('#frmL4AddDestinosForm input[name=destino_address_lng]').val();
                    if(lat!=""&&lng!="") {
                    const center = {
                        lat: lat*1,
                        lng: lng*1
                    };
                    console.log(center);
                    addMarkerL4Destino(center);
                    mapL4Destino.setCenter(center);
                    } else {
                    mapL4Destino.setCenter(center);
                    }
                    //Click Listener Google Maps
                    mapL4Destino.addListener("click", (event) => {
                        deleteMarkersL4Destino();
                        addMarkerL4Destino(event.latLng);
                        $('#frmL4AddDestinosForm input[name=destino_address_lat]').val(event.latLng.lat());
                        $('#frmL4AddDestinosForm input[name=destino_address_lng]').val(event.latLng.lng());
                        Core.getGeocoder(event.latLng).then(function(res){
                        neighborhood = "";
                        for(var component of res.address_components) {
                            for(var type of component.types) {
                            if(component.types[0] =='locality' && component.types[1]=='political') {
                                neighborhood = component.long_name
                                break;
                            }
                            }
                            if(neighborhood!="") {
                            break;
                            }
                        }
                        $('#frmL4AddDestinosForm input[name=destino_neighborhood]').val(neighborhood);
                        $('#frmL4AddDestinosForm input[name=destino_address]').val(res.formatted_address);
                        }).catch(function(){
                        Core.showToastStr('error','No ha sido posible obtener dirección');
                        });
                    });
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        }
        autocompleteDestino = new google.maps.places.Autocomplete($('#frmL4AddDestinosForm input[name=destino_address]')[0], {types: ['geocode']});
        autocompleteDestino.setFields(['geometry']);
        autocompleteDestino.addListener('place_changed', function() {
            var place = autocompleteDestino.getPlace();
            $('#frmL4AddDestinosForm input[name=destino_address_lat]').val(place.geometry.location.lat());
            $('#frmL4AddDestinosForm input[name=destino_address_lng]').val(place.geometry.location.lng());
            Core.getGeocoder(place.geometry.location).then(function(res){
            neighborhood = "";
            for(var component of res.address_components) {
                for(var type of component.types) {
                if(component.types[0] =='locality' && component.types[1]=='political') {
                    neighborhood = component.long_name;
                    console.log(neighborhood);
                    break;
                }
                }
                if(neighborhood!="") {
                break;
                }
            }
            $('#frmL4AddDestinosForm input[name=destino_neighborhood]').val(neighborhood);
            $('#frmL4AddDestinosForm input[name=destino_address]').val(res.formatted_address);
            }).catch(function(){
            Core.showToastStr('error','No ha sido posible obtener dirección');
            });
            clearMarkersL4Destino();
            var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            mapL4Destino.panTo(center);
            addMarkerL4Destino(center);
        });
  }
  // Adds a marker to the map and push to the array.
  function addMarkerL4Destino(location) {
    const marker = new google.maps.Marker({
      position: location,
      map: mapL4Destino,
    });
    markersL4Destino.push(marker);
  }

  // Sets the map on all markersL1 in the array.
  function setMapOnAllL4Destino(map) {
    for (let i = 0; i < markersL4Destino.length; i++) {
      markersL4Destino[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkersL4Destino() {
    setMapOnAllL4Destino(null);
  }

  // Shows any markers currently in the array.
  function showMarkersL4Destino() {
    setMapOnAllL4Destino(mapL4Destino);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkersL4Destino() {
    clearMarkersL4Destino();
    markersL4Destino = [];
  }
//Destinos
$(document).on('click','#btnDestinosList', function() {
    origen = arrOrigenesL4[$(this).data('index')];
    $('#idOrigenDestinosL4').val($(this).data('index'));
    arrDestinosL4 = [];
    if(origen.deliveries!=null) {
        arrDestinosL4 = origen.deliveries;
    }
    Core.setLocalStorage('arrDestinosL4',arrDestinosL4);
    $('#frmL4AddOrigenForm').hide();
    $('#frmListDestinosOrigenesL4').hide();
    $('#frmL4AddDestinos').show();
    $('#frmAddDestinosL4List').show();
    listarDestinosL4();
});

$('#btnListAddDestinoL4Cancel').click(function(){
    $('#frmL4AddDestinosForm').hide();
    $('#frmListDestinosOrigenesL4').show();
    $('#frmL4AddDestinosForm').hide();
    $('#frmAddDestinosL4List').hide();
    listarOrigenesDestinosL4();
});
$('#btnAddDestinoL4Add').click(function() {
    $('#frmL4AddDestinosForm').show();
    $('#destino_name').val('')
    $('#destino_phone').val('')
    $('#destino_quantity').val('')
    $('#destino_type').val('')
    $('#destino_note').val('')
    $('#destino_address').val('')
    $('#destino_address_lat').val('')
    $('#destino_address_lng').val('')
    $('#destino_neighborhood').val()
    $('#frmAddDestinosL4List').hide();
    loadMapL4Destinos();
});
$('#btnAddDestinoL4Cancel').click(function(){
    $('#frmL4AddDestinosForm').hide();
    $('#frmAddDestinosL4List').show();
});
$('#btnAddDestinoL4').click(function() {
    action = 'add';
    idDestino=null;
    if($('#idDestinoL4').val()!="") {
        if(arrDestinosL4[$('#idDestinoL4').val()].id!=null) {
          idDestino = arrDestinosL4[$('#idDestinoL4').val()].id;
          action = 'edit'
        }
    }
    destino = {
        id              : idDestino,
        action          : action,
        name:             $('#frmL4AddDestinosForm input[name=destino_name]').val(),
        phone:            $('#frmL4AddDestinosForm input[name=destino_phone]').val(),
        note:             $('#frmL4AddDestinosForm input[name=destino_note]').val(),
        quantity:         $('#frmL4AddDestinosForm input[name=destino_quantity]').val(),
        address:          $('#frmL4AddDestinosForm input[name=destino_address]').val(),
        lat:              $('#frmL4AddDestinosForm input[name=destino_address_lat]').val(),
        lng:              $('#frmL4AddDestinosForm input[name=destino_address_lng]').val(),
        neighborhood:     $('#frmL4AddDestinosForm input[name=destino_neighborhood]').val(),
        distance:0
    };
    //Si deseamos editar un destino
    if($('#idDestinoL4').val()!="") {
        arrDestinosL4[$('#idDestinoL4').val()] = destino;
        //Si editamos un destino que no es el primero comparamos la distrancia con el destino anterior a este
        if($('#idDestinoL4').val()!=0) {
            destino.distance = parseFloat(Core.getDistancia({
                a: {
                    lat: arrDestinosL4[$('#idDestinoL4').val()-1].lat,
                    lng: arrDestinosL4[$('#idDestinoL4').val()-1].lng
                },
                b: {
                    lat: destino.lat,
                    lng: destino.lng
                }
            })).toFixed(2);
        } else {
            //Si el destino es el primero de la lista lo comparamos con el origen de recoleccion
            destino.distance = parseFloat(Core.getDistancia({
                a: {
                    lat: origen.lat,
                    lng: origen.lng
                },
                b: {
                    lat: destino.lat,
                    lng: destino.lng
                }
            })).toFixed(2);
        }
    } else {
        //Si el destino a agregar es nuevo, no a editar, verificamos si existe algun destino anterior
        if(arrDestinosL4.length>0) {
            //Como existe ya un destino registrado comparamos la distancia con este y el nuevo destino
            destino.distance = parseFloat(Core.getDistancia({
                a: {
                    lat: arrDestinosL4[arrDestinosL4.length-1].lat,
                    lng: arrDestinosL4[arrDestinosL4.length-1].lng
                },
                b: {
                    lat: destino.lat,
                    lng: destino.lng
                }
            })).toFixed(2);
        } else {
         // Si el destino es el primero que agregaremos entonces obtenemos la distancia entre el origen y el nuevo destino
            destino.distance = parseFloat(Core.getDistancia({
                a: {
                    lat: origen.lat,
                    lng: origen.lng
                },
                b: {
                    lat: destino.lat,
                    lng: destino.lng
                }
            })).toFixed(2);
        }
        arrDestinosL4.push(destino);
    }
    Core.setLocalStorage('arrDestinosL4',arrDestinosL4);
    if($('#idDestinoL4').val()=="") {
    bootbox.confirm({
        message: '<center><b>¿Desea agregar otro destino?</b></center>',
        buttons: {
            confirm: {
                      label: 'Sí',
                      className: 'btn-success'
                },
            cancel: {
                      label: 'No',
                      className: 'btn-danger'
                }
            },
          callback: function(result) {
              if (!result) {
                    $('#idDestinoL4').val('');
                    listarDestinosL4();
                    $('#btnAddDestinoL4Cancel').trigger('click');
                    $('#frmL4AddDestinosForm input[name=destino_name]').val('')
                    $('#frmL4AddDestinosForm input[name=destino_phone]').val('')
                    $('#frmL4AddDestinosForm input[name=destino_note]').val('')
                    $('#frmL4AddDestinosForm input[name=destino_quantity]').val('')
                    $('#frmL4AddDestinosForm input[name=destino_address]').val(''),
                    $('#frmL4AddDestinosForm input[name=destino_address_lat]').val(''),
                    $('#frmL4AddDestinosForm input[name=destino_address_lng]').val(''),
                    $('#frmL4AddDestinosForm input[name=destino_neighborhood]').val('')
                  }
              }
      });
    } else {
        $('#idDestinoL4').val('');
        $('#btnAddDestinoL4Cancel').trigger('click');
        listarDestinosL4();
    }
});
$(document).on('click','#btnEditDestinoL4',function() {
    destino = arrDestinosL4[$(this).data('index')];
    console.log(destino);
    $('#idDestinoL4').val($(this).data('index'));
    $('#idOrigenL4').val($(this).data('index'));
    $('#frmAddDestinosL4List').hide();
    $('#frmL4AddDestinosForm').show();
    $('#frmL4AddDestinosForm input[name=destino_name]').val(destino.name);
    $('#frmL4AddDestinosForm input[name=destino_phone]').val(destino.phone);
    $('#frmL4AddDestinosForm input[name=destino_note]').val(destino.note);
    $('#frmL4AddDestinosForm input[name=destino_quantity]').val(destino.quantity);
    $('#frmL4AddDestinosForm input[name=destino_address]').val(destino.address);
    $('#frmL4AddDestinosForm input[name=destino_address_lat]').val(destino.lat);
    $('#frmL4AddDestinosForm input[name=destino_address_lng]').val(destino.lng);
    $('#frmL4AddDestinosForm input[name=destino_neighborhood]').val(destino.neighborhood);
    loadMapL4Destinos();
});
$('#btnAddDestinosSaveL4').click(function() {
    if(arrDestinosL4.length>0) {
        arrOrigenesL4[$('#idOrigenDestinosL4').val()].deliveries = arrDestinosL4;
        countAssigneds = 0;
        sumDistance = 0;
        arrDestinosL4.map(function(destino) {
            if(destino.action!=null) {
                if(destino.action != 'delete') {
                    countAssigneds = countAssigneds + parseInt(destino.quantity);
                    sumDistance = sumDistance + parseFloat(destino.distance);
                }
            } else {
                countAssigneds = countAssigneds + parseInt(destino.quantity);
                sumDistance = sumDistance + parseFloat(destino.distance);
            }
        });
        arrOrigenesL4[$('#idOrigenDestinosL4').val()].assigneds = countAssigneds;
        arrOrigenesL4[$('#idOrigenDestinosL4').val()].distance = sumDistance;
        arrOrigenesL4[$('#idOrigenDestinosL4').val()].action = 'edit';
        if(countAssigneds>arrOrigenesL4[$('#idOrigenDestinosL4').val()].quantity) {
            Core.showToastStr('error','Ha asignado más paquetes que los indicados como retiro.');
        } else if(countAssigneds < arrOrigenesL4[$('#idOrigenDestinosL4').val()].quantity) {
            bootbox.confirm({
               message: '<br>La cantidad de paquetes asignados es menor que la cantidad de retiro, <center><b>¿Desea continuar?</b></center>',
               buttons: {
                   confirm: {
                            label: 'Sí',
                            className: 'btn-success'
                       },
                   cancel: {
                            label: 'No',
                            className: 'btn-danger'
                       }
                  },
                callback: function(result) {
                     if (result==true) {
                            console.log('Sí');
                            Core.setLocalStorage('arrOrigenesL4',arrOrigenesL4);
                            Core.setLocalStorage('arrData',arrData);
                            $('#btnListAddDestinoL4Cancel').trigger('click');
                        }
                     }
            });
        } else {
            Core.setLocalStorage('arrOrigenesL4',arrOrigenesL4);
            Core.setLocalStorage('arrData',arrData);
            $('#btnListAddDestinoL4Cancel').trigger('click');
        }
    } else {
        Core.showToastStr('error','Debe agregar minimo 1 destino de entrega');
    }
});
$(document).on('click','#btnEditDeleteDestinoL4',function() {
    index = $(this).data('index');
    bootbox.confirm({
    message: 'Está acción será irreversible, <br><center><b>¿Desea continuar?</b></center>',
    buttons: {
        confirm: {
                label: 'Sí',
                className: 'btn-success'
            },
        cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result==true) {
                if(arrDestinosL4[index].id!=null) {
                    arrDestinosL4[index].action = 'delete';
                } else {
                    arrDestinosL4.splice(index,1);
                }
                Core.setLocalStorage('arrDestinosL4',arrDestinosL4).then(function() {
                    listarDestinosL4();
                });
                }
            }
    });
});
$('#frmL4AddDestinos').validate({
  submitHandler: function (form) {
    itemData = new FormData(form);
    itemSelected.collect = arrOrigenesL4;
    countDistance = 0;
    arrOrigenesL4.map(function(origen){
        console.log(origen);
        if(origen.distance!=null){
            countDistance = countDistance + parseFloat(origen.distance);
        }
    });
    itemSelected.distance = countDistance;
    mergeArrayDeliveries = [];
        arrOrigenesL4.map(function(collect){
            if(collect.deliveries!=null) {
                mergeArrayDeliveries = mergeArrayDeliveries.concat(collect.deliveries);
            } else {
                Core.showToastStr('error','El paquete con remitente - '+collect.name+' no se han asignado direcciones de retiro.');
            }
        })
    itemSelected.delivery = mergeArrayDeliveries;
    itemSelected.action = 'edit';
    localStorage.setItem('arrData',JSON.stringify(arrData));
    drawData();
    $('#mdlDestinoL4').modal('hide');
}});
function listarDestinosL4() {
    //Planificar Ruta
    $('#listDestinosL4').empty();
        Core.getLocalStorage('arrDestinosL4').then(function(res) {
        arrDestinosL4 = [];
        if(data.deliveries!=null) {
            arrDestinosL4 = data.deliveries;
        }
        let drawListDestinosL4 = res;
        arrDestinosL4 = res;
        var countLista = 0;
        $.each(drawListDestinosL4,function(index,item) {
            if(index == 0) {
                item.distance = Core.getDistancia({
                    a: origen,
                    b: item
                });
            } else {
                item.distance = Core.getDistancia({
                    a: drawListDestinosL4[index-1],
                    b: item
                });
            }
            return item;
        });
        $.each(drawListDestinosL4,function(index,item) {
            mostrar = true;
            if(item.action!=null){
                if(item.action=='delete') {
                    mostrar = false;
                }
            }
            if(mostrar) {
                itemDestinos = `
            <tr>
                <td width="1" class="text-center">${labels[index+1 % labels.length]}</td>
                <td width="1" class="text-center">${item.quantity}</td>
                <th width="1" class="text-center">${item.distance!=null?item.distance:'0Km'}</th>
                <td>${item.name}</td>
                <td>${item.address}</td>
                <td width="80" class="text-center">
                <center>
                    <svg class="c-icon mr-2" id="btnEditDestinoL4" data-index='${index}' style="cursor: pointer;">
                        <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                    </svg>
                    <svg class="c-icon mr-2" id="btnEditDeleteDestinoL4" data-index="${index}" style="cursor: pointer;">
                        <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                    </svg>
                </center>
                </td>
            </tr>
            `;
            $('#listDestinosL4').append(itemDestinos);
            }
        });
    });
}

function listarOrigenesDestinosL4() {
    //Planificar Ruta
    $('#listOrigenesDestinosL4').empty();
    Core.getLocalStorage('arrOrigenesL4').then(function(res){
    let drawListL4 = res;
    arrOrigenesL4 = res;
        $.each(drawListL4,function(index,item) {
            itemOrigen = `
            <tr>
            <td width="1" class="text-center">${labels[index % labels.length]}</td>
            <td width="1" class="text-center">${item.quantity}</td>
            <td width="1" class="text-center">${item.quantity!=null?item.assigneds!=null?item.quantity - item.assigneds:item.quantity:0}</td>
            <td width="1" class="text-center">${item.assigneds!=null?item.assigneds:0}</td>
            <th width="1" class="text-center">${parseFloat(getDistanceOrderDeliveryL4(item)).toFixed(2)}Km</th>
            <td>${item.name}</td>
            <td>${item.address}</td>
            <td width="1"><button type="button" id="btnDestinosList" data-tipo="${item.type}" data-index="${index}" class="btn btn-secondary">Destinos</button></td>
            </tr>
            `;
            $('#listOrigenesDestinosL4').append(itemOrigen);
        });
    });
}

function getDistanceOrderDeliveryL4(item) {
    distance = 0;
    if(item.deliveries!=null) {
        item.deliveries.map(function(delivery) {
            distance = distance + parseFloat(Core.getDistancia({
                a: {
                    lat: item.lat,
                    lng: item.lng
                },
                b:{
                    lat: delivery.lat,
                    lng: delivery.lng
                }
            }));
        });
    }
    return distance;
}
function getDistanceDeliveryL4(item) {
    distance = parseFloat(Core.getDistancia({
        a: {
            lat: origen.lat,
            lng: origen.lng
        },
        b:{
            lat: item.lat,
            lng: item.lng
        }
    }));
    return distance;
}
