arrOrigenesL4 = [];
arrDestinosL4 = [];
var origen=null;
//Renderear Lista de origenes
function listarOrigenesL4() {
    //Planificar Ruta
    $('#listOrigenesL4').empty();
    Core.getLocalStorage('arrOrigenesL4').then(function(res) {
    let drawListL4 = res;
    arrOrigenesL4 = res;
        $.each(drawListL4,function(index,item) {
            itemOrigen = `
            <tr>
            <td width="1" class="text-center">${labels[index % labels.length]}</td>
            <td width="1" class="text-center">${item.quantity}</td>
            <td width="1" class="text-center">${item.quantity!=null?item.assigneds!=null?item.quantity - item.assigneds:item.quantity:0}</td>
            <td width="1" class="text-center">${item.assigneds!=null?item.assigneds:0}</td>
            <td>${item.name}</td>
            <td>${item.address}</td>
            <td width="80" class="text-center">
            <center>
                <svg class="c-icon mr-2" id="btnEditOrigenL4" data-index='${index}' style="cursor: pointer;">
                    <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                </svg>
                <svg class="c-icon mr-2" id="btnDeleteOrigenL4" data-index="${index}" style="cursor: pointer;">
                    <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                </svg>
            </center>
            </td>
            </tr>
            `;
            $('#listOrigenesL4').append(itemOrigen);
        });
    });
}
