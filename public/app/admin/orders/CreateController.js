//Impor Google Api
document.write(`<script src="https://maps.googleapis.com/maps/api/js?key=${$('#googleKey').val()}&libraries=places&callback" async defer></script>`);
route = $('#idRoute').val() + "/orders";
var fees = {
    'local_price': 0,
    'external_price': 0,
    'external_price_km': 0
};
var rates = [];
var lines = [];
const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var itemData;
var arrData = [];
var itemSelected;
var cost;
var quantity = 0;
var indexSelected = 0;
var arrPrices = [];
var totalDelivery = 0;
//var precioDelivery = 0;
Core.setDataTable('arrData');

// Vaciar tabla "Lista de Paquetes" al cargar la página
arrData.splice(1, 1);
localStorage.setItem('arrData', JSON.stringify(arrData));
$('#totalPreview').html('0');


$("#frmNew").validate({
    submitHandler: function (form) {
        $('.btn-guardar-orden').text('Guardando ...').prop('disabled', true);
        itemData = {
            branchoffice_id: $('#branchoffice_id').val(),
            dealer_id: $('#dealer_id').val(),
            customer_name: $('#customer_name').val(),
            customer_phone: $('#customer_phone').val(),
            customer_email: $('#customer_email').val(),
            zone_start: $('#zone_start').val(),
            zone_arrival: $('#zone_arrival').val(),
            date: $('#date').val(),
            round: $('#round').val(),
            status: $('#status').val(),
            service: $('#service').val(),
            type: $('#type').val(),
            quote_id: $('#quote_id').val(),
            payment_method: $('#payment_method').val(),
            packages: arrData
        };
        if (arrData.length > 0) {
            // Se pisa el valor del delivery de acuerdo a lo ingresado por el cliente
            totalDelivery = $('.price').val().replace(/[.]/g,'');
            arrData[0].total = parseInt(totalDelivery);
            console.log(arrData);
            Core.post(route + '/store', itemData)
                .then(function (res) {
                    $('#mdlNew').modal('hide');
                    $('#frmNew').trigger('reset');
                    $('#previewDefault').attr('src', urlBase + 'dashboard/assets/img/default_user.png');
                    Core.showToastStr("success", res.data.message);
                    bootbox.confirm({
                        message: 'Desea registrar otra orden de entrega?',
                        buttons: {
                            confirm: {
                                label: 'Sí',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result == true) {
                                Core.removeLocalStorage([
                                    'arrData',
                                    'arrOrigenesL2',
                                    'arrOrigenesL1',
                                    'arrOrigenesL3',
                                    'arrDestinosL2',
                                    'arrDestinosL1',
                                    'arrDestinosL3'
                                ]).finally(function () {
                                    $('.btn-guardar-orden').text('Guardar').prop('disabled', false);
                                    window.location.href = urlBase + route
                                })
                            } else {
                                Core.removeLocalStorage([
                                    'arrData',
                                    'arrOrigenesL1',
                                    'arrOrigenesL2',
                                    'arrDestinosL2',
                                    'arrDestinosL1'
                                ]).finally(function () {
                                    $('.btn-guardar-orden').text('Guardar').prop('disabled', false);
                                    window.location.href = urlBase + route
                                });
                            }
                        }
                    });
                })
                .catch(function (err) {
                    //return false;
                    Core.showToastStr('error', err.response.data.error.message);
                    $('.btn-guardar-orden').text('Guardar').prop('disabled', false);
                })
        } else {
            //return false;
            Core.showToastStr('error', 'Debe tener almenos 1 paquete para envio.')
            $('.btn-guardar-orden').text('Guardar').prop('disabled', false);
        }
    }
});



<!-- BTN ELIMINAR  -->
$(document).on('click', '#btnDeleteCustome', function () {
    index = $(this).data('index');
    bootbox.confirm({
        message: 'Por favor confirme la eliminación, esta acción será irreversible, <br><center><b>¿Desea continuar?</b></center>',
        buttons: {
            confirm: {
                label: 'Sí',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result == true) {
                arrData.splice(index, 1);
                localStorage.setItem('arrData', JSON.stringify(arrData));
                drawData();
            }
        }
    });
});



$('#service').on('change', function () {
    switch (parseInt($(this).val())) {
        case 1: {
            $('#type_view').show();
            $('#type').attr('required', true);
            $('#total_view').hide();
            $('#total').attr('required', false);
            $('#quote_view').hide();
            $('#totalPreview').show();
            break;
        }
        case 2: {
            $('#total_view').show();
            $('#total').val('');
            $('#total').attr('required', true);
            $('#type_view').hide();
            $('#type').val('');
            $('#date_view').show();
            $('#date').val('');
            $('#date').attr('required', true);
            $('#round_view').show();
            $('#round').attr('required', true);
            $('#quote_view').show();
            $('#quote_id').attr('required', false);
            $('#totalPreview').hide();
            break;
        }
        default: {
            $('#type_view').hide();
            $('#type').attr('required', false);
            $('#total_view').hide();
            $('#total').attr('required', false);
            $('#quote_view').hide();
            break;
        }
    }
    drawData();
});

$('#type').on('change', function () {
    if ($(this).val() == 1) {
        $('#date_view').hide();
        $('#date').attr('required', false);
        $('#date').val('');
        $('#round_view').hide();
        $('#round').attr('required', false);
    } else if ($(this).val() == 2) {
        $('#date').attr('required', true);
        $('#date_view').show();
        $('#round_view').show();
        $('#round').attr('required', true);
    }
    drawData();
});

$('#branchoffice_id').on('change', (function () {
    $('#frmNew select[name=dealer_id]').empty();
    Core.get(route + '/dealers/' + $(this).val())
        .then(function (res) {
            $.each(res.data.data, function (index, item) {
                $('#frmNew select[name=dealer_id]').append($('<option data-index="' + index + '"></option>').val(item.id).html(item.name));
            });
        }).catch(function (err) {
        Core.showToastStr('error', err.response.data.error.message);
    });
}));

$('#quote_id').change(function () {
    Core.get($('#idRoute').val() + '/quotes/find/' + $(this).val()).then(function (res) {
        quote = res.data.data;
        $('#customer_name').val(quote.customer_data.name);
        $('#customer_phone').val(quote.customer_data.phone);
        $('#customer_email').val(quote.customer_data.email);
        $('#zone_start').val(quote.zone_start).trigger('change');
        $('#zone_arrival').val(quote.zone_arrival).trigger('change');
    }).catch(function (err) {
        console.log(err);
        Core.showToastStr('error', 'No ha sido posible obtener datos de cotizaciones, por favor verifique que la solicitud aun exista.')
    });
});

// Modal "Lista de paquetes"
$(document).on('click', '#btnLogistic', function () {
    $('#btnLogisticCancel').trigger('click');
    switch (parseInt($(this).data('index'))) {
        // L1
        case 1: {
            arrData.push({
                type: 'L1',
                quantity: 0,
                collect: null,
                delivery: null,
                total: 0,
                status: 0,
                distance: 0
            });
            localStorage.setItem('arrData', JSON.stringify(arrData));
            console.log(arrData);
            break;
        }

        // L2
        case 2: {
            arrData.push({
                type: 'L2',
                quantity: 0,
                collect: null,
                delivery: null,
                total: 0,
                status: 0,
                distance: 0
            });
            localStorage.setItem('arrData', JSON.stringify(arrData));
            break;
        }

        // L3
        case 3: {
            arrData.push({
                type: 'L3',
                quantity: 0,
                collect: null,
                delivery: null,
                total: 0,
                status: 0,
                distance: 0
            });
            localStorage.setItem('arrData', JSON.stringify(arrData));
            break;
        }

        // L4
        case 4: {
            arrData.push({
                type: 'L4',
                quantity: 0,
                collect: null,
                delivery: null,
                total: 0,
                status: 0,
                distance: 0
            });
            localStorage.setItem('arrData', JSON.stringify(arrData));
            break;
        }
    }

    // Después llama a esta función
    drawData();
    $('#btnLogistic').prop('disabled', true);
});

function drawData() {
    totalPreview = 0;
    tblData.rows().remove().draw();
    //Reestructurar de memoria
    arrData = JSON.parse(localStorage.getItem('arrData'));
    if (!arrData) {
        arrData = [];
    }
    $.each(arrData, (function (index, item) {
        collect = "";
        delivery = "";
        switch (item.type) {
            case 'L1': {
                collect = item.collect != null ? '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Origen (' + item.collect.length + ')</button>'
                    : '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Origen</button>';
                delivery = item.delivery != null ? '<button type="button" id="btnDestinos" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Destino (' + item.delivery.length + ')</button>'
                    : '<button type="button" id="btnDestinos"' + ' data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Destino</button>';
                break;
            }
            case 'L2': {
                collect = item.collect != null ? '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Origen(' + item.collect.length + ')</button>' : '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Origen</button>';
                delivery = item.delivery != null ? '<button type="button" id="btnDestinos" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Destinos(' + item.delivery.length + ')</button>' : '<button type="button" id="btnDestinos" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Destinos</button>';
                break;
            }
            case 'L3': {
                collect = item.collect != null ? '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Origen(' + item.collect.length + ')</button>' : '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Origen</button>';
                delivery = item.delivery != null ? '<button type="button" id="btnDestinos" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Destinos(' + item.delivery.length + ')</button>' : '<button type="button" id="btnDestinos" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Destinos</button>';
                break;
            }
            case 'L4': {
                collect = item.collect != null ? '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Origens(' + item.collect.length + ')</button>' : '<button type="button" id="btnOrigenes" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Origenes</button>';
                delivery = item.delivery != null ? '<button type="button" id="btnDestinos" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-success">Destinos(' + item.delivery.length + ')</button>' : '<button type="button" id="btnDestinos" data-tipo="' + item.type + '" data-index="' + index + '" class="btn btn-secondary">Destinos</button>';
                break;
            }
        }

        price = '-';
        if ($('#service').val() == 1) {
            //price = `₲${Core.setMiles(calcularprecio(item))}`;
            price = `${Core.setMiles(calcularprecio(item))}`;
        }
        distance = 0;
        if ($('#service').val() == 1 && $('#type').val() == 1) {
            distance = Core.setMiles(item.distance);
        } else {
            distance = '-';
        }
        tblData.row.add([
            index,
            item.type,
            '<center>' + item.quantity + '</center>',
            '<center>' + collect + '</center>',
            '<center>' + delivery + '</center>',
            `<center>${isNaN(distance) ? '-' : distance + 'Km'}</center>`,
            `<center> <input type="text" id="price" value="${price}" class="price" onkeyup="formatNumberInput(this)" onchange="formatNumberInput(this)"> </center>`,
            `<center>
                        <svg class="c-icon mr-2" id="btnRouteMap" data-tipo="${item.type}" data-index='${index}' style="cursor: pointer;" ${item.type == 'L4' ? 'hidden' : ''}>
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-map"></use>
                        </svg>
                        <svg class="c-icon mr-2" id="btnDeleteCustome" data-index='${index}' style="cursor: pointer;">
                            <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                        </svg>
                    </center>`
        ]).draw();
        Core.setLocalStorage('arrData', arrData);
        totalPreview = totalPreview + item.total;
        //$('#totalPreview').html('₲' + Core.setMiles(totalPreview));
        $('#totalPreview').html(Core.setMiles(totalPreview));
    }));
}



$(document).on('click', '#btnOrigenes', function () {
    //console.log($(this).data('tipo'));
    switch ($(this).data('tipo')) {
        case "L1": {
            $('#mdlOrigenL1').modal('show');
            itemSelected = arrData[$(this).data('index')];

            if (itemSelected.collect != null) {
                loadMapL1Origen();
                $('#frmL1AddOrigen input[name=l1_name]').val(itemSelected.collect[0].name),
                    $('#frmL1AddOrigen input[name=l1_phone]').val(itemSelected.collect[0].phone),
                    $('#frmL1AddOrigen input[name=l1_note]').val(itemSelected.collect[0].note),
                    $('#frmL1AddOrigen input[name=l1_quantity]').val(itemSelected.collect[0].quantity),
                    $('#frmL1AddOrigen select[name=l1_type]').val(itemSelected.collect[0].type),
                    $('#frmL1AddOrigen input[name=l1_address]').val(itemSelected.collect[0].address),
                    $('#frmL1AddOrigen input[name=l1_address_lat]').val(itemSelected.collect[0].lat),
                    $('#frmL1AddOrigen input[name=l1_adddres_lng]').val(itemSelected.collect[0].lng);
                $('#frmL1AddOrigen input[name=l1_neighborhood]').val(itemSelected.collect[0].neighborhood);
                setTimeout(function () {
                    var center = new google.maps.LatLng(itemSelected.collect[0].lat, itemSelected.collect[0].lng);
                    mapL1Origen.panTo(center);
                    addMarkerL1Origen(center);
                }, 1000);
            } else {
                loadMapL1Origen();
            }
            break;
        }
        case "L2": {
            $('#mdlOrigenL2').modal('show');
            itemSelected = arrData[$(this).data('index')];
            //FillForm
            if (itemSelected.collect != null) {
                loadMapL2Origen();
                $('#frmL2AddOrigen input[name=name]').val(itemSelected.collect[0].name),
                    $('#frmL2AddOrigen input[name=phone]').val(itemSelected.collect[0].phone),
                    $('#frmL2AddOrigen input[name=note]').val(itemSelected.collect[0].note),
                    $('#frmL2AddOrigen input[name=quantity]').val(itemSelected.collect[0].quantity),
                    $('#frmL2AddOrigen select[name=type]').val(itemSelected.collect[0].type),
                    $('#frmL2AddOrigen input[name=address]').val(itemSelected.collect[0].address),
                    $('#frmL2AddOrigen input[name=address_lat]').val(itemSelected.collect[0].lat),
                    $('#frmL2AddOrigen input[name=adddres_lng]').val(itemSelected.collect[0].lng);
                $('#frmL2AddOrigen input[name=neighborhood]').val(itemSelected.collect[0].neighborhood);
                setTimeout(function () {
                    var center = new google.maps.LatLng(itemSelected.collect[0].lat, itemSelected.collect[0].lng);
                    mapL2Origen.panTo(center);
                    addMarkerL2Origen(center);
                }, 1000);
            } else {
                loadMapL2Origen();
            }
            break;
        }
        case "L3": {
            itemSelected = arrData[$(this).data('index')];
            if (itemSelected.delivery != null) {
                $('#mdlOrigenesL3').modal('show');
                $('#frmAddOrigenL3Form').hide();
                $('#mapL3Origen').hide();
                $('#mapL3Origenes').show();
                if (itemSelected.collect != null) {
                    arrOrigenesL3 = itemSelected.collect;
                } else {
                    arrOrigenesL3 = [];
                }
                Core.setLocalStorage('arrOrigenesL3', arrOrigenesL3);
                loadMapL3OrigenesAdd();
            } else {
                Core.showAlert('error', 'Para esté caso debe agregar primero el lugar de entrega.');
            }
            break;
        }
        case "L4": {
            itemSelected = arrData[$(this).data('index')];
            $('#mdlOrigenesL4').modal('show');
            $('#frmL4AddOrigenForm').hide();
            $('#frmAddOrigenL4List').show();
            $('#mapL4Origen').hide();
            $('#mapL4Origenes').show();
            //Hide components for Deliveries
            $('#frmL4AddDestinosForm').hide();
            $('#frmAddDestinosL4List').hide();
            if (itemSelected.collect != null) {
                arrOrigenesL4 = itemSelected.collect;
            } else {
                arrOrigenesL4 = [];
            }
            Core.setLocalStorage('arrOrigenesL4', arrOrigenesL4);
            listarOrigenesL4();
            break;
        }
    }
});

$(document).on('click', '#btnDestinos', function () {
    switch ($(this).data('tipo')) {
        case "L1": {
            $('#mdlDestinoL1').modal('show');
            itemSelected = arrData[$(this).data('index')];
            //FillForm
            if (itemSelected.collect != null) {
                loadMapL1Destino();
                $('#frmL1AddDestino input[name=l1_name]').val(itemSelected.delivery[0].name),
                    $('#frmL1AddDestino input[name=l1_phone]').val(itemSelected.delivery[0].phone),
                    $('#frmL1AddDestino input[name=l1_note]').val(itemSelected.delivery[0].note),
                    $('#frmL1AddDestino input[name=l1_quantity]').val(itemSelected.delivery[0].quantity),
                    $('#frmL1AddDestino select[name=l1_type]').val(itemSelected.delivery[0].type),
                    $('#frmL1AddDestino input[name=l1_address]').val(itemSelected.delivery[0].address),
                    $('#frmL1AddDestino input[name=l1_address_lat]').val(itemSelected.delivery[0].lat),
                    $('#frmL1AddDestino input[name=l1_adddres_lng]').val(itemSelected.delivery[0].lng);
                $('#frmL1AddOrigen input[name=l1_neighborhood]').val(itemSelected.delivery[0].neighborhood);
                dataDestinoL1Address = itemSelected.delivery[0].address_components;
                setTimeout(function () {
                    var center = new google.maps.LatLng(itemSelected.delivery[0].lat, itemSelected.delivery[0].lng);
                    mapL1Destino.panTo(center);
                    addMarkerL1Destino(center);
                }, 1000);
            } else {
                loadMapL1Destino();
            }
            break;
        }
        case "L2": {
            itemSelected = arrData[$(this).data('index')];
            limitPackagesDestinosL2 = parseInt(itemSelected.quantity);
            $('#idLimitePaquetes').html(limitPackagesDestinosL2);
            if (itemSelected.collect != null) {
                $('#mdlDestinosL2').modal('show');
                if (itemSelected.delivery != null) {
                    arrDestinosL2 = itemSelected.delivery;
                } else {
                    arrDestinosL2 = [];
                }
                Core.setLocalStorage('arrDestinosL2', arrDestinosL2);
                $('#frmAddDestinoL2Form').hide();
                $('#frmAddDestinoL2List').show();
                $('#mapL2Destino').hide();
                $('#mapL2Destinos').show();
                loadMapL2Destinos();
            } else {
                Core.showAlert('error', 'Para esté caso debe agregar primero el lugar de retiro.');
            }
            break;
        }
        case "L3": {
            $('#mdlDestinoL3').modal('show');
            itemSelected = arrData[$(this).data('index')];
            //FillForm
            if (itemSelected.delivery != null) {
                loadMapL3Destino();
                $('#frmL3AddDestino input[name=name]').val(itemSelected.delivery[0].name),
                    $('#frmL3AddDestino input[name=phone]').val(itemSelected.delivery[0].phone),
                    $('#frmL3AddDestino input[name=note]').val(itemSelected.delivery[0].note),
                    $('#frmL3AddDestino input[name=quantity]').val(itemSelected.delivery[0].quantity),
                    $('#frmL3AddDestino select[name=type]').val(itemSelected.delivery[0].type),
                    $('#frmL3AddDestino input[name=address]').val(itemSelected.delivery[0].address),
                    $('#frmL3AddDestino input[name=address_lat]').val(itemSelected.delivery[0].lat),
                    $('#frmL3AddDestino input[name=adddres_lng]').val(itemSelected.delivery[0].lng);
                $('#frmL3AddDestino input[name=neighborhood]').val(itemSelected.delivery[0].neighborhood);
                dataDestinoL3Address = itemSelected.delivery[0].address_components;
                setTimeout(function () {
                    var center = new google.maps.LatLng(itemSelected.delivery[0].lat, itemSelected.delivery[0].lng);
                    mapL3Destino.panTo(center);
                    addMarkerL3Destino(center);
                }, 1000);
            } else {
                loadMapL3Destino();
            }
            break;
        }
        case "L4": {
            itemSelected = arrData[$(this).data('index')];
            arrOrigenesL4 = itemSelected.collect;
            Core.setLocalStorage('arrOrigenesL4', arrOrigenesL4);
            $('#mdlDestinoL4').modal('show');
            $('#frmAddDestinosL4List').hide();
            $('#frmListDestinosOrigenesL4').show();
            listarOrigenesDestinosL4();
            $('#frmL4AddDestinosForm').hide();
            break;
        }
    }
});

$(document).on('click', '#btnRouteMap', function () {
    tipo = $(this).data('tipo');
    itemSelected = arrData[$(this).data('index')];
    console.log(itemSelected);
    switch (tipo) {
        case "L1": {
            loadMapL1Route();
            $('#mdlRouteL1').modal('show');
            break;
        }
        case "L2": {
            loadMapL2Route();
            $('#mdlRouteL2').modal('show');
            itemSelected = arrData[$(this).data('index')];
            if (itemSelected.delivery != null) {
                arrDestinosL2 = itemSelected.delivery;
            } else {
                arrDestinosL2 = [];
            }
            break;
        }
        case "L3": {
            $('#mdlRouteL3').modal('show');
            loadMapL3Route();
            itemSelected = arrData[$(this).data('index')];
            if (itemSelected.delivery != null) {
                arrOrigenesL3 = itemSelected.collect;
            } else {
                arrOrigenesL3 = [];
            }
            break;
        }
        case "L4": {
            $('#mdlRouteL4').modal('show');
            loadMapL4Route();
            break;
        }
    }
});

//Calcular el precio segun el caso de uso
/* function calcularprecio(order) {
     return 40000;

 }*/


function calcularprecio(order) {
    // console.log('Función calcularprecio');
    // console.log(order);
    if ($('#service').val() == 1 && $('#type').val() == 1) {
        if (order.collect != null && order.delivery != null) {
            switch (order.type) {
                case 'L1': {
                    order.total = 0;
                    if (order.collect.length > 0 && order.delivery.length >= 0) {
                        if (order.collect[0].neighborhood == "Asunción" && order.delivery[0].neighborhood == "Asunción") {
                            order.total = fees.local_price;
                        } else {
                            if (order.distance < 8) {
                                order.total = fees.local_price;
                            } else {
                                order.total = Math.ceil(order.distance) * fees.external_price_km;
                            }
                        }
                    } else {
                        return 0;
                    }
                    break;
                }
                case 'L2': {
                    order.total = 0;
                    if (order.collect.length > 0 && order.delivery.length >= 0) {
                        for (let i = 0; i < order.delivery.length; i++) {
                            if (i == 0) {
                                if (order.collect[0].neighborhood == "Asunción" && order.delivery[0].neighborhood == "Asunción") {
                                    order.total = order.total + fees.local_price;
                                } else {
                                    if (order.delivery[0].distance < 8) {
                                        order.total = order.total + fees.local_price;
                                    } else {
                                        order.total = order.total + Math.ceil(order.delivery[0].distance) * fees.external_price_km;
                                    }
                                }
                            } else if (order.delivery[i - 1].neighborhood == "Asunción" && order.delivery[i].neighborhood == "Asunción") {
                                order.total = order.total + fees.local_price;
                            } else {
                                if (order.delivery[i].distance < 8) {
                                    order.total = order.total + fees.local_price;
                                } else {
                                    order.total = order.total + Math.ceil(order.delivery[i].distance) * fees.external_price_km;
                                }
                            }
                        }
                    }
                    break;
                }
                case 'L3': {
                    order.total = 0;
                    if (order.collect.length > 0 && order.delivery.length >= 0) {
                        for (let i = 0; i < order.collect.length; i++) {
                            if (i == 0) {
                                if (order.delivery[0].neighborhood == "Asunción" && order.collect[0].neighborhood == "Asunción") {
                                    order.total = order.total + fees.local_price;
                                } else {
                                    if (order.collect[0].distance < 8) {
                                        order.total = order.total + fees.local_price;
                                    } else {
                                        order.total = order.total + Math.ceil(order.collect[0].distance) * fees.external_price_km;
                                    }
                                }
                            } else if (order.collect[i - 1].neighborhood == "Asunción" && order.collect[i].neighborhood == "Asunción") {
                                order.total = order.total + fees.local_price;
                            } else {
                                if (order.collect[i].distance < 8) {
                                    order.total = order.total + fees.local_price;
                                } else {
                                    order.total = order.total + Math.ceil(order.collect[i].distance) * fees.external_price_km;
                                }
                            }
                        }
                    }
                    break;
                }
                case 'L4': {
                    order.total = 0;
                    if (order.collect != null) {
                        if (order.collect.length > 0) {
                            for (var i = 0; i < order.collect.length; i++) {
                                if (order.collect[i].deliveries != null) {
                                    for (var j = 0; j < order.collect[i].deliveries.length; j++) {
                                        if (j == 0) {
                                            if (order.collect[i].neighborhood == order.collect[i].deliveries[j].neighborhood && order.collect[i].neighborhood == 'Asunción') {
                                                order.total = order.total + fees.local_price;
                                            } else {
                                                order.collect[i].deliveries[j].distance = Core.getDistancia({
                                                    a: order.collect[i],
                                                    b: order.collect[i].deliveries[j]
                                                });
                                                if (order.collect[i].deliveries[j].distance < 8) {
                                                    order.total = order.total + fees.local_price;
                                                } else {
                                                    order.total = order.total + Math.ceil(order.collect[i].deliveries[j].distance) * fees.external_price_km;
                                                }
                                            }
                                        } else {
                                            asuncion = (order.collect[i].deliveries[j].neighborhood == order.collect[i].deliveries[j - 1].neighborhood && order.collect[i].deliveries[j].neighborhood == 'Asunción');
                                            order.collect[i].deliveries[j].distance = Core.getDistancia({
                                                a: order.collect[i].deliveries[j],
                                                b: order.collect[i].deliveries[j - 1]
                                            });
                                            if (order.collect[i].deliveries[j].distance < 8 || asuncion == true) {
                                                order.total = order.total + fees.local_price;
                                            } else {
                                                order.total = order.total + Math.ceil(order.collect[i].deliveries[j].distance) * fees.external_price_km;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            }
            return order.total;
        } else {
            return 0;
        }
    } else if ($('#service').val() == 1 && $('#type').val() == 2) {
        total = 0;
        if (order.collect != null && order.delivery != null) {
            //  total = 0;
            //Collect
            //priceService = 0;
            switch (order.type) {
                case 'L1': {
                    priceService = 0;
                    for (var i = 0; i < rates.length; i++) {
                        if (order.delivery[0].neighborhood == rates[i].zone_delivery && order.collect[0].neighborhood == rates[i].zone_collect  ) {
                            console.log('zone_collect');
                            priceService= rates[i].price;
                        }
                        else if (order.delivery[0].neighborhood == rates[i].zone_delivery &&  order.collect[0].neighborhood != rates[i].zone_collect  ) {
                            console.log('zone_collect');
                            priceService = rates[i].price;
                        }

                    }
                    if (priceService == 0) {
                        Core.showAlert('error', 'Importante uno o más direcciones de retiro o entrega se encuentran fuera del area de servicio, revise paquete tipo L1');
                    }
                    total = total + parseFloat(priceService);
                    break;
                }
                case 'L2': {
                    priceService = 0;
                    //Buscamos cada direccion de entrega y la contrastamos con la direccion de retiro en la lista de precios
                    for (var j = 0; j < order.delivery.length; j++) {
                        addressDeliveryPrice = 0;//Precio buscado en cada siclo.
                        encontrado = false;
                        //Buscamos en la lista de precios
                        for (var i = 0; i < rates.length; i++) {
                            //Que la direccion de entrega y retiro coincida, si coincde rompemos el siclo.
                            if (order.collect[0].neighborhood == rates[i].zone_collect && order.delivery[j].neighborhood == rates[i].zone_delivery) {
                                addressDeliveryPrice = parseFloat(rates[i].price);
                                encontrado = true;
                                break;
                            }
                        }
                        if (addressDeliveryPrice == 0) {
                            Core.showAlert('error', `Importante uno o más direcciones de retiro o entrega se encuentran fuera del area de servicio, revise paquete tipo L2,RETIRO:${order.collect[0].neighborhood}, ENTREGA:${order.delivery[j].neighborhood}`);
                            break;//Rompemos el siclo.
                        } else {
                            priceService = priceService + parseFloat(addressDeliveryPrice);
                        }
                    }
                    total = total + parseFloat(priceService);
                    break;
                }
                case 'L3': {
                    priceService = 0;
                    //Buscamos cada direccion de entrega y la contrastamos con la direccion de retiro en la lista de precios
                    for (var j = 0; j < order.collect.length; j++) {
                        addressDeliveryPrice = 0;//Precio buscado en cada siclo.
                        encontrado = false;
                        //Buscamos en la lista de precios
                        for (var i = 0; i < rates.length; i++) {
                            //Que la direccion de entrega y retiro coincida, si coincde rompemos el siclo.
                            if (order.collect[j].neighborhood == rates[i].zone_collect && order.delivery[0].neighborhood == rates[i].zone_delivery) {
                                addressDeliveryPrice = parseFloat(rates[i].price);
                                encontrado = true;
                                break;
                            }
                        }
                        if (addressDeliveryPrice == 0) {
                            Core.showAlert('error', `Importante uno o más direcciones de retiro o entrega se encuentran fuera del area de servicio, revise paquete tipo L3,RETIRO:${order.collect[j].neighborhood}, ENTREGA:${order.delivery[0].neighborhood}`);
                            break;//Rompemos el siclo.
                        } else {
                            priceService = priceService + parseFloat(addressDeliveryPrice);
                        }
                    }
                    total = total + parseFloat(priceService);
                    break;
                }
                case 'L4': {
                    priceService = 0;
                    for (var i = 0; i < order.collect.length; i++) {
                        if (order.collect[i].deliveries != null) {
                            for (var j = 0; j < order.collect[i].deliveries.length; j++) {
                                addressDeliveryPrice = 0;
                                encontrado = false;
                                for (var k = 0; k < rates.length; k++) {
                                    //Que la direccion de entrega y retiro coincida, si coincde rompemos el siclo.
                                    if (order.collect[i].neighborhood == rates[k].zone_collect && order.collect[i].deliveries[j].neighborhood == rates[k].zone_delivery) {
                                        addressDeliveryPrice = parseFloat(rates[k].price);
                                        encontrado = true;
                                        break;
                                    }
                                }
                                if (addressDeliveryPrice == 0) {
                                    Core.showAlert('error', `Importante uno o más direcciones de retiro o entrega se encuentran fuera del area de servicio, revise paquete tipo L4,RETIRO:${order.collect[i].neighborhood}, ENTREGA:${order.collect[i].deliveries[j].neighborhood}`);
                                    break;//Rompemos el siclo.
                                } else {
                                    priceService = priceService + parseFloat(addressDeliveryPrice);
                                }
                            }
                        }
                        return 40000;


                    }
                    total = total + parseFloat(priceService);
                    break;
                }
            }
            order.total = total;
            return total;
        } else {
            order.total = 0;
        }
    } else {
        return 0;
    }
}

function getFees() {
    //Axios Http Post Request
    Core.get(route + '/fees').then(function (res) {
        this.fees = res.data.data;
    }).catch(function (err) {
        console.log(err);
    })
}

function getRates() {
    //Axios Http Post Request
    Core.get(route + '/rates').then(function (res) {
        rates = res.data.data;
    }).catch(function (err) {
        console.log(err);
    })
}

function formatNumberInput(input){
    var num = input.value.replace(/\./g,'');
    if(!isNaN(num)){
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/,'');
        input.value = num;
    }else{
        input.value = input.value.replace(/[^\d\.]*/g,'');
    }
}

// Poner el precio del delivery en el total de la tabla (onChange de input precio delivery)
$(document).on('blur', '.price', function() {
    var precioDelivery = $(this).val();
    //console.log('Total: '+precioDelivery);

    $('#totalPreview').text(precioDelivery);
});

function init() {
    getFees();
    // Core.removeLocalStorage([
    //     'arrData',
    //     'arrDestinosL2'
    // ]);
    $('#type_view').hide();
    $('#date_view').hide();
    $('#round_view').hide();
    $('#quote_view').hide();
    $("#total_view").hide();
    drawData();
    getRates();
}

init();
