let mapL3Destino = 'x';
let markersL3ODestino = [];
function loadMapL3Destino() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const center = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                mapL3Destino = new google.maps.Map(document.getElementById('mapL3Destino'), {zoom: 14, center: center});
                mapL3Destino.setCenter(center);
                //Click Listener Google Maps
                mapL3Destino.addListener("click", (event) => {
                    deleteMarkersL3Destino();
                    addMarkerL3Destino(event.latLng);
                    $('#frmL3AddDestino input[name=address_lat]').val(event.latLng.lat());
                    $('#frmL3AddDestino input[name=address_lng]').val(event.latLng.lng());
                    Core.getGeocoder(event.latLng).then(function(res){
                      neighborhood = "";
                      for(var component of res.address_components) {
                        for(var type of component.types) {
                          if(component.types[0] =='locality' && component.types[1]=='political') {
                            neighborhood = component.long_name
                            break;
                          }
                        }
                        if(neighborhood!="") {
                          break;
                        }
                      }
                      $('#frmL3AddDestino input[name=neighborhood]').val(neighborhood);
                      $('#frmL3AddDestino input[name=address]').val(res.formatted_address);
                    }).catch(function(){
                      Core.showToastStr('error','No ha sido posible obtener dirección');
                    });
                });
            },
            () => {
                handleLocationError(true, infoWindow, map.getCenter());
            }
        );
    }
    autocompleteDestino = new google.maps.places.Autocomplete($('#frmL3AddDestino input[name=address]')[0], {types: ['geocode']});
    autocompleteDestino.setFields(['geometry']);
    autocompleteDestino.addListener('place_changed', function() {
        var place = autocompleteDestino.getPlace();
        $('#frmL3AddDestino input[name=address_lat]').val(place.geometry.location.lat());
        $('#frmL3AddDestino input[name=address_lng]').val(place.geometry.location.lng());
        Core.getGeocoder(place.geometry.location).then(function(res){
          neighborhood = "";
          for(var component of res.address_components) {
            for(var type of component.types) {
              if(component.types[0] =='locality' && component.types[1]=='political') {
                neighborhood = component.long_name;
                console.log(neighborhood);
                break;
              }
            }
            if(neighborhood!="") {
              break;
            }
          }
          $('#frmL3AddDestino input[name=neighborhood]').val(neighborhood);
          $('#frmL3AddDestino input[name=address]').val(res.formatted_address);
        }).catch(function(){
          Core.showToastStr('error','No ha sido posible obtener dirección');
        });
        clearMarkersL3Destino();
        var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        mapL3Destino.panTo(center);
        addMarkerL3Destino(center);
    });
}
  // Adds a marker to the map and push to the array.
  function addMarkerL3Destino(location) {
    const marker = new google.maps.Marker({
      position: location,
      map: mapL3Destino,
    });
    markersL3ODestino.push(marker);
  }

  // Sets the map on all markersL1 in the array.
  function setMapOnAllL3Destino(map) {
    for (let i = 0; i < markersL3ODestino.length; i++) {
      markersL3ODestino[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkersL3Destino() {
    setMapOnAllL3Destino(null);
  }

  // Shows any markers currently in the array.
  function showMarkersL3Destino() {
    setMapOnAllL3Destino(mapL3Destino);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkersL3Destino() {
    clearMarkersL3Destino();
    markersL3ODestino = [];
  }

//Save and update data Origenes
$('#frmL3AddDestino').validate({
  submitHandler: function (form) {
    itemSelected.delivery = [];
    action = 'add';
    if(itemSelected.id!=null) {
      action = 'edit'
    }
    itemSelected.delivery[0] = {
        id              : $('#frmL3AddDestino input[name=idDestinoL3]').val(),
        action          : action,
        name:             $('#frmL3AddDestino input[name=name]').val(),
        phone:            $('#frmL3AddDestino input[name=phone]').val(),
        note:             $('#frmL3AddDestino input[name=note]').val(),
        address:          $('#frmL3AddDestino input[name=address]').val(),
        lat:              $('#frmL3AddDestino input[name=address_lat]').val(),
        lng:              $('#frmL3AddDestino input[name=address_lng]').val(),
        neighborhood:     $('#frmL3AddDestino input[name=neighborhood]').val()
    }
    itemSelected.distance = 0;
    localStorage.setItem('arrData',JSON.stringify(arrData));
    drawData();
    $('#frmL3AddDestino').trigger('reset');
    $('#mdlDestinoL3').modal('hide');
},
  errorPlacement: function(error, element) {
    $(element).addClass('is-invalid');
  },
  highlight: function(element) {
    $(element).addClass("is-invalid");
    },
  unhighlight: function(element) {
        $(element).removeClass("is-invalid").addClass('is-valid');
    }
});
