  var mapL2Origen = 'x';
  var markersL2Origen = [];
  var arrRutaOptimizada = [];
  function loadMapL2Origen() {
      if(navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
              (position) => {
                  const center = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude,
                  };
                  mapL2Origen = new google.maps.Map(document.getElementById('mapL2Origen'), {zoom: 14, center: center});
                  mapL2Origen.setCenter(center);
                  //Click Listener Google Maps
                  mapL2Origen.addListener("click", (event) => {
                      deleteMarkersL2Origen();
                      $('#frmL2AddOrigen input[name=address_lat]').val(event.latLng.lat());
                      $('#frmL2AddOrigen input[name=address_lng]').val(event.latLng.lng());
                      addMarkerL2Origen(event.latLng);
                      Core.getGeocoder(event.latLng).then(function(res){
                        neighborhood = "";
                        for(var component of res.address_components) {
                          for(var type of component.types) {
                            if(component.types[0] =='locality' && component.types[1]=='political') {
                              neighborhood = component.long_name
                              break;
                            }
                          }
                          if(neighborhood!="") {
                            break;
                          }
                        }
                        $('#frmL2AddOrigen input[name=neighborhood]').val(neighborhood);
                        $('#frmL2AddOrigen input[name=address]').val(res.formatted_address);
                      }).catch(function(){
                        Core.showToastStr('error','No ha sido posible obtener dirección');
                      });
                  });
              },
              () => {
                  handleLocationError(true, infoWindow, map.getCenter());
              }
          );
      }
      autocompleteAL2 = new google.maps.places.Autocomplete($('#frmL2AddOrigen input[name=address]')[0], {types: ['geocode']});
      autocompleteAL2.setFields(['geometry']);
      autocompleteAL2.addListener('place_changed', function() {
          var place = autocompleteAL2.getPlace();
          $('#frmL2AddOrigen input[name=address_lat]').val(place.geometry.location.lat());
          $('#frmL2AddOrigen input[name=address_lng]').val(place.geometry.location.lng());
          Core.getGeocoder(place.geometry.location).then(function(res){
            neighborhood = "";
            for(var component of res.address_components) {
              for(var type of component.types) {
                if(component.types[0] =='locality' && component.types[1]=='political') {
                  neighborhood = component.long_name;
                  console.log(neighborhood);
                  break;
                }
              }
              if(neighborhood!="") {
                break;
              }
            }
            $('#frmL2AddOrigen input[name=neighborhood]').val(neighborhood);
            $('#frmL2AddOrigen input[name=address]').val(res.formatted_address);
          }).catch(function(){
            Core.showToastStr('error','No ha sido posible obtener dirección');
          });
          clearMarkersL2Origen();
          var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
          mapL2Origen.panTo(center);
          addMarkerL2Origen(center);
      });
  }
  // Adds a marker to the map and push to the array.
  function addMarkerL2Origen(location) {
    const marker = new google.maps.Marker({
      position: location,
      map: mapL2Origen,
    });
    markersL2Origen.push(marker);
  }

  // Sets the map on all markersL2 in the array.
  function setMapOnAllL2Origen(map) {
    for (let i = 0; i < markersL2Origen.length; i++) {
      markersL2Origen[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkersL2Origen() {
    setMapOnAllL2Origen(null);
  }

  // Shows any markers currently in the array.
  function showMarkersL2Origen() {
    setMapOnAllL2Origen(mapL2Origen);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkersL2Origen() {
    clearMarkersL2Origen();
    markersL2Origen = [];
  }

  //Save and update data Origenes
  $('#frmL2AddOrigen').validate({
  submitHandler: function (form) {
    itemSelected.collect = [];
    itemSelected.collect[0] = {
        name:             $('#frmL2AddOrigen input[name=name]').val(),
        phone:            $('#frmL2AddOrigen input[name=phone]').val(),
        note:             $('#frmL2AddOrigen input[name=note]').val(),
        quantity:         $('#frmL2AddOrigen input[name=quantity]').val(),
        type:             $('#frmL2AddOrigen select[name=type]').val(),
        address:          $('#frmL2AddOrigen input[name=address]').val(),
        lat:              $('#frmL2AddOrigen input[name=address_lat]').val(),
        lng:              $('#frmL2AddOrigen input[name=address_lng]').val(),
        neighborhood:     $('#frmL2AddOrigen input[name=neighborhood]').val()
    }
    itemSelected.quantity  = itemSelected.collect[0].quantity;
    itemSelected.distance = 0;
    if(itemSelected.delivery!=null) {
     itemSelected.distance = Core.getDistancia({
        a:{
          lat:itemSelected.collect[0].lat,
          lng:itemSelected.collect[0].lng
        },
        b:{
          lat:itemSelected.delivery[0].lat,
          lng:itemSelected.delivery[0].lng
        }
     });
    }
    localStorage.setItem('arrData',JSON.stringify(arrData));
     drawData();
    $('#frmL2AddOrigen').trigger('reset');
    $('#mdlOrigenL2').modal('hide');
  },
    errorPlacement: function(error, element) {
      $(element).addClass('is-invalid');
    },
    highlight: function(element) {
      $(element).addClass("is-invalid");
      },
    unhighlight: function(element) {
          $(element).removeClass("is-invalid").addClass('is-valid');
      }
  });

  var mapL2Destinos = 'x';
  var markersL2Destinos = [];
  var arrDestinosL2;
  var directionsServiceDestinosL2;
  var directionsRendererDestinosL2;
  function loadMapL2Destinos() {
      if(navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
              (position) => {
                  const center = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude,
                  };
                  mapL2Destinos = new google.maps.Map(document.getElementById('mapL2Destinos'), {zoom: 14, center: center});
                  mapL2Destinos.setCenter(center);
                  listarDestinosL2();
              },
              () => {
                  handleLocationError(true, infoWindow, map.getCenter());
              }
          );
      }
  }
  // Adds a marker to the map and push to the array.
  function addMarkerL2AddDestinos(location,index,data=null) {
    const marker = new google.maps.Marker({
      position: location,
      label: labels[index++ % labels.length],
      map: mapL2Destinos
    });
    markersL2Destinos.push(marker);
  }

  // Sets the map on all markersL2 in the array.
  function setMapOnAllL2AddDestinos(map) {
    for (let i = 0; i < markersL2Destinos.length; i++) {
      markersL2Destinos[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkersL2AddDestinos() {
    setMapOnAllL2AddDestinos(null);
  }

  // Shows any markers currently in the array.
  function showMarkersL2AddDestinos() {
    setMapOnAllL2AddDestinos(mapL2Destinos);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkersL2OAddDestinos() {
    clearMarkersL2AddDestinos();
    markersL2Destinos = [];
  }
  var mapL2Route = 'x';
  var markersL2Route = [];
  function loadMapL2Route() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const center = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                mapL2Route = new google.maps.Map(document.getElementById('mapL2Route'), {zoom: 14, center: center});
                mapL2Route.setCenter(center);
                //Ordenar Ruta
                arrRutaOptimizada = ordernarRutaL2();
                console.log(arrRutaOptimizada);
                var coords = [];
                for (let i = 0; i < lines.length; i++) {
                  lines[i].setMap(null);
                }
                for (let i = 0; i < arrRutaOptimizada.length; i++) {
                  const cords = {
                      lat: arrRutaOptimizada[i].lat*1,
                      lng: arrRutaOptimizada[i].lng*1
                    };
                  coords.push(cords);
                  addMarkerL2Route(cords,i,arrRutaOptimizada[i]);
                  //Draw Lines
                  var line = new google.maps.Polyline({
                        path: coords,
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    line.setMap(mapL2Route);
                    lines.push(line);
                } if(mapL2Route) {
                  mapL2Route.setCenter(coords[0]);
                }
            },
            () => {
                handleLocationError(true, infoWindow, map.getCenter());
            }
        );
    }

  }
  // Adds a marker to the map and push to the array.
  function addMarkerL2Route(location,index,data=null) {
    const marker = new google.maps.Marker({
      position: location,
      label: labels[index++ % labels.length],
      map: mapL2Route
    });
    markersL2Route.push(marker);
  }
  function ordernarRutaL2() {
      arrRutaOptimizada = [];
      if(arrDestinosL2.length>0) {
        //Punto de partida
        arrRutaOptimizada.push(itemSelected.collect[0]);
        //Si solo son 2 rutas de entrega integrar la siguiente
        for(var i=0;i<arrDestinosL2.length;i++) {
            arrDestinosL2[i].distance = Core.getDistancia({
                a: {
                  lat:arrRutaOptimizada[i].lat,
                  lng:arrRutaOptimizada[i].lng
                },
                b: {
                  lat: arrDestinosL2[i].lat,
                  lng: arrDestinosL2[i].lng
                }
            });
            arrRutaOptimizada.push(arrDestinosL2[i]);
        }
        Core.setLocalStorage('arrDestinosL2',arrDestinosL2);
      }
      console.log(arrRutaOptimizada);
      return arrRutaOptimizada;
  }
