var mapL3Origenes = 'x';
var markersL3Origenes = [];
var arrRutaOptimizada = [];
var arrOrigenesL3 = [];
function loadMapL3OrigenesAdd() {
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
        (position) => {
            const center = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            };
            mapL3Origenes = new google.maps.Map(document.getElementById('mapL3Origenes'), {zoom: 14, center: center});
            mapL3Origenes.setCenter(center);
            listarOrigenesL3();
        },
        () => {
            handleLocationError(true, infoWindow, map.getCenter());
        }
    );
  }
}
// Adds a marker to the map and push to the array.
function addMarkerL3AddOrigenes(location,index) {
  const marker = new google.maps.Marker({
    position: location,
    map: mapL3Origenes,
    label: labels[index++ % labels.length],
  });
  markersL3Origenes.push(marker);
}

// Sets the map on all markersL1 in the array.
function setMapOnAllL3Origenes(map) {
  for (let i = 0; i < markersL3Origenes.length; i++) {
    markersL3Origenes[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkersL3Origenes() {
  setMapOnAllL3Origenes(null);
}
// Shows any markers currently in the array.
function showMarkersL3Origenes() {
  setMapOnAllL3Origenes(mapL3Origenes);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkersL3Destino() {
  clearMarkersL3Origenes();
  markersL3Origenes = [];
}
//Renderear Lista y Ruta más optima
function listarOrigenesL3() {
  //Planificar Ruta
  $('#listOrigenesL3').empty();
  optimizarTrazarRutaL3();
  setTimeout(function () {
    Core.getLocalStorage('arrOrigenesL3').then(function(res){
      let drawListL3 = res;
      $.each(drawListL3,function(index,item) {
        itemOrigen = `
        <tr>
          <td width="1" class="text-center">${labels[index+1 % labels.length]}</td>
          <td width="1" class="text-center">${item.quantity}</td>
          <td width="1" class="text-center">${item.distance!=null?item.distance:0}</td>
          <td width="80" class="text-center">
          <center>
              <svg class="c-icon mr-2" id="btnEditOrigenL3" data-index='${index}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                  <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
              </svg>
              <svg class="c-icon mr-2" id="btnDeleteOrigenL3" data-index='${index}' style="cursor: pointer;">
                  <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
              </svg>
          </center>
          </td>
        </tr>
        `;
        $('#listOrigenesL3').append(itemOrigen);
      });
    });
  },1500);
}

$('#btnAddOrigenL3Add').click(function(){
  $('#frmAddOrigenL3Form').show();
  $('#frmAddOrigenL3List').hide();
  $('#mapL3Origenes').hide();
  $('#mapL3Origen').show();
  loadMapL3OrigenAdd();
});
$('#btnAddOrigenL3Cancel').click(function(){
  $('#frmAddOrigenL3Form').hide();
  $('#frmAddOrigenL3List').show();
  $('#frmL3AddOrigenes').trigger('reset');
  $('#mapL3Origenes').show();
  $('#mapL3Origen').hide();
  loadMapL3OrigenesAdd();
});
function ordernarRutaL3() {
    console.log('RutaL3');
    arrRutaOptimizada = [];
    if(arrOrigenesL3.length>0) {
    //Punto de partida
    arrRutaOptimizada.push(itemSelected.delivery[0]);
    //Si solo son 2 rutas de entrega integrar la siguiente
    for(var i=0;i<arrOrigenesL3.length;i++) {
        arrOrigenesL3[i].distance = Core.getDistancia({
            a: {
                lat:arrRutaOptimizada[i].lat,
                lng:arrRutaOptimizada[i].lng
            },
            b: {
                lat: arrOrigenesL3[i].lat,
                lng: arrOrigenesL3[i].lng
            }
        });
        arrRutaOptimizada.push(arrOrigenesL3[i]);
    }
    Core.setLocalStorage('arrOrigenesL3',arrOrigenesL3);
    }
    console.log(arrRutaOptimizada);
return arrRutaOptimizada;
}
function optimizarTrazarRutaL3() {
  clearMarkersL3Origenes();
  if(arrOrigenesL3.length>0) {
    arrRutaOptimizada = ordernarRutaL3();
    Core.setLocalStorage('arrOrigenesL3',arrOrigenesL3);
    var coords = [];
    for (let i = 0; i < lines.length; i++) {
      lines[i].setMap(null);
    }
    for (let i =0; i < arrRutaOptimizada.length; i++) {
      const cords = {
          lat: arrRutaOptimizada[i].lat*1,
          lng: arrRutaOptimizada[i].lng*1
        };
      coords.push(cords);
      addMarkerL3AddOrigenes(cords,i);
      //Draw Lines
      var line = new google.maps.Polyline({
            path: coords,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        line.setMap(mapL3Origenes);
        lines.push(line);
    } if(mapL3Origenes) {
      mapL3Origenes.setCenter(coords[0]);
    }
  }
}

//Vista previa de ruta
var mapL3Route = 'x';
var markersL3Route = [];
function loadMapL3Route() {
  if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
          (position) => {
              const center = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
              };
              mapL3Route = new google.maps.Map(document.getElementById('mapL3Route'), {zoom: 14, center: center});
              mapL3Route.setCenter(center);
              //Ordenar Ruta
              arrRutaOptimizada = ordernarRoutaL3();
              console.log(arrRutaOptimizada);
              var coords = [];
              for (let i = 0; i < lines.length; i++) {
                lines[i].setMap(null);
              }
              for (let i = 0; i < arrRutaOptimizada.length; i++) {
                const cords = {
                    lat: arrRutaOptimizada[i].lat*1,
                    lng: arrRutaOptimizada[i].lng*1
                  };
                coords.push(cords);
                addMarkerL3Route(cords,i,arrRutaOptimizada[i]);
                //Draw Lines
                var line = new google.maps.Polyline({
                      path: coords,
                      geodesic: true,
                      strokeColor: '#FF0000',
                      strokeOpacity: 1.0,
                      strokeWeight: 2
                  });
                  line.setMap(mapL3Route);
                  lines.push(line);
              } if(mapL3Route) {
                mapL3Route.setCenter(coords[0]);
              }
          },
          () => {
              handleLocationError(true, infoWindow, map.getCenter());
          }
      );
  }

}
// Adds a marker to the map and push to the array.
function addMarkerL3Route(location,index,data=null) {
  const marker = new google.maps.Marker({
    position: location,
    label: labels[index++ % labels.length],
    map: mapL3Route
  });
  markersL3Route.push(marker);
}
function ordernarRoutaL3() {
    console.log('RutaL3');
        arrRutaOptimizada = [];
        if(arrOrigenesL3.length>0) {
        //Punto de partida
        arrRutaOptimizada.push(itemSelected.delivery[0]);
        //Si solo son 2 rutas de entrega integrar la siguiente
        for(var i=0;i<arrOrigenesL3.length;i++) {
            arrOrigenesL3[i].distance = Core.getDistancia({
                a: {
                    lat:arrRutaOptimizada[i].lat,
                    lng:arrRutaOptimizada[i].lng
                },
                b: {
                    lat: arrOrigenesL3[i].lat,
                    lng: arrOrigenesL3[i].lng
                }
            });
            arrRutaOptimizada.push(arrOrigenesL3[i]);
        }
        Core.setLocalStorage('arrOrigenesL3',arrOrigenesL3);
        }
        console.log(arrRutaOptimizada);
        return arrRutaOptimizada;
}
