route = $('#idRoute').val() + "/orders";
arrPackages = [];

tblData = $('#arrData').DataTable({
    processing: true,
    serverSide: true,
    ajax: "orders/all",

    columns: [
        {data: 'id', name: 'id',},
        {data: 'customer_name', name: 'customer_name'},
        {data: 'payment_method', name: 'payment_method'},
        {data: 'service', name: 'service'},
        {data: 'type', name: 'type'},
        {data: 'quantity', name: 'quantity'},
        {data: 'total', name: 'total'},
        {data: 'round', name: 'round'},
        {data: 'created_at', name: 'created_at'},
        {data: 'status', name: 'status'}

    ],
    lengthMenu: [
        [10, 50, 100, -1],
        [10, 50, 100, 'Todo']
    ],
    language: {
        url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
        pageLength: 5
    },
    "scrollX": true,
    "autoWidth": true,
    columnDefs: [{
        targets: [0],
        visible: false,
        searchable: false
    },
        {
            targets: [1],
            render: function (data, type, row, meta) {
                return row.id;
            },
            visible: false,
            searchable: false
        },
        {
            targets: [2],
            render: function (data, type, row, meta) {
                return row.customer_name;
            }
        },
        {
            targets: [3],
            render: function (data, type, row, meta) {
                var payment = 'Efectivo';
                switch (row.payment_method) {
                    case 1: {
                        payment = 'Efectivo';
                        break;
                    }
                    case 2: {
                        payment = 'Cheque';
                        break;
                    }
                    case 3: {
                        payment = 'Transferencia';
                        break;
                    }
                }
                return '<center>' + payment + '</center>';
            }
        },
        {
            targets: [4],
            render: function (data, type, row, meta) {
                return '<center>' + (row.service == 1 ? 'Express' : 'Currier') + '</center>';
            }
        },
        {
            targets: [5],
            render: function (data, type, row, meta) {
                return '<center>' + (row.type == 1 ? 'Inmediato' : 'Programado') + '</center>';
            }
        },
        {
            targets: [6],
            render: function (data, type, row, meta) {
                return '<center>' + row.quantity + '</center>';
            }
        },
        {
            targets: [7],
            render: function (data, type, row, meta) {
                return '<center>₲' + Core.setMiles(row.total) + '</center>';
            }
        },
        {
            targets: [8],
            render: function (data, type, row, meta) {
                if (row.round == 0 || row.type == 1) {
                    return "-";
                } else {
                    return row.round -1;
                }
            }
        },
        {
            targets: [9],
            render: function (data, type, row, meta) {
                return row.created_at.substr(0, 10) + "\n" + row.created_at.substr(11, 5);
            }
        },
        {
            targets: [10],
            render: function (data, type, row, meta) {
                switch (row.status) {

                    case 0: {
                        return '<p style=" text-align:center; background-color: #006eff; color:white ">Nuevo</p>';
                        break;
                    }
                    case 1: {
                        return '<p style=" text-align:center; background-color: #c0ad00; color:white ">Asignado</p>';
                        break;
                    }
                    case 2: {
                        return '<p style=" text-align:center; background-color: #00d200; color:white ">En proceso de entrega</p>';
                        break;
                    }
                    case 3: {
                        return '<p style=" text-align:center; background-color: green; color:white ">Entregado</p>';
                        break;
                    }
                    case 4: {
                        return '<p style=" text-align:center; background-color: red; color:white ">Cancelado</p>';
                        break;
                    }
                }
                return `${row.status == 1 ? 'Activo' : 'Concluido'}`;
            }
        },
        {
            targets: [11],
            render: function (data, type, row, meta) {
                return `<center>
                            <a href="${urlBase + route + "/edit/" + row.id}">
                            <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                            </svg>
                            </a>
                            <svg class="c-icon mr-2" id="btnDelete" data-index='${meta.row}' style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                            </svg>
                        </center>`
            }
        }]
});

tblData.on('draw', function () {
    arrData = tblData.rows().data().toArray();
    //console.log(arrData);
});

//Crate new Dealer
$("#frmNew").validate({
    submitHandler: function (form) {
        itemData = new FormData(form);
        Core.post(route + '/store', itemData)
            .then(function (res) {
                $('#mdlNew').modal('hide');
                $('#frmNew').trigger('reset');
                Core.showToastStr("success", res.data.message);
            })
            .catch(function (err) {
                Core.showToastStr('error', err.response.data.error.message);
            }).finally(function () {
            tblData.ajax.reload();
        });
    }
});

//Edit Dealer
$("#frmEdit").validate({
    submitHandler: function (form) {
        itemData = new FormData(form);
        Core.post(route + '/update/' + $('#id').val(), itemData)
            .then(function (res) {
                $('#mdlEdit').modal('hide');
                Core.showToastStr("success", res.data.message);
                $("#frmEdit").trigger('reset');
            })
            .catch(function (err) {
                Core.showToastStr('error', err.response.data.error.message);
            }).finally(function () {
            tblData.ajax.reload();
        });
    }
});

window.showItem = function () {
    $('#id').val(itemData.id);
    $('#frmEdit select[name=branchoffice_id]').val(itemData.branchoffice_id);
    $('#frmEdit input[name=name]').val(itemData.name);
    $('#frmEdit input[name=email]').val(itemData.email);
    $('#frmEdit input[name=phone]').val(itemData.phone);
    $('#frmEdit input[name=cedula]').val(itemData.cedula);
    $('#frmEdit input[name=password]').val('');
    $('#frmEdit select[name=status]').val(itemData.status);
    $('#previewDefaultEdit').attr('src', itemData.photo);
}

$('#frmNew input[name=picture]').change(function (e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e);
            $('#previewDefault').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }
});

$('#frmEdit input[name=picture]').change(function (e) {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e);
            $('#previewDefaultEdit').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }
});

window.getData = function () {
    tblData.ajax.reload();
}

$('#btnUpdate').click(function () {
    tblData.ajax.reload();
})

function init() {
}

init();

tblData.columns([9]).every(function () {
    var that = this;
    $('#filtro1').on('keyup change', function () {
        if (that.search() !== this.value) {
            if (this.value == 8) {
                tblData.search('').columns().search('').draw();
            } else if (this.value == 0) {
                that.search(0).draw();
            } else if (this.value == 1) {
                that.search(1).draw();
            } else if (this.value == 2) {
                that.search(2).draw();
            } else if (this.value == 3) {
                that.search(3).draw();
            } else if (this.value == 4) {
                that.search(4).draw();
            } else if (this.value == 5) {
                that.search(5).draw();
            } else if (this.value == 6) {
                that.search(6).draw();
            } else if (this.value == 7) {
                that.search(7).draw();
            }
        }
    });
});

tblData.columns([4]).every(function () {
    var that = this;
    $('#filtro2').on('keyup change', function () {
        if (that.search() !== this.value) {
            if (this.value == 3) {
                tblData.search('').columns().search('').draw();
            } else if (this.value == 1) {
                that.search(1).draw();
            } else if (this.value == 2) {
                that.search(2).draw();
            }
        }
    });
});



$('#btnExportar').click(function () {
    exportAllOrders();
})

// Centrar datos de la tabla (No cabecera)
$('#arrData tbody').css('text-align', 'center');