let mapL4Origen = 'x';
let markersL4Origen = [];
function loadMapL4Origenes() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const center = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                mapL4Origen = new google.maps.Map(document.getElementById('mapL4Origenes'), {zoom: 14, center: center});
                lat = $('#frmL4AddOrigenForm input[name=address_lat]').val();
                lng = $('#frmL4AddOrigenForm input[name=address_lng]').val();
                if(lat!=""&&lng!="") {
                  const center = {
                      lat: lat*1,
                      lng: lng*1
                  };
                  console.log(center);
                  addMarkerL4Origen(center);
                  mapL4Origen.setCenter(center);
                } else {
                  mapL4Origen.setCenter(center);
                }
                //Click Listener Google Maps
                mapL4Origen.addListener("click", (event) => {
                    deleteMarkersL4Origen();
                    addMarkerL4Origen(event.latLng);
                    $('#frmL4AddOrigenForm input[name=address_lat]').val(event.latLng.lat());
                    $('#frmL4AddOrigenForm input[name=address_lng]').val(event.latLng.lng());
                    Core.getGeocoder(event.latLng).then(function(res){
                      neighborhood = "";
                      for(var component of res.address_components) {
                        for(var type of component.types) {
                          if(component.types[0] =='locality' && component.types[1]=='political') {
                            neighborhood = component.long_name
                            break;
                          }
                        }
                        if(neighborhood!="") {
                          break;
                        }
                      }
                      $('#frmL4AddOrigenForm input[name=neighborhood]').val(neighborhood);
                      $('#frmL4AddOrigenForm input[name=address]').val(res.formatted_address);
                    }).catch(function(){
                      Core.showToastStr('error','No ha sido posible obtener dirección');
                    });
                });
            },
            () => {
                handleLocationError(true, infoWindow, map.getCenter());
            }
        );
    }
    autocompleteDestino = new google.maps.places.Autocomplete($('#frmL4AddOrigenForm input[name=address]')[0], {types: ['geocode']});
    autocompleteDestino.setFields(['geometry']);
    autocompleteDestino.addListener('place_changed', function() {
        var place = autocompleteDestino.getPlace();
        $('#frmL4AddOrigenForm input[name=address_lat]').val(place.geometry.location.lat());
        $('#frmL4AddOrigenForm input[name=address_lng]').val(place.geometry.location.lng());
        Core.getGeocoder(place.geometry.location).then(function(res){
          neighborhood = "";
          for(var component of res.address_components) {
            for(var type of component.types) {
              if(component.types[0] =='locality' && component.types[1]=='political') {
                neighborhood = component.long_name;
                console.log(neighborhood);
                break;
              }
            }
            if(neighborhood!="") {
              break;
            }
          }
          $('#frmL4AddOrigenForm input[name=neighborhood]').val(neighborhood);
          $('#frmL4AddOrigenForm input[name=address]').val(res.formatted_address);
        }).catch(function(){
          Core.showToastStr('error','No ha sido posible obtener dirección');
        });
        clearMarkersL4Origen();
        var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        mapL4Origen.panTo(center);
        addMarkerL4Origen(center);
    });
}
  // Adds a marker to the map and push to the array.
  function addMarkerL4Origen(location) {
    const marker = new google.maps.Marker({
      position: location,
      map: mapL4Origen,
    });
    markersL4Origen.push(marker);
  }

  // Sets the map on all markersL1 in the array.
  function setMapOnAllL4Origen(map) {
    for (let i = 0; i < markersL4Origen.length; i++) {
      markersL4Origen[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkersL4Origen() {
    setMapOnAllL4Origen(null);
  }
  // Shows any markers currently in the array.
  function showMarkersL4Origen() {
    setMapOnAllL4Origen(mapL4Origen);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkersL4Origen() {
    clearMarkersL4Origen();
    markersL4Origen = [];
  }
$('#btnAddOrigenesL4Save').click(function() {
    itemSelected.collect = arrOrigenesL4;
    if(arrOrigenesL4.length>0) {
        itemSelected.collect = arrOrigenesL4;
        countCollect = 0;
        arrOrigenesL4.map(function(collect){
            countCollect = countCollect + parseInt(collect.quantity);
        })
        itemSelected.quantity = countCollect;
        localStorage.setItem('arrData',JSON.stringify(arrData));
        drawData();
        $('#mdlOrigenesL4').modal('hide');
    } else {
        Core.showToastStr('error','Debe agregar minimo 1 destino de entrega');
    }
});
$('#btnAddOrigenL4Add').click(function() {
    $('#frmAddOrigenL4List').hide();
    $('#frmL4AddOrigenForm').show();
    $('#idOrigenL4').val('');
    loadMapL4Origenes();
});
$('#btnAddOrigenL4Cancel').click(function(){
    $('#frmAddOrigenL4List').show();
    $('#frmL4AddOrigenForm').hide();
    $('#frmL4AddOrigen input[name=name]').val('')
    $('#frmL4AddOrigen input[name=phone]').val('')
    $('#frmL4AddOrigen input[name=note]').val('')
    $('#frmL4AddOrigen input[name=quantity]').val('')
    $('#frmL4AddOrigen input[name=address]').val(''),
    $('#frmL4AddOrigen input[name=address_lat]').val(''),
    $('#frmL4AddOrigen input[name=address_lng]').val(''),
    $('#frmL4AddOrigen input[name=neighborhood]').val('')
});
$(document).on('click','#btnDeleteOrigenL4',function() {
    index = $(this).data('index');
    bootbox.confirm({
    message: 'Está acción será irreversible, <br><center><b>¿Desea continuar?</b></center>',
    buttons: {
        confirm: {
                label: 'Sí',
                className: 'btn-success'
            },
        cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result==true) {
                arrOrigenesL4.splice(index,1);
                Core.setLocalStorage('arrOrigenesL4',arrOrigenesL4).then(function() {
                    listarOrigenesL4();
                });
                }
            }
    });
});
$('#btnAddOrigenL4').click(function() {
    origen = {
        name:             $('#frmL4AddOrigen input[name=name]').val(),
        phone:            $('#frmL4AddOrigen input[name=phone]').val(),
        note:             $('#frmL4AddOrigen input[name=note]').val(),
        quantity:         $('#frmL4AddOrigen input[name=quantity]').val(),
        address:          $('#frmL4AddOrigen input[name=address]').val(),
        lat:              $('#frmL4AddOrigen input[name=address_lat]').val(),
        lng:              $('#frmL4AddOrigen input[name=address_lng]').val(),
        neighborhood:     $('#frmL4AddOrigen input[name=neighborhood]').val(),
        deliveries: $('#idOrigenL4').val()!=""?arrOrigenesL4[$('#idOrigenL4').val()].deliveries!=null?arrOrigenesL4[$('#idOrigenL4').val()].deliveries:null:null
    };
    if($('#idOrigenL4').val()!="") {
        arrOrigenesL4[$('#idOrigenL4').val()] = origen;
    } else {
        arrOrigenesL4.push(origen);
    }
    if($('#idOrigenL4').val()!="") {
        $('#btnAddOrigenL4Cancel').trigger('click');
        $('#frmL4AddOrigen input[name=name]').val('')
        $('#frmL4AddOrigen input[name=phone]').val('')
        $('#frmL4AddOrigen input[name=note]').val('')
        $('#frmL4AddOrigen input[name=quantity]').val('')
        $('#frmL4AddOrigen input[name=address]').val('')
        $('#frmL4AddOrigen input[name=address_lat]').val('')
        $('#frmL4AddOrigen input[name=address_lng]').val('')
        $('#frmL4AddOrigen input[name=neighborhood]').val('')
        Core.setLocalStorage('arrOrigenesL4',arrOrigenesL4);
        listarOrigenesL4();
    } else {
        bootbox.confirm({
            message: '<center><b>¿Desea agregar otro retiro?</b></center>',
            buttons: {
                confirm: {
                          label: 'Sí',
                          className: 'btn-success'
                    },
                cancel: {
                          label: 'No',
                          className: 'btn-danger'
                    }
                },
              callback: function(result) {
                  if (!result) {
                        $('#btnAddOrigenL4Cancel').trigger('click');
                        $('#frmL4AddOrigen input[name=name]').val('')
                        $('#frmL4AddOrigen input[name=phone]').val('')
                        $('#frmL4AddOrigen input[name=note]').val('')
                        $('#frmL4AddOrigen input[name=quantity]').val('')
                        $('#frmL4AddOrigen input[name=address]').val('')
                        $('#frmL4AddOrigen input[name=address_lat]').val('')
                        $('#frmL4AddOrigen input[name=address_lng]').val('')
                        $('#frmL4AddOrigen input[name=neighborhood]').val('')
                        Core.setLocalStorage('arrOrigenesL4',arrOrigenesL4);
                        listarOrigenesL4();
                      }
                  }
          });
    }
});
$(document).on('click','#btnEditOrigenL4',function() {
    origen = arrOrigenesL4[$(this).data('index')];
    $('#idOrigenL4').val($(this).data('index'));
    $('#frmAddOrigenL4List').hide();
    $('#frmL4AddOrigenForm').show();
    $('#frmL4AddOrigenForm input[name=name]').val(origen.name);
    $('#frmL4AddOrigenForm input[name=phone]').val(origen.phone);
    $('#frmL4AddOrigenForm input[name=note]').val(origen.note);
    $('#frmL4AddOrigenForm input[name=quantity]').val(origen.quantity);
    $('#frmL4AddOrigenForm input[name=address]').val(origen.address);
    $('#frmL4AddOrigenForm input[name=address_lat]').val(origen.lat);
    $('#frmL4AddOrigenForm input[name=address_lng]').val(origen.lng);
    $('#frmL4AddOrigenForm input[name=neighborhood]').val(origen.neighborhood);
    loadMapL4Origenes();
  });

