var limitPackagesDestinosL2;
var mapL2AddDestino;
var markersL2AddDestino = [];
function loadMapL2DestinosAdd() {
  if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
          (position) => {
                const center = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                if(!mapL2AddDestino) {
                  mapL2AddDestino = new google.maps.Map(document.getElementById('mapL2Destino'), {zoom: 14, center: center});
                }
                deleteMarkersL2OAddDestino();
                lat = $('#frmL2AddDestinos input[name=address_lat]').val();
                lng = $('#frmL2AddDestinos input[name=address_lng]').val();
                if(lat!=""&&lng!="") {
                  const center = {
                      lat: lat*1,
                      lng: lng*1
                  };
                  console.log(center);
                  addMarkerL2AddDestino(center);
                  mapL2AddDestino.setCenter(center);
                } else {
                  mapL2AddDestino.setCenter(center);
                }
                //Click Listener Google Maps
                mapL2AddDestino.addListener("click", (event) => {
                $('#frmL2AddDestinos input[name=address_lat]').val(event.latLng.lat());
                $('#frmL2AddDestinos input[name=address_lng]').val(event.latLng.lng());
                deleteMarkersL2OAddDestino();
                addMarkerL2AddDestino(event.latLng);
                Core.getGeocoder(event.latLng).then(function(res){
                  neighborhood = "";
                  for(var component of res.address_components) {
                    for(var type of component.types) {
                      if(component.types[0] =='locality' && component.types[1]=='political') {
                        neighborhood = component.long_name
                        break;
                      }
                    }
                    if(neighborhood!="") {
                      break;
                    }
                  }
                  $('#frmL2AddDestinos input[name=neighborhood]').val(neighborhood);
                  $('#frmL2AddDestinos input[name=address]').val(res.formatted_address);
                });
              });
          },
          () => {
              handleLocationError(true, infoWindow, map.getCenter());
          }
      );
  }
  autocompleteL2AddDestino = new google.maps.places.Autocomplete($('#frmL2AddDestinos input[name=address]')[0], {types: ['geocode']});
  autocompleteL2AddDestino.setFields(['geometry']);
  autocompleteL2AddDestino.addListener('place_changed', function() {
      var place = autocompleteL2AddDestino.getPlace();
      $('#frmL2AddDestinos input[name=address_lat]').val(place.geometry.location.lat());
      $('#frmL2AddDestinos input[name=address_lng]').val(place.geometry.location.lng());
      Core.getGeocoder(place.geometry.location).then(function(res){
        neighborhood = "";
        for(var component of res.address_components) {
          for(var type of component.types) {
            if(component.types[0] =='locality' && component.types[1]=='political') {
              neighborhood = component.long_name;
              console.log(neighborhood);
              break;
            }
          }
          if(neighborhood!="") {
            break;
          }
        }
        $('#frmL2AddDestinos input[name=neighborhood]').val(neighborhood);
        $('#frmL2AddDestinos input[name=address]').val(res.formatted_address);
      })
      clearMarkersL2AddDestino();
      var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
      mapL2AddDestino.panTo(center);
      addMarkerL2AddDestino(center);
  });
}
//Markers Temps while adding points to delivery--------------------------------------------------------------------------------------------
  // Adds a marker to the map and push to the array.
  function addMarkerL2AddDestino(location) {
    const marker = new google.maps.Marker({
      position: location,
      map: mapL2AddDestino,
    });
    markersL2AddDestino.push(marker);
  }

  // Sets the map on all markersL2 in the array.
  function setMapOnAllL2AddDestino(map) {
    for (let i = 0; i < markersL2AddDestino.length; i++) {
      markersL2AddDestino[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkersL2AddDestino() {
    setMapOnAllL2AddDestino(null);
  }

  // Shows any markers currently in the array.
  function showMarkersL2AddDestino() {
    setMapOnAllL2AddDestino(mapL2AddDestino);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkersL2OAddDestino() {
    clearMarkersL2AddDestino();
    markersL2AddDestino = [];
  }
  $('#btnAddDestinoL2Add').click(function() {
    $('#frmAddDestinoL2List').hide();
    $('#frmAddDestinoL2Form').show();
    $('#mapL2Destinos').hide();
    $('#mapL2Destino').show();
    loadMapL2DestinosAdd();
  });
  $('#btnAddDestinoL2Cancel').click(function() {
    $('#frmAddDestinoL2Form').hide();
    $('#frmAddDestinoL2List').show();
    $('#mapL2Destino').hide();
    $('#mapL2Destinos').show();
    $('#idDestinoL2').val('')
  });
  $('#frmL2AddDestinos').validate({
    submitHandler: function (form) {
      itemData = new FormData(form);
      entrega = {
        name:             $('#frmL2AddDestinos input[name=name]').val(),
        phone:            $('#frmL2AddDestinos input[name=phone]').val(),
        note:             $('#frmL2AddDestinos input[name=note]').val(),
        quantity:         $('#frmL2AddDestinos input[name=quantity]').val(),
        address:          $('#frmL2AddDestinos input[name=address]').val(),
        lat:              $('#frmL2AddDestinos input[name=address_lat]').val(),
        lng:              $('#frmL2AddDestinos input[name=address_lng]').val(),
        neighborhood:     $('#frmL2AddDestinos input[name=neighborhood]').val()
      };
      countPackages = 0;
      $.each(arrDestinosL2, function(index,item) {
        countPackages= countPackages + parseInt(item.quantity);
      });
      if(countPackages < limitPackagesDestinosL2 && (countPackages+parseInt(entrega.quantity)) <= limitPackagesDestinosL2) {
        if($('#idDestinoL2').val()!="") {
          //Actualizar destino
            arrDestinosL2[$('#idDestinoL2').val()]= entrega;
        } else {
          arrDestinosL2.push(entrega);
        }
        Core.setLocalStorage('arrDestinosL2',arrDestinosL2);
        listarDestinosL2();//Redraw lista
        Core.showToastStr('success','Destino de entrega agregada correctamente, esto será permanente al guardar.');
        $('#frmL2AddDestinos').trigger('reset').validate().resetForm();
        $('.is-valid').removeClass('is-valid');
        $('.is-invalid').removeClass('is-invalid');
        if($('#idDestinoL2').val()!="") {
          $('#btnAddDestinoL2Cancel').trigger('click');
          $('#idDestinoL2').val('');
        } else {
          bootbox.confirm({
            message: '<center><b>¿Desea agregar otro destino de entrega?</b></center>',
            buttons: {
                confirm: {
                          label: 'Sí',
                          className: 'btn-success'
                    },
                cancel: {
                          label: 'No',
                          className: 'btn-danger'
                    }
                },
              callback: function(result) {
                  if (!result) {
                       $('#btnAddDestinoL2Cancel').trigger('click');
                      }
                  }
          });
        }
      } else {
        if((countPackages+parseInt(entrega.quantity)) > limitPackagesDestinosL2 && $('#idDestinoL2').val()=="") {
          Core.showToastStr('error','Lo sentimos, la cantidad de paquetes a entregar son mayores a la cantidad de paquetes de retiro.');
        } else {
          if($('#idDestinoL2').val()!="") {
            //Actualizar destino
              if((entrega.quantity - parseInt(arrDestinosL2[$('#idDestinoL2').val()].quantity)) >limitPackagesDestinosL2){
                Core.showToastStr('error','Lo sentimos, la cantidad de paquetes a entregar son mayores a la cantidad de paquetes de retiro.');
              } else {
                arrDestinosL2[$('#idDestinoL2').val()]= entrega;
                Core.setLocalStorage('arrDestinosL2',arrDestinosL2);
                $('#frmL2AddDestinos').trigger('reset').validate().resetForm();
                Core.showToastStr('success','Destino de entrega agregada correctamente, esto será permanente al guardar.')
                listarDestinosL2();//Redraw lista
                $('#btnAddDestinoL2Cancel').trigger('click');
              }
          } else {
            Core.showToastStr('error','Lo sentimos, la cantidad de paquetes a entregar son mayores a la cantidad de paquetes de retiro.');
          }
        }
      }
    },
    errorPlacement: function(error, element) {
      $(element).addClass('is-invalid');
    },
    highlight: function(element) {
      $(element).addClass("is-invalid");
      },
    unhighlight: function(element) {
          $(element).removeClass("is-invalid").addClass('is-valid');
    }
  });
  $('#btnAddDestinosL2Save').click(function() {
    if(arrDestinosL2.length>0) {
      countPackages = 0;
      distance = 0;
    $.each(arrDestinosL2, function(index,item){
      console.log(index,item);
      countPackages = countPackages + parseInt(item.quantity);
      distance = distance + parseFloat(item.distance);
    });
    console.log(countPackages, limitPackagesDestinosL2);
    if(countPackages == limitPackagesDestinosL2) {
      itemSelected.delivery = arrDestinosL2;
      itemSelected.distance = distance;
      Core.setLocalStorage('arrData',arrData);
      $('#mdlDestinosL2').modal('hide');
      drawData();
    } else if(countPackages < limitPackagesDestinosL2) {
        bootbox.confirm({
          message: 'Aún quedan paquetes por asignar destino de entrega,<br><center><b>¿Desea continuar?</b></center>',
          buttons: {
              confirm: {
                        label: 'Sí',
                        className: 'btn-success'
                  },
              cancel: {
                        label: 'No',
                        className: 'btn-danger'
                  }
              },
            callback: function(result) {
                if (result==true) {
                      itemSelected.delivery = arrDestinosL2;
                      itemSelected.distance = distance;
                      Core.setLocalStorage('arrData',arrData);
                      $('#mdlDestinosL2').modal('hide');
                      drawData();
                    }
                }
        });
      } else {
        Core.showToastStr('error','La cantidad de paquetes a entregar es superiora a la cantidad de paquetes a retirar.,<br><center><b>¿Desea continuar?</b></center>');
      }
    } else {
        Core.showToastStr('error','Debe agregar minimo 1 destino de entrega');
    }
  });

  $(document).on('click','#btnEditDestinoL2', function() {
      data = arrDestinosL2[$(this).data('index')];
      $('#idDestinoL2').val($(this).data('index'));
      $('#btnAddDestinoL2Add').trigger('click');
      $('#frmAddDestinoL2Form').show();
      $('#mapL2Destinos').hide();
      $('#mapL2Destino').show();
      $('#frmL2AddDestinos input[name=name]').val(data.name);
      $('#frmL2AddDestinos input[name=phone]').val(data.phone);
      $('#frmL2AddDestinos input[name=note]').val(data.note);
      $('#frmL2AddDestinos input[name=quantity]').val(data.quantity);
      $('#frmL2AddDestinos input[name=address]').val(data.address);
      $('#frmL2AddDestinos input[name=address_lat]').val(data.lat);
      $('#frmL2AddDestinos input[name=address_lng]').val(data.lng);
      $('#frmL2AddDestinos input[name=neighborhood]').val(data.neighborhood);
      loadMapL2DestinosAdd();
  });
  $(document).on('click','#btnDeleteDestinoL2',function() {
    index = $(this).data('index');
    bootbox.confirm({
       message: 'Está acción será irreversible, <br><center><b>¿Desea continuar?</b></center>',
       buttons: {
           confirm: {
                    label: 'Sí',
                    className: 'btn-success'
               },
           cancel: {
                    label: 'No',
                    className: 'btn-danger'
               }
          },
        callback: function(result) {
             if (result==true) {
                  arrDestinosL2.splice(index,1);
                  Core.setLocalStorage('arrDestinosL2',arrDestinosL2).then(function() {
                    listarDestinosL2();
                  });
                }
             }
    });
  });

  //Renderear Lista y Ruta más optima
  function listarDestinosL2() {
    //Planificar Ruta
    $('#listDestinosL2').empty();
    optimizarTrazarRutaL2();
    setTimeout(function () {
      Core.getLocalStorage('arrDestinosL2').then(function(res){
        let drawListL2 = res;
        var asignados = 0;
        $.each(drawListL2,function(index,item) {
          asignados = asignados + parseInt(item.quantity);
          itemDestino = `
          <tr>
            <td width="1" class="text-center">${labels[index+1 % labels.length]}</td>
            <td width="1" class="text-center">${item.quantity}</td>
            <td width="1" class="text-center">${item.distance}</td>
            <td width="80" class="text-center">
            <center>
                <svg class="c-icon mr-2" id="btnEditDestinoL2" data-index='${index}' data-toggle="modal" data-target="#mdlEdit" style="cursor: pointer;">
                    <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                </svg>
                <svg class="c-icon mr-2" id="btnDeleteDestinoL2" data-index='${index}' style="cursor: pointer;">
                    <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                </svg>
            </center>
            </td>
          </tr>
          `;
          $('#listDestinosL2').append(itemDestino);
        });
        $('#idPaquetesAsignados').html(asignados);
      });
    },1500);
  }
  function optimizarTrazarRutaL2() {
      clearMarkersL2AddDestinos();
      if(arrDestinosL2.length>0) {
        arrRutaOptimizada = ordernarRutaL2();
        Core.setLocalStorage('arrDestinosL2',arrDestinosL2);
        var coords = [];
        for (let i = 0; i < lines.length; i++) {
          lines[i].setMap(null);
        }
        for (let i = 0; i < arrRutaOptimizada.length; i++) {
          const cords = {
              lat: arrRutaOptimizada[i].lat*1,
              lng: arrRutaOptimizada[i].lng*1
            };
          coords.push(cords);
          addMarkerL2AddDestinos(cords,i,arrRutaOptimizada[i]);
          //Draw Lines
          var line = new google.maps.Polyline({
                path: coords,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            line.setMap(mapL2Destinos);
            lines.push(line);
        } if(mapL2Destinos) {
          mapL2Destinos.setCenter(coords[0]);
        }
      }
  }
