route = $('#idRoute').val()+ "/express-fees";
$('#frmEdit').validate({
  submitHandler: function (form) {
  itemData = new FormData(form);
  //Axios Http Post Request
  Core.crud.update($('#idFees').val()!=""?$('#idFees').val():1).then(function(res){
    console.log(res.data);
    Core.showToastStr('success','Datos actualizados correctamente');
  }).catch(function(err){
    console.log(err);
    Core.showToastStr('error','No ah sido posible actualizar datos, por favor verifique su información e intente nuevamente.');
  });
}});
window.getData = function() {
    //Axios Http Get Request
    Core.crud.find(1).then(function(res){
      console.log(res.data);
      $('#local_price').val(res.data.data.local_price);
      $('#external_price').val(res.data.data.external_price);
      $('#external_price_km').val(res.data.data.external_price_km);
    }).catch(function(err){
      console.log(err);
    })
}
function init() {
    getData();
}
init();
