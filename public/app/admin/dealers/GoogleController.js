function initMap() {
  //For New Branch Office
  autocomplete = new google.maps.places.Autocomplete(
      $('#frmNew input[name=address]')[0], {types: ['geocode']});
  autocomplete.setFields(['geometry']);
  autocomplete.addListener('place_changed', function(){
      var place = autocomplete.getPlace();
      console.log(place);
        $('#frmNew input[name=lat]').val(place.geometry.location.lat());
        $('#frmNew input[name=lng]').val(place.geometry.location.lng());
  });
  
  //For Edit Branch Office
  autocompleteEdit = new google.maps.places.Autocomplete(
      $('#frmEdit input[name=address]')[0], {types: ['geocode']});
  autocompleteEdit.setFields(['geometry']);
  autocompleteEdit.addListener('place_changed', function(){
      var place = autocompleteEdit.getPlace();
      console.log(place);
        $('#frmEdit input[name=lat]').val(place.geometry.location.lat());
        $('#frmEdit input[name=lng]').val(place.geometry.location.lng());
  });
}
googleKey = $('#googleKey').val();
document.write(`<script src="https://maps.googleapis.com/maps/api/js?key=${googleKey}&libraries=places&callback=initMap"async defer></script>'`);


