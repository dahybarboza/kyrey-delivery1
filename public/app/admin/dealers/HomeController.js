    route = $('#idRoute').val() + "/dealers";
    arrStates = [];
    Core.setDataTable('arrData');

    //Create new Dealer
    $("#frmNew").validate({
        submitHandler: function (form) {
            $('.btn-success').text('Guardando ...').prop('disabled', true);
            itemData = new FormData(form);
            Core.post(route + '/store', itemData)
                .then(function (res) {
                    $('#mdlNew').modal('hide');
                    $('#frmNew').trigger('reset');
                    $('#previewDefault').attr('src', urlBase + 'dashboard/assets/img/default_user.png');
                    Core.showToastStr("success", res.data.message);
                    $('.btn-success').text('Guardar').prop('disabled', false);
                })
                .catch(function (err) {
                    Core.showToastStr('error', err.response.data.error.message);
                    $('.btn-success').text('Guardar').prop('disabled', false);
                }).finally(function () {
                $('.btn-success').text('Guardar').prop('disabled', false);
                getData();
            });
        }
    });

    //Edit Dealer
    $("#frmEdit").validate({
        submitHandler: function (form) {
            $('.btn-danger').text('Actualizando ...').prop('disabled', true);
            itemData = new FormData(form);
            Core.post(route + '/update/' + $('#id').val(), itemData)
                .then(function (res) {
                    $('#mdlEdit').modal('hide');
                    $("#frmEdit").trigger('reset');
                    Core.showToastStr("success", res.data.message);
                    $('.btn-danger').text('Actualizar').prop('disabled', false);
                })
                .catch(function (err) {
                    Core.showToastStr('error', err.response.data.error.message);
                    $('.btn-danger').text('Actualizar').prop('disabled', false);
                }).finally(function () {
                    $('.btn-danger').text('Actualizar').prop('disabled', false);
                    getData();
            });
        }
    });

    // Get Data
    window.getData = function () {
        tblData.rows().remove().draw();
        Core.get(route + "/all").then(function (res) {
            arrData = res.data.data;
            res.data.data.forEach(function (item, index) {
                tblData.row.add([
                    index,
                    `<img src="${item.photo}" style="width:35px; height:35px; border-radius:50%;">`,
                    item.name,
                    item.cedula,
                    //item.branchoffice.name,
                    item.phone,
                    item.status == 1 ? 'Activo' : 'Inactivo',
                    Core.buttons(index)
                ]).draw();
            });
        }).catch(function (err) {
            console.log(err);
            Core.showToastStr('error', 'No ha sido posible cargar repartidores.');
        });
    }

    // Show Item
    window.showItem = function () {
        $('#id').val(itemData.id);
        $('#frmEdit select[name=branchoffice_id]').val(itemData.branchoffice_id);
        $('#frmEdit input[name=name]').val(itemData.name);
        $('#frmEdit input[name=email]').val(itemData.email);
        $('#frmEdit input[name=phone]').val(itemData.phone);
        $('#frmEdit input[name=cedula]').val(itemData.cedula);
        $('#frmEdit input[name=password]').val('');
        $('#frmEdit select[name=status]').val(itemData.status);
        $('#previewDefaultEdit').attr('src', itemData.photo);
    }

    $('#frmNew input[name=picture]').change(function (e) {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
             //   console.log(e);
                $('#previewDefault').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $('#frmEdit input[name=picture]').change(function (e) {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                console.log(e);
                $('#previewDefaultEdit').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    function init() {
            getData();
    }

    init();
