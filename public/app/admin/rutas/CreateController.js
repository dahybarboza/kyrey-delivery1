    //Impor Google Api
    document.write(`<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=${$('#googleKey').val()}&libraries=places&callback" async defer></script>`);
    //document.write(`<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"  defer></script>`);
    route = $('#idRoute').val() + "/rutas";
    var fees = {
        'local_price': 0,
        'external_price': 0,
        'external_price_km': 0
    };
    var rates = [];
    var lines = [];
    const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var itemData;
    var arrData = [];
    var itemSelected;
    var cost;
    var quantity = 0;
    var indexSelected = 0;
    var arrPrices = [];
    var totalDelivery = 0;
    //var precioDelivery = 0;
    Core.setDataTable('arrData');

    // Vaciar tabla "Lista de Paquetes" al cargar la página
    arrData.splice(1, 1);
    localStorage.setItem('arrData', JSON.stringify(arrData));

    $("#frmNew").validate({
        submitHandler: function (form) {
            $('.btn-guardar').text('Guardando ...').prop('disabled', true);

            var dia = $('#dia').val();
            if (dia === '1'){
                dia = 'Lunes';
            }else if (dia === '2'){
                dia = 'Martes';
            }else if (dia === '3'){
                dia = 'Miércoles';
            }else if (dia === '4'){
                dia = 'Jueves';
            }else if (dia === '5'){
                dia = 'Viernes';
            }else if (dia === '6'){
                dia = 'Sábado';
            }else if (dia === '7'){
                dia = 'Domingo';
            }

            itemData = {
                customer_id             : $('#customers_id').val(),
                branchoffice_id         : $('#branchoffice_id').val(),
                dealer_id               : $('#dealer_id').val(),
                dia                     : dia,
                hora_inicio_programado  : $('#hora_entrada').val(),
                hora_fin_programado     : $('#hora_salida').val(),
                lat                     : $('#l1_address_lat').val(),
                lng                     : $('#l1_address_lng').val(),
                address                 : $('#l1_address').val(),
                neighborhood            : $('#l1_neighborhood').val(),
                status                  : parseInt($('#status').val()),
                en_rango                : parseInt($('#en_rango').val()),
                observaciones           : $('#observaciones').val(),
            };

            Core.post(route + '/store', itemData)
                .then(function (res) {
                    Core.showToastStr("success", res.data.message);
                    $('.btn-guardar').text('Guardado').prop('disabled', false);
                    setTimeout(function (){
                        window.location.href = urlBase + route
                    }, 3000);
                })
                .catch(function (err) {
                    //return false;
                    Core.showToastStr('error', err.response.data.error.message);
                    $('.btn-guardar').text('Guardar').prop('disabled', false);
                })


        }
    });

    // Deshabilitar enter
    $('#frmNew').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('#customers_id').on('change', (function () {
        $('#frmNew select[name=branchoffice_id]').empty();
        Core.get(route + '/branchoffices/' + $(this).val())
            .then(function (res) {
                console.log(res);
                $.each(res.data.data, function (index, item) {
                    $('#frmNew select[name=branchoffice_id]').append($('<option data-index="' + index + '"></option>').val(item.id).html(item.name));
                });
            }).catch(function (err) {
            Core.showToastStr('error', err.response.data.error.message);
        });
    }));

    function drawData() {
        totalPreview = 0;
        tblData.rows().remove().draw();
        //Reestructurar de memoria
        arrData = JSON.parse(localStorage.getItem('arrData'));
        if (!arrData) {
            arrData = [];
        }
        $.each(arrData, (function (index, item) {
            collect = "";
            delivery = "";
            price = '-';
            distance = 0;
            Core.setLocalStorage('arrData', arrData);
            prepararMapa();
        }));
    }

    function prepararMapa(){
        itemSelected = arrData[0];
        if (itemSelected.collect != null) {
            loadMapL1Origen();
            $('#l1_address').val(itemSelected.collect[0].address);
            $('#l1_address_lat').val(itemSelected.collect[0].lat);
            $('#l1_adddres_lng').val(itemSelected.collect[0].lng);
            $('#l1_neighborhood').val(itemSelected.collect[0].neighborhood);
            setTimeout(function () {
                var center = new google.maps.LatLng(itemSelected.collect[0].lat, itemSelected.collect[0].lng);
                mapL1Origen.panTo(center);
                addMarkerL1Origen(center);
            }, 1000);
        } else {
            loadMapL1Origen();
        }
    }

    function prepararArrayParaMapa(){
        arrData.push({
            type: 'L1',
            quantity: 0,
            collect: null,
            delivery: null,
            total: 0,
            status: 0,
            distance: 0
        });
        localStorage.setItem('arrData', JSON.stringify(arrData));

        drawData();
    }

    function init() {
        //getFees();
        // $('#type_view').hide();
        // $('#date_view').hide();
        // $('#round_view').hide();
        // $('#quote_view').hide();
        // $("#total_view").hide();
        //drawData();
        //getRates();
        setTimeout(function (){
            prepararArrayParaMapa();
        },1000);
    }

    init();
