    let mapL1Origen = 'x';
    let markersL1Origen = [];

    function loadMapL1Origen() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const center = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    mapL1Origen = new google.maps.Map(document.getElementById('mapL1Origen'), {zoom: 14, center: center});
                    mapL1Origen.setCenter(center);
                    //Click Listener Google Maps
                    mapL1Origen.addListener("click", (event) => {
                        deleteMarkersL1Origen();
                        addMarkerL1Origen(event.latLng);
                        $('#l1_address_lat').val(event.latLng.lat());
                        $('#l1_address_lng').val(event.latLng.lng());
                        Core.getGeocoder(event.latLng).then(function (res) {
                            console.log(res);
                            neighborhood = "";
                            for (var component of res.address_components) {
                                for (var type of component.types) {
                                    if (component.types[0] == 'locality' && component.types[1] == 'political') {
                                        neighborhood = component.long_name
                                        break;
                                    }
                                }
                                if (neighborhood != "") {
                                    break;
                                }
                            }
                            $('#l1_neighborhood').val(neighborhood);
                            $('#l1_address').val(res.formatted_address);
                        }).catch(function () {
                            Core.showToastStr('error', 'No ha sido posible obtener dirección');
                        });
                    });
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        }
        autocompleteA = new google.maps.places.Autocomplete($('#l1_address')[0], {types: ['geocode']});
        autocompleteA.setFields(['geometry']);
        autocompleteA.addListener('place_changed', function () {
            var place = autocompleteA.getPlace();
            $('#l1_address_lat').val(place.geometry.location.lat());
            $('#l1_address_lng').val(place.geometry.location.lng());
            Core.getGeocoder(place.geometry.location).then(function (res) {
                neighborhood = "";
                for (var component of res.address_components) {
                    for (var type of component.types) {
                        if (component.types[0] == 'locality' && component.types[1] == 'political') {
                            neighborhood = component.long_name;
                            console.log(neighborhood);
                            break;
                        }
                    }
                    if (neighborhood != "") {
                        break;
                    }
                }
                $('#l1_neighborhood').val(neighborhood);
                $('#l1_address').val(res.formatted_address);
            }).catch(function () {
                Core.showToastStr('error', 'No ha sido posible obtener dirección');
            });
            clearMarkersL1Origen();
            var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            mapL1Origen.panTo(center);
            addMarkerL1Origen(center);
        });
    }

    // Adds a marker to the map and push to the array.
    function addMarkerL1Origen(location) {
        const marker = new google.maps.Marker({
            position: location,
            map: mapL1Origen,
        });
        markersL1Origen.push(marker);
    }

    // Sets the map on all markersL1 in the array.
    function setMapOnAllL1Origen(map) {
        for (let i = 0; i < markersL1Origen.length; i++) {
            markersL1Origen[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkersL1Origen() {
        setMapOnAllL1Origen(null);
    }

    // Shows any markers currently in the array.
    function showMarkersL1Origen() {
        setMapOnAllL1Origen(mapL1Origen);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkersL1Origen() {
        clearMarkersL1Origen();
        markersL1Origen = [];
    }

    //Save and update data Origenes
    $('#frmL1AddOrigen').validate({
        submitHandler: function (form) {
            itemSelected.collect = [];
            action = 'add';
            if (itemSelected.id != null) {
                action = 'edit'
            }
            itemSelected.collect[0] = {
                id: $('#frmL1AddOrigen input[name=idOrigenL1]').val(),
                action: action,
                name: $('#frmL1AddOrigen input[name=l1_name]').val(),
                phone: $('#frmL1AddOrigen input[name=l1_phone]').val(),
                note: $('#frmL1AddOrigen input[name=l1_note]').val(),
                quantity: $('#frmL1AddOrigen input[name=l1_quantity]').val(),
                type: $('#frmL1AddOrigen select[name=l1_type]').val(),
                address: $('#frmL1AddOrigen input[name=l1_address]').val(),
                lat: $('#frmL1AddOrigen input[name=l1_address_lat]').val(),
                lng: $('#frmL1AddOrigen input[name=l1_address_lng]').val(),
                neighborhood: $('#frmL1AddOrigen input[name=l1_neighborhood]').val()
            }
            itemSelected.quantity = itemSelected.collect[0].quantity;
            itemSelected.distance = 0;
            if (itemSelected.delivery != null) {
                itemSelected.distance = Core.getDistancia({
                    a: {
                        lat: itemSelected.collect[0].lat,
                        lng: itemSelected.collect[0].lng
                    },
                    b: {
                        lat: itemSelected.delivery[0].lat,
                        lng: itemSelected.delivery[0].lng
                    }
                });
            }
            localStorage.setItem('arrData', JSON.stringify(arrData));
            drawData();
            $('#frmL1AddOrigen').trigger('reset');
            $('#mdlOrigenL1').modal('hide');
        },
        errorPlacement: function (error, element) {
            $(element).addClass('is-invalid');
        },
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid").addClass('is-valid');
        }
    });

    //Destino----------------------------------------------------------------------------------------------------
    let mapL1Destino = 'x';
    let markersL1Destinos = [];

    function loadMapL1Destino() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const center = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    mapL1Destino = new google.maps.Map(document.getElementById('mapL1Destino'), {zoom: 14, center: center});
                    mapL1Destino.setCenter(center);
                    //Click Listener Google Maps
                    mapL1Destino.addListener("click", (event) => {
                        deleteMarkersL1Destinos();
                        addMarkerL1Destino(event.latLng);
                        $('#frmL1AddDestino input[name=l1_address_lat]').val(event.latLng.lat());
                        $('#frmL1AddDestino input[name=l1_address_lng]').val(event.latLng.lng());
                        Core.getGeocoder(event.latLng).then(function (res) {
                            console.log(res);
                            neighborhood = "";
                            for (var component of res.address_components) {
                                for (var type of component.types) {
                                    if (component.types[0] == 'locality' && component.types[1] == 'political') {
                                        neighborhood = component.long_name
                                        break;
                                    }
                                }
                                if (neighborhood != "") {
                                    break;
                                }
                            }
                            $('#frmL1AddDestino input[name=l1_neighborhood]').val(neighborhood);
                            $('#frmL1AddDestino input[name=l1_address]').val(res.formatted_address);
                        }).catch(function () {
                            Core.showToastStr('error', 'No ha sido posible obtener dirección');
                        });
                    });
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        }
        autocompleteA = new google.maps.places.Autocomplete($('#frmL1AddDestino input[name=l1_address]')[0], {types: ['geocode']});
        autocompleteA.setFields(['geometry']);
        autocompleteA.addListener('place_changed', function () {
            var place = autocompleteA.getPlace();
            $('#frmL1AddDestino input[name=l1_address_lat]').val(place.geometry.location.lat());
            $('#frmL1AddDestino input[name=l1_address_lng]').val(place.geometry.location.lng());
            Core.getGeocoder(place.geometry.location).then(function (res) {
                console.log(res);
                neighborhood = "";
                for (var component of res.address_components) {
                    for (var type of component.types) {
                        if (component.types[0] == 'locality' && component.types[1] == 'political') {
                            neighborhood = component.long_name
                            break;
                        }
                    }
                    if (neighborhood != "") {
                        break;
                    }
                }
                $('#frmL1AddDestino input[name=l1_neighborhood]').val(neighborhood);
                $('#frmL1AddDestino input[name=l1_address]').val(res.formatted_address);
            }).catch(function () {
                Core.showToastStr('error', 'No ha sido posible obtener dirección');
            });
            deleteMarkersL1Destinos();
            var center = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            mapL1Destino.panTo(center);
            addMarkerL1Destino(center);
        });
    }

    // Adds a marker to the map and push to the array.
    function addMarkerL1Destino(location) {
        const marker = new google.maps.Marker({
            position: location,
            map: mapL1Destino,
        });
        markersL1Destinos.push(marker);
    }

    // Sets the map on all markersL1 in the array.
    function setMapOnAllL1Destinos(map) {
        for (let i = 0; i < markersL1Destinos.length; i++) {
            markersL1Destinos[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkersL1Destinos() {
        setMapOnAllL1Destinos(null);
    }

    // Shows any markers currently in the array.
    function showMarkersL1Destinos() {
        setMapOnAllL1Destinos(mapL1Destino);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkersL1Destinos() {
        clearMarkersL1Destinos();
        markersL1Destinos = [];
    }

    //Save and update data Destino
    $('#frmL1AddDestino').validate({
        submitHandler: function (form) {
            itemSelected.delivery = [];
            action = 'add';
            if (itemSelected.id != null) {
                action = 'edit'
            }
            addData = {
                id: $('#frmL1AddDestino input[name=idDestinoL1]').val(),
                action: action,
                name: $('#frmL1AddDestino input[name=l1_name]').val(),
                phone: $('#frmL1AddDestino input[name=l1_phone]').val(),
                note: $('#frmL1AddDestino input[name=l1_note]').val(),
                type: $('#frmL1AddDestino select[name=l1_type]').val(),
                address: $('#frmL1AddDestino input[name=l1_address]').val(),
                lat: $('#frmL1AddDestino input[name=l1_address_lat]').val(),
                lng: $('#frmL1AddDestino input[name=l1_address_lng]').val(),
                neighborhood: $('#frmL1AddDestino input[name=l1_neighborhood]').val()
            }
            itemSelected.delivery[0] = addData;
            itemSelected.distance = 0;
            if (itemSelected.collect != null) {
                itemSelected.distance = Core.getDistancia({
                    a: {
                        lat: itemSelected.collect[0].lat,
                        lng: itemSelected.collect[0].lng
                    },
                    b: {
                        lat: itemSelected.delivery[0].lat,
                        lng: itemSelected.delivery[0].lng
                    }
                });
            }
            localStorage.setItem('arrData', JSON.stringify(arrData));
            drawData();
            $('#frmL1AddDestino').trigger('reset');
            $('#mdlDestinoL1').modal('hide');
        },
        errorPlacement: function (error, element) {
            $(element).addClass('is-invalid');
        },
        highlight: function (element) {
            $(element).addClass("is-invalid");
        },
        unhighlight: function (element) {
            $(element).removeClass("is-invalid").addClass('is-valid');
        }
    });

    //SHOW ROUTE GOOGLEMAPS--------------------------------------------------------------------------------------
    var mapL1Route;
    var markersL1OrigenRuta = [];

    function loadMapL1Route() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const center = {
                        lat: (parseFloat(itemSelected.collect[0].lat) + parseFloat(itemSelected.delivery[0].lat)) / 2,
                        lng: (parseFloat(itemSelected.collect[0].lng) + parseFloat(itemSelected.delivery[0].lng)) / 2
                    };
                    mapL1Route = new google.maps.Map(document.getElementById('mapL1Route'), {zoom: 12, center: center});
                    var origen = new google.maps.LatLng(itemSelected.collect[0].lat, itemSelected.collect[0].lng);
                    var destino = new google.maps.LatLng(itemSelected.delivery[0].lat, itemSelected.delivery[0].lng);
                    addMarkerL1OrigenRuta(origen, 0);
                    addMarkerL1OrigenRuta(destino, 1, itemSelected);
                    mapL1Route.setCenter(center);
                    for (let i = 0; i < lines.length; i++) {
                        lines[i].setMap(null);
                    }
                    //Draw PolyLine
                    var line = new google.maps.Polyline({
                        path: [origen, destino],
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    line.setMap(mapL1Route);
                    lines.push(line);
                },
                () => {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        }
    }

    // Adds a marker to the map and push to the array.
    function addMarkerL1OrigenRuta(location, index, data = '') {
        const marker = new google.maps.Marker({
            position: location,
            label: labels[index++ % labels.length] + (index != 1 ? (' ' + data.distance + 'km') : ''),
            map: mapL1Route,
        });
        markersL1OrigenRuta.push(marker);
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkersL1DestinosRuta() {
        setMapOnAllL1DestinosRuta(null);
    }

    // Sets the map on all markersL1 in the array.
    function setMapOnAllL1DestinosRuta(map) {
        for (let i = 0; i < markersL1OrigenRuta.length; i++) {
            markersL1OrigenRuta[i].setMap(map);
        }
    }
