    //Impor Google Api
    document.write(`<script src="https://maps.googleapis.com/maps/api/js?key=${$('#googleKey').val()}&libraries=places&callback" async defer></script>`);
    route = $('#idRoute').val() + "/rutas";
    var itemData;
    var arrData = [];
    var itemSelected;
    var cost;
    var quantity = 0;
    var indexSelected = 0;
    var rates = [];
    var lines = [];
    const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var totalDelivery = 0;
    var totalDeliveryDB = 0;
    var banderaBranchOffice = 0;
    var dia;
    var diaLetras;
    Core.setDataTable('arrData');





    $("#frmEdit").validate({
        submitHandler: function (form) {
            $('.btn-guardar').text('Guardando ...').prop('disabled', true);

            dia = $('#dia').val();
            if (dia == '1'){
                diaLetras = 'Lunes';
            }else if (dia == '2'){
                diaLetras = 'Martes';
            }else if (dia == '3'){
                diaLetras = 'Miércoles';
            }else if (dia == '4'){
                diaLetras = 'Jueves';
            }else if (dia == '5'){
                diaLetras = 'Viernes';
            }else if (dia == '6'){
                diaLetras = 'Sábado';
            }else if (dia == '7'){
                diaLetras = 'Domingo';
            }

            itemData = {
                customer_id             : $('#customers_id').val(),
                branchoffice_id         : $('#branchoffice_id').val(),
                dealer_id               : $('#dealer_id').val(),
                dia                     : diaLetras,
                hora_inicio_programado  : $('#hora_entrada').val(),
                hora_fin_programado     : $('#hora_salida').val(),
                lat                     : $('#l1_address_lat').val(),
                lng                     : $('#l1_address_lng').val(),
                address                 : $('#l1_address').val(),
                neighborhood            : $('#l1_neighborhood').val(),
                status                  : parseInt($('#status').val()),
                en_rango                : parseInt($('#en_rango').val()),
                observaciones           : $('#observaciones').val(),
            };
            console.log(itemData);
            //return false;

            Core.crud.update($('#idRuta').val())
                .then(function (res) {
                    Core.showToastStr("success", res.data.message);
                    $('.btn-guardar').text('Editar').prop('disabled', false);
                    setTimeout(function (){
                        window.location.href = urlBase + route
                    }, 3000);
                })
                .catch(function (err) {
                    Core.showToastStr('error', err.response.data.error.message);
                    $('.btn-guardar').text('Editar').prop('disabled', false);
                });
        }
    });

    // Deshabilitar enter
    $('#frmEdit').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    // $('#branchoffice_id').on('change', (function () {
    //     if (banderaBranchOffice === 1){
    //         $('#frmEdit select[name=dealer_id]').empty();
    //         if ($('#branchoffice_id').val() != "") {
    //             Core.get(route + '/dealers/' + $(this).val())
    //                 .then(function (res) {
    //                     $.each(res.data.data, function (index, item) {
    //                         $('#frmEdit select[name=dealer_id]').append($('<option data-index="' + index + '"></option>').val(item.id).html(item.name));
    //                     });
    //                 }).catch(function (err) {
    //                 Core.showToastStr('error', err.response.data.error.message);
    //             });
    //         }
    //     }
    //     if( $('#dealer_id').has('option').length > 0 ) {
    //         banderaBranchOffice = 1;
    //     }
    // }));

    $('#customers_id').on('change', (function () {
        // console.log('Cliente change');
        // return false;
        if (banderaBranchOffice === 1){
            $('#frmEdit select[name=branchoffice_id]').empty();
            Core.get(route + '/branchoffices/' + $(this).val())
                .then(function (res) {
                    console.log(res);
                    $.each(res.data.data, function (index, item) {
                        $('#frmEdit select[name=branchoffice_id]').append($('<option data-index="' + index + '"></option>').val(item.id).html(item.name));
                    });
                }).catch(function (err) {
                Core.showToastStr('error', err.response.data.error.message);
            });
        }
        if( $('#branchoffice_id').has('option').length > 0 ) {
            banderaBranchOffice = 1;
        }
    }));

    $('#quote_id').change(function () {
        Core.get($('#idRoute').val() + '/quotes/find/' + $(this).val()).then(function (res) {
            quote = res.data.data;
            $('#customer_name').val(quote.customer_data.name);
            $('#customer_phone').val(quote.customer_data.phone);
            $('#customer_email').val(quote.customer_data.email);
            $('#zone_start').val(quote.zone_start).trigger('change');
            $('#zone_arrival').val(quote.zone_arrival).trigger('change');
        }).catch(function (err) {
            console.log(err);
            Core.showToastStr('error', 'No ha sido posible obtener datos de cotizaciones, por favor verifique que la solicitud aun exista.')
        });
    });

    function drawData() {
        totalPreview = 0;
        tblData.rows().remove().draw();
        //Reestructurar de memoria
        arrData = JSON.parse(localStorage.getItem('arrData'));
        if (!arrData) {
            arrData = [];
        }
        $.each(arrData, (function (index, item) {
            collect = "";
            delivery = "";
            price = '-';
            distance = 0;
            Core.setLocalStorage('arrData', arrData);
            prepararMapa();
        }));
    }

    function prepararMapa(){
        // console.log('prepararMapa');
        // console.log(itemData.lat, itemData.lng);
        itemSelected = arrData[0];
        if (itemSelected.collect != null) {
          //  return false;
            loadMapL1Origen();
            $('#l1_address').val(itemSelected.collect[0].address);
            $('#l1_address_lat').val(itemSelected.collect[0].lat);
            $('#l1_adddres_lng').val(itemSelected.collect[0].lng);
            $('#l1_neighborhood').val(itemSelected.collect[0].neighborhood);
            setTimeout(function () {
                var center = new google.maps.LatLng(itemData.lat, itemData.lng);
                mapL1Origen.panTo(center);
                addMarkerL1Origen(center);
            }, 1500);
        } else {
            loadMapL1Origen();
            setTimeout(function () {
                var center = new google.maps.LatLng(itemData.lat, itemData.lng);
                mapL1Origen.panTo(center);
                addMarkerL1Origen(center);
            }, 1500);
        }
    }

    window.getData = function () {
        //Axios Http Get Request
        Core.crud.find($('#idRuta').val()).then(function (res) {
            banderaBranchOffice = 0;
            itemData = res.data.data[0];
            //console.log('getData');
            $('#frmEdit select[name=customers_id]').val(itemData.customer_id).trigger('change');
            $('#frmEdit select[name=branchoffice_id]').val(itemData.branchoffice_id).trigger('change');
            $('#frmEdit select[name=dealer_id]').val(itemData.dealer_id).trigger('change');

            var setDia = 0;

            if (itemData.dia === 'Lunes'){
                setDia = 1;
            }else if (itemData.dia === 'Martes'){
                setDia = 2;
            }else if(itemData.dia === 'Miércoles'){
                setDia = 3;
            }else if(itemData.dia === 'Jueves'){
                setDia = 4;
            }else if(itemData.dia === 'Viernes'){
                console.log('vie');
                setDia = 5;
            }else if(itemData.dia === 'Sábado'){
                setDia = 6;
            }

            $('#frmEdit select[name=dia]').val(setDia).trigger('change');
            $('#frmEdit input[name=hora_entrada]').val(itemData.hora_inicio_programado);
            $('#frmEdit input[name=hora_salida]').val(itemData.hora_fin_programado);

            $('#frmEdit select[name=en_rango]').val(itemData.en_rango).trigger('change');
            $('#frmEdit select[name=status]').val(itemData.status).trigger('change');

            $('#frmEdit textarea[name=observaciones]').val(itemData.observaciones);

            $('#l1_address').val(itemData.address);
            $('#l1_address_lat').val(itemData.lat);
            $('#l1_address_lng').val(itemData.lng);
            $('#l1_neighborhood').val(itemData.neighborhood);

            setTimeout(function (){
                prepararArrayParaMapa();
            }, 500);

        }).catch(function (err) {
            console.log(err);
        })
    }

    function prepararArrayParaMapa(){
        arrData.push({
            type: 'L1',
            quantity: 0,
            collect: null,
            delivery: null,
            total: 0,
            status: 0,
            distance: 0
        });
        //localStorage.setItem('arrData', JSON.stringify(arrData));

       // drawData();
    }

    function init() {
       // getFees();
      //  $('#type_view').hide();
        //$('#date_view').hide();
        //$('#round_view').hide();
        //$('#quote_view').hide();
        //$("#total_view").hide();
        drawData();
      //  getRates();
        setTimeout(function (){
            prepararArrayParaMapa();
        },1000);
    }



    init();
