    route = $('#idRoute').val() + "/rutas";
    arrStates = [];

    tblData = $('#arrData').DataTable({
        processing: true,
        serverSide: true,
        ajax: "rutas/all",

        columns: [
            {data: 'id', name: 'id',},
            {data: 'customer.name', name: 'customer.name'},
        //    {data: 'branchoffice.name', name: 'branchoffice.name'},
            {data: 'dia', name: 'dia'},
            {data: 'dealer.name', name: 'dealer.name'},
            {data: 'hora_inicio_programado', name: 'hora_inicio_programado'},
            {data: 'hora_fin_programado', name: 'hora_fin_programado'},
            {data: 'hora_inicio_real', name: 'hora_inicio_real'},
            {data: 'hora_fin_real', name: 'hora_fin_real'},
            {data: 'observaciones', name: 'observaciones'},
            {data: 'fecha', name: 'fecha'},
            {data: 'en_rango', name: 'en_rango'},
            {data: 'status', name:'status'}

        ],
        lengthMenu: [
            [10, 50, 100, -1],
            [10, 50, 100, 'Todo']
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json",
            pageLength: 10
        },
        "scrollX": true,
        "autoWidth": true,
        columnDefs: [{
            targets: [0],
            visible: false,
            searchable: false
        },
           /* {
                targets: [1],
                render: function (data, type, row, meta) {
                    return row.id;
                },
                visible: false,
                searchable: false
            },*/

            {
                targets: [2],
                render: function (data, type, row, meta) {
                    return '<center>' + row.customer.name + '</center>';
                }
            },


          /*  {
                targets: [3],
                render: function (data, type, row, meta) {
                    return row.branchoffice.name;

                }
            },*/


            {
                targets: [3],
                render: function (data, type, row, meta) {
                    var dia = 'Lunes';
                    switch (row.payment_method) {
                        case 1: {
                            dia = 'Lunes';
                            break;
                        }
                        case 2: {
                            dia = 'Martes';
                            break;
                        }
                        case 3: {
                            dia = 'Miercoles';
                            break;
                        }
                        case 4: {
                            dia = 'Jueves';
                            break;
                        }
                        case 5: {
                            dia = 'Viernes';
                            break;
                        }
                        case 6: {
                            dia = 'Sabado';
                            break;
                        }
                        case 7: {
                            dia = 'Domingo';
                            break;
                        }
                    }
                    return '<center>' + dia + '</center>';
                }
            },
            {
                targets: [4],
                render: function (data, type, row, meta) {
                    return '<center>' + row.dealer.name + '</center>';
                }
            },
            {
                targets: [5],
                render: function (data, type, row, meta) {
                    return '<center>' + row.hora_inicio_programado + '</center>';
                    //return '<center>' + (row.type == 1 ? 'Inmediato' : 'Programado') + '</center>';
                }
            },
            {
                targets: [6],
                render: function (data, type, row, meta) {
                    return '<center>' + row.hora_fin_programado + '</center>';
                    //return '<center>' + row.quantity + '</center>';
                }
            },
            {
                targets: [7],
                render: function (data, type, row, meta) {
                  return '<center>' + row.hora_inicio_real + '</center>';
                    //  return '<center>₲' + Core.setMiles(row.total) + '</center>';
                }
            },
            {
                targets: [8],
                render: function (data, type, row, meta) {
                  return '<center>' + row.hora_fin_real + '</center>';
                }
            },
            {
                targets: [9],
                render: function (data, type, row, meta) {
                    return '<center>' + row.observaciones + '</center>';
                }
            },
            {
                targets: [12],
                render: function (data, type, row, meta) {
                    return '<center>' + row.fecha + '</center>';

                }
            },
            {
                targets: [10],
                render: function (data, type, row, meta) {
                    switch (row.en_rango) {

                        case 0: {
                            return '<p style=" text-align:center; background-color: #006eff; color:white ">Si</p>';
                            break;
                        }
                        case 1: {
                            return '<p style=" text-align:center; background-color: #c0bd00; color:white ">No</p>';
                            break;
                        }
                    }
                 //   return `${row.status == 1 ? 'Activo' : 'Realizado'}`;
                }
            },
            {
                targets: [11],
                render: function (data, type, row, meta) {
                    switch (row.status) {

                        case 0: {
                            return '<p style=" text-align:center; background-color: #006eff; color:white ">Activo</p>';
                            break;
                        }
                        case 1: {
                            return '<p style=" text-align:center; background-color: #c0bd00; color:white ">Inactivo</p>';
                            break;
                        }
                    }
                      return `${row.status == 1 ? 'Activo' : 'Realizado'}`;
                }
            },

            {
                targets: [13],
                render: function (data, type, row, meta) {
                    return `<center>
                            <a href="${urlBase + route + "/edit/" + row.id}">
                            <svg class="c-icon mr-2" id="btnEdit" data-index='${meta.row}' style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-pencil"></use>
                            </svg>
                            </a>
                            <svg class="c-icon mr-2" id="btnDelete" data-index='${meta.row}' style="cursor: pointer;">
                                <use xlink:href="${urlBase}dashboard/vendors/@coreui/icons/svg/free.svg#cil-trash"></use>
                            </svg>
                        </center>`
                }
            }


        ]
    });

/*
    */

    tblData.on('draw', function () {
        arrData = tblData.rows().data().toArray();
        //console.log(arrData);
    });


    $("#frmNew").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
            Core.post(route + '/store', itemData)
                .then(function (res) {
                    $('#mdlNew').modal('hide');
                    $('#frmNew').trigger('reset');
                    Core.showToastStr("success", res.data.message);
                })
                .catch(function (err) {
                    Core.showToastStr('error', err.response.data.error.message);
                }).finally(function () {
                tblData.ajax.reload();
            });
        }
    });

    $("#frmEdit").validate({
        submitHandler: function (form) {
            itemData = new FormData(form);
            Core.post(route + '/update/' + $('#id').val(), itemData)
                .then(function (res) {
                    $('#mdlEdit').modal('hide');
                    Core.showToastStr("success", res.data.message);
                    $("#frmEdit").trigger('reset');
                })
                .catch(function (err) {
                    Core.showToastStr('error', err.response.data.error.message);
                }).finally(function () {
                tblData.ajax.reload();
            });
        }
    });

    window.showItem = function () {
        $('#id').val(itemData.id);
        $('#frmEdit select[name=customer_id]').val(itemData.customer_id);
        $('#frmEdit select[name=dealer_id]').val(itemData.dealer_id);
        $('#frmEdit select[name=branchoffice_id]').val(itemData.branchoffice_id);
        $('#frmEdit input[name=dia]').val(itemData.dia);
        $('#frmEdit input[name=fecha]').val(itemData.fecha);
        $('#frmEdit input[name=hora_inicio_programado]').val(itemData.hora_inicio_programado);
        $('#frmEdit input[name=hora_fin_programado]').val(itemData.hora_fin_programado);
        $('#frmEdit select[name=observaciones]').val(itemData.observaciones);
        $('#frmEdit select[name=en_rango]').val(itemData.en_rango);
        $('#frmEdit select[name=status]').val(itemData.status);
    }

    window.getData = function () {
        tblData.ajax.reload();
    }


    //Agregue
    $('#btnUpdate').click(function () {
        tblData.ajax.reload();
    })

    /***********************/

 /*   $('#btnEliminar').on('click', function (){
        console.log('Aca');
    });*/


    function init() {
        getData();
        tblData.ajax.reload();
    }

    init();

    $('#arrData tbody').css('text-align', 'center');